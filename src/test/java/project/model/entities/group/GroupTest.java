package project.model.entities.group;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class GroupTest {

    private PersonID newPersonID1;
    private PersonID newPersonID2;
    private PersonID newPersonID3;
    private PersonID newPersonID4;
    private LedgerID newLedgerPersonID1;
    private LedgerID newLedgerPersonID2;
    private LedgerID newLedgerGroupID1;
    private LedgerID newLedgerGroupID2;
    private AccountID newAccountID1;
    private GroupID newGroupID1;
    private GroupID newGroupID2;
    private Group newGroup;

    @BeforeEach
    public void init() {
        newPersonID1 = new PersonID("101@switch.pt");
        newPersonID2 = new PersonID("102@switch.pt");
        newPersonID3 = new PersonID("103@switch.pt");
        newPersonID4 = new PersonID("104@switch.pt");

        newGroupID1 = new GroupID("201");
        newGroupID2 = new GroupID("202");

        newLedgerPersonID1 = new LedgerID("301");
        newLedgerPersonID2 = new LedgerID("302");

        newLedgerGroupID1 = new LedgerID("401");
        newLedgerGroupID2 = new LedgerID("402");

        newAccountID1 = new AccountID("501");

    }

    /**
     * Test for Group constructor
     * Happy Case
     */
    @Test
    @DisplayName("Test for Group Constructor - HappyCase")
    void GroupConstructorHappyCaseTest() {
        newGroup = new Group(newGroupID1, "family", "2019-12-18", newPersonID1, newLedgerPersonID2);
        assertTrue(newGroup instanceof Group);
    }

    /**
     * Test for setGroupDescription
     * Ensure exception is throw with null groupDescription
     */
    @Test
    @DisplayName("Test Method setGroupDescription - Ensure exception with null group description")
    void setGroupDescriptionEnsureExceptionWithNullTest() {
        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            newGroup = new Group(newGroupID1, null, "2019-12-18", newPersonID1, newLedgerPersonID2);
        });
    }

    /**
     * Test for setBirthDate
     * Ensure exception is thrown with null birthDate
     */
    @Test
    @DisplayName("Test setBirthDate - Ensure IllegalArgumentException with null creationDate")
    void setBirthDateEnsureExceptionWithNullCreationDateTest() {

        // Assert
        assertThrows(NullPointerException.class, () -> {
            newGroup = new Group(newGroupID1, "Test Group", null, newPersonID1, newLedgerPersonID2);
        });
    }

    /**
     * Test for addMember
     * Happy case - ensure if members are added to the membersList
     */
    @Test
    @DisplayName("Test addMember - ensure if members are added to the membersList - test")
    void addMemberHappyCaseTest() {
        // Arrange
        newGroup = new Group(newGroupID1, "family", "2019-12-18", newPersonID1, newLedgerPersonID2);

        // Act
        boolean result = newGroup.addMemberID(newPersonID2);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for addMember
     * Ensure false if already there
     */
    @Test
    @DisplayName("Test addMember - ensure false if already there")
    void addMemberEnsureFalseTest() {
        // Arrange
        newGroup = new Group(newGroupID1, "family", "2019-12-18", newPersonID1, newLedgerPersonID2);

        // Act
        boolean result = newGroup.addMemberID(newPersonID1);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for addManager
     * Happy case - ensure managers are added to the group's list of managers
     */
    @Test
    @DisplayName("Test addManager - ensure if managers are added to the group's list of managers - test")
    void addManagerHappyCaseTest() {
        // Arrange
        newGroup = new Group(newGroupID1, "family", "2019-12-18", newPersonID1, newLedgerPersonID2);

        // Act
        boolean result = newGroup.addManagerID(newPersonID2);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for addManager
     * Ensure false if already there
     */
    @Test
    @DisplayName("Test addManager - ensure false if already there")
    void addManagerEnsureFalseTest() {
        // Arrange
        newGroup = new Group(newGroupID1, "family", "2019-12-18", newPersonID1, newLedgerPersonID2);

        // Act
        boolean result = newGroup.addManagerID(newPersonID1);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for Test for addAccountID - Happy case
     * Group Account is created
     */
    @Test
    @DisplayName("Test for addAccount - Happy case")
    void addAccountHappyCaseTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);
        AccountID id = new AccountID("5");

        // Act
        boolean expected = family.addAccount(id);

        // Arrange
        assertTrue(expected);
    }

    /**
     * Test for addAccountID
     * Ensure false if already there
     */
    @Test
    @DisplayName("Test for addAccount - Ensure false")
    void addAccountEnsureFalseTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);

        family.addAccount(newAccountID1);

        // Act

        boolean expected = family.addAccount(newAccountID1);

        // Arrange
        assertFalse(expected);
    }

//    /**
//     * Test for addTransaction
//     * Happy Case
//     */
//    @Test
//    @DisplayName("Test for addTransaction - Happy case")
//    void addTransactionHappyCaseTest() {
//        // Arrange
//        Person isabel = new Person(newPersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID);
//        Person paulo = new Person(newPersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID2);
//        Person andre = new Person(newPersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22",
//        newPersonID, newPersonID2, newLedgerGroup);
//        Group family = new Group(newGroupID, "Familia", "2020-01-14", andre, newLedgerID4);
//        Account smithsGroceriesAccount = new Account(newAccountID, "Smith's Groceries", "Account just for Smith
//        family's groceries");
//        family.addAccountID(newAccountID);
//        Account zeCreditAccount = new Account(newAccountID2, "Zé & Associados", "Current account of the 'A Loja do
//        Zé' grocery shop");
//        family.addAccountID(newAccountID2);
//        Category newCategory = new Category("Groceries");
//        family.addCategory("Groceries");
//
//        // Act
//        boolean result = family.addTransaction(49.99, Type.DEBIT, "2020-01-13 00:00:00", "Groceries for January",
//        newCategory, newAccountID, newAccountID2);
//
//        // Assert
//        assertTrue(result);
//    }
//
//    /**
//     * Test for addTransaction
//     * Ensure exception with invalid field
//     */
//    @Test
//    @DisplayName("Test for addTransaction - Ensure exception with invalid field")
//    void addTransactionEnsureExceptionWithInvalidDescriptionTest() {
//        // Arrange
//        Person isabel = new Person(newPersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID);
//        Person paulo = new Person(newPersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID2);
//        Person andre = new Person(newPersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22",
//        newPersonID, newPersonID2, newLedgerGroup);
//        Group family = new Group(newGroupID, "Familia", "2020-01-14", andre, newLedgerGroup);
//
//        Account smithsGroceriesAccount = new Account(newAccountID, "Smith's Groceries", "Account just for Smith
//        family's groceries");
//        family.addAccountID(newAccountID);
//        Account zeCreditAccount = new Account(newAccountID2, "Zé & Associados", "Current account of the 'A Loja do
//        Zé' grocery shop");
//        family.addAccountID(newAccountID2);
//
//        Category newCategory = new Category("Groceries");
//        family.addCategory("Groceries");
//
//        // Assert
//        assertThrows(IllegalArgumentException.class, () -> {
//            family.addTransaction(49.99, Type.DEBIT, "2020-01-13 00:00:00", null, newCategory, newAccountID,
//            newAccountID2);
//        });
//    }
//
//    /**
//     * Test for addTransaction
//     * Ensure exception if category not in person
//     */
//    @Test
//    @DisplayName("Test for addTransaction - Ensure exception if category not in person")
//    void addTransactionEnsureExceptionIfCategoryNotInPersonTest() {
//        // Arrange
//        Person isabel = new Person(newPersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID);
//        Person paulo = new Person(newPersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID2);
//        Person andre = new Person(newPersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22",
//        newPersonID, newPersonID2, newLedgerID3);
//        Group family = new Group(newGroupID, "Familia", "2020-01-14", andre, newLedgerGroup);
//        Account smithsGroceriesAccount = new Account(newAccountID, "Smith's Groceries", "Account just for Smith
//        family's groceries");
//        family.addAccountID(newAccountID);
//        Account zeCreditAccount = new Account(newAccountID2, "Zé & Associados", "Current account of the 'A Loja do
//        Zé' grocery shop");
//        family.addAccountID(newAccountID2);
//        Category newCategory = new Category("Groceries");
//
//        // Assert
//        assertThrows(IllegalArgumentException.class, () -> {
//            family.addTransaction(49.99, Type.DEBIT, "2020-01-13", "Groceries", newCategory, newAccountID,
//            newAccountID2);
//        });
//    }
//
//    /**
//     * Test for addTransaction
//     * Ensure exception if debitAccount not in person
//     */
//    @Test
//    @DisplayName("Test for addTransaction - Ensure exception if debitAccount not in person")
//    void addTransactionEnsureExceptionIfDebitAccountNotInPersonTest() {
//        // Arrange
//        Person isabel = new Person(newPersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID);
//        Person paulo = new Person(newPersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID2);
//        Person andre = new Person(newPersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22",
//        newPersonID, newPersonID2, newLedgerID3);
//        Group family = new Group(newGroupID, "Familia", "2020-01-14", andre, newLedgerGroup);
//        Account smithsGroceriesAccount = new Account(newAccountID, "Smith's Groceries", "Account just for Smith
//        family's groceries");
//        Account zeCreditAccount = new Account(newAccountID2, "Zé & Associados", "Current account of the 'A Loja do
//        Zé' grocery shop");
//        family.addAccountID(newAccountID2);
//        Category newCategory = new Category("Groceries");
//        family.addCategory("Groceries");
//
//        // Assert
//        assertThrows(IllegalArgumentException.class, () -> {
//            family.addTransaction(49.99, Type.DEBIT, "2020-01-13", "Groceries", newCategory, newAccountID,
//            newAccountID2);
//        });
//    }
//
//    /**
//     * Test for addTransaction
//     * Ensure exception if creditAccount not in person
//     */
//    @Test
//    @DisplayName("Test for addTransaction - Ensure exception if credit account not in person")
//    void addTransactionEnsureExceptionIfCreditAccountNotInPersonTest() {
//        // Arrange
//        Person isabel = new Person(newPersonID, "isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID);
//        Person paulo = new Person(newPersonID2, "Paulo", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID2);
//        Person andre = new Person(newPersonID3, "andre", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22",
//        newPersonID, newPersonID2, newLedgerID3);
//        Group family = new Group(newGroupID, "Familia", "2020-01-14", andre, newLedgerGroup);
//
//        Account smithsGroceriesAccount = new Account(newAccountID, "Smith's Groceries", "Account just for Smith
//        family's groceries");
//        family.addAccountID(newAccountID);
//
//        Account zeCreditAccount = new Account(newAccountID2, "Zé & Associados", "Current account of the 'A Loja do
//        Zé' grocery shop");
//        Category newCategory = new Category("Groceries");
//        family.addCategory("Groceries");
//
//        // Assert
//        assertThrows(IllegalArgumentException.class, () -> {
//            family.addTransaction(49.99, Type.DEBIT, "2020-01-13", "Groceries", newCategory, newAccountID,
//            newAccountID2);
//        });
//    }

    /**
     * Test for addCategory
     * Happy case
     */
    @Test
    @DisplayName("Test for addCategory - True if category was added")
    void addCategoryHappyCaseTest() {
        // Arrange
        Group g1 = new Group(newGroupID1, "isep", "2019-07-10", newPersonID1, newLedgerGroupID1);

        // Act
        boolean result = g1.addCategory("health");

        // Assert
        assertTrue(result);
    }

    /**
     * Test for addCategory
     * Sad case
     */
    @Test
    @DisplayName("Test for addCategory - False since category already existed")
    void addCategorySadCaseEnsureFalseSinceCategoryAlreadyExistedTest() {
        // Arrange
        Group g1 = new Group(newGroupID1, "isep", "2019-07-10", newPersonID1, newLedgerGroupID1);

        // Act
        g1.addCategory("health");
        boolean result = g1.addCategory("health");

        // Assert
        assertFalse(result);
    }

    /**
     * Test for addCategory
     * Ensure Exception is thrown since category cant have null designation
     */
    @Test
    @DisplayName("Test for addCategory - Ensure exception is thrown- designation of category cant be null")
    void addCategoryEnsureExceptionTest() {
        // Arrange
        Group g1 = new Group(newGroupID1, "isep", "2019-07-10", newPersonID1, newLedgerGroupID1);

        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            g1.addCategory(null);
        });
    }

//    /**
//     * Test for getBalanceWithinPeriod
//     * Happy Case
//     */
//    @Test
//    @DisplayName("Test for getBalanceWithinPeriod - Happy Case")
//    void getBalanceWithinPeriodHappyCaseTest() {
//
//        // Arrange
//        Person joao = new Person(newPersonID, "João", "Rua De Santa Bárbara", "Porto", "1990-01-01", null, null,
//        newLedgerID);
//        Group isep = new Group(newGroupID, "Isep", "2019-10-20", joao, newLedgerGroup);
//        Category education = new Category("Educação");
//
//        Account credit = new Account(newAccountID, "credit", "despesa propina");
//        Account debit = new Account(newAccountID2, "debit", "acerto propina");
//        Period period = new Period("2018-10-01 00:00:00", "2020-10-03 00:00:00");
//        // Act
//        isep.addCategory("Educação");
//        isep.addAccountID(newAccountID);
//        isep.addAccountID(newAccountID2);
//        isep.addTransaction(10, Type.CREDIT, "2019-12-03 00:00:00", "propina", education, newAccountID,
//        newAccountID2);
//        double expected = 10;
//        double result = isep.getBalanceWithinPeriod(period);
//
//        // Assert
//        assertEquals(expected, result);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriod
//     * Happy Case
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - HappyCase")
//    void getTransactionsOfAccountWithinPeriodHappyCaseTest() {
//        // Arrange
//        Person ze = new Person(newPersonID, "Ze Branca", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID);
//
//        Group lojaZeAssociados = new Group(newGroupID, "lojaZeAssociados", "1991-12-22", ze, newLedgerGroup);
//
//        Account smithsGroceriesAccount = new Account(newAccountID, "Smith's Groceries", "Account just for Smith
//        family's groceries");
//        lojaZeAssociados.addAccountID(newAccountID);
//
//        Account zeCreditAccount = new Account(newAccountID2, "Zé & Associados", "Current account of the 'A Loja do
//        Zé' grocery shop");
//        lojaZeAssociados.addAccountID(newAccountID2);
//
//        Category newCategory = new Category("Groceries");
//        lojaZeAssociados.addCategory("Groceries");
//
//        lojaZeAssociados.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Bags for concrete", newCategory,
//        newAccountID, newAccountID2);
//        lojaZeAssociados.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of concrete",
//        newCategory, newAccountID, newAccountID2);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Bags for concrete", newCategory,
//        newAccountID, newAccountID2));
//        expected.add(new Transaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of concrete", newCategory,
//        newAccountID, newAccountID2));
//
//        // Act
//
//        Set<Transaction> actual = new HashSet<>();
//        actual = lojaZeAssociados.getTransactionsOfAccountWithinPeriod(newAccountID2, period);
//
//        // Assert
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriod
//     * Ensure transactions outside the period are not in the result
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Ensure period filter is working")
//    void getTransactionsOfAccountWithinPeriodEnsurePeriodFilterTest() {
//        // Arrange
//        Person ze = new Person(newPersonID, "Ze Branca", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID);
//
//        Group lojaZeAssociados = new Group(newGroupID, "lojaZeAssociados", "1991-12-22", ze, newLedgerGroup);
//
//        Account smithsGroceriesAccount = new Account(newAccountID, "Smith's Groceries", "Account just for Smith
//        family's groceries");
//        lojaZeAssociados.addAccountID(newAccountID);
//
//        Account zeCreditAccount = new Account(newAccountID2, "Zé & Associados", "Current account of the 'A Loja do
//        Zé' grocery shop");
//        lojaZeAssociados.addAccountID(newAccountID2);
//
//        Category newCategory = new Category("Groceries");
//        lojaZeAssociados.addCategory("Groceries");
//
//        lojaZeAssociados.addTransaction(20, Type.DEBIT, "2023-01-01 00:00:00", "Bags for concrete", newCategory,
//        newAccountID, newAccountID2);
//        lojaZeAssociados.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of concrete",
//        newCategory, newAccountID, newAccountID2);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of concrete", newCategory,
//        newAccountID, newAccountID2));
//
//        // Act
//
//        Set<Transaction> actual = new HashSet<>();
//        actual = lojaZeAssociados.getTransactionsOfAccountWithinPeriod(newAccountID2, period);
//
//        // Assert
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriod
//     * Ensure transactions without the requested account are not in the result
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Ensure account filter is working")
//    void getTransactionsOfAccountWithinPeriodEnsureAccountFilterTest() {
//        // Arrange
//        Person ze = new Person(newPersonID, "Ze Branca", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID);
//
//        Group lojaZeAssociados = new Group(newGroupID, "lojaZeAssociados", "1991-12-22", ze, newLedgerGroup);
//
//        Account smithsGroceriesAccount = new Account(newAccountID, "Smith's Groceries", "Account just for Smith
//        family's groceries");
//        lojaZeAssociados.addAccountID(newAccountID);
//
//
//        Account zeCreditAccount = new Account(newAccountID2, "Zé & Associados", "Current account of the 'A Loja do
//        Zé' grocery shop");
//        lojaZeAssociados.addAccountID(newAccountID2);
//
//        Account testAccount = new Account(newAccountID3, "Teste", "Conta a testar como input");
//        lojaZeAssociados.addAccountID(newAccountID3);
//
//        Category newCategory = new Category("Groceries");
//        lojaZeAssociados.addCategory("Groceries");
//
//        lojaZeAssociados.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Bags for concrete", newCategory,
//        newAccountID, newAccountID2);
//        lojaZeAssociados.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of concrete",
//        newCategory, newAccountID3, newAccountID2);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of concrete", newCategory,
//        newAccountID3, newAccountID2));
//
//        // Act
//
//        Set<Transaction> actual = new HashSet<>();
//        actual = lojaZeAssociados.getTransactionsOfAccountWithinPeriod(newAccountID3, period);
//
//        // Assert
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriod
//     * Ensure empty HashSet result when none of the transactions are fullfill the input parameters
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Ensure empty result test")
//    void getTransactionsOfAccountWithinPeriodEnsureEmptyResultTest() {
//        // Arrange
//        Person ze = new Person(newPersonID, "Ze Branca", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID);
//
//        Group lojaZeAssociados = new Group(newGroupID, "lojaZeAssociados", "1991-12-22", ze, newLedgerGroup);
//
//        Account smithsGroceriesAccount = new Account(newAccountID, "Smith's Groceries", "Account just for Smith
//        family's groceries");
//        lojaZeAssociados.addAccountID(newAccountID);
//
//        Account zeCreditAccount = new Account(newAccountID2, "Zé & Associados", "Current account of the 'A Loja do
//        Zé' grocery shop");
//        lojaZeAssociados.addAccountID(newAccountID2);
//
//        Account testAccount = new Account(newAccountID3, "Teste", "Conta a testar como input");
//        lojaZeAssociados.addAccountID(newAccountID3);
//
//        Category newCategory = new Category("Groceries");
//        lojaZeAssociados.addCategory("Groceries");
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        lojaZeAssociados.addTransaction(20, Type.DEBIT, "2025-01-01 00:00:00", "Bags for concrete", newCategory,
//        newAccountID, newAccountID2);
//        lojaZeAssociados.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of concrete",
//        newCategory, newAccountID, newAccountID2);
//
//        Set<Transaction> expected = new HashSet<>();
//
//        // Act
//
//        Set<Transaction> actual = new HashSet<>();
//        actual = lojaZeAssociados.getTransactionsOfAccountWithinPeriod(newAccountID3, period);
//
//        // Assert
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionOfAccountWithinPeriod
//     * Ensure empty HashSet result when the given account is not in the Group's listOfAccounts
//     */
//    @Test
//    @DisplayName("Test for getTransactionsOfAccountWithinPeriod - Ensure empty result test")
//    void getTransactionsOfAccountWithinPeriodAccountNotInTheListTest() {
//        // Arrange
//        Person ze = new Person(newPersonID, "Ze Branca", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID);
//
//        Group lojaZeAssociados = new Group(newGroupID, "lojaZeAssociados", "1991-12-22", ze, newLedgerGroup);
//
//        Account smithsGroceriesAccount = new Account(newAccountID, "Smith's Groceries", "Account just for Smith
//        family's groceries");
//        lojaZeAssociados.addAccountID(newAccountID);
//
//        Account zeCreditAccount = new Account(newAccountID2, "Zé & Associados", "Current account of the 'A Loja do
//        Zé' grocery shop");
//        lojaZeAssociados.addAccountID(newAccountID2);
//
//        Account testAccount = new Account(newAccountID3, "Teste", "Conta a testar como input");
//
//        Category newCategory = new Category("Groceries");
//        lojaZeAssociados.addCategory("Groceries");
//
//        lojaZeAssociados.addTransaction(20, Type.DEBIT, "2025-01-01 00:00:00", "Bags for concrete", newCategory,
//        newAccountID, newAccountID2);
//        lojaZeAssociados.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of concrete",
//        newCategory, newAccountID, newAccountID2);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//
//        // Act
//
//        Set<Transaction> actual = new HashSet<>();
//        actual = lojaZeAssociados.getTransactionsOfAccountWithinPeriod(newAccountID3, period);
//
//        // Assert
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getTransactionsWithinPeriod
//     * Ensure works with all Transactions Within Time frame
//     */
//    @Test
//    @DisplayName("Test for getTransactionsWithinPeriod - Ensure works with all Transactions Within Time frame")
//    void getTransactionsWithinPeriodEnsureWorksWithAllTransactionsWithinTimeframeTest() {
//        // Arrange
//        Person ze = new Person(newPersonID, "Ze Branca", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null,
//        null, newLedgerID);
//        Group lojaZeAssociados = new Group(newGroupID, "lojaZeAssociados", "1991-12-22", ze, newLedgerGroup);
//
//        Account smithsGroceriesAccount = new Account(newAccountID, "Smith's Groceries", "Account just for Smith
//        family's groceries");
//        lojaZeAssociados.addAccountID(newAccountID);
//
//        Account zeCreditAccount = new Account(newAccountID2, "Zé & Associados", "Current account of the 'A Loja do
//        Zé' grocery shop");
//        lojaZeAssociados.addAccountID(newAccountID2);
//
//        Category newCategory = new Category("Groceries");
//        lojaZeAssociados.addCategory("Groceries");
//
//        lojaZeAssociados.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Bags for concrete", newCategory,
//        newAccountID, newAccountID2);
//        lojaZeAssociados.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of concrete",
//        newCategory, newAccountID, newAccountID2);
//
//        Period period = new Period("2019-01-01 00:00:00", "2021-01-01 00:00:00");
//
//        Set<Transaction> expected = new HashSet<>();
//        expected.add(new Transaction(20, Type.DEBIT, "2020-01-01 00:00:00", "Bags for concrete", newCategory,
//        newAccountID, newAccountID2));
//        expected.add(new Transaction(100, Type.CREDIT, "2020-01-10 00:00:00", "Selling of concrete", newCategory,
//        newAccountID, newAccountID2));
//
//        // ACT
//        Set<Transaction> actual = lojaZeAssociados.getTransactionsWithinPeriod(period);
//
//        // Assert
//        assertEquals(expected, actual);
//    }

    /**
     * Test for hasMemberID
     * Happy Case
     */
    @Test
    @DisplayName("Test for hasMemberID - Happy Case")
    void hasMemberIDHappyCaseTest() {
        // Arrange
        Group newGroup = new Group(newGroupID1, "Grupo Teste", "2020-01-01", newPersonID4, newLedgerGroupID1);
        newGroup.addMemberID(newPersonID1);
        newGroup.addMemberID(newPersonID2);
        newGroup.addMemberID(newPersonID3);

        // Act
        boolean actual = newGroup.hasMemberID(newPersonID3);

        // Assert

        assertTrue(actual);
    }

    /**
     * Test for hasMemberID
     * Ensure false when personID given as parameter is not member of the group
     */
    @Test
    @DisplayName("Test for hasMemberID - Ensure False Test")
    void hasMemberIDHappyEnsureFalseTest() {
        // Arrange
        Group newGroup = new Group(newGroupID1, "Grupo Teste", "2020-01-01", newPersonID4, newLedgerGroupID1);
        newGroup.addMemberID(newPersonID1);
        newGroup.addMemberID(newPersonID2);

        // Act
        boolean actual = newGroup.hasMemberID(newPersonID3);

        // Assert

        assertFalse(actual);
    }

    /**
     * Test for hasManagerID
     * Happy Case
     */
    @Test
    @DisplayName("Test for hasManagerID - Happy Case")
    void hasManagerIDHappyCaseTest() {
        // Arrange
        Group newGroup = new Group(newGroupID1, "Grupo Teste", "2020-01-01", newPersonID4, newLedgerGroupID1);

        // Act
        boolean actual = newGroup.hasManagerID(newPersonID4);

        // Assert

        assertTrue(actual);
    }

    /**
     * Test for hasManagerID
     * Ensure false when personID given as parameter is not manager of the group
     */
    @Test
    @DisplayName("Test for hasMemberID - Ensure False Test")
    void hasManagerIDHappyEnsureFalseTest() {
        // Arrange
        Group newGroup = new Group(newGroupID1, "Grupo Teste", "2020-01-01", newPersonID4, newLedgerGroupID1);

        // Act
        boolean actual = newGroup.hasMemberID(newPersonID3);

        // Assert

        assertFalse(actual);
    }

    /**
     * Test for Override Equals - Happy case
     * Same attributes
     */
    @Test
    @DisplayName("Test for equals - Happy case")
    void equalsHappyCaseTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);
        Group football = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);

        // Act
        boolean result = family.equals(football);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for Override Equals - Happy case
     * Different ledgerID
     */
    @Test
    @DisplayName("Test for Equals - Ensure true when id is the same and other attributes not")
    void EqualsEnsureTrueWithSameIdDifferentAttributesTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);
        Group football = new Group(newGroupID1, "Family", "2020-01-14", newPersonID1, newLedgerGroupID2);

        // Act
        boolean result = family.equals(football);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for Override Equals
     * Ensure False with different ID's
     */
    @Test
    @DisplayName("Test for equals - ensure false with different IDs")
    void equalsEnsureFalseDifferentIDs() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);
        GroupID id2 = new GroupID("4");
        Group football = new Group(id2, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);

        // Act
        boolean result = family.equals(football);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for Override Equals
     * Ensure False with different ID's
     */
    @Test
    @DisplayName("Test for equals - ensure false with different Identities")
    void equalsEnsureFalseDifferentIdentities() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);
        GroupID id2 = new GroupID("4");
        Group football = new Group(id2, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);

        // Act
        boolean result = family.sameAs(football);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for Override Equals - Happy case
     */
    @Test
    @DisplayName("Test for notEquals - Happy case")
    void notEqualsCreationDateHappyCaseTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Family", "2020-01-14", newPersonID1, newLedgerGroupID1);
        Group football = new Group(newGroupID2, "Family", "1920-01-14", newPersonID1, newLedgerGroupID2);

        // Act
        boolean result = family.equals(football);

        // Arrange
        assertFalse(result);
    }

    /**
     * Test for Override Equals -Same Object - Happy case
     */
    @Test
    @DisplayName("Test for equals - Same Object - Happy case")
    void equalsSameObjectHappyCaseTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);

        // Act
        boolean result = family.equals(family);

        // Arrange
        assertTrue(result);
    }

    /**
     * Test for Override Equals - null Object
     */
    @Test
    @DisplayName("Test for equals - null Object")
    void equalsNullObjectTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);
        Group football = null;

        // Act
        boolean result = family.equals(football);

        // Arrange
        assertFalse(result);
    }

    /**
     * Test for Override Equals - different class objects
     */
    @Test
    @DisplayName("Test for equals - Objects from different Classes")
    void equalsDifferentObjectClassesTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);

        // Act
        boolean expected = family.equals(newPersonID1);

        // Arrange
        assertFalse(expected);
    }

    /**
     * Test for Group hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Ensure true with same attributes")
    void hashCodeEnsureTrueTest() {
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);
        int expectedHash = 856190498;
        int actualHash = family.hashCode();
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for Group toString
     * Ensure true
     */
    @Test
    @DisplayName("Test for toString - Ensure true")
    void toStringEnsureTrueTest() {
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);
        String expected = "Group{groupID=GroupID{groupID=201}, members=Persons{persons=[PersonID{personID=101@switch" +
                ".pt}]}, " +
                "managers=Persons{persons=[PersonID{personID=101@switch.pt}]}, ledger=LedgerID{ledgerID=401}," +
                " categories=Categories{categories=[]}, accountIDs=AccountsIDs{AccountsIDs=[]}," +
                " description=Description{description='Familia'}, " +
                "creationDate=CreationDate{creationDate='2020-01-14'}}";
        String result = family.toString();
        assertEquals(expected, result);
    }

    /**
     * Test for SetGroupIDNull
     */
    @Test
    @DisplayName("Throw exception if GroupID is null")
    void setGroupIdEnsureExceptionWithNull() {
        //Assert
        assertThrows(InvalidFieldException.class, () -> {
            newGroup = new Group(null, "Test Group", "2019-12-18", newPersonID1, newLedgerPersonID2);
        });
    }

    /**
     * Test for SetLedgerIDNull
     */
    @Test
    @DisplayName("Throw exception if LedgerID is null")
    void setLedgerIdEnsureExceptionWithNull() {
        //Assert
        assertThrows(InvalidFieldException.class, () -> {
            newGroup = new Group(newGroupID1, "Test Group", "2019-12-18", newPersonID1, null);
        });
    }

    /**
     * Test for hasAccountID
     * <p>
     * Group has the accountID
     * Happy case
     */
    @Test
    void hasAccountIDHappyCase() {
        //ARRANGE
        Group testGroup = new Group(newGroupID1, "test group", "2019-05-27", newPersonID1, newLedgerGroupID1);

        testGroup.addAccount(newAccountID1);

        //ACT
        boolean result = testGroup.hasAccountID(newAccountID1);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for hasAccountID
     * <p>
     * Group doesn't have the accountID
     * Sad case
     */
    @Test
    void hasAccountIDSadCase() {
        //ARRANGE
        Group testGroup = new Group(newGroupID1, "test group", "2019-05-27", newPersonID1, newLedgerGroupID1);

        //ACT
        boolean result = testGroup.hasAccountID(newAccountID1);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Method GetLedgerID
     * Happy Case
     */
    @Test
    @DisplayName("getLedgerID - HappyCase")
    void getLedgerIDHappyCase() {
        //Arrange
        Group testGroup = new Group(newGroupID1, "test group", "2019-05-27", newPersonID1, newLedgerGroupID1);
        LedgerID expected = new LedgerID("401");
        //Act
        LedgerID result = testGroup.getLedgerID();
        //Assert
        assertEquals(expected, result);
    }

}