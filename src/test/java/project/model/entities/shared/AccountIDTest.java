package project.model.entities.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountIDTest {

    /**
     * Test for AccountID Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for AccountID constructor - Happy case")
    void AccountIDConstructorHappyCaseTest() {
        AccountID one = new AccountID("1");
        assertTrue(one instanceof AccountID);

    }

    /**
     * Test for sameValueAs - True case
     */
    @Test
    @DisplayName("Test for SameValuesAs - Happy case")
    void AccountIDSameValueAsTrueTest() {
        //Arrange
        String one = "1";
        AccountID a = new AccountID(one);
        String two = "1";
        AccountID b = new AccountID(two);
        //Act
        boolean result = a.sameValueAs(b);
        //Arrange
        assertTrue(result);
    }

    /**
     * Test for sameValueAs - False case
     */
    @Test
    @DisplayName("Test for SameValuesAsr - False case")
    void AccountIDSameValueAsFalseTest() {
        //Arrange
        String one = "1";
        AccountID a = new AccountID(one);
        String two = "2";
        AccountID b = new AccountID(two);
        //Act
        boolean result = a.sameValueAs(b);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for sameValueAs - False case
     */
    @Test
    @DisplayName("Test for SameValuesAs - Null case")
    void AccountIDSameValueAsFalseIfNullTest() {
        //Arrange
        String one = "1";
        AccountID a = new AccountID(one);
        //Act
        boolean result = a.sameValueAs(null);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for hashcode - Ensure works
     */
    @Test
    @DisplayName("Test for hashcode - Ensure works")
    void hashCodeEnsureWorksTest() {
        // Arrange
        AccountID a = new AccountID("1");
        int expected = 32;

        // Act
        int result = a.hashCode();

        // Assert
        assertEquals(expected, result);
    }

    /**
     * Test for toString - Ensure works
     */
    @Test
    @DisplayName("Test for toString - Ensure works")
    void toStringEnsureWorksTest() {
        // Arrange
        AccountID a = new AccountID("1");
        String expected = "AccountID{accountID=1}";

        // Act
        String result = a.toString();

        // Assert
        assertEquals(expected, result);
    }

    /**
     * Test for equals - Happy Case
     */
    @Test
    @DisplayName("Test for equals - Happy Case")
    void equalsHappyCaseTest() {
        //Arrange
        AccountID food = new AccountID("1");
        AccountID drinks = new AccountID("1");
        //Act
        Boolean result = food.equals(drinks);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals - Not equals case
     */
    @Test
    @DisplayName("Test for equals - Not Equals Case")
    void notEqualsHappyCaseTest() {
        //Arrange
        AccountID food = new AccountID("1");
        AccountID drinks = new AccountID("2");
        //Act
        Boolean result = drinks.equals(food);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals - Null object
     */
    @Test
    @DisplayName("Test for equals - Null object")
    void equalsNotNullTest() {
        //Arrange
        AccountID food = new AccountID("1");
        AccountID beer = null;
        //Act
        Boolean result = food.equals(beer);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals - objects different classes
     */
    @Test
    @DisplayName("Test for equals - objects different classes")
    void equalsDifferentClassesTest() {
        //Arrange
        AccountID food = new AccountID("1");
        PersonID ana = new PersonID("1@switch.pt");
        //Act
        Boolean result = food.equals(ana);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for getId - Happy Case
     */
    @Test
    @DisplayName("test for getId")
    void getIdHappyCaseTest() {
        //ARRANGE
        AccountID a = new AccountID("1");
        Long expected = 1L;
        //ACT
        Long result = a.getAccountIdValue();
        //ASSERT
        assertEquals(expected, result);
    }
}