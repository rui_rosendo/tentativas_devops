package project.model.entities.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LedgerIDTest {

    @DisplayName("Test for LedgerID - Constructor test - Happy Case")
    @Test
    void constructorLedgerIDHappyCase() {
        LedgerID ledgerID = new LedgerID("1");
        assertTrue(ledgerID instanceof LedgerID);
    }

    /**
     * Test for sameValueAs
     */
    @DisplayName("Test for LedgerID sameValueAs - True case")
    @Test
    void ledgerIDSameValueAsTrueTest() {
        String one = "1";
        LedgerID a = new LedgerID(one);
        String two = "1";
        LedgerID b = new LedgerID(two);
        boolean result = a.sameValueAs(b);
        assertTrue(result);
    }

    /**
     * Test for sameValueAs
     */
    @DisplayName("Test for LedgerID sameValueAs - False case")
    @Test
    void ledgerIDSameValueAsFalseTest() {
        String one = "1";
        LedgerID a = new LedgerID(one);
        String two = "2";
        LedgerID b = new LedgerID(two);
        boolean result = a.sameValueAs(b);
        assertFalse(result);
    }

    /**
     * Test for sameValueAs
     * Ensure false with null
     */
    @DisplayName("Test for LedgerID sameValueAs - False case")
    @Test
    void sameValueAsEnsureFalseWithNullTest() {
        // Arrange
        LedgerID a = new LedgerID("1");

        // Act
        boolean result = a.sameValueAs(null);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for LedgerID toString
     * Happy Case
     */
    @Test
    @DisplayName("Test for toString - Happy Case")
    void toStringEnsureTrueTest() {
        LedgerID newLedgerID = new LedgerID("834583");
        String expected = "LedgerID{ledgerID=834583}";

        String result = newLedgerID.toString();

        assertEquals(expected, result);
    }

    /**
     * Test for toString - not equals case
     */
    @Test
    @DisplayName("test for toString - not equals case")
    void toStringNotEqualsTest() {
        //Arrange
        LedgerID ledgerID = new LedgerID("1");
        String expected = "";
        //Act
        String result = ledgerID.toString();
        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @DisplayName("Test for hashcode - Ensure works")
    @Test
    void hashCodeEnsureWorksTest() {
        // Arrange
        LedgerID a = new LedgerID("1");
        int expected = 32;

        // Act
        int result = a.hashCode();

        // Assert
        assertEquals(expected, result);
    }

    /**
     * Test for equals
     * Ensure true with same object
     */
    @DisplayName("Test for equals - Ensure true with same object")
    @Test
    void equalsEnsureTrueWithSameObjectTest() {
        // Arrange
        LedgerID a = new LedgerID("1");

        // Act
        boolean result = a.equals(a);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with object from different class
     */
    @DisplayName("Test for equals - Ensure false with object from different class")
    @Test
    void equalsEnsureFalseWithObjectFromDifferentClassTest() {
        // Arrange
        LedgerID a = new LedgerID("1");
        AccountID b = new AccountID("1");

        // Act
        boolean result = a.equals(b);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true
     */
    @DisplayName("Test for equals - Ensure true")
    @Test
    void equalsEnsureTrueTest() {
        // Arrange
        LedgerID a = new LedgerID("1");
        LedgerID b = new LedgerID("1");

        // Act
        boolean result = a.equals(b);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for LedgerID toStringDTO
     * Happy Case
     */
    @Test
    @DisplayName("Test for toStringDTO - Happy Case")
    void toStringDTOEnsureTrueTest() {
        LedgerID newLedgerID = new LedgerID("834583");
        String expected = "834583";

        String result = newLedgerID.toStringDTO();

        assertEquals(expected, result);
    }

    /**
     * Test for toStringDTO - not equals case
     */
    @Test
    @DisplayName("test for toStringDTO - not equals case")
    void toStringDTONotEqualsTest() {
        //Arrange
        LedgerID ledgerID = new LedgerID("1");
        String expected = "";
        //Act
        String result = ledgerID.toStringDTO();
        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getId - Happy Case
     */
    @Test
    @DisplayName("test for getId")
    void getIdHappyCaseTest() {
        //ARRANGE
        LedgerID a = new LedgerID("1");
        Long expected = 1L;
        //ACT
        Long result = a.getLedgerIdValue();
        //ASSERT
        assertEquals(expected, result);
    }
}