//package project.model.domainentities.ledger;
//
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import project.model.domainentities.account.Account;
//import project.model.domainentities.ledger.Ledger;
//import project.model.domainentities.ledger.Scheduler;
//import project.model.domainentities.ledger.Type;
//import project.model.domainentities.shared.Category;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//class SchedulerTest {
//
//    /**
//     * Test for scheduleTransaction
//     * Ensure works recurrently
//     */
//    @Test
//    @DisplayName("Test for scheduleTransaction - Ensure works recurrently")
//    public void scheduleTransactionEnsureWorksRecurrentlyTest() throws InterruptedException {
//        // Arrange
//        Scheduler sc = new Scheduler(1);
//
//        Ledger ledger = new Ledger();
//        double amount = 10;
//        String dateTime = null;
//        String description = "KitKat";
//        Category category = new Category("Food");
//        Account debitAccount = new Account("wallet", "small change");
//        Account creditAccount = new Account("NorteShopping", "shop register");
//
//        int expectedSize = 2;
//
//        // Act
//        sc.scheduleTransaction(ledger, amount, Type.DEBIT, dateTime, description, category, debitAccount, creditAccount);
//        Thread.sleep(1500);
//        int actualSize = ledger.getTransactionsSize();
//
//        // Assert
//        assertEquals(expectedSize, actualSize);
//
//    }
//
//}