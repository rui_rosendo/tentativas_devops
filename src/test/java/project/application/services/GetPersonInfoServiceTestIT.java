package project.application.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.GetPersonInfoResponseDTO;
import project.frameworkddd.GetPersonInfo_Service;
import project.model.specifications.repositories.PersonRepository;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
class GetPersonInfoServiceTestIT {

    @Autowired
    PersonRepository personRepo;
    @Autowired
    GetPersonInfo_Service service;

    /**
     * Test for constructor
     */
    @Test
    @DisplayName("Test for GetPersonInfoServiceTest Constructor")
    void GetPersonInfoServiceTestConstructorTest() {
        // Assert
        assertTrue(service instanceof GetPersonInfoService);
    }

    /**
     * Test for getPersonInfo
     * Happy Case
     */
    @Test
    @DisplayName("Test for getPersonInfo - Happy Case")
    void getGroupByIDHappyCaseTest() {
        //ARRANGE
        GetPersonInfoResponseDTO expected = new GetPersonInfoResponseDTO("21001@switch.pt", "João",
                "Travessa Santa Bárbara", "1987-04-17", "Estarreja");

        //ACT
        GetPersonInfoResponseDTO result = service.getPersonInfo("21001@switch.pt");

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getPersonInfo
     * Fail Case
     */
    @Test
    @DisplayName("Test for getPersonInfo - Fail Case")
    void getGroupByIDFailCaseTest() {
        //ARRANGE
        GetPersonInfoResponseDTO expected = new GetPersonInfoResponseDTO("21001@switch.pt", "João",
                "Travessa Santa Bárbara", "1987-04-17", "Estarreja");

        //ACT
        GetPersonInfoResponseDTO result = service.getPersonInfo("21002@switch.pt");

        //ASSERT
        assertNotEquals(expected, result);
    }
}