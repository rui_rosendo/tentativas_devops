package project.application.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CheckIfPeopleAreSiblingsRequestDTO;
import project.dto.CheckIfPeopleAreSiblingsResponseDTO;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS001Service;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
class CheckIfPeopleAreSiblingsServiceTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private IUS001Service service;

    /**
     * Test for CheckIfPeopleAreSiblingsService
     * Happy case
     */
    @Test
    @DisplayName("Is sibling - Ensure true")
    void isSiblingEnsureTrueTest() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO dto = new CheckIfPeopleAreSiblingsRequestDTO("15@switch.pt", "16@switch.pt");

        PersonID pID1 = new PersonID(dto.getPersonEmail1());
        PersonID pID2 = new PersonID(dto.getPersonEmail2());

        Person mockedPerson1 = Mockito.mock(Person.class);
        Person mockedPerson2 = Mockito.mock(Person.class);

        Mockito.when(personRepository.findById(pID1)).thenReturn(Optional.of(mockedPerson1));
        Mockito.when(personRepository.findById(pID2)).thenReturn(Optional.of(mockedPerson2));

        Mockito.when(mockedPerson1.isSibling(mockedPerson2)).thenReturn(true);

        CheckIfPeopleAreSiblingsResponseDTO expected = new CheckIfPeopleAreSiblingsResponseDTO("true");

        //Act
        CheckIfPeopleAreSiblingsResponseDTO result = service.isSibling(dto);

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for CheckIfPeopleAreSiblingsService
     * False case
     */
    @Test
    @DisplayName("Is sibling - False true")
    void isSiblingEnsureFalseTest() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO dto = new CheckIfPeopleAreSiblingsRequestDTO("15@switch.pt", "16@switch.pt");

        PersonID pID1 = new PersonID(dto.getPersonEmail1());
        PersonID pID2 = new PersonID(dto.getPersonEmail2());

        Person mockedPerson1 = Mockito.mock(Person.class);
        Person mockedPerson2 = Mockito.mock(Person.class);

        Mockito.when(personRepository.findById(pID1)).thenReturn(Optional.of(mockedPerson1));
        Mockito.when(personRepository.findById(pID2)).thenReturn(Optional.of(mockedPerson2));

        Mockito.when(mockedPerson1.isSibling(mockedPerson2)).thenReturn(false);

        CheckIfPeopleAreSiblingsResponseDTO expected = new CheckIfPeopleAreSiblingsResponseDTO("false");

        //Act
        CheckIfPeopleAreSiblingsResponseDTO result = service.isSibling(dto);

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for CheckIfPeopleAreSiblingsService
     * First person not present case
     */
    @Test
    @DisplayName("CheckIfPeopleAreSiblingsServiceTest - Ensure exception when person not found in repository")
    void isSiblingEnsureExceptionWhenPersonNotFoundTest() {
        //Arrange
        CheckIfPeopleAreSiblingsRequestDTO dto = new CheckIfPeopleAreSiblingsRequestDTO("15@switch.pt", "16@switch.pt");

        PersonID pID1 = new PersonID(dto.getPersonEmail1());
        PersonID pID2 = new PersonID(dto.getPersonEmail2());

        Person mockedPerson1 = Mockito.mock(Person.class);
        Person mockedPerson2 = Mockito.mock(Person.class);

        Mockito.when(personRepository.findById(pID1)).thenReturn(Optional.empty());
        Mockito.when(personRepository.findById(pID2)).thenReturn(Optional.of(mockedPerson2));

        Mockito.when(mockedPerson1.isSibling(mockedPerson2)).thenReturn(false);

        //Act
        //Assert
        assertThrows(PersonNotFoundException.class, () -> {
            CheckIfPeopleAreSiblingsResponseDTO result = service.isSibling(dto);
        });
    }
}