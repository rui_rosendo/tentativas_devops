package project.application.services;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.frameworkddd.IUS010Service;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
public class GetTransactionsOfAccountWithinPeriodServiceTest {

    @Autowired
    IUS010Service service;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    LedgerRepository ledgerRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    PersonRepository personRepository;


//    /**
//     * Unit test for method getTransactionOfAccountWithinPeriodByGroupID
//     * Happy Case
//     */
//    @Test
//    void getTransactionOfAccountWithinPeriodServiceHappyByGroupIDCaseTest() {
//
//        //ARRANGE
//
//
//        //Variables
//        String personIdStg = "1001@switch.pt";
//        PersonID personID = new PersonID(personIdStg);
//        String groupIdStg = "1002";
//        GroupID groupID = new GroupID(groupIdStg);
//        String ledgerIdStg = "1002";
//        LedgerID groupLedgerID  = new LedgerID(ledgerIdStg);
//        Ledger groupLedger = new Ledger(groupLedgerID);
//        String typeDebitStr = "DEBIT";
//        Type typeDebit = Type.DEBIT;
//        String typeCreditStr = "CREDIT";
//        Type typeCredit = Type.CREDIT;
//        String transactionDescription = "transaction description";
//        String categoryDesignation = "category designation";
//        String debitAccountIDStg = "1002";
//        AccountID debitAccountId = new AccountID(debitAccountIDStg);
//        String creditAccountIDStg = "1004";
//        AccountID creditAccountId = new AccountID(creditAccountIDStg);
//        String initialDateTime = "1999-01-01 12:00:00";
//        String finalDateTime = "2021-01-01 12:00:00";
//        Period period = new Period(initialDateTime,finalDateTime);
//        Category category = new Category("comida e bebida");
//
//
//        //Mocks
//        Person mockPerson = Mockito.mock(Person.class);
//        GroupID mockGroupID = Mockito.mock(GroupID.class);
//        Group mockGroup = Mockito.mock(Group.class);
//        Account mockAccount = Mockito.mock(Account.class);
//        LedgerID mockLedgerID = Mockito.mock(LedgerID.class);
//        //todo: não posso fazer mocks do ledger porque preciso das transações que estão no ledger
//        Ledger mockLedger = Mockito.mock(Ledger.class);
//        Set<Transaction> mockSetTransactions = Mockito.mock(Set.class);
//
//        //ACT
//        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
//        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockGroup));
//        Mockito.when(mockGroup.hasMemberID(personID)).thenReturn(true);
//        Mockito.when(mockGroup.hasAccountID(debitAccountId)).thenReturn(true);
//        Mockito.when(accountRepository.findById(debitAccountId)).thenReturn(Optional.of(mockAccount));
//        Mockito.when(mockGroup.getLedgerID()).thenReturn(mockLedgerID);
//
//        Mockito.when(ledgerRepository.findById(groupLedgerID)).thenReturn(Optional.of(groupLedger));
//
//
//
//
//        //Transactions for expectedTransactionList to build expectedResponseDTO
//        Transaction t1 = new Transaction("500", typeCredit, "2000-01-01 23:20:58", "Pacote de Oreos", category, debitAccountId, creditAccountId);
//        Transaction t2 = new Transaction("400", typeDebit, "2010-01-01 11:39:23", "Pacote de Belgas", category, debitAccountId, creditAccountId);
//        Transaction t3 = new Transaction("100", typeCredit, "2020-01-01 14:12:15", "Pacote de Maria", category, debitAccountId, creditAccountId);
//
//        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
//        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
//        TransactionDTO tdto3 = new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");
//
//
//        List<TransactionDTO> expectedTransactionDTOList = new ArrayList<>();
//        expectedTransactionDTOList.add(tdto1);
//        expectedTransactionDTOList.add(tdto2);
//        expectedTransactionDTOList.add(tdto3);
//
//
//        Set<Transaction> expectedTransactionSet = new HashSet<>();
//        expectedTransactionSet.add(t1);
//        expectedTransactionSet.add(t2);
//        expectedTransactionSet.add(t3);
//
//        Mockito.when(groupLedger.getTransactionsOfAccountWithinPeriod(debitAccountId, period)).thenReturn(mockSetTransactions);
//
//        TransactionsResponseDTO expectedResponseDTO = new TransactionsResponseDTO(expectedTransactionDTOList);
//
//
//        //Act
//        TransactionsResponseDTO actualResponseDTO = service.getTransactionsOfAccountWithinPeriodByGroupID("1001@switch.pt", "1002", "1002", "1999-01-01 23:20:58", "2021-01-01 23:20:58");
//
//        //Assert
//        assertEquals(expectedResponseDTO, actualResponseDTO);
//
//        // End My test
//    }


}
