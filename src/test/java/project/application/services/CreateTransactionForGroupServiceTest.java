package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CreateTransactionForGroupRequestDTO;
import project.dto.CreateTransactionForGroupResponseDTO;
import project.dto.TransactionsAssembler;
import project.dto.TransactionsResponseDTO;
import project.exceptions.*;
import project.frameworkddd.IUS008_1Service;
import project.model.entities.Categories;
import project.model.entities.Transactions;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.ledger.Transaction;
import project.model.entities.ledger.Type;
import project.model.entities.shared.*;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
class CreateTransactionForGroupServiceTest {

    @Autowired
    IUS008_1Service service;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    LedgerRepository ledgerRepository;

    @Autowired
    AccountRepository accountRepository;

    /**
     * Unit test for method createTransactionForGroup
     * Happy Case with Debit Type
     */
    @Test
    void createTransactionForGroupHappyCaseDebitTypeTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        String amount = "2950.90";
        String dateTime = "2000-01-01 12:00:00";
        String type = "DEBIT";
        String transactionDescription = "transaction description";
        String categoryDesignation = "category designation";
        String debitAccountIDStg = "1000";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);
        String creditAccountIDStg = "2000";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);

        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personIdStg,
                groupIdStg,
                amount, dateTime, type, transactionDescription, categoryDesignation, debitAccountIDStg,
                creditAccountIDStg);

        String groupDescriptionStg = "group description";
        LedgerID ledgerID = new LedgerID("123");
        PersonID personID = new PersonID(personIdStg);
        Category category = new Category("category designation");

        Group mockedGroup = Mockito.mock(Group.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);
        GroupID mockedGroupId = Mockito.mock(GroupID.class);
        Description mockedDescription = Mockito.mock(Description.class);
        AccountsIDs mockedAccountsIds = Mockito.mock(AccountsIDs.class);
        Categories mockedCategories = Mockito.mock(Categories.class);
        Set<AccountID> accountIDSet = new HashSet<>();
        accountIDSet.add(debitAccountId);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));
        Mockito.when(mockedGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(accountRepository.existsById(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.existsById(creditAccountId)).thenReturn(true);

        Mockito.when(mockedGroup.getCategories()).thenReturn(mockedCategories);
        Mockito.when(mockedCategories.hasCategory(category)).thenReturn(true);

        Mockito.when(mockedGroup.getAccountsIDs()).thenReturn(mockedAccountsIds);
        Mockito.when(mockedAccountsIds.getAccountsIDsValue()).thenReturn(accountIDSet);

        Mockito.when(mockedGroup.getID()).thenReturn(mockedGroupId);
        Mockito.when(mockedGroupId.toStringDTO()).thenReturn(groupIdStg);
        Mockito.when(mockedGroup.getDescription()).thenReturn(mockedDescription);
        Mockito.when(mockedDescription.getDescriptionValue()).thenReturn(groupDescriptionStg);


        CreateTransactionForGroupResponseDTO expectedResponseDTO = new CreateTransactionForGroupResponseDTO(groupIdStg,
                groupDescriptionStg, amount, type, transactionDescription);

        //Act
        CreateTransactionForGroupResponseDTO actualResponseDTO = service.createTransactionForGroup(requestDTO);

        //Assert
        assertEquals(expectedResponseDTO, actualResponseDTO);

    }

    /**
     * Unit test for method createTransactionForGroup
     * Happy Case with Credit Type
     */
    @Test
    void createTransactionForGroupHappyCaseCreditTypeTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        String amount = "2950.90";
        String dateTime = "2000-01-01 12:00:00";
        String type = "CREDIT";
        String transactionDescription = "transaction description";
        String categoryDesignation = "category designation";
        String debitAccountIDStg = "1000";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);
        String creditAccountIDStg = "2000";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);

        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personIdStg,
                groupIdStg,
                amount, dateTime, type, transactionDescription, categoryDesignation, debitAccountIDStg,
                creditAccountIDStg);

        String groupDescriptionStg = "group description";
        LedgerID ledgerID = new LedgerID("123");
        PersonID personID = new PersonID(personIdStg);
        Category category = new Category("category designation");

        Group mockedGroup = Mockito.mock(Group.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);
        GroupID mockedGroupId = Mockito.mock(GroupID.class);
        Description mockedDescription = Mockito.mock(Description.class);
        AccountsIDs mockedAccountsIds = Mockito.mock(AccountsIDs.class);
        Categories mockedCategories = Mockito.mock(Categories.class);
        Set<AccountID> accountIDSet = new HashSet<>();
        accountIDSet.add(creditAccountId);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));
        Mockito.when(mockedGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(accountRepository.existsById(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.existsById(creditAccountId)).thenReturn(true);

        Mockito.when(mockedGroup.getCategories()).thenReturn(mockedCategories);
        Mockito.when(mockedCategories.hasCategory(category)).thenReturn(true);

        Mockito.when(mockedGroup.getAccountsIDs()).thenReturn(mockedAccountsIds);
        Mockito.when(mockedAccountsIds.getAccountsIDsValue()).thenReturn(accountIDSet);

        Mockito.when(mockedGroup.getID()).thenReturn(mockedGroupId);
        Mockito.when(mockedGroupId.toStringDTO()).thenReturn(groupIdStg);
        Mockito.when(mockedGroup.getDescription()).thenReturn(mockedDescription);
        Mockito.when(mockedDescription.getDescriptionValue()).thenReturn(groupDescriptionStg);


        CreateTransactionForGroupResponseDTO expectedResponseDTO = new CreateTransactionForGroupResponseDTO(groupIdStg,
                groupDescriptionStg, amount, type, transactionDescription);

        //Act
        CreateTransactionForGroupResponseDTO actualResponseDTO = service.createTransactionForGroup(requestDTO);

        //Assert
        assertEquals(expectedResponseDTO, actualResponseDTO);

    }

    /**
     * Unit test for method createTransactionForGroup
     * Ensure throws GroupNotFoundException when group is not in repo
     */
    @Test
    void createTransactionForGroupEnsureThrowsExceptionWhenGroupIsNotInRepoTest() {
        //Arrange
        String personId = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        String amount = "2950.90";
        String dateTime = "2000-01-01 12:00:00";
        String type = "DEBIT";
        String transactionDescription = "transaction description";
        String categoryDesignation = "category designation";
        String debitAccountIDStg = "1000";
        String creditAccountIDStg = "2000";

        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personId, groupIdStg,
                amount, dateTime, type, transactionDescription, categoryDesignation, debitAccountIDStg,
                creditAccountIDStg);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.createTransactionForGroup(requestDTO);
        });

    }

    /**
     * Unit test for method createTransactionForGroup
     * Ensure throws LedgerNotFoundException when ledger is not in repo
     */
    @Test
    void createTransactionForGroupEnsureThrowsExceptionWhenLedgerIsNotInRepoTest() {
        //Arrange
        String personId = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        String amount = "2950.90";
        String dateTime = "2000-01-01 12:00:00";
        String type = "DEBIT";
        String transactionDescription = "transaction description";
        String categoryDesignation = "category designation";
        String debitAccountIDStg = "1000";
        String creditAccountIDStg = "2000";

        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personId, groupIdStg,
                amount, dateTime, type, transactionDescription, categoryDesignation, debitAccountIDStg,
                creditAccountIDStg);

        LedgerID ledgerID = new LedgerID("123");
        Group mockedGroup = Mockito.mock(Group.class);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(LedgerNotFoundException.class, () -> {
            service.createTransactionForGroup(requestDTO);
        });
    }

    /**
     * Unit test for method createTransactionForGroup
     * Ensure throws GroupConflictException when person is not member of the group
     */
    @Test
    void createTransactionForGroupEnsureThrowsExceptionWhenPersonIsNotMemberOfTheGroupTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        String amount = "2950.90";
        String dateTime = "2000-01-01 12:00:00";
        String type = "CREDIT";
        String transactionDescription = "transaction description";
        String categoryDesignation = "category designation";
        String debitAccountIDStg = "1000";
        String creditAccountIDStg = "2000";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);

        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personIdStg,
                groupIdStg,
                amount, dateTime, type, transactionDescription, categoryDesignation, debitAccountIDStg,
                creditAccountIDStg);

        LedgerID ledgerID = new LedgerID("123");
        PersonID personID = new PersonID(personIdStg);

        Group mockedGroup = Mockito.mock(Group.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);

        Set<AccountID> accountIDSet = new HashSet<>();
        accountIDSet.add(creditAccountId);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));
        Mockito.when(mockedGroup.hasMemberID(personID)).thenReturn(false);

        //Act
        //Assert
        Assertions.assertThrows(GroupConflictException.class, () -> {
            service.createTransactionForGroup(requestDTO);
        });
    }

    /**
     * Unit test for method createTransactionForGroup
     * Ensure throws AccountNotFoundException when debit account is not in repo
     */
    @Test
    void createTransactionForGroupEnsureThrowsExceptionWhenDebitAccountIsNotInRepoTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        String amount = "2950.90";
        String dateTime = "2000-01-01 12:00:00";
        String type = "DEBIT";
        String transactionDescription = "transaction description";
        String categoryDesignation = "category designation";
        String debitAccountIDStg = "1000";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);
        String creditAccountIDStg = "2000";

        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personIdStg,
                groupIdStg,
                amount, dateTime, type, transactionDescription, categoryDesignation, debitAccountIDStg,
                creditAccountIDStg);

        LedgerID ledgerID = new LedgerID("123");
        PersonID personID = new PersonID(personIdStg);

        Group mockedGroup = Mockito.mock(Group.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));
        Mockito.when(mockedGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(accountRepository.existsById(debitAccountId)).thenReturn(false);

        //Act
        //Assert
        Assertions.assertThrows(AccountNotFoundException.class, () -> {
            service.createTransactionForGroup(requestDTO);
        });
    }

    /**
     * Unit test for method createTransactionForGroup
     * Ensure throws AccountNotFoundException when credit account is not in repo
     */
    @Test
    void createTransactionForGroupEnsureThrowsExceptionWhenCreditAccountIsNotInRepoTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        String amount = "2950.90";
        String dateTime = "2000-01-01 12:00:00";
        String type = "DEBIT";
        String transactionDescription = "transaction description";
        String categoryDesignation = "category designation";
        String debitAccountIDStg = "1000";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);
        String creditAccountIDStg = "2000";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);

        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personIdStg,
                groupIdStg,
                amount, dateTime, type, transactionDescription, categoryDesignation, debitAccountIDStg,
                creditAccountIDStg);

        LedgerID ledgerID = new LedgerID("123");
        PersonID personID = new PersonID(personIdStg);

        Group mockedGroup = Mockito.mock(Group.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));
        Mockito.when(mockedGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(accountRepository.existsById(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.existsById(creditAccountId)).thenReturn(false);

        //Act
        //Assert
        Assertions.assertThrows(AccountNotFoundException.class, () -> {
            service.createTransactionForGroup(requestDTO);
        });
    }

    /**
     * Unit test for method createTransactionForGroup
     * Ensure throws CategoryNotFoundException when group doesn't have the transaction's category in its category list
     */
    @Test
    void createTransactionForGroupEnsureThrowsExceptionWhenCategoryIsNotInGroupCategoryListTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        String amount = "2950.90";
        String dateTime = "2000-01-01 12:00:00";
        String type = "CREDIT";
        String transactionDescription = "transaction description";
        String categoryDesignation = "category designation";
        String debitAccountIDStg = "1000";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);
        String creditAccountIDStg = "2000";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);

        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personIdStg,
                groupIdStg,
                amount, dateTime, type, transactionDescription, categoryDesignation, debitAccountIDStg,
                creditAccountIDStg);

        LedgerID ledgerID = new LedgerID("123");
        PersonID personID = new PersonID(personIdStg);
        Category category = new Category("category designation");

        Group mockedGroup = Mockito.mock(Group.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);
        Categories mockedCategories = Mockito.mock(Categories.class);
        Set<AccountID> accountIDSet = new HashSet<>();
        accountIDSet.add(creditAccountId);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));
        Mockito.when(mockedGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(accountRepository.existsById(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.existsById(creditAccountId)).thenReturn(true);

        Mockito.when(mockedGroup.getCategories()).thenReturn(mockedCategories);
        Mockito.when(mockedCategories.hasCategory(category)).thenReturn(false);

        //Act
        //Assert
        Assertions.assertThrows(CategoryNotFoundException.class, () -> {
            service.createTransactionForGroup(requestDTO);
        });
    }

    /**
     * Unit test for method createTransactionForGroup
     * Ensure throws InvalidFieldException when type content is not valid repo
     */
    @Test
    void createTransactionForGroupEnsureThrowsExceptionWhenTypeContentIsNotValidTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        String amount = "2950.90";
        String dateTime = "2000-01-01 12:00:00";
        String type = "invalid";
        String transactionDescription = "transaction description";
        String categoryDesignation = "category designation";
        String debitAccountIDStg = "1000";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);
        String creditAccountIDStg = "2000";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);

        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personIdStg,
                groupIdStg,
                amount, dateTime, type, transactionDescription, categoryDesignation, debitAccountIDStg,
                creditAccountIDStg);

        LedgerID ledgerID = new LedgerID("123");
        PersonID personID = new PersonID(personIdStg);
        Category category = new Category("category designation");


        Group mockedGroup = Mockito.mock(Group.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);
        AccountsIDs mockedAccountsIds = Mockito.mock(AccountsIDs.class);
        Categories mockedCategories = Mockito.mock(Categories.class);
        Set<AccountID> accountIDSet = new HashSet<>();
        accountIDSet.add(creditAccountId);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));
        Mockito.when(mockedGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(accountRepository.existsById(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.existsById(creditAccountId)).thenReturn(true);

        Mockito.when(mockedGroup.getCategories()).thenReturn(mockedCategories);
        Mockito.when(mockedCategories.hasCategory(category)).thenReturn(true);

        Mockito.when(mockedGroup.getAccountsIDs()).thenReturn(mockedAccountsIds);
        Mockito.when(mockedAccountsIds.getAccountsIDsValue()).thenReturn(accountIDSet);

        //Assert
        Assertions.assertThrows(InvalidFieldException.class, () -> {
            service.createTransactionForGroup(requestDTO);
        });
    }

    /**
     * Unit test for method createTransactionForGroup
     * Happy Case with Debit Type
     */
    @Test
    void createTransactionForGroupEnsureExceptionIfAccountOfTransactionTypeDebitNotOwnedByGroupTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        String amount = "2950.90";
        String dateTime = "2000-01-01 12:00:00";
        String type = "DEBIT";
        String transactionDescription = "transaction description";
        String categoryDesignation = "category designation";
        String debitAccountIDStg = "1000";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);
        String creditAccountIDStg = "2000";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);

        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personIdStg,
                groupIdStg,
                amount, dateTime, type, transactionDescription, categoryDesignation, debitAccountIDStg,
                creditAccountIDStg);

        String groupDescriptionStg = "group description";
        LedgerID ledgerID = new LedgerID("123");
        PersonID personID = new PersonID(personIdStg);
        Category category = new Category("category designation");

        Group mockedGroup = Mockito.mock(Group.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);
        GroupID mockedGroupId = Mockito.mock(GroupID.class);
        Description mockedDescription = Mockito.mock(Description.class);
        AccountsIDs mockedAccountsIds = Mockito.mock(AccountsIDs.class);
        Categories mockedCategories = Mockito.mock(Categories.class);
        Set<AccountID> accountIDSet = new HashSet<>();
        accountIDSet.add(creditAccountId);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));
        Mockito.when(mockedGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(accountRepository.existsById(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.existsById(creditAccountId)).thenReturn(true);

        Mockito.when(mockedGroup.getCategories()).thenReturn(mockedCategories);
        Mockito.when(mockedCategories.hasCategory(category)).thenReturn(true);

        Mockito.when(mockedGroup.getAccountsIDs()).thenReturn(mockedAccountsIds);
        Mockito.when(mockedAccountsIds.getAccountsIDsValue()).thenReturn(accountIDSet);

        Mockito.when(mockedGroup.getID()).thenReturn(mockedGroupId);
        Mockito.when(mockedGroupId.toStringDTO()).thenReturn(groupIdStg);
        Mockito.when(mockedGroup.getDescription()).thenReturn(mockedDescription);
        Mockito.when(mockedDescription.getDescriptionValue()).thenReturn(groupDescriptionStg);

        //Act
        //Assert
        Assertions.assertThrows(AccountNotFoundException.class, () -> {
            service.createTransactionForGroup(requestDTO);
        });

    }

    /**
     * Unit test for method createTransactionForGroup
     * Happy Case with Debit Type
     */
    @Test
    void createTransactionForGroupEnsureExceptionIfAccountOfTransactionTypeCreditNotOwnedByGroupTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        String amount = "2950.90";
        String dateTime = "2000-01-01 12:00:00";
        String type = "CREDIT";
        String transactionDescription = "transaction description";
        String categoryDesignation = "category designation";
        String debitAccountIDStg = "1000";
        AccountID debitAccountId = new AccountID(debitAccountIDStg);
        String creditAccountIDStg = "2000";
        AccountID creditAccountId = new AccountID(creditAccountIDStg);

        CreateTransactionForGroupRequestDTO requestDTO = new CreateTransactionForGroupRequestDTO(personIdStg,
                groupIdStg,
                amount, dateTime, type, transactionDescription, categoryDesignation, debitAccountIDStg,
                creditAccountIDStg);

        String groupDescriptionStg = "group description";
        LedgerID ledgerID = new LedgerID("123");
        PersonID personID = new PersonID(personIdStg);
        Category category = new Category("category designation");

        Group mockedGroup = Mockito.mock(Group.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);
        GroupID mockedGroupId = Mockito.mock(GroupID.class);
        Description mockedDescription = Mockito.mock(Description.class);
        AccountsIDs mockedAccountsIds = Mockito.mock(AccountsIDs.class);
        Categories mockedCategories = Mockito.mock(Categories.class);
        Set<AccountID> accountIDSet = new HashSet<>();
        accountIDSet.add(debitAccountId);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));
        Mockito.when(mockedGroup.hasMemberID(personID)).thenReturn(true);
        Mockito.when(accountRepository.existsById(debitAccountId)).thenReturn(true);
        Mockito.when(accountRepository.existsById(creditAccountId)).thenReturn(true);

        Mockito.when(mockedGroup.getCategories()).thenReturn(mockedCategories);
        Mockito.when(mockedCategories.hasCategory(category)).thenReturn(true);

        Mockito.when(mockedGroup.getAccountsIDs()).thenReturn(mockedAccountsIds);
        Mockito.when(mockedAccountsIds.getAccountsIDsValue()).thenReturn(accountIDSet);

        Mockito.when(mockedGroup.getID()).thenReturn(mockedGroupId);
        Mockito.when(mockedGroupId.toStringDTO()).thenReturn(groupIdStg);
        Mockito.when(mockedGroup.getDescription()).thenReturn(mockedDescription);
        Mockito.when(mockedDescription.getDescriptionValue()).thenReturn(groupDescriptionStg);

        //Act
        //Assert
        Assertions.assertThrows(AccountNotFoundException.class, () -> {
            service.createTransactionForGroup(requestDTO);
        });

    }

    /**
     * Unit test for method getTransactionsByGroupID
     * HappyCase
     */
    @Test
    void getTransactionsByGroupIDHappyCaseTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        LedgerID ledgerID = new LedgerID("1000");

        Group mockedGroup = Mockito.mock(Group.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);
        Transactions mockedTransactions = Mockito.mock(Transactions.class);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));

        Transaction transaction = new Transaction("1000", Type.DEBIT, "2020-01-01 12:00:00", "description",
                new Category("category"), new AccountID("500"), new AccountID("600"));
        List<Transaction> transactionsSet = new ArrayList<>();
        transactionsSet.add(transaction);
        Mockito.when(mockedLedger.getTransactions()).thenReturn(mockedTransactions);
        Mockito.when(mockedTransactions.getAll()).thenReturn(transactionsSet);

        TransactionsResponseDTO expectedResponseDTO = TransactionsAssembler.mapToDTO(transactionsSet);

        System.out.println(expectedResponseDTO);

        //Act
        TransactionsResponseDTO actualResponseDTO = service.getTransactionsByGroupID(personIdStg, groupIdStg);

        //Assert
        assertEquals(expectedResponseDTO, actualResponseDTO);

    }

    /**
     * Unit test for method getTransactionsByGroupID
     * Ensure throws exception when group doesn't exist in group repo
     */
    @Test
    void getTransactionsByGroupIDEnsureThrowsExceptionWhenGroupDoesNotExistInRepoTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.getTransactionsByGroupID(personIdStg, groupIdStg);
        });
    }

    /**
     * Unit test for method getTransactionsByGroupID
     * Ensure throws exception when ledger doesn't exist in ledger repo
     */
    @Test
    void getTransactionsByGroupIDEnsureThrowsExceptionWhenLedgerDoesNotExistInRepoTest() {
        //Arrange
        String personIdStg = "10@switch.pt";
        String groupIdStg = "100";
        GroupID groupID = new GroupID(groupIdStg);
        LedgerID ledgerID = new LedgerID("1000");

        Group mockedGroup = Mockito.mock(Group.class);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.getLedgerID()).thenReturn(ledgerID);
        Mockito.when(ledgerRepository.findById(ledgerID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(LedgerNotFoundException.class, () -> {
            service.getTransactionsByGroupID(personIdStg, groupIdStg);
        });
    }
}