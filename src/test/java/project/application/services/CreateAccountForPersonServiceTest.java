package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.*;
import project.exceptions.AccountAlreadyExistsException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS006Service;
import project.model.entities.account.Account;
import project.model.entities.person.Person;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.AccountsIDs;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
public class CreateAccountForPersonServiceTest {

    @Autowired
    IUS006Service service;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    PersonRepository personRepository;

    private PersonID personID;

    @BeforeEach
    public void init() {
        String prefix = "600";
        personID = new PersonID(prefix + "1@switch.pt");
    }

    /**
     * createAccountForPerson
     * HappyCase
     */
    @Test
    @DisplayName("createAccountForPerson - HappyCase")
    public void createAccountForPersonHappyCase() {
        //ARRANGE
        CreateAccountForPersonRequestDTO requestDTO = CreateAccountForPersonAssembler.mapToRequestDTO("6002",
                "6001@switch.pt", "food", "food");
        CreateAccountForPersonResponseDTO expectedResponseDTO = CreateAccountForPersonAssembler.mapToResponseDTO
                ("6001@switch.pt", "joao", "6002", "food");

        AccountID accountID = new AccountID("6002");
        PersonID personID = new PersonID("6001@switch.pt");
        LedgerID ledgerID = new LedgerID("6001");
        Person savedPerson = new Person(personID, "joao", "Porto", "Porto", "1987-01-12", null, null, ledgerID);
        savedPerson.addAccount(accountID);
        Account savedAccount = new Account(accountID, "food", "food");

        Person mockedPerson = Mockito.mock(Person.class);

        Account mockedSavedAccount = Mockito.mock(Account.class);

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(accountRepository.findById(accountID)).thenReturn(Optional.empty());

        Mockito.when(accountRepository.save(savedAccount)).thenReturn(savedAccount);

        Mockito.when(mockedPerson.addAccount(accountID)).thenReturn(true);
        Mockito.when(mockedSavedAccount.getID()).thenReturn(accountID);

        Mockito.when(personRepository.save(mockedPerson)).thenReturn(savedPerson);
        //ACT
        CreateAccountForPersonResponseDTO actualResponseDTO = service.createAccountForPerson(requestDTO);

        //ASSERT
        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    /**
     * Method createAccountForPerson
     * Person doesn't exist in the Person Repository
     */
    @Test
    @DisplayName("createAccountForPerson - person doesn't exist in the Person Repository")
    public void createAccountForPersonRepositoryDoesntContainPersonTest() throws Exception {
        //ARRANGE
        CreateAccountForPersonRequestDTO requestDTO = CreateAccountForPersonAssembler.mapToRequestDTO("6002",
                "6002@switch.pt", "food", "food");

        PersonID personID = new PersonID("6002@switch.pt");

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.empty());

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.createAccountForPerson(requestDTO);
        });
    }

    /**
     * createAccountForPerson
     * Account already exists in the account repository
     *
     * @throws Exception JsonProcessingException
     */
    @Test
    @DisplayName("createAccountForPerson - Account already exists in the account repository")
    public void createAccountForPersonAlreadyExistsInTheAccountRepository() throws Exception {
        //ARRANGE
        CreateAccountForPersonRequestDTO requestDTO = CreateAccountForPersonAssembler.mapToRequestDTO("6003",
                "6001@switch.pt", "food", "food");

        AccountID accountID = new AccountID("6003");
        PersonID personID = new PersonID("6001@switch.pt");

        Person mockedPerson = Mockito.mock(Person.class);
        Account mockedAccount = Mockito.mock(Account.class);

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(accountRepository.findById(accountID)).thenReturn(Optional.of(mockedAccount));

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            service.createAccountForPerson(requestDTO);
        });
    }

    /**
     * createAccountForPerson
     * Account already exists in the account repository and in the list of AccountsIDs of the person
     */
    @Test
    @DisplayName("createAccountForPerson - Account already exists in the account repository and in the list of " +
            "AccountsIDs of the person")
    public void createAccountForPersonAccountAlreadyExistsInTheAccountRepositoryAndInThePersonTest() throws Exception {
        //ARRANGE
        CreateAccountForPersonRequestDTO requestDTO = CreateAccountForPersonAssembler.mapToRequestDTO("6005",
                "6005@switch.pt", "food", "food");

        AccountID accountID = new AccountID("6005");
        PersonID personID = new PersonID("6005@switch.pt");

        Person mockedPerson = Mockito.mock(Person.class);
        Account mockedAccount = Mockito.mock(Account.class);

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(accountRepository.findById(accountID)).thenReturn(Optional.of(mockedAccount));

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            service.createAccountForPerson(requestDTO);
        });
    }

    /**
     * getAccountByPersonID - Unit Test
     * Happy Case
     */
    @Test
    @DisplayName("getAccountsByPersonID - HappyCase")
    public void getAccountsByPersonIDHappyCaseTest() throws Exception {
        //Arrange
        String personIDStg = "6001@switch.pt";
        Person mockedPerson = Mockito.mock(Person.class);

        String denomination = "bla";
        String description = "ble";
        String accountIDStr = "1";
        AccountID accountID = new AccountID(accountIDStr);
        Account account = new Account(accountID, denomination, description);

        List<AccountDTO> expectedList = new ArrayList<>();
        expectedList.add(new AccountDTO(denomination, description, accountIDStr));
        AccountsDTO expectedDTO = new AccountsDTO(expectedList);

        AccountsIDs accountsIDs = new AccountsIDs();
        accountsIDs.addAccountID(accountID);

        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(accountRepository.findById(accountID)).thenReturn(Optional.of(account));

        Mockito.when(mockedPerson.getAccountsIDs()).thenReturn(accountsIDs);

        //Act
        AccountsDTO actual = service.getAccountsByPersonID(personIDStg);

        //Assert
        assertEquals(expectedDTO, actual);
    }

    /**
     * getAccountsByPersonID - Unit Test
     * Ensure throws exception when the person doesn't exist in PersonRepository
     */
    @Test
    @DisplayName("getAccountsByPersonID -Ensure throws exception when the person doesn't exist in PersonRepository")
    public void getAccountsByPersonIDEnsureThrowsExceptionWhenPersonDoesNotExistInRepoTest() {
        //Arrange
        String personIDStg = "6001@switch.pt";
        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.getAccountsByPersonID(personIDStg);
        });
    }
}


