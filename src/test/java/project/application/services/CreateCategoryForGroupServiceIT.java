package project.application.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.CategoriesDTO;
import project.dto.CreateCategoryForGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;
import project.exceptions.CategoryAlreadyExistsException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.InvalidFieldException;
import project.exceptions.PersonIsNotManagerOfTheGroupException;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
class CreateCategoryForGroupServiceIT {

    private CreateCategoryForGroupService service;

    @Autowired
    GroupRepository groupRepository;

    private PersonID personID;
    private LedgerID ledgerID;
    private GroupID groupID;
    private Group futebol;

    @BeforeEach
    public void init() {
        service = new CreateCategoryForGroupService(groupRepository);
        personID = new PersonID("10@switch.pt");
        ledgerID = new LedgerID("20");
        groupID = new GroupID("100");
        futebol = new Group(groupID, "Futebol", "2020-01-20", personID, ledgerID);
        futebol.addCategory("Equipamentos");
        groupRepository.save(futebol);
    }

    /**
     * Test for Constructor CreateCategoryForGroup Service
     * Happy case
     */
    @Test
    @DisplayName("Test for Constructor CreateCategoryForGroup - Happy Case")
    void createCategoryGroupControllerConstructorTest() {
        assertTrue(service instanceof CreateCategoryForGroupService);
    }

    /**
     * Test for CreateCategoryForGroup - HappyCase
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryForGroup - Happy Case")
    void createCategoryForGroupHappyCaseTest() {

        //Arrange
        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO("10@switch.pt", "100", "Bolas");

        CreateCategoryForGroupResponseDTO expected = new CreateCategoryForGroupResponseDTO("100", "Futebol", "Bolas");

        //Act
        CreateCategoryForGroupResponseDTO result = service.createCategoryForGroup(requestDTO);

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for CreateCategoryForGroup Service
     * Ensure throws a CategoryAlreadyExistsException when added a Category that already exists
     */
    @Test
    @DisplayName("Ensure throws a CategoryAlreadyExistsException when added a Category that already exists")
    void createCategoryForGroupEnsureExceptionWhenCategoryAlreadyExistsTest() {

        //Arrange
        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO("10@switch.pt", "100", "Equipamentos");

        //ACT-ASSERT
        assertThrows(CategoryAlreadyExistsException.class, () -> {
            service.createCategoryForGroup(requestDTO);
        });
    }

    /**
     * Test for CreateCategoryForGroup Service
     * Ensure PersonIsNotManagerOfTheGroupException is thrown when the Person Is Manager Of The Group
     */
    @Test
    @DisplayName("Ensure PersonIsNotManagerOfTheGroupException is thrown when the Person Is Manager Of The Group ")
    void createCategoryForGroupEnsurePersonIsManagerOfTheGroupTest() {

        //Arrange
        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO("50001@switch.pt", "100", "Luvas");

        //ACT-ASSERT
        assertThrows(PersonIsNotManagerOfTheGroupException.class, () -> {
            service.createCategoryForGroup(requestDTO);
        });

    }

    /**
     * Test for CreateCategoryForGroup Service
     * Ensure GroupNotFoundException is thrown when addCategory to a groupID that is not in groupRepository
     */
    @Test
    @DisplayName("Ensure GroupNotFoundException is thrown when addCategory to a groupID that is not in groupRepository")
    void createCategoryForGroupEnsureExceptionIsThrownWhenGroupIDDontExistInTheRepositoryTest() {

        //Arrange
        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO("10@switch.pt", "2000", "Luvas");

        //ACT-ASSERT
        assertThrows(GroupNotFoundException.class, () -> {
            service.createCategoryForGroup(requestDTO);
        });
    }

    /**
     * Test for CreateCategoryForGroup Service
     * Ensure InvalidFieldException when the designation input is invalid
     */
    @Test
    @DisplayName("Ensure InvalidFieldException when the designation input is invalid ")
    void createCategoryForGroupEnsureExceptionDesignationInvalidInputTest() {

        //Arrange
        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO("10@switch.pt", "100", "");

        //ACT-ASSERT
        assertThrows(InvalidFieldException.class, () -> {
            service.createCategoryForGroup(requestDTO);
        });
    }

    /**
     * Test for getCategoriesByGroupID Unit Test
     * Happy Case
     */
    @Test
    @DisplayName("Test for getCategoriesByGroupID - Happy Case")
    void getCategoriesByGroupIDHappyCaseTest() {
        //Arrange
        Set<String> expectedCategoriesString = new HashSet<>();
        expectedCategoriesString.add("Equipamentos");
        CategoriesDTO expected = new CategoriesDTO(expectedCategoriesString);

        //Act
        CategoriesDTO result = service.getCategoriesByGroupID("100");

        //Assert
        assertEquals(expected, result);
    }
}