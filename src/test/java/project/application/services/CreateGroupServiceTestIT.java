package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.CreateGroupRequestDTO;
import project.dto.CreateGroupResponseDTO;
import project.dto.GroupDTO;
import project.exceptions.GroupAlreadyExistsException;
import project.exceptions.GroupLedgerAlreadyExistsException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS002_1Service;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
class CreateGroupServiceTestIT {

    @Autowired
    PersonRepository personRepo;
    @Autowired
    GroupRepository groupRepo;
    @Autowired
    LedgerRepository ledgerRepo;
    @Autowired
    IUS002_1Service service;

    /**
     * Test for constructor
     */
    @Test
    @DisplayName("Test for CreateGroupServiceTest Constructor")
    void CreateGroupServiceConstructorTest() {
        // Assert
        assertTrue(service instanceof CreateGroupService);
    }

    /**
     * Test for createGroup method
     * <p>
     * Happy Case
     */
    @Test
    @DisplayName("Test for createGroup method - Happy Case")
    void createGroupHappyCaseTest() {
        //ARRANGE
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO("21012", "Switchadas", "1987-04-17",
                "21001@switch.pt", "21032");

        CreateGroupResponseDTO expected = new CreateGroupResponseDTO("21012", "Switchadas");

        //ACT
        CreateGroupResponseDTO result = service.createGroup(requestDTO);

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for createGroup method
     * <p>
     * Ensure exception is throw if group's creator doesn't exist in person repository
     */
    @Test
    @DisplayName("Ensure exception is throw if group's creator doesn't exist in person repository")
    void createGroupEnsureExceptionIfCreatorDoesNotExistTest() {
        //ARRANGE
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO("21013", "Switchadas", "1987-04-17",
                "21003@switch.pt", "21032");

        CreateGroupService service = new CreateGroupService(personRepo, groupRepo, ledgerRepo);

        //ACT
        //ASSERT
        assertThrows(PersonNotFoundException.class, () -> {
            service.createGroup(requestDTO);
        });
    }

    /**
     * Test for createGroup method
     * <p>
     * Ensure exception is throw if group with all the same attributes (including ID) already exists
     * in group repository.
     */
    @Test
    @DisplayName("Ensure exception is throw if group with all the same attributes (including ID) already exists in group repository")
    void createGroupEnsureExceptionOptionalIfGroupExistsTest() {
        //ARRANGE
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO("21011", "Switchadas", "1987-04-17",
                "21001@switch.pt", "21031");

        //ACT
        //ASSERT
        assertThrows(GroupAlreadyExistsException.class, () -> {
            service.createGroup(requestDTO);
        });
    }

    /**
     * Test for createGroup method
     * <p>
     * Ensure exception is throw if group with different attributes but same ID already exists
     */
    @Test
    @DisplayName("Ensure exception is throw if group with different attributes but same ID already exists")
    void createGroupEnsureExceptionIfGroupWithSameIDAlreadyExistsTest() {
        //ARRANGE
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO("21011", "other", "1987-04-18",
                "21001@switch.pt", "21033");

        //ACT
        //ASSERT
        assertThrows(GroupAlreadyExistsException.class, () -> {
            service.createGroup(requestDTO);
        });
    }

    /**
     * Test for createGroup method
     * <p>
     * Ensure exception is throw if ledger already exists in the ledger repository
     */
    @Test
    @DisplayName("Ensure exception is throw if ledger already exists in the ledger repository")
    void createGroupEnsureExceptionIfLedgerAlreadyExistsTest() {
        //ARRANGE
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO("21013", "Switchadas", "1987-04-17",
                "21001@switch.pt", "21031");

        //ACT
        //ASSERT
        assertThrows(GroupLedgerAlreadyExistsException.class, () -> {
            service.createGroup(requestDTO);
        });
    }

    /**
     * Test for getGroupByID
     * Happy Case
     */
    @Test
    @DisplayName("Test for getGroupByID - Happy Case -Service")
    void getGroupByIDHappyCaseTest() {
        //ARRANGE
        Set<String> members;
        members = new HashSet<>();
        members.add("21001@switch.pt");

        Set<String> managers;
        managers = new HashSet<>();
        managers.add("21001@switch.pt");

        GroupDTO expected = new GroupDTO(members, managers, "21011", "21031", "Switchadas", "1987-04-17");

        //ACT
        GroupDTO result = service.getGroupByID("21011");

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getGroupByID
     * EnsureException if not there.
     */
    @Test
    @DisplayName("Test for getGroupByID - EnsureException")
    void getGroupByIDEnsureExceptionTest() {
        //Arrange
        //ACT
        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.getGroupByID("21013");
        });
    }
}