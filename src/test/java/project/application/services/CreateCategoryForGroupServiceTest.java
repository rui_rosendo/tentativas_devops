package project.application.services;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CategoriesDTO;
import project.dto.CreateCategoryForGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;
import project.exceptions.CategoryAlreadyExistsException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonIsNotManagerOfTheGroupException;
import project.frameworkddd.IUS005_1Service;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.Description;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
public class CreateCategoryForGroupServiceTest {

    @Autowired
    IUS005_1Service service;
    @Autowired
    GroupRepository groupRepo;

    /**
     * Test for createCategoryForGroup Unit Test
     * Happy case
     */
    @Test
    @DisplayName("Test for create category for group - Happy Case")
    public void createCategoryForGroupHappyCaseTest() {

        //ARRANGE
        String categoryDesignation = "Bolas";
        String groupIDStg = "510100";
        String groupDescription = "Futebol";

        GroupID gID = new GroupID("510100");
        PersonID creatorID = new PersonID("51010@switch.pt");

        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO("51010@switch.pt", "510100", "Bolas");
        CreateCategoryForGroupResponseDTO expected = new CreateCategoryForGroupResponseDTO("510100", "Futebol", "Bolas");

        Group mockedGroup = Mockito.mock(Group.class);
        Description mockedDescription = Mockito.mock(Description.class);
        GroupID mockedGroupID = Mockito.mock(GroupID.class);

        Mockito.when(groupRepo.findById(gID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.hasManagerID(creatorID)).thenReturn(true);
        Mockito.when(mockedGroup.addCategory(categoryDesignation)).thenReturn(true);

        Group mockedGroup1 = Mockito.mock(Group.class);

        Mockito.when(groupRepo.save(mockedGroup)).thenReturn(mockedGroup1);
        Mockito.when(mockedGroup1.getID()).thenReturn(mockedGroupID);
        Mockito.when(mockedGroupID.toStringDTO()).thenReturn(groupIDStg);

        Mockito.when(mockedGroup1.getDescription()).thenReturn(mockedDescription);
        Mockito.when(mockedDescription.getDescriptionValue()).thenReturn(groupDescription);

        //ACT
        CreateCategoryForGroupResponseDTO actual = service.createCategoryForGroup(requestDTO);

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for createCategoryForGroup Unit Test
     * Ensure exception is thrown when added a Category that already exists
     */
    @Test
    @DisplayName("Ensure exception is thrown when added a Category that already exists")
    public void createCategoryForGroupEnsureExceptionWhenCategoryAlreadyExistsTest() {

        //ARRANGE
        String categoryDesignation = "Equipamentos";

        GroupID gID = new GroupID("510100");
        PersonID creatorID = new PersonID("51010@switch.pt");

        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO("51010@switch.pt", "510100", "Equipamentos");

        Group mockedGroup = Mockito.mock(Group.class);

        Mockito.when(groupRepo.findById(gID)).thenReturn(Optional.of(mockedGroup));
        Mockito.when(mockedGroup.hasManagerID(creatorID)).thenReturn(true);
        Mockito.when(mockedGroup.addCategory(categoryDesignation)).thenReturn(false);

        //ACT
        //ASSERT
        Assertions.assertThrows(CategoryAlreadyExistsException.class, () -> {
            service.createCategoryForGroup(requestDTO);
        });
    }

    /**
     * Test for createCategoryForGroup Unit Test
     * Ensure exception is thrown when group doesn't exist
     */
    @Test
    @DisplayName("Ensure exception is thrown when group doesn't exist")
    public void createCategoryForGroupGroupNotPresentTest() {

        //ARRANGE

        GroupID gID = new GroupID("510100");
        PersonID creatorID = new PersonID("51010@switch.pt");
        LedgerID ledgerID = new LedgerID("51020");

        Group groupUS5 = new Group(gID, "Futebol", "2020-01-20", creatorID, ledgerID);
        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO("51010@switch.pt", "510200", "Bolas");

        Mockito.when(groupRepo.findById(gID)).thenReturn(Optional.of(groupUS5));

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.createCategoryForGroup(requestDTO);
        });
    }

    /**
     * Test for createCategoryForGroup Unit Test
     * Ensure exception is thrown when Manager doesn't exist
     */
    @Test
    @DisplayName("Ensure exception is thrown when Manager doesn't exist ")
    public void createCategoryForGroupEnsurePersonIsManagerOfTheGroupTest() {

        //ARRANGE

        PersonID creatorID = new PersonID("51010@switch.pt");
        GroupID gID = new GroupID("510100");

        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO("51020@switch.pt", "510100", "Bolas");

        Group mockedGroup = Mockito.mock(Group.class);


        Mockito.when(groupRepo.findById(gID)).thenReturn(Optional.of(mockedGroup));

        Mockito.when(mockedGroup.hasManagerID(creatorID)).thenReturn(false);

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonIsNotManagerOfTheGroupException.class, () -> {
            service.createCategoryForGroup(requestDTO);
        });
    }

//    /**
//     * Test for createCategoryForGroup Unit Test
//     * Ensure InvalidFieldException when the designation input is invalid
//     */
//    @Test
//    @DisplayName("Ensure InvalidFieldException when the designation input is invalid ")
//    public void createCategoryForGroupEnsureExceptionDesignationInvalidInputTest() {
//
//        //ARRANGE
//        //String categoryDesignation = "bolas";
//        //String groupIDStg = "100";
//        //String groupDescription = "Futebol";
//
//        GroupID gID = new GroupID("510100");
//        PersonID creatorID = new PersonID("51010");
//        LedgerID ledgerID = new LedgerID("51020");
//
//        Group groupUS5 = new Group(gID, "Futebol", "2020-01-20", creatorID, ledgerID);
//
//
//        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO("51010", "510100", "");
//
//        Group mockedGroup = Mockito.mock(Group.class);
//        //Description mockedDescription = Mockito.mock(Description.class);
//        //GroupID mockedGroupID = Mockito.mock(GroupID.class);
//
//        Mockito.when(groupRepo.findByGroupId(gID)).thenReturn(Optional.of(mockedGroup));
//        //Mockito.when(groupRepo.findByID(gID)).thenReturn();
//        Mockito.when(mockedGroup.hasManagerID(creatorID)).thenReturn(true);
//        Mockito.when(mockedGroup.addCategory("")).thenReturn(false);
//
//        //Mockito.when(mockedGroup.getID()).thenReturn(mockedGroupID);
//        //Mockito.when(mockedGroupID.toStringDTO()).thenReturn(groupIDStg);
//
//        //Mockito.when(mockedGroup.getDescription()).thenReturn(mockedDescription);
//        //Mockito.when(mockedDescription.getDescriptionValue()).thenReturn(groupDescription);
//
//        //ACT
//        //ASSERT
//        Assertions.assertThrows(InvalidFieldException.class, () -> {
//            service.createCategoryForGroup(requestDTO);
//        });
//    }


    /**
     * Test for getCategoriesByGroupID Unit Test
     * Happy case
     */
    @Test
    @DisplayName("Test for getCategoriesByGroupID - Happy Case")
    public void getCategoriesByGroupIDHappyCaseTest() {

        //ARRANGE
        GroupID gID = new GroupID("510100");
        PersonID creatorID = new PersonID("51010@switch.pt");
        LedgerID ledgerID = new LedgerID("51020");

        Set<String> expectedCategoriesSring = new HashSet<>();
        expectedCategoriesSring.add("Equipamentos");

        Group groupUS5 = new Group(gID, "Futebol", "2020-01-20", creatorID, ledgerID);
        groupUS5.addCategory("Equipamentos");

        CategoriesDTO expectedCategoriesDTO = new CategoriesDTO((expectedCategoriesSring));
        Mockito.when(groupRepo.findById(gID)).thenReturn(Optional.of(groupUS5));

        //ACT
        CategoriesDTO result = service.getCategoriesByGroupID("510100");

        //ASSERT
        assertEquals(expectedCategoriesDTO, result);

    }

}

