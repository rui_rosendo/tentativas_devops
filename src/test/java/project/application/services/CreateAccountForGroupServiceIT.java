package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.AccountDTO;
import project.dto.AccountsDTO;
import project.dto.CreateAccountForGroupRequestDTO;
import project.dto.CreateAccountForGroupResponseDTO;
import project.exceptions.AccountAlreadyExistsException;
import project.exceptions.AccountNotFoundException;
import project.exceptions.GroupConflictException;
import project.exceptions.GroupNotFoundException;
import project.frameworkddd.IUS007Service;
import project.model.entities.account.Account;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
class CreateAccountForGroupServiceIT {

    @Autowired
    GroupRepository groupRepo;

    @Autowired
    AccountRepository accountRepo;

    @Autowired
    IUS007Service service;

    private Group group;
    private AccountID newAccountID;
    private AccountID newAccountID2;
    private Account account;
    private Account account2;

    @BeforeEach
    public void init() {
        GroupID groupID = new GroupID("7771");
        LedgerID ledgerID = new LedgerID("7771");
        PersonID personID = new PersonID("7771@switch.pt");
        group = new Group(groupID, "family", "2019-12-20", personID, ledgerID);
        newAccountID = new AccountID("7771");
        account = new Account(newAccountID, "food", "food");
        newAccountID2 = new AccountID("7772");
        account2 = new Account(newAccountID2, "Gym", "Health");
    }


    /**
     * Test for CreateAccountForGroupService Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor- Happy case")
    void CreateAccountForGroupServiceConstructorTest() {
        //ASSERT
        assertTrue(service instanceof CreateAccountForGroupService);
    }

    /**
     * Test for createAccountForGroup
     * Happy Case
     * <p>
     * GroupRepository contains groupID
     * AccountRepository doesn't contain accountID
     * Group list of accounts doesn't contain accountID
     */
    @Test
    @DisplayName("Test for createAccountForGroup - Happy case")
    void createAccountForGroupHappyCaseTest() {

        //ARRANGE
        groupRepo.save(group);

        CreateAccountForGroupRequestDTO requestDTO = new CreateAccountForGroupRequestDTO("7771", "7771", "food", "food", "7771@switch.pt");

        String expectedGroupID = "7771";
        String expectedGroupDescription = "family";
        String expectedAccountID = "7771";
        String expectedAccountDenomination = "food";
        CreateAccountForGroupResponseDTO expected = new CreateAccountForGroupResponseDTO(expectedGroupID, expectedGroupDescription, expectedAccountID, expectedAccountDenomination);

        //ACT
        CreateAccountForGroupResponseDTO result = service.createAccountForGroup(requestDTO);

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for createAccountForGroup
     * Sad Case
     * GroupRepository doesn't contain a Group with groupID
     */
    @Test
    @DisplayName("Test for createAccountForGroup - GroupRepository doesn't contain groupID")
    void createAccountForGroupGroupDoesNotExistInRepoTest() {
        //ARRANGE
        CreateAccountForGroupRequestDTO requestDTO = new CreateAccountForGroupRequestDTO("7771", "7772", "food", "food", "7771@switch.pt");

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.createAccountForGroup(requestDTO);
        });
    }

    /**
     * Test for createAccountForGroup
     * Sad case
     * GroupRepository contains groupID
     * AccountRepository contains accountID
     * Group list of accounts doesn't contain accountID
     * Group's list of managers contains personID
     */
    @Test
    @DisplayName("Test for createAccountForGroup - Account already exist in the accountRepository")
    void createAccountForGroupAccountAlreadyExistsInRepoTest() {
        //ARRANGE
        groupRepo.save(group);
        accountRepo.save(account);

        CreateAccountForGroupRequestDTO requestDTO = new CreateAccountForGroupRequestDTO("7771", "7771", "food", "pay food", "7771@switch.pt");

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            service.createAccountForGroup(requestDTO);
        });

    }

    /**
     * Test for createAccountForGroup
     * GroupRepository contains groupID
     * AccountRepository doesn't contain accountID
     * Group's list of accounts contains accountID
     * Group's list of managers doesn't contain personID
     */
    @Test
    @DisplayName("Test for createAccountForGroup - personID is not in group's list of managers")
    void createAccountForGroupAccountPersonIsNotManagerOfTheGroupTest() {
        //ARRANGE
        groupRepo.save(group);

        CreateAccountForGroupRequestDTO requestDTO = new CreateAccountForGroupRequestDTO("7771", "7771", "food", "pay food", "7772@switch.pt");

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupConflictException.class, () -> {
            service.createAccountForGroup(requestDTO);
        });
    }

    /**
     * Test for getAccountByID
     * Happy Case
     */
    @Test
    @DisplayName("Test for getAccountByID - Happy case")
    void getAccountByIDHappyCaseTest() {

        //ARRANGE
        group.addAccount(newAccountID);
        groupRepo.save(group);
        accountRepo.save(account);


        String expectedAccountDenomination = "food";
        String expectedAccountDescription = "food";
        String expectedAccountID = "7771";
        AccountDTO expected = new AccountDTO(expectedAccountDenomination, expectedAccountDescription, expectedAccountID);

        //ACT
        AccountDTO result = service.getAccountByID("7771");

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getAccountByID
     * Sad Case
     * AccountRepository doesn't contain an Account with accountID given by parameter
     * AccountNotFoundException is expected to be thrown
     */
    @Test
    @DisplayName("Test for createAccountForGroup - Ensure throws exception when account doesn't exist in account repo")
    void getAccountByIDEnsureExceptionWhenGroupDoesNotExistTest() {
        //ARRANGE
        String accountID = "7773";

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountNotFoundException.class, () -> {
            service.getAccountByID(accountID);
        });
    }

    /**
     * Test for getAccountsByGroupID
     * Happy Case
     */
    @Test
    @DisplayName("Test for getAccountsByGroupID - Happy case")
    void getAccountsByGroupIDHappyCaseTest() {

        //ARRANGE
        group.addAccount(newAccountID);
        group.addAccount(newAccountID2);
        groupRepo.save(group);
        accountRepo.save(account);
        accountRepo.save(account2);

        List<AccountDTO> expectedList = new ArrayList<>();
        expectedList.add(new AccountDTO("food", "food", "7771"));
        expectedList.add(new AccountDTO("Gym", "Health", "7772"));

        AccountsDTO expectedDTO = new AccountsDTO(expectedList);

        //ACT
        AccountsDTO result = service.getAccountsByGroupID("7771");

        //ASSERT
        assertEquals(expectedDTO, result);
    }

    /**
     * Test for getAccountsByGroupID
     * Sad Case
     * Group doesn't exist in GroupRepository
     * GroupNotFoundException is expected to be thrown
     */
    @Test
    @DisplayName("Test for getAccountsByGroupID - Happy case")
    void getAccountsByGroupIDEnsureExceptionWhenGroupDoesNotExistTest() {

        //ARRANGE
        String groupID = "7773";

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.getAccountsByGroupID(groupID);
        });
    }
}