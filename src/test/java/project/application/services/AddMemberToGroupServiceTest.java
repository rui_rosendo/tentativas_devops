package project.application.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.AddMemberRequestDTO;
import project.dto.AddMemberResponseDTO;
import project.dto.MembersDTO;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonNotFoundException;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
public class AddMemberToGroupServiceTest {

    @Autowired
    private AddMemberToGroupService service;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private GroupRepository groupRepository;

    /**
     * Test for addMember Unit Test
     * Happy case
     */
    @Test
    @DisplayName("Test for addMember - HappyCase")
    void addMemberToGroupHappyCaseTest() {
        //Arrange
        AddMemberRequestDTO newAddMemberRequestDTO = new AddMemberRequestDTO("121@switch.pt", "4444");
        AddMemberResponseDTO expected = new AddMemberResponseDTO("4444", "Familia Alves", "121@switch.pt");

        GroupID groupID = new GroupID(newAddMemberRequestDTO.getGroupID());
        PersonID personID = new PersonID(newAddMemberRequestDTO.getNewMemberEmail());

        LedgerID newGroupLedgerID = new LedgerID("444444");
        Group newGroup = new Group(groupID, expected.getGroupDescription(), "2020-01-01", new PersonID("333@switch.pt"), newGroupLedgerID);

        Group mockedGroup = mock(Group.class);
        Person mockedPerson = mock(Person.class);

        Mockito.when(mockedGroup.getID()).thenReturn(newGroup.getID());
        Mockito.when(mockedGroup.getDescription()).thenReturn(newGroup.getDescription());


        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(mockedGroup));


        Mockito.when(mockedGroup.addMemberID(personID)).thenReturn(true);
        Mockito.when(groupRepository.save(mockedGroup)).thenReturn(mockedGroup);

        //Act
        AddMemberResponseDTO result = service.addMember(newAddMemberRequestDTO);

        //Assert
        assertEquals(expected, result);
    }


    /**
     * Test for getPersonByID
     * <p>
     * Happy case
     */
    @Test
    void getPersonByIDHappyCaseTest() {

        PersonID newPersonID = new PersonID("222@switch.pt");
        Person mockedPerson = mock(Person.class);

        Mockito.when(personRepository.findById(newPersonID)).thenReturn(Optional.of(mockedPerson));

        Person result = service.getPersonByID(newPersonID);

        assertEquals(mockedPerson, result);

    }

    /**
     * Test for getPersonByID
     * <p>
     * Sad case - Ensure 'PersonNotFoundException' is thrown
     */
    @Test
    void getPersonIDSadCaseTest() {

        PersonID newPersonID = new PersonID("45464@switch.pt");

        Mockito.when(personRepository.findById(newPersonID)).thenReturn(Optional.empty());

        //ASSERT
        assertThrows(PersonNotFoundException.class, () -> {
            service.getPersonByID(newPersonID);
        });

    }

    /**
     * Test for getGroupByID
     * <p>
     * Happy case
     */
    @Test
    void getGroupByIDHappyCaseTest() {

        GroupID mockedGroupID = new GroupID("4444");
        Group mockedGroup = mock(Group.class);

        Mockito.when(groupRepository.findById(mockedGroupID)).thenReturn(Optional.of(mockedGroup));
        Group result = service.getGroupByID(mockedGroupID);

        assertEquals(mockedGroup, result);

    }

    /**
     * Test for getGroupByID
     * <p>
     * Sad case - Ensure ' GroupNotFoundException' is thrown
     */
    @Test
    void getGroupByIdSadCaseTest() {

        GroupID mockedGroupID = new GroupID("4444");

        Mockito.when(groupRepository.findById(mockedGroupID)).thenReturn(Optional.empty());

        assertThrows(GroupNotFoundException.class, () -> {
            service.getGroupByID(mockedGroupID);
        });

    }


    /**
     * Test for getMembersByGroupIDJpa Unit Test
     * Happy Case
     */
    @Test
    @DisplayName("Test for getMembersByGroupIDJpa - Happy Case")
    void getMembersByGroupIDHappyCaseTest() {
        //Arrange
        List<String> expectedMembersString = new ArrayList<>();
        List<String> expectedMembersNames = new ArrayList<>();
        List<Boolean> expectedManagers = new ArrayList<>();

        expectedMembersString.add("111@switch.pt");
        expectedMembersNames.add("Isabel");
        expectedManagers.add(true);

        String address = "Rua Dr. Roberto Frias s/n";
        GroupID groupID = new GroupID("4444");
        PersonID isabelID = new PersonID ("111@switch.pt");
        LedgerID isabelLedgerID = new LedgerID("11111");
        Person person = new Person(isabelID, "Isabel", address, "Porto", "1991-12-22", null, null, isabelLedgerID);

        Group group = new Group(groupID, "Familia Alves", "2020-03-03", isabelID, new LedgerID("444"));

        MembersDTO expectedMembersDTO = new MembersDTO(expectedMembersString, expectedMembersNames, expectedManagers);

        Mockito.when(groupRepository.findById(groupID)).thenReturn(Optional.of(group));
        Mockito.when(personRepository.findById(isabelID)).thenReturn(Optional.of(person));

        //Act
        MembersDTO result = service.getMembersByGroupID("4444");

        //Assert
        assertEquals(expectedMembersDTO, result);
    }

}
