//package project.application.services;
//
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import project.dto.CategoriesDTO;
//import project.dto.CreateCategoryForPersonRequestDTO;
//import project.dto.CreateCategoryForPersonResponseDTO;
//import project.exceptions.CategoryAlreadyExistsException;
//import project.exceptions.PersonNotFoundException;
//import project.model.entities.person.Person;
//import project.model.entities.shared.Description;
//import project.model.entities.shared.LedgerID;
//import project.model.entities.shared.PersonID;
//import project.model.specifications.repositories.PersonRepository;
//
//import java.util.HashSet;
//import java.util.Optional;
//import java.util.Set;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//@SpringBootTest
//@ActiveProfiles("serviceTest")
//@ExtendWith(SpringExtension.class)
//public class CreateCategoryForPersonServiceTest {
//
//    @Autowired
//    PersonRepository personRepository;
//    @Autowired
//    CreateCategoryForPersonService service;
//
//
//    //private PersonRepositoryDB personRepositoryDB;
//    private PersonID personID;
//    private PersonID motherPersonID;
//    private PersonID fatherPersonID;
//    private LedgerID ledgerID;
//    //private PersonID personID;
//    private Person manuel;
//
//    @BeforeEach
//    public void init() {
//        String prefix = "5000";
//        //personRepo = new PersonRepositoryDB();
//        //service = new CreateCategoryForPersonService(personRepository);
//        personID = new PersonID(prefix + "1");
//        motherPersonID = new PersonID(prefix + "2");
//        fatherPersonID = new PersonID(prefix + "3");
//        ledgerID = new LedgerID(prefix + "1");
//        manuel = new Person(personID, "Manuel", "Rua Santa Catarina 88","Vila Real","2020-01-20", motherPersonID, fatherPersonID, ledgerID);
//        manuel.addCategory("food");
//        personRepository.save(manuel);
//    }
//
//    /**
//     * Test for createCategoryForPerson Unit Test
//     * Happy case
//     */
//    @Test
//    @DisplayName("Test for create category for person - Happy Case")
//    public void createCategoryForPersonHappyCaseTest() {
//
//
//        //ARRANGE
//        String designation = "videogames";
//        String personIDStr = "50001";
//
//        CreateCategoryForPersonRequestDTO requestDTO = new CreateCategoryForPersonRequestDTO(personIDStr,  designation);
//        CreateCategoryForPersonResponseDTO expected = new CreateCategoryForPersonResponseDTO(personIDStr,  designation);
//        Person person = new Person (personID, "Manuel", "Rua Santa Catarina 88","Vila Real","2020-01-20", motherPersonID, fatherPersonID, ledgerID);
//
//        Person mockedPerson = Mockito.mock(Person.class);
//        //Description mockedDescription = Mockito.mock(Description.class);
//        //String mockedDescriptionStr = Mockito.mock(String.class);
//        //PersonID mockedPersonID = Mockito.mock(PersonID.class);
//        CreateCategoryForPersonRequestDTO mockedRequestDTO = Mockito.mock(CreateCategoryForPersonRequestDTO.class);
//
//
//        Mockito.when(personRepository.findByPersonId(personID)).thenReturn(Optional.of(mockedPerson));
//        Mockito.when(mockedRequestDTO.getDesignation()).thenReturn(designation);
//        Mockito.when(mockedPerson.addCategory(designation)).thenReturn(true);
//        Mockito.when(personRepository.save(mockedPerson)).thenReturn(person);
//        //Mockito.when(mockedPerson.getID()).thenReturn(mockedPersonID);
//        //Mockito.when(mockedPersonID.toStringDTO()).thenReturn(personIDStr);
//
//        //ACT
//        CreateCategoryForPersonResponseDTO actual = service.createCategoryForPerson(requestDTO);
//
//        //ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for createCategoryForPerson Unit Test
//     * Ensure exception is thrown when added a Category that already exists
//     */
//    @Test
//    @DisplayName("Ensure exception is thrown when added a Category that already exists")
//    public void createCategoryForPersonEnsureExceptionWhenCategoryAlreadyExistsTest() {
//
//
//        //ARRANGE
//        String designation = "food";
//
//        CreateCategoryForPersonRequestDTO requestDTO = new CreateCategoryForPersonRequestDTO("50001",  "food");
//        CreateCategoryForPersonResponseDTO expected = new CreateCategoryForPersonResponseDTO("50001",  "food");
//
//        Person mockedPerson = Mockito.mock(Person.class);
//        //Description mockedDescription = Mockito.mock(Description.class);
//        //String mockedDescriptionStr = Mockito.mock(String.class);
//        PersonID mockedPersonID = Mockito.mock(PersonID.class);
//        CreateCategoryForPersonRequestDTO mockedRequestDTO = Mockito.mock(CreateCategoryForPersonRequestDTO.class);
//
//
//        //Mockito.when(personRepo.containsByID(personID)).thenReturn(true);
//        Mockito.when(personRepository.findByPersonId(personID)).thenReturn(Optional.of(mockedPerson));
//
//        //Mockito.when(personRepo.findByPersonId(personID)).thenReturn(Optional.of(mockedPerson));
//
//        Mockito.when(mockedRequestDTO.getDesignation()).thenReturn(designation);
//
//        Mockito.when(mockedPerson.addCategory(designation)).thenReturn(false);
//
//        //ACT
//        //ASSERT
//        Assertions.assertThrows(CategoryAlreadyExistsException.class, () -> {
//            service.createCategoryForPerson(requestDTO);
//        });
//    }
//
//    /**
//     * Test for createCategoryForPerson Unit Test
//     * Ensure exception is thrown when person doesn't exist
//     */
//    @Test
//    @DisplayName("Ensure exception is thrown when person doesn't exist")
//    public void createCategoryForPersonPersonNotPresentTest() {
//
//        //ARRANGE
//        String designation = "food";
//
//        CreateCategoryForPersonRequestDTO requestDTO = new CreateCategoryForPersonRequestDTO("50001",  "food");
//        CreateCategoryForPersonResponseDTO expected = new CreateCategoryForPersonResponseDTO("50001",  "food");
//
//        Person mockedPerson = Mockito.mock(Person.class);
//        Description mockedDescription = Mockito.mock(Description.class);
//        //String mockedDescriptionStr = Mockito.mock(String.class);
//        PersonID mockedPersonID = Mockito.mock(PersonID.class);
//
//        Mockito.when(personRepository.findByPersonId(personID)).thenReturn(Optional.empty());
//        //Mockito.when(optPerson.isPresent()).thenReturn(false);
//
//
//        //ACT
//        //ASSERT
//        Assertions.assertThrows(PersonNotFoundException.class, () -> {
//            service.createCategoryForPerson(requestDTO);
//        });
//    }
//
//
//
//
//    /**
//     * Test for getCategoriesByPersonID Unit Test
//     * Happy case
//     */
//    @Test
//    @DisplayName("Test for getCategoriesByPersonID - Happy Case")
//    public void getCategoriesByPersonIDHappyCaseTest() {
//
//        //ARRANGE
//        Set<String> expectedCategoriesString = new HashSet<>();
//        expectedCategoriesString.add("food");
//
//        CategoriesDTO expectedCategoriesDTO = new CategoriesDTO(expectedCategoriesString);
//        Mockito.when(personRepository.findByPersonId(personID)).thenReturn(Optional.of(manuel));
//
//        //ACT
//        CategoriesDTO result = service.getCategoriesByPersonID("50001");
//
//        //ASSERT
//        assertEquals(expectedCategoriesDTO, result);
//
//    }
//
//}
//
