package project.application.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import project.ProjectApplication;
import project.dto.TransactionDTO;
import project.dto.TransactionsResponseDTO;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.Period;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = ProjectApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GetTransactionsOfAccountWithinPeriodServiceIT {
    @Autowired
    private GetTransactionsOfAccountWithinPeriodService service;

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor - Happy case")
    void constructorHappyCaseTest() {
        // Arrange
        // Act
        // Assert
        assertTrue(service instanceof GetTransactionsOfAccountWithinPeriodService);
    }

    //todo: fazer Get By PersonID -

    /**
     * Get By PersonID - Ensure it gets all transactions for person
     */
    @Test
    @DisplayName("Test for GetTransactionsOfAccountWithinPeriod - Ensure it gets all transactions")
    void getByPersonIDAllTransactionsHappyCaseTest() {
        // Arrange
        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1001", "1003");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1001", "1003");
        TransactionDTO tdto3 = new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1001", "1003");

        List<TransactionDTO> expectedString = new ArrayList<>();
        expectedString.add(tdto1);
        expectedString.add(tdto2);
        expectedString.add(tdto3);

        TransactionsResponseDTO expected = new TransactionsResponseDTO(expectedString);

        // Act
        TransactionsResponseDTO actual = service.getTransactionsOfAccountWithinPeriodByPersonID("1001@switch.pt", "1001", "1999-01-01 23:20:58", "2021-01-01 23:20:58");

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Get By PersonID - Ensure it just gets the transactions above start date for person
     */
    @Test
    @DisplayName("Test for GetTransactionsOfAccountWithinPeriod - Ensure it gets just the transactions above start date")
    void getByPersonIDEnsureStartDateLimitWorksHappyCaseTest() {
        // Arrange
        //TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
        TransactionDTO tdto3 = new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedString = new ArrayList<>();
        //expectedString.add(tdto1);
        expectedString.add(tdto2);
        expectedString.add(tdto3);

        TransactionsResponseDTO expected = new TransactionsResponseDTO(expectedString);

        // Act
        TransactionsResponseDTO actual = service.getTransactionsOfAccountWithinPeriodByPersonID("1001@switch.pt", "1001", "2001-01-01 23:20:58", "2021-01-01 23:20:58");

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Get By PersonID - Ensure it just gets the transactions below end date for person
     */
    @Test
    @DisplayName("Test for GetTransactionsOfAccountWithinPeriod - Ensure it gets just the transactions below end date")
    void getByPersonIDEnsureEndDateLimitWorksHappyCaseTest() {
        // Arrange
        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
        //TransactionDTO tdto3= new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedString = new ArrayList<>();
        expectedString.add(tdto1);
        expectedString.add(tdto2);
        //expectedString.add(tdto3);

        TransactionsResponseDTO expected = new TransactionsResponseDTO(expectedString);

        // Act
        TransactionsResponseDTO actual = service.getTransactionsOfAccountWithinPeriodByPersonID("1001@switch.pt", "1001", "1999-01-01 23:20:58", "2019-01-01 23:20:58");

        // Assert
        assertEquals(expected, actual);
    }


    /**
     * Get By GroupID - Ensure it gets all transactions for group
     */
    @Test
    @DisplayName("Test for GetTransactionsOfAccountWithinPeriod - Ensure it gets all transactions")
    void getByGroupIDAllTransactionsHappyCaseTest() {
        // Arrange
        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
        TransactionDTO tdto3 = new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedString = new ArrayList<>();
        expectedString.add(tdto1);
        expectedString.add(tdto2);
        expectedString.add(tdto3);

        TransactionsResponseDTO expected = new TransactionsResponseDTO(expectedString);

        // Act
        TransactionsResponseDTO actual = service.getTransactionsOfAccountWithinPeriodByGroupID("1001@switch.pt", "1002", "1002", "1999-01-01 23:20:58", "2021-01-01 23:20:58");

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Get By GroupID - Ensure it just gets the transactions above start date for group
     */
    @Test
    @DisplayName("Test for GetTransactionsOfAccountWithinPeriod - Ensure it gets just the transactions above start date")
    void getByGroupIDEnsureStartDateLimitWorksHappyCaseTest() {
        // Arrange
        //TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
        TransactionDTO tdto3 = new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedString = new ArrayList<>();
        //expectedString.add(tdto1);
        expectedString.add(tdto2);
        expectedString.add(tdto3);

        TransactionsResponseDTO expected = new TransactionsResponseDTO(expectedString);

        // Act
        TransactionsResponseDTO actual = service.getTransactionsOfAccountWithinPeriodByGroupID("1001@switch.pt", "1002", "1002", "2001-01-01 23:20:58", "2021-01-01 23:20:58");

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Get By GroupID - Ensure it just gets the transactions below end date for group
     */
    @Test
    @DisplayName("Test for GetTransactionsOfAccountWithinPeriod - Ensure it gets just the transactions below end date")
    void getByGroupIDEnsureEndDateLimitWorksHappyCaseTest() {
        // Arrange
        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
        //TransactionDTO tdto3= new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedString = new ArrayList<>();
        expectedString.add(tdto1);
        expectedString.add(tdto2);
        //expectedString.add(tdto3);

        TransactionsResponseDTO expected = new TransactionsResponseDTO(expectedString);

        // Act
        TransactionsResponseDTO actual = service.getTransactionsOfAccountWithinPeriodByGroupID("1001@switch.pt", "1002", "1002", "1999-01-01 23:20:58", "2019-01-01 23:20:58");

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Get By LedgerID - Ensure it gets all transactions with ledgerID
     */
    @Test
    @DisplayName("Test for GetTransactionsOfAccountWithinPeriod - Ensure it gets all transactions")
    void getByLedgerIDAllTransactionsHappyCaseTest() {
        // Arrange
        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
        TransactionDTO tdto3 = new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedString = new ArrayList<>();
        expectedString.add(tdto1);
        expectedString.add(tdto2);
        expectedString.add(tdto3);

        LedgerID ledgerIDManuel = new LedgerID("1001");
        AccountID accountIDManuel = new AccountID("1001");
        Period period = new Period("1999-01-01 23:20:58", "2021-01-01 23:20:58");


        TransactionsResponseDTO expected = new TransactionsResponseDTO(expectedString);

        // Act
        TransactionsResponseDTO actual = service.getTransactionsOfAccountWithinPeriodByLedgerID(ledgerIDManuel, accountIDManuel, period);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Get By LedgerID - Ensure it gets all transactions with ledgerID
     */
    @Test
    @DisplayName("Test for GetTransactionsOfAccountWithinPeriod - Ensure it gets just the transactions above start date")
    void getByLedgerIDEnsureStartDateLimitWorksHappyCaseTest() {
        // Arrange
        //TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
        TransactionDTO tdto3 = new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedString = new ArrayList<>();
        //expectedString.add(tdto1);
        expectedString.add(tdto2);
        expectedString.add(tdto3);

        LedgerID ledgerIDManuel = new LedgerID("1001");
        AccountID accountIDManuel = new AccountID("1001");
        Period period = new Period("2001-01-01 23:20:58", "2021-01-01 23:20:58");


        TransactionsResponseDTO expected = new TransactionsResponseDTO(expectedString);

        // Act
        TransactionsResponseDTO actual = service.getTransactionsOfAccountWithinPeriodByLedgerID(ledgerIDManuel, accountIDManuel, period);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Get By LedgerID - Ensure it gets all transactions with ledgerID
     */
    @Test
    @DisplayName("Test for GetTransactionsOfAccountWithinPeriod - Ensure it gets just the transactions below end date")
    void getByLedgerIDEnsureEndDateLimitWorksHappyCaseTest() {
        // Arrange
        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
        //TransactionDTO tdto3 = new TransactionDTO("100", "CREDIT", "2020-01-01 14:12:15", "Pacote de Maria", "comida e bebida", "1002", "1004");

        List<TransactionDTO> expectedString = new ArrayList<>();
        expectedString.add(tdto1);
        expectedString.add(tdto2);
        //expectedString.add(tdto3);

        LedgerID ledgerIDManuel = new LedgerID("1001");
        AccountID accountIDManuel = new AccountID("1001");
        Period period = new Period("1999-01-01 23:20:58", "2019-01-01 23:20:58");


        TransactionsResponseDTO expected = new TransactionsResponseDTO(expectedString);

        // Act
        TransactionsResponseDTO actual = service.getTransactionsOfAccountWithinPeriodByLedgerID(ledgerIDManuel, accountIDManuel, period);

        // Assert
        assertEquals(expected, actual);
    }


}
