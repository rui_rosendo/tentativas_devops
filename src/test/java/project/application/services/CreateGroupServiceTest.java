package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CreateGroupRequestDTO;
import project.dto.CreateGroupResponseDTO;
import project.dto.GroupDTO;
import project.exceptions.GroupAlreadyExistsException;
import project.exceptions.GroupLedgerAlreadyExistsException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS002_1Service;
import project.infrastructure.repositories.GroupRepositoryDB;
import project.infrastructure.repositories.LedgerRepositoryDB;
import project.infrastructure.repositories.PersonRepositoryDB;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
public class CreateGroupServiceTest {

    @Autowired
    IUS002_1Service service;
    @Autowired
    PersonRepositoryDB personRepo;
    @Autowired
    GroupRepositoryDB groupRepo;
    @Autowired
    LedgerRepositoryDB ledgerRepo;

    private PersonID personID;
    private GroupID groupID;
    private LedgerID ledgerID;

    /**
     * createGroup Unit Test
     * Happy Case
     */
    @Test
    @DisplayName("createGroup Unit Test - Happy Case")
    public void createGroupHappyCaseUnitTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO("21012", "Switchadas", "1987-04-17",
                "21001@switch.pt", "21032");
        CreateGroupResponseDTO expected = new CreateGroupResponseDTO("21012", "Switchadas");

        personID = new PersonID("21001@switch.pt");
        groupID = new GroupID("21012");
        ledgerID = new LedgerID("21032");

        Group group = new Group(groupID, "Switchadas", "1987-04-17", personID, ledgerID);

        Person mockedPerson = Mockito.mock(Person.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);

        Mockito.when(personRepo.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(groupRepo.findById(groupID)).thenReturn(Optional.empty());
        Mockito.when(ledgerRepo.findById(ledgerID)).thenReturn(Optional.empty());
        Mockito.when(groupRepo.save(group)).thenReturn(group);
        Mockito.when(ledgerRepo.save(mockedLedger)).thenReturn(mockedLedger);

        //Act
        CreateGroupResponseDTO result = service.createGroup(requestDTO);

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for createGroup method
     * <p>
     * Ensure exception is throw if ledger ID already exists in the ledger repository - Unit test
     */
    @Test
    @DisplayName("Ensure exception is throw if ledger ID already exists in the ledger repository - Unit test")
    void createGroupEnsureExceptionIfGroupLedgerIDAlreadyExistsUnitTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO("21013", "Switchadas", "1987-04-17",
                "21001@switch.pt", "21032");

        personID = new PersonID("21001@switch.pt");
        groupID = new GroupID("21013");
        ledgerID = new LedgerID("21032");

        Person mockedPerson = Mockito.mock(Person.class);
        Ledger mockedLedger = Mockito.mock(Ledger.class);

        Mockito.when(personRepo.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(groupRepo.findById(groupID)).thenReturn(Optional.empty());
        Mockito.when(ledgerRepo.findById(ledgerID)).thenReturn(Optional.of(mockedLedger));

        //Act
        //Assert
        Assertions.assertThrows(GroupLedgerAlreadyExistsException.class, () -> {
            service.createGroup(requestDTO);
        });
    }

    /**
     * Test for createGroup method
     * <p>
     * Ensure exception is throw if group's creator doesn't exist in person repository - Unit test
     */
    @Test
    @DisplayName("Ensure exception is throw if group's creator doesn't exist in person repository - Unit test")
    void createGroupEnsureExceptionIfCreatorDoesNotExistUnitTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO("21033", "Switchadas", "1987-04-17",
                "21003@switch.pt", "21032");

        Mockito.when(personRepo.findById(personID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            service.createGroup(requestDTO);
        });
    }

    /**
     * Test for createGroup method
     * <p>
     * Ensure exception is throw if group ID already exists in group repository - Unit test
     */
    @Test
    @DisplayName("Ensure exception is throw if group ID already exists in group repository - Unit test")
    void createGroupEnsureExceptionIfGroupIDAlreadyExistsUnitTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO("21011", "Switchadas", "1987-04-17",
                "21001@switch.pt", "21031");

        personID = new PersonID("21001@switch.pt");
        groupID = new GroupID("21011");

        Person mockedPerson = Mockito.mock(Person.class);
        Group mockedGroup = Mockito.mock(Group.class);

        Mockito.when(personRepo.findById(personID)).thenReturn(Optional.of(mockedPerson));
        Mockito.when(groupRepo.findById(groupID)).thenReturn(Optional.of(mockedGroup));

        //Act
        //Assert
        Assertions.assertThrows(GroupAlreadyExistsException.class, () -> {
            service.createGroup(requestDTO);
        });
    }

    /**
     * Test for getGroupByID - Unit Test
     * Happy Case
     */
    @Test
    @DisplayName("Test for getGroupByID - Happy Case - Unit Test Service")
    void getGroupByIdHappyCaseUnitTest() {
        //Arrange
        Set<String> membersString = new HashSet<>();
        membersString.add("21001@switch.pt");

        Set<String> managersString = new HashSet<>();
        managersString.add("21001@switch.pt");

        personID = new PersonID("21001@switch.pt");
        groupID = new GroupID("21011");
        ledgerID = new LedgerID("21031");

        GroupDTO expectedDTO = new GroupDTO(membersString, managersString, "21011", "21031", "Switchadas",
                "1987-04-17");

        Group group = new Group(groupID, "Switchadas", "1987-04-17", personID, ledgerID);

        Mockito.when(groupRepo.findById(groupID)).thenReturn(Optional.of(group));

        //Act
        GroupDTO result = service.getGroupByID("21011");

        //Assert
        assertEquals(expectedDTO, result);
    }

    /**
     * Test for getGroupById - Unit Test
     * Ensure exception is throw if group is not found
     */
    @Test
    @DisplayName("Test for getGroupByID - Ensure exception is throw if group is not found - Unit test")
    void getGroupByIdEnsureExceptionIsThrowIfGroupIsNotFoundUnitTest() {
        //Arrange
        String groupID2String = "21013";

        Mockito.when(groupRepo.findById(groupID)).thenReturn(Optional.empty());

        //Act
        //Assert
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            service.getGroupByID(groupID2String);
        });
    }
}
