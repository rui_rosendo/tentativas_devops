package project.application.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.frameworkddd.IUS003Service;
import project.infrastructure.repositories.GroupRepositoryDB;
import project.infrastructure.repositories.PersonRepositoryDB;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@Transactional
class AddMemberToGroupServiceIT {

    @Autowired
    IUS003Service service;

    @Autowired
    PersonRepositoryDB newPersonRepositoryDB;

    @Autowired
    GroupRepositoryDB newGroupRepositoryDB;

    private PersonID isabelID;
    private LedgerID isabelLedgerID;
    private Person isabel;

    private PersonID joaoID;
    private LedgerID joaoLedgerID;
    private Person joao;

    private PersonID andreID;
    private LedgerID andreLedgerID;
    private Person andre;

    private GroupID alvesGroupID;
    private LedgerID alvesGroupLedgerID;
    private Group alvesFamily;

    @BeforeEach
    public void init() {

        isabelID = new PersonID("111@switch.pt");
        isabelLedgerID = new LedgerID("11111");
        isabel = new Person(isabelID, "Isabel", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, isabelLedgerID);

        joaoID = new PersonID("222@switch.pt");
        joaoLedgerID = new LedgerID("22222");
        joao = new Person(joaoID, "João", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", null, null, joaoLedgerID);

        andreID = new PersonID("333@switch.pt");
        andreLedgerID = new LedgerID("33333");
        andre = new Person(andreID, "André", "Rua Dr. Roberto Frias s/n", "Porto", "1991-12-22", isabelID, joaoID, andreLedgerID);

        newPersonRepositoryDB.save(isabel);
        newPersonRepositoryDB.save(joao);
        newPersonRepositoryDB.save(andre);

        alvesGroupID = new GroupID("4444");
        alvesGroupLedgerID = new LedgerID("444444");
        alvesFamily = new Group(alvesGroupID, "Grupo da familia Alves", "2020-01-01", andreID, alvesGroupLedgerID);

        newGroupRepositoryDB.save(alvesFamily);
    }

    /**
     * Test for constructor
     * Happy Case
     */
    @Test
    @DisplayName("Test for AddMemberToGroupServiceTest Constructor - HappyCase")
    void GroupConstructorHappyCaseTest() {
        // Assert
        assertTrue(service instanceof IUS003Service);
    }

//    /**
//     * Test for addMember
//     * Happy case
//     */
//    @Test
//    @DisplayName("Test for addMember - HappyCase")
//    void addMemberToGroupHappyCaseTest() {
//        //Arrange
//        AddMemberRequestDTO newAddMemberRequestDTO = new AddMemberRequestDTO("111", "4444");
//
//        AddMemberResponseDTO expected = new AddMemberResponseDTO("4444", "Grupo da familia Alves", "111");
//
//        //Act
//        AddMemberResponseDTO result = service.addMemberJpa(newAddMemberRequestDTO);
//
//        //Assert
//        assertEquals(expected, result);
//    }

//    /**
//     * Test for addMember
//     * Ensure exception is thrown when MemberID already present in group
//     */
//    @Test
//    @DisplayName("Test for addMember - memberID already present case")
//    void addMemberToGroupAddingMemberIDAlreadyPresentCaseTest() {
//        //ARRANGE
//        AddMemberRequestDTO newAddMemberRequestDTO = new AddMemberRequestDTO("333", "4444");
//
//        //ACT-ASSERT
//        assertThrows(MemberAlreadyExistsException.class, () -> {
//            service.addMemberJpa(newAddMemberRequestDTO);
//        });
//    }

//    /**
//     * Test for addMember
//     * Ensure exception is thrown when no Person with given PersonID is found in repository case
//     */
//    @Test
//    @DisplayName("Test for addMember - No person with given PersonID case")
//    void addMemberToGroupNoPersonWithGivenIDCaseTest() {
//        //Arrange
//        AddMemberRequestDTO newAddMemberRequestDTO = new AddMemberRequestDTO("555", "4444");
//
//        //Act
//        //Assert
//        assertThrows(PersonNotFoundException.class, () -> {
//            service.addMemberJpa(newAddMemberRequestDTO);
//        });
//    }
//
//    /**
//     * Test for addMember
//     * Ensure exception is thrown when no Group with given GroupID is found in repository case
//     */
//    @Test
//    @DisplayName("Test for addMember - No group with given GroupID case")
//    void addMemberToGroupNoGroupWithGivenIDCaseTest() {
//        // Arrange
//        AddMemberRequestDTO newAddMemberRequestDTO = new AddMemberRequestDTO("111", "666");
//
//        //Act
//        //Assert
//        assertThrows(GroupNotFoundException.class, () -> {
//            service.addMemberJpa(newAddMemberRequestDTO);
//        });
//    }

}