package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.CheckIfPeopleAreSiblingsAssembler;
import project.dto.CheckIfPeopleAreSiblingsRequestDTO;
import project.dto.CheckIfPeopleAreSiblingsResponseDTO;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS001Service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class CheckIfPeopleAreSiblingsRESTControllerTest {

    @Autowired
    private IUS001Service service;

    @Autowired
    private CheckIfPeopleAreSiblingsRESTController controller;

    /**
     * Person1ID 10005@switch.pt and Person1ID 10006@switch.pt have the same mother.
     * Ensures it returns true when two people have the same mother.
     * Expected content true.
     */
    @Test
    @DisplayName("Ensure true with same mother test")
    void ensureTrueWithSameMotherTest() {
        //ARRANGE
        String personID1 = "10005@switch.pt";
        String personID2 = "10006@switch.pt";

        CheckIfPeopleAreSiblingsRequestDTO requestDTO = CheckIfPeopleAreSiblingsAssembler.mapToRequestDTO(personID1, personID2);
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = CheckIfPeopleAreSiblingsAssembler.mapToResponseDTO("true");

        Mockito.when(service.isSibling(requestDTO)).thenReturn(responseDTO);

        //ACT
        Object result = controller.checkIfPeopleAreSiblings(personID1, personID2).getBody();

        //ASSERT
        assertEquals(responseDTO, result);
    }

    /**
     * Person1ID 10005@switch.pt and Person1ID 10007@switch.pt have the same father.
     * Ensures it returns true when two people have the same father.
     * Expected content true.
     */
    @Test
    @DisplayName("Ensure true with same father test")
    void ensureTrueWithSameFatherTest() {
        //ARRANGE
        String personID1 = "10005@switch.pt";
        String personID2 = "10007@switch.pt";

        CheckIfPeopleAreSiblingsRequestDTO requestDTO = CheckIfPeopleAreSiblingsAssembler.mapToRequestDTO(personID1, personID2);
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = CheckIfPeopleAreSiblingsAssembler.mapToResponseDTO("true");

        Mockito.when(service.isSibling(requestDTO)).thenReturn(responseDTO);

        //ACT
        Object result = controller.checkIfPeopleAreSiblings(personID1, personID2).getBody();

        //ASSERT
        assertEquals(responseDTO, result);
    }

    /**
     * Person1ID 10006@switch.pt and Person1ID 10007@switch.pt are in each other's siblings list.
     * Ensures it returns true when two people are in each other's siblings list.
     * Expected content boolean true.
     */
    @Test
    @DisplayName("Ensure true by siblings list: Person2 is present in Person1 Siblings list")
    void ensureTrueBySiblingsListTest() {
        //ARRANGE
        String personID1 = "10006@switch.pt";
        String personID2 = "10007@switch.pt";

        CheckIfPeopleAreSiblingsRequestDTO requestDTO = CheckIfPeopleAreSiblingsAssembler.mapToRequestDTO(personID1, personID2);
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = CheckIfPeopleAreSiblingsAssembler.mapToResponseDTO("true");

        Mockito.when(service.isSibling(requestDTO)).thenReturn(responseDTO);

        //ACT
        Object result = controller.checkIfPeopleAreSiblings(personID1, personID2).getBody();

        //ASSERT
        assertEquals(responseDTO, result);
    }

    /**
     * Person1ID 10007@switch.pt and Person1ID 10006@switch.pt are in each other's siblings list.
     * Ensures it returns true when two people are in each other's siblings list,
     * whatever the order of the parameters inputted (reversing the order).
     * Expected content boolean true.
     */
    @Test
    @DisplayName("Ensure true by siblings list: Person1 is present in Person2 Siblings list")
    void ensureTrueByReciprocalSiblingsListTest() {
        //ARRANGE
        String personID1 = "10007@switch.pt";
        String personID2 = "10006@switch.pt";

        CheckIfPeopleAreSiblingsRequestDTO requestDTO = CheckIfPeopleAreSiblingsAssembler.mapToRequestDTO(personID1, personID2);
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = CheckIfPeopleAreSiblingsAssembler.mapToResponseDTO("true");

        Mockito.when(service.isSibling(requestDTO)).thenReturn(responseDTO);

        //ACT
        Object result = controller.checkIfPeopleAreSiblings(personID1, personID2).getBody();

        //ASSERT
        assertEquals(responseDTO, result);
    }

    /**
     * Ensures it returns false when two people are not in each other's siblings list,
     * don´t have the same father and don´t have the same mother.
     * Expected content boolean false.
     */
    @Test
    @DisplayName("Ensure false by mother and father and siblings list: people don't have the same mother, father or are present in the siblings list")
    void ensureFalseByMotherAndFatherAndSiblingsListTest() {
        //ARRANGE
        String personID1 = "10007@switch.pt";
        String personID2 = "10008@switch.pt";

        CheckIfPeopleAreSiblingsRequestDTO requestDTO = CheckIfPeopleAreSiblingsAssembler.mapToRequestDTO(personID1, personID2);
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = CheckIfPeopleAreSiblingsAssembler.mapToResponseDTO("false");

        Mockito.when(service.isSibling(requestDTO)).thenReturn(responseDTO);

        //ACT
        Object result = controller.checkIfPeopleAreSiblings(personID1, personID2).getBody();

        //ASSERT
        assertEquals(responseDTO, result);
    }

    /**
     * Assert PersonNotFoundException is thrown if person at least one of the person not in the Person Repository.
     */
    @Test
    @DisplayName("Ensure fails when person not found in repository")
    void ensureFailsWhenPersonNotFoundTest() {
        //ARRANGE
        String personID1 = "10007@switch.pt";
        String personID2 = "10009@switch.pt";

        CheckIfPeopleAreSiblingsRequestDTO requestDTO = CheckIfPeopleAreSiblingsAssembler.mapToRequestDTO(personID1, personID2);

        Mockito.when(service.isSibling(requestDTO)).thenThrow(PersonNotFoundException.class);

        //ACT
        //ASSERT
        assertThrows(PersonNotFoundException.class, () -> {
            CheckIfPeopleAreSiblingsResponseDTO result = service.isSibling(requestDTO);
        });
    }

}
