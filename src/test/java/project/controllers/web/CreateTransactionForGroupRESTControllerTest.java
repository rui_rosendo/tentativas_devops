package project.controllers.web;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.*;
import project.exceptions.AccountNotFoundException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.InvalidFieldException;
import project.exceptions.LedgerNotFoundException;
import project.frameworkddd.IUS008_1Service;
import project.model.entities.ledger.Transaction;
import project.model.entities.ledger.Type;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class CreateTransactionForGroupRESTControllerTest {

    @Autowired
    private IUS008_1Service service;

    @Autowired
    private CreateTransactionForGroupRESTController controller;

    /**
     * Test for method createTransactionForGroup
     * Happy Case
     */
    @Test
    void createTransactionForGroupHappyCaseTest() {
        //Arrange
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO("1995.55", "2000-01-01 12:00:00", "Debit",
                "transaction description", "category designation", "1000", "2000");
        String personId = "10@switch.com";
        String groupId = "100";

        CreateTransactionForGroupRequestDTO requestDTO = CreateTransactionForGroupAssembler.mapToRequestDTO(personId,
                groupId, infoDTO);
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupId, "group " +
                "description", "1995.55", "Debit", "transaction description");

        Mockito.when(service.createTransactionForGroup(requestDTO)).thenReturn(responseDTO);

        //Act
        Object expectedBody = controller.createTransactionForGroup(infoDTO, personId, groupId).getBody();
        Object expectedStatus = controller.createTransactionForGroup(infoDTO, personId, groupId).getStatusCode();

        //Assert
        assertEquals(expectedBody, responseDTO);
        assertEquals(expectedStatus, HttpStatus.CREATED);

    }

    /**
     * Test for method createTransactionForGroup
     * Ensure throws GroupNotFoundException
     */
    @Test
    void createTransactionForGroupGroupEnsureThrowsGroupNotFoundExceptionTest() {
        //Arrange
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO("1995.55", "2000-01-01 12:00:00", "Debit",
                "transaction description", "category designation", "1000", "2000");
        String personId = "20@switch.com";
        String groupId = "200";

        CreateTransactionForGroupRequestDTO requestDTO = CreateTransactionForGroupAssembler.mapToRequestDTO(personId,
                groupId, infoDTO);

        Mockito.when(service.createTransactionForGroup(requestDTO)).thenThrow(GroupNotFoundException.class);

        //Act
        //Assert
        assertThrows(GroupNotFoundException.class, () -> {
            controller.createTransactionForGroup(infoDTO, personId, groupId);
        });

    }

    /**
     * Test for method createTransactionForGroup
     * Ensure throws LedgerNotFoundException
     */
    @Test
    void createTransactionForGroupEnsureThrowsLedgerNotFoundExceptionTest() {
        //Arrange
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO("1995.55", "2000-01-01 12:00:00", "Debit",
                "transaction description", "category designation", "1000", "2000");
        String personId = "30@switch.com";
        String groupId = "300";

        CreateTransactionForGroupRequestDTO requestDTO = CreateTransactionForGroupAssembler.mapToRequestDTO(personId,
                groupId, infoDTO);

        Mockito.when(service.createTransactionForGroup(requestDTO)).thenThrow(LedgerNotFoundException.class);

        //Act
        //Assert
        assertThrows(LedgerNotFoundException.class, () -> {
            controller.createTransactionForGroup(infoDTO, personId, groupId);
        });

    }

    /**
     * Test for method createTransactionForGroup
     * Ensure throws AccountNotFoundException
     */
    @Test
    void createTransactionForGroupEnsureThrowsAccountNotFoundExceptionTest() {
        //Arrange
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO("1995.55", "2000-01-01 12:00:00", "Debit",
                "transaction description", "category designation", "1000", "2000");
        String personId = "40@switch.com";
        String groupId = "400";

        CreateTransactionForGroupRequestDTO requestDTO = CreateTransactionForGroupAssembler.mapToRequestDTO(personId,
                groupId, infoDTO);

        Mockito.when(service.createTransactionForGroup(requestDTO)).thenThrow(AccountNotFoundException.class);

        //Act
        //Assert
        assertThrows(AccountNotFoundException.class, () -> {
            controller.createTransactionForGroup(infoDTO, personId, groupId);
        });

    }

    /**
     * Test for method createTransactionForGroup
     * Ensure throws InvalidFieldException
     */
    @Test
    void createTransactionForGroupEnsureThrowsInvalidFieldExceptionTest() {
        //Arrange
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO("1995.55", "2000-01-01 12:00:00", "Debit",
                "transaction description", "category designation", "1000", "2000");
        String personId = "50@switch.com";
        String groupId = "500";

        CreateTransactionForGroupRequestDTO requestDTO = CreateTransactionForGroupAssembler.mapToRequestDTO(personId,
                groupId, infoDTO);

        Mockito.when(service.createTransactionForGroup(requestDTO)).thenThrow(InvalidFieldException.class);

        //Act
        //Assert
        assertThrows(InvalidFieldException.class, () -> {
            controller.createTransactionForGroup(infoDTO, personId, groupId);
        });

    }

    /**
     * Test for method getTransactionsByGroupID
     * Happy Case Test
     */
    @Test
    void getTransactionByGroupIDHappyCaseTest() {
        //Arrange
        String personId = "8101@switch.com";
        String groupId = "8101";

        Transaction transaction = new Transaction("1000", Type.DEBIT, "2020-01-01 12:00:00", "description",
                new Category("category"), new AccountID("500"), new AccountID("600"));
        List<Transaction> transactionsSet = new ArrayList<>();
        transactionsSet.add(transaction);
        TransactionsResponseDTO responseDTO = TransactionsAssembler.mapToDTO(transactionsSet);

        Mockito.when(service.getTransactionsByGroupID(personId, groupId)).thenReturn(responseDTO);

        //Act
        ResponseEntity<Object> actualResponse = controller.getTransactionsByGroupID(personId, groupId);
        Object actualBody = actualResponse.getBody();
        Object actualStatus = actualResponse.getStatusCode();

        //Assert
        assertEquals(responseDTO, actualBody);
        assertEquals(HttpStatus.OK, actualStatus);
    }

    /**
     * Test for method getTransactionsByGroupID
     * Ensure Throws exception when group doesn't exist in repo
     */
    @Test
    void getTransactionByGroupIDEnsureThrowsExceptionWhenGroupDoesNotExistInRepoTest() {
        //Arrange
        String personId = "8101@switch.com";
        String groupId = "9999";

        Mockito.when(service.getTransactionsByGroupID(personId, groupId)).thenThrow(GroupNotFoundException.class);

        //Act
        //Assert
        assertThrows(GroupNotFoundException.class, () -> {
            controller.getTransactionsByGroupID(personId, groupId);
        });
    }

    /**
     * Test for method getTransactionsByGroupID
     * Ensure Throws exception when ledger doesn't exist in repo
     */
    @Test
    void getTransactionByGroupIDEnsureThrowsWhenLedgerDoesNotExistInRepoExceptionTest() {
        //Arrange
        String personId = "8101@switch.com";
        String groupId = "8201";

        Mockito.when(service.getTransactionsByGroupID(personId, groupId)).thenThrow(LedgerNotFoundException.class);

        //Act
        //Assert
        assertThrows(LedgerNotFoundException.class, () -> {
            controller.getTransactionsByGroupID(personId, groupId);
        });
    }

}