package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.*;
import project.exceptions.GroupNotFoundException;
import project.exceptions.MemberAlreadyExistsException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS003Service;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class AddMemberToGroupRESTControllerTest {
    @Autowired
    private IUS003Service service;
    @Autowired
    private AddMemberToGroupRESTController controller;

    /**
     * Test for AddMemberToGroupRESTController.addMemberToGroup Unit Test
     * Happy case
     */
    @Test
    @DisplayName("Test for addMember - HappyCase")
    void addMemberToGroupHappyCaseTest() {
        //Arrange
        String newMemberID = "111@switch.com";
        String groupID = "4444";
        String groupDescription = "Grupo da familia Alves";

        AddMemberInfoDTO newMemberInfoDTO = new AddMemberInfoDTO(newMemberID);
        AddMemberRequestDTO requestDTO = AddMemberAssembler.mapToRequestDTO(newMemberID, groupID);
        AddMemberResponseDTO expectedResponseDTO = new AddMemberResponseDTO(groupID, groupDescription, newMemberID);

        Mockito.when(service.addMember(requestDTO)).thenReturn(expectedResponseDTO);

        //Act
        Object actualResponseDTO = controller.addMemberToGroup(newMemberInfoDTO, groupID).getBody();

        //Assert
        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    /**
     * Test for AddMemberToGroupRESTController.addMemberToGroup Unit Test
     * Ensure exception is thrown when Adding creator ID again in group
     */
    @Test
    @DisplayName("Test for addMember - Adding creator ID again case")
    void addMemberToGroupAddingCreatorAgainCaseTest() {
        //Arrange
        String newMemberID = "333@switch.com";
        String groupID = "4444";

        AddMemberInfoDTO newMemberInfoDTO = new AddMemberInfoDTO(newMemberID);
        AddMemberRequestDTO requestDTO = AddMemberAssembler.mapToRequestDTO(newMemberID, groupID);

        Mockito.when(service.addMember(requestDTO)).thenThrow(MemberAlreadyExistsException.class);

        //Act
        //Assert
        assertThrows(MemberAlreadyExistsException.class, () -> {
            controller.addMemberToGroup(newMemberInfoDTO, groupID).getBody();
        });
    }

    /**
     * Test for AddMemberToGroupRESTController.addMemberToGroup Unit Test
     * Ensure exception is thrown when MemberID already present in group
     */
    @Test
    @DisplayName("Test for addMember - memberID already present case")
    void addMemberToGroupAddingMemberIDAlreadyPresentCaseTest() {
        //Arrange
        String newMemberID = "222@switch.com";
        String groupID = "4444";

        AddMemberInfoDTO newMemberInfoDTO = new AddMemberInfoDTO(newMemberID);
        AddMemberRequestDTO requestDTO = AddMemberAssembler.mapToRequestDTO(newMemberID, groupID);

        Mockito.when(service.addMember(requestDTO)).thenThrow(MemberAlreadyExistsException.class);

        //Act
        //Assert
        assertThrows(MemberAlreadyExistsException.class, () -> {
            controller.addMemberToGroup(newMemberInfoDTO, groupID).getBody();
        });
    }

    /**
     * Test for AddMemberToGroupRESTController.addMemberToGroup Unit Test
     * Ensure exception is thrown when no Person with given PersonID is found in repository case
     */
    @Test
    @DisplayName("Test for addMember - No person with given PersonID case")
    void addMemberToGroupNoPersonWithGivenIDCaseTest() {
        //Arrange
        String newMemberID = "999@switch.com";
        String groupID = "4444";

        AddMemberInfoDTO newMemberInfoDTO = new AddMemberInfoDTO(newMemberID);
        AddMemberRequestDTO requestDTO = AddMemberAssembler.mapToRequestDTO(newMemberID, groupID);

        Mockito.when(service.addMember(requestDTO)).thenThrow(PersonNotFoundException.class);

        //Act
        //Assert
        assertThrows(PersonNotFoundException.class, () -> {
            controller.addMemberToGroup(newMemberInfoDTO, groupID).getBody();
        });
    }

    /**
     * Test for AddMemberToGroupRESTController.addMemberToGroup Unit Test
     * Ensure exception is thrown when no Group with given GroupID is found in repository case
     */
    @Test
    @DisplayName("Test for addMember - No group with given GroupID case")
    void addMemberToGroupNoGroupWithGivenIDCaseTest() {
        //Arrange
        String newMemberID = "111@switch.com";
        String groupID = "6666";

        AddMemberInfoDTO newMemberInfoDTO = new AddMemberInfoDTO(newMemberID);
        AddMemberRequestDTO requestDTO = AddMemberAssembler.mapToRequestDTO(newMemberID, groupID);

        Mockito.when(service.addMember(requestDTO)).thenThrow(GroupNotFoundException.class);

        //Act
        //Assert
        assertThrows(GroupNotFoundException.class, () -> {
            controller.addMemberToGroup(newMemberInfoDTO, groupID).getBody();
        });
    }

    /**
     * Test for getMembersByGroupID Unit Test
     * Happy Case
     */
    @Test
    @DisplayName("Test for getMembersByGroupID - Happy Case")
    void getMembersByGroupIDHappyCaseTest() {
        //Arrange
        List<String> expectedMembersString = new ArrayList<>();
        List<String> expectedMembersNames = new ArrayList<>();
        List<Boolean> expectedManagers = new ArrayList<>();

        expectedMembersString.add("222@switch.com");
        expectedMembersString.add("333@switch.com");

        MembersDTO expectedMembersDTO = new MembersDTO(expectedMembersString, expectedMembersNames, expectedManagers);

        Mockito.when(service.getMembersByGroupID("4444")).thenReturn(expectedMembersDTO);

        //Act
        Object actualResponseDTO = controller.getMembersByGroupID("4444").getBody();

        //Assert
        assertEquals(expectedMembersDTO, actualResponseDTO);
    }
}
