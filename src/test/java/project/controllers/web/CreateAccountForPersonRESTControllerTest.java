package project.controllers.web;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.*;
import project.exceptions.AccountAlreadyExistsException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS006Service;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class CreateAccountForPersonRESTControllerTest {

    @Autowired
    private CreateAccountForPersonRESTController controller;

    @Autowired
    private IUS006Service service;

    /**
     * Test for createAccountForPerson
     * Happy Case
     */
    @Test
    @DisplayName("Test for createAccountForPerson - Happy case")
    public void createAccountForPersonHappyCaseTest() {

        String accountID = "66607";
        String denomination = "flamethrowers";
        String description = "gear";
        String name = "Ripley, Elen";
        String personID = "6662@switch.com";

        CreateAccountForPersonInfoDTO infoDTO = new CreateAccountForPersonInfoDTO(accountID, denomination, description);
        CreateAccountForPersonRequestDTO requestDTO = CreateAccountForPersonAssembler.mapToRequestDTO(accountID, personID, denomination, description);
        CreateAccountForPersonResponseDTO expectedResponseDTO = new CreateAccountForPersonResponseDTO(personID, name, accountID, denomination);

        Mockito.when(service.createAccountForPerson(requestDTO)).thenReturn(expectedResponseDTO);

        //ACT
        Object actualResponseDTO = controller.createAccountForPerson(infoDTO, personID).getBody();

        //ASSERT
        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    /**
     * Test for createAccountForPerson
     * Person not in repository
     */
    @Test
    @DisplayName("Test for createAccountForPerson - Person Not In Repository")
    public void createAccountForPersonPersonDoesNotExistInRepoTest() {

        String accountID = "6001";
        String denomination = "food";
        String description = "food";
        String personID = "6002@switch.com";

        CreateAccountForPersonInfoDTO infoDTO = new CreateAccountForPersonInfoDTO(accountID, denomination, description);
        CreateAccountForPersonRequestDTO requestDTO = CreateAccountForPersonAssembler.mapToRequestDTO(accountID, personID, denomination, description);

        Mockito.when(service.createAccountForPerson(requestDTO)).thenThrow(PersonNotFoundException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            controller.createAccountForPerson(infoDTO, personID);
        });
    }

    /**
     * Test for createAccountForPerson
     * Account Already Exists In Repository And Person List
     */
    @Test
    @DisplayName("Test for createAccountForPerson - Account Already Exists In Repository And Person List")
    public void createAccountForPersonAccountAlreadyExistsInRepoAndPersonListTest() {

        String accountID = "60012";
        String denomination = "food";
        String description = "food";
        String personID = "60012@switch.com";

        CreateAccountForPersonInfoDTO infoDTO = new CreateAccountForPersonInfoDTO(accountID, denomination, description);
        CreateAccountForPersonRequestDTO requestDTO = CreateAccountForPersonAssembler.mapToRequestDTO(accountID, personID, denomination, description);

        Mockito.when(service.createAccountForPerson(requestDTO)).thenThrow(AccountAlreadyExistsException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            controller.createAccountForPerson(infoDTO, personID);
        });
    }

    /**
     * Test for createAccountForPerson
     * Account Already Exists In Repository
     */
    @Test
    @DisplayName("Test for createAccountForPerson - Account Does Not Exists In Repository")
    public void createAccountForPersonAccountAlreadyExistsInRepoTest() {

        String accountID = "60014";
        String denomination = "food";
        String description = "food";
        String personID = "60014@switch.com";

        CreateAccountForPersonInfoDTO infoDTO = new CreateAccountForPersonInfoDTO(accountID, denomination, description);
        CreateAccountForPersonRequestDTO requestDTO = CreateAccountForPersonAssembler.mapToRequestDTO(accountID, personID, denomination, description);

        Mockito.when(service.createAccountForPerson(requestDTO)).thenThrow(AccountAlreadyExistsException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            controller.createAccountForPerson(infoDTO, personID);
        });
    }

    /**
     * Test for getAccountsByPersonID
     * Happy Case
     */
    @Test
    @DisplayName("Test for getAccountsByPersonID - Happy Case")
    public void getAccountsByPersonIDHappyCaseTest() {

        String personID = "60015@switch.com";

        List<AccountDTO> accountsDTOList = new ArrayList<>();
        accountsDTOList.add(new AccountDTO("bla", "ble", "7001"));
        accountsDTOList.add(new AccountDTO("bla", "ble", "7002"));

        AccountsDTO expectedResponseDTO = new AccountsDTO(accountsDTOList);

        Mockito.when(service.getAccountsByPersonID(personID)).thenReturn(expectedResponseDTO);

        //ACT
        Object actualResponseDTO = controller.getAccountsByPersonID(personID).getBody();

        //ASSERT
        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    /**
     * Test for getAccountsByPersonID
     * Person Not Found In Repo
     */
    @Test
    @DisplayName("Test for getAccountsByPersonID - Person Not Found In Repo")
    public void getAccountsByPersonIDPersonNotFoundInRepo() {

        String personID = "60016@switch.com";

        Mockito.when(service.getAccountsByPersonID(personID)).thenThrow(PersonNotFoundException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            controller.getAccountsByPersonID(personID);
        });
    }
}
