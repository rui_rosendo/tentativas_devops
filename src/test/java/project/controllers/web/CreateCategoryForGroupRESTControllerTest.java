package project.controllers.web;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.application.services.CreateCategoryForGroupService;
import project.dto.*;
import project.exceptions.CategoryAlreadyExistsException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.InvalidFieldException;
import project.exceptions.PersonIsNotManagerOfTheGroupException;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class CreateCategoryForGroupRESTControllerTest {
    @Autowired
    private CreateCategoryForGroupService service;
    @Autowired
    private CreateCategoryForGroupRESTController controller;

    // TODO rever testes unitários para mockitos de JPA

    /**
     * createCategoryForGroup - Happy Case
     */
    @Test
    @DisplayName("Test for createCategoryForGroup - Happy Case")
    public void createCategoryForGroupHappyCaseTest() {

        //ARRANGE
        String personID = "51010@switch.com";
        String groupID = "510200";
        String designation = "Bolas";
        String groupDescription = "Futebol";

        CreateCategoryForGroupInfoDTO infoDTO = new CreateCategoryForGroupInfoDTO(designation);
        CreateCategoryForGroupRequestDTO requestDTO = CreateCategoryForGroupAssembler.mapToRequestDTO(personID, groupID, designation);
        CreateCategoryForGroupResponseDTO expected = new CreateCategoryForGroupResponseDTO(groupID, groupDescription, designation);

        Mockito.when(service.createCategoryForGroup(requestDTO)).thenReturn(expected);

        //ACT
        Object result = controller.createCategoryForGroup(infoDTO, personID, groupID).getBody();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Ensure that a category is not added if already exists in the group"
     */
    @Test
    @DisplayName("ensure that a category is not added if already exists in the group")
    public void createCategoryForGroupEnsureCategoryCantBeAddedIfAlreadyExistInTheGroupTest() {

        //ARRANGE
        String personID = "51010@switch.com";
        String groupID = "510200";
        String designation = "Arbitros";

        CreateCategoryForGroupInfoDTO infoDTO = new CreateCategoryForGroupInfoDTO(designation);
        CreateCategoryForGroupRequestDTO requestDTO = CreateCategoryForGroupAssembler.mapToRequestDTO(personID, groupID, designation);

        Mockito.when(service.createCategoryForGroup(requestDTO)).thenThrow(CategoryAlreadyExistsException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(CategoryAlreadyExistsException.class, () -> {
            controller.createCategoryForGroup(infoDTO, personID, groupID);
        });
    }

    /**
     * Ensure that a category is not added to a group that is not in the repository
     */
    @Test
    @DisplayName("ensure that a category is not added to a group that is not in the repository")
    public void createCategoryForGroupEnsureCategoryIsNotAddedToAGroupThatIsNotInTheRepositoryTest() {

        //ARRANGE
        String personID = "51010@switch.com";
        String groupID = "510300";
        String designation = "Comida";

        CreateCategoryForGroupInfoDTO infoDTO = new CreateCategoryForGroupInfoDTO(designation);
        CreateCategoryForGroupRequestDTO requestDTO = CreateCategoryForGroupAssembler.mapToRequestDTO(personID, groupID, designation);

        Mockito.when(service.createCategoryForGroup(requestDTO)).thenThrow(GroupNotFoundException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            controller.createCategoryForGroup(infoDTO, personID, groupID);
        });
    }

    /**
     * Ensure Person Is Manager Of The Group
     */
    @Test
    @DisplayName("Ensure Person Is Manager Of The Group ")
    public void createCategoryForGroupEnsurePersonIsManagerOfTheGroupTest() {

        //ARRANGE
        String personID = "5101@switch.com";
        String groupID = "510200";
        String designation = "Comida";

        CreateCategoryForGroupInfoDTO infoDTO = new CreateCategoryForGroupInfoDTO(designation);
        CreateCategoryForGroupRequestDTO requestDTO = CreateCategoryForGroupAssembler.mapToRequestDTO(personID, groupID, designation);

        Mockito.when(service.createCategoryForGroup(requestDTO)).thenThrow(PersonIsNotManagerOfTheGroupException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonIsNotManagerOfTheGroupException.class, () -> {
            controller.createCategoryForGroup(infoDTO, personID, groupID);
        });
    }

    /**
     * ensure that a exception is thrown when the category input is invalid
     */
    @Test
    @DisplayName("ensure that a exception is thrown when the category input is invalid")
    public void createCategoryForGroupEnsureExceptionWhenTheCategoryInputIsInvalidTest() {

        //ARRANGE
        String personID = "51010@switch.com";
        String groupID = "510200";
        String designation = "";

        CreateCategoryForGroupInfoDTO infoDTO = new CreateCategoryForGroupInfoDTO(designation);
        CreateCategoryForGroupRequestDTO requestDTO = CreateCategoryForGroupAssembler.mapToRequestDTO(personID, groupID, designation);

        Mockito.when(service.createCategoryForGroup(requestDTO)).thenThrow(InvalidFieldException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(InvalidFieldException.class, () -> {
            controller.createCategoryForGroup(infoDTO, personID, groupID);
        });
    }

    /**
     * getCategoriesByGroupID - Happy Case
     */
    @Test
    @DisplayName("getCategoriesByGroupID - Happy Case")
    public void getCategoryByGroupIDHappyCaseTest() {

        //ARRANGE
        String groupID = "510200";
        Set<String> categories = new HashSet<>();
        categories.add("Arbitros");

        CategoriesDTO expected = new CategoriesDTO(categories);

        Mockito.when(service.getCategoriesByGroupID(groupID)).thenReturn(expected);

        //ACT
        Object result = controller.getCategoriesByGroupID(groupID).getBody();

        //ASSERT
        assertEquals(expected, result);
    }

}
