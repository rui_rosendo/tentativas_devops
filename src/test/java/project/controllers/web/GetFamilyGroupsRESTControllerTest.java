package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GetFamilyGroupsDTO;
import project.frameworkddd.IUS004Service;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GetFamilyGroupsRESTControllerTest {
    @Autowired
    private IUS004Service service;
    @Autowired
    private GetFamilyGroupsRESTController controller;

    /**
     * Test for getFamilyGroups
     * Ensure works with repository with family and non-family groups
     */
    @Test
    @DisplayName("Test for getFamilyGroups - Ensure works with repository with family and non-family groups")
    public void getFamilyGroupsEnsureWorksWithRepositoryWithFamilyAndNonFamilyGroupsTest() {
        // Arrange
        Set<String> expectedString = new HashSet<>();
        expectedString.add("4507");
        expectedString.add("4508");
        expectedString.add("4501");
        GetFamilyGroupsDTO outputDTOExpected = new GetFamilyGroupsDTO(expectedString);

        Mockito.when(service.getFamilyGroups()).thenReturn(outputDTOExpected);

        //ACT
        Object outputDTO = controller.getFamilyGroups().getBody();

        //ASSERT
        assertEquals(outputDTOExpected, outputDTO);
    }

    /**
     * Test for getFamilyGroups
     * Ensure works with repository with only non-family groups
     */
    @Test
    @DisplayName("Test for getFamilyGroups - Ensure works with repository with only non-family groups")
    public void getFamilyGroupsEnsureWorksWithRepositoryWithOnlyNonFamilyGroupsTestJpa() {
        // Arrange
        Set<String> expectedString = new HashSet<>();
        GetFamilyGroupsDTO outputDTOExpected = new GetFamilyGroupsDTO(expectedString);

        Mockito.when(service.getFamilyGroups()).thenReturn(outputDTOExpected);

        //ACT
        Object outputDTO = controller.getFamilyGroups().getBody();

        //ASSERT
        assertEquals(outputDTOExpected, outputDTO);
    }

}