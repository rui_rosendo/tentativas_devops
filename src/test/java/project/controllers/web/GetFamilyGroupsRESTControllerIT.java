package project.controllers.web;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetFamilyGroupsRESTControllerIT extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }


    /**
     * Test for getFamilyGroups
     * Ensure works with repository with family and non-family groups
     */
    @Test
    @DisplayName("Test for getFamilyGroups - Ensure works with repository with family and non-family groups")
    public void getFamilyGroupsEnsureWorksWithRepositoryWithFamilyAndNonFamilyGroupsTest() throws Exception {
        String uri = "/groups/family";
        int expectedStatus = 200;
        JSONObject expectedContent = new JSONObject()
                .put("familyGroupsIDs", new JSONArray(Arrays.asList("4507", "4508", "4501")))
                .put("_links", new JSONObject()
                        .put("group4507", new JSONObject().
                                put("href", "http://localhost/groups/4507"))
                        .put("group4508", new JSONObject().
                                put("href", "http://localhost/groups/4508"))
                        .put("group4501", new JSONObject().
                                put("href", "http://localhost/groups/4501")));

        // Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }

}