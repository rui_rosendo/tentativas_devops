package project.controllers.web;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.dto.CreateTransactionInfoDTO;
import project.model.specifications.repositories.LedgerRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateTransactionForGroupRESTControllerIT extends AbstractTest {

    @Autowired
    LedgerRepository ledgerRepository;

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * Test for method createTransactionForGroup
     * Happy Case - Transaction successfully created and added to group's ledger
     */
    @Transactional
    @Test
    void createTransactionForGroupHappyCaseTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8101@switch.pt/groups/8101/transactions";
        String expectedContent1 = "{\"transactions\":[]}";
        int expectedStatus1 = 200;

        //Act
        MvcResult mvcResult1 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //STAGE 2 - Testing post method (the content and status returned)

        //Arrange
        String uri = "/people/8101@switch.pt/groups/8101/transactions";

        String amount = "2595.20";
        String dateTime = "2000-01-01 11:30:30";
        String type = "Debit";
        String description = "jantar switch";
        String categoryDesignation = "comida";
        String debitAccountID = "8101";
        String creditAccountID = "8102";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String inputJson = super.mapToJson(infoDTO);

        String expectedContent2 = "{\"groupID\":\"8101\",\"groupDescription\":\"Grupo de amigos do ISEP\"," +
                "\"amount\":\"2595.20\",\"type\":\"Debit\",\"transactionDescription\":\"jantar switch\"," +
                "\"_links\":{\"group_transactions\":{\"href\":\"http://localhost/people/8101@switch.pt/groups/8101/transactions" +
                "\"}}}";
        int expectedStatus2 = 201;

        //Act
        MvcResult mvcResult2 = this.mvc.perform(MockMvcRequestBuilders.post(uri).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        String actualContent2 = mvcResult2.getResponse().getContentAsString();
        int actualStatus2 = mvcResult2.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent2, actualContent2);
        assertEquals(expectedStatus2, actualStatus2);

        //STAGE 3 - Verify if the new added transaction was saved in database

        //Arrange
        String expectedContent3 = "{\"transactions\":[{\"amount\":\"2595.2\",\"dateTime\":\"2000-01-01T11:30:30\",\"type\":\"-1\",\"description\":\"jantar switch\",\"category\":\"comida\",\"debitAccountID\":\"8101\",\"creditAccountID\":\"8102\"}]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);

    }

    /**
     * Test for method createTransactionForGroup
     * Transaction not created - Group doesn't exist in group repository
     */
    @Transactional
    @Test
    void createTransactionForGroupCaseGroupNotInRepoTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8101@switch.pt/groups/8101/transactions";
        String expectedContent1 = "{\"transactions\":[]}";
        int expectedStatus1 = 200;

        //Act
        MvcResult mvcResult1 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //STAGE 2 - Testing post method (the content and status returned)

        //Arrange
        String uri = "/people/8101@switch.pt/groups/8150/transactions";

        String amount = "2595.20";
        String dateTime = "2000-01-01 11:30:30";
        String type = "Debit";
        String description = "jantar switch";
        String categoryDesignation = "comida";
        String debitAccountID = "8101";
        String creditAccountID = "8102";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String inputJson = super.mapToJson(infoDTO);

        String expectedContent2 = "{\"status\":\"NOT_FOUND\",\"message\":\"Group not found\"," +
                "\"errors\":[\"Group not found\"]}";
        int expectedStatus2 = 404;

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        String actualContent2 = mvcResult.getResponse().getContentAsString();
        int actualStatus2 = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent2, actualContent2);
        assertEquals(expectedStatus2, actualStatus2);

        //STAGE 3 - Verify if the new added transaction was saved in database

        //Arrange
        String expectedContent3 = "{\"transactions\":[]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);
    }

    /**
     * Test for method createTransactionForGroup
     * Transaction not created - Ledger doesn't exist in ledger repository
     */
    @Transactional
    @Test
    void createTransactionForGroupCaseLedgerNotInRepoTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8102@switch.pt/groups/8102/transactions";
        String expectedContent1 = "{\"status\":\"NOT_FOUND\",\"message\":\"Ledger not found\",\"errors\":[\"Ledger " +
                "not found\"]}";
        int expectedStatus1 = 404;

        //Act
        MvcResult mvcResult1 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //STAGE 2 - Testing post method (the content and status returned)

        //Arrange
        String uri = "/people/8102@switch.pt/groups/8102/transactions";

        String amount = "2595.20";
        String dateTime = "2000-01-01 11:30:30";
        String type = "Debit";
        String description = "jantar switch";
        String categoryDesignation = "comida";
        String debitAccountID = "8105";
        String creditAccountID = "8102";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String inputJson = super.mapToJson(infoDTO);

        String expectedContent2 = "{\"status\":\"NOT_FOUND\",\"message\":\"Ledger not found\"," +
                "\"errors\":[\"Ledger not found\"]}";
        int expectedStatus2 = 404;

        //Act
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        String actualContent2 = mvcResult2.getResponse().getContentAsString();
        int actualStatus2 = mvcResult2.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent2, actualContent2);
        assertEquals(expectedStatus2, actualStatus2);

        //STAGE 3 - Verify if the new added transaction was saved in database

        //Arrange
        String expectedContent3 = "{\"status\":\"NOT_FOUND\",\"message\":\"Ledger not found\",\"errors\":[\"Ledger " +
                "not found\"]}";
        int expectedStatus3 = 404;

        //Act
        MvcResult mvcResult3 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);
    }

    /**
     * Test for method createTransactionForGroup
     * Transaction not created - Person is not member of the group
     */
    @Transactional
    @Test
    void createTransactionForGroupCasePersonIsNotMemberOfTheGroupTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8102@switch.pt/groups/8101/transactions";
        String expectedContent1 = "{\"transactions\":[]}";
        int expectedStatus1 = 200;

        //Act
        MvcResult mvcResult1 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //STAGE 2 - Testing post method (the content and status returned)

        //Arrange
        String uri = "/people/8102@switch.pt/groups/8101/transactions";

        String amount = "2595.20";
        String dateTime = "2000-01-01 11:30:30";
        String type = "Debit";
        String description = "jantar switch";
        String categoryDesignation = "comida";
        String debitAccountID = "8105";
        String creditAccountID = "8102";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String inputJson = super.mapToJson(infoDTO);

        String expectedContent2 = "{\"status\":\"UNPROCESSABLE_ENTITY\",\"message\":\"Person is not member of the " +
                "group\",\"errors\":[\"Person is not member of the group\"]}";
        int expectedStatus2 = 422;

        //Act
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        String actualContent2 = mvcResult2.getResponse().getContentAsString();
        int actualStatus2 = mvcResult2.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent2, actualContent2);
        assertEquals(expectedStatus2, actualStatus2);

        //STAGE 3 - Verify if the new added transaction was saved in database

        //Arrange
        String expectedContent3 = "{\"transactions\":[]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);
    }

    /**
     * Test for method createTransactionForGroup
     * Transaction not created - Debit account doesn't exist in account repository
     */
    @Transactional
    @Test
    void createTransactionForGroupCaseDebitAccountNotInRepoTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8101@switch.pt/groups/8101/transactions";
        String expectedContent1 = "{\"transactions\":[]}";
        int expectedStatus1 = 200;

        //Act
        MvcResult mvcResult1 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //STAGE 2 - Testing post method (the content and status returned)

        //Arrange
        String uri = "/people/8101@switch.pt/groups/8101/transactions";

        String amount = "2595.20";
        String dateTime = "2000-01-01 11:30:30";
        String type = "Credit";
        String description = "jantar switch";
        String categoryDesignation = "comida";
        String debitAccountID = "8150";
        String creditAccountID = "8102";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String inputJson = super.mapToJson(infoDTO);

        String expectedContent2 = "{\"status\":\"NOT_FOUND\",\"message\":\"Debit account not found\"," +
                "\"errors\":[\"Debit account not found\"]}";
        int expectedStatus2 = 404;

        //Act
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        String actualContent2 = mvcResult2.getResponse().getContentAsString();
        int actualStatus2 = mvcResult2.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent2, actualContent2);
        assertEquals(expectedStatus2, actualStatus2);

        //STAGE 3 - Verify if the new added transaction was saved in database

        //Arrange
        String expectedContent3 = "{\"transactions\":[]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);
    }

    /**
     * Test for method createTransactionForGroup
     * Transaction not created - Credit account doesn't exist in account repository
     */
    @Transactional
    @Test
    void createTransactionForGroupCaseCreditAccountNotInRepoTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8101@switch.pt/groups/8101/transactions";
        String expectedContent1 = "{\"transactions\":[]}";
        int expectedStatus1 = 200;

        //Act
        MvcResult mvcResult1 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //STAGE 2 - Testing post method (the content and status returned)

        //Arrange
        String uri = "/people/8101@switch.pt/groups/8101/transactions";

        String amount = "2595.20";
        String dateTime = "2000-01-01 11:30:30";
        String type = "Debit";
        String description = "jantar switch";
        String categoryDesignation = "comida";
        String debitAccountID = "8102";
        String creditAccountID = "8150";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String inputJson = super.mapToJson(infoDTO);

        String expectedContent2 = "{\"status\":\"NOT_FOUND\",\"message\":\"Credit account not found\"," +
                "\"errors\":[\"Credit account not found\"]}";
        int expectedStatus2 = 404;

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        String actualContent2 = mvcResult.getResponse().getContentAsString();
        int actualStatus2 = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent2, actualContent2);
        assertEquals(expectedStatus2, actualStatus2);

        //STAGE 3 - Verify if the new added transaction was saved in database

        //Arrange
        String expectedContent3 = "{\"transactions\":[]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);
    }

    /**
     * Test for method createTransactionForGroup
     * Transaction not created - Transaction's category is not in group's categories list
     */
    @Transactional
    @Test
    void createTransactionForGroupCaseCategoryIsNotInListOfCategoriesOfGroupTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8101@switch.pt/groups/8101/transactions";
        String expectedContent1 = "{\"transactions\":[]}";
        int expectedStatus1 = 200;

        //Act
        MvcResult mvcResult1 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //STAGE 2 - Testing post method (the content and status returned)

        //Arrange
        String uri = "/people/8101@switch.pt/groups/8101/transactions";

        String amount = "2595.20";
        String dateTime = "2000-01-01 11:30:30";
        String type = "Debit";
        String description = "jantar switch";
        String categoryDesignation = "casa";
        String debitAccountID = "8102";
        String creditAccountID = "8103";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String inputJson = super.mapToJson(infoDTO);

        String expectedContent2 = "{\"status\":\"NOT_FOUND\",\"message\":\"Category not found\"," +
                "\"errors\":[\"Category not found\"]}";
        int expectedStatus2 = 404;

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        String actualContent2 = mvcResult.getResponse().getContentAsString();
        int actualStatus2 = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent2, actualContent2);
        assertEquals(expectedStatus2, actualStatus2);

        //STAGE 3 - Verify if the new added transaction was saved in database

        //Arrange
        String expectedContent3 = "{\"transactions\":[]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);
    }

    /**
     * Test for method createTransactionForGroup
     * Transaction not created - Debit account not owned by the group when transaction type is debit
     */
    @Transactional
    @Test
    void createTransactionForGroupCaseDebitAccountNotOwnedByGroupTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8101@switch.pt/groups/8101/transactions";
        String expectedContent1 = "{\"transactions\":[]}";
        int expectedStatus1 = 200;

        //Act
        MvcResult mvcResult1 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //STAGE 2 - Testing post method (the content and status returned)

        //Arrange
        String uri = "/people/8101@switch.pt/groups/8101/transactions";

        String amount = "2595.20";
        String dateTime = "2000-01-01 11:30:30";
        String type = "Debit";
        String description = "jantar switch";
        String categoryDesignation = "comida";
        String debitAccountID = "8103";
        String creditAccountID = "8102";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String inputJson = super.mapToJson(infoDTO);

        String expectedContent2 = "{\"status\":\"NOT_FOUND\",\"message\":\"Debit account is not owned by " +
                "the group\",\"errors\":[\"Debit account is not owned by the group\"]}";
        int expectedStatus2 = 404;

        //Act
        MvcResult mvcResult2 = mvc.perform(MockMvcRequestBuilders.post(uri).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        String actualContent2 = mvcResult2.getResponse().getContentAsString();
        int actualStatus2 = mvcResult2.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent2, actualContent2);
        assertEquals(expectedStatus2, actualStatus2);

        //STAGE 3 - Verify if the new added transaction was saved in database

        //Arrange
        String expectedContent3 = "{\"transactions\":[]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);
    }

    /**
     * Test for method createTransactionForGroup
     * Transaction not created - Credit account not owned by the group when transaction type is credit
     */
    @Transactional
    @Test
    void createTransactionForGroupCaseCreditAccountNotOwnedByGroupTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8101@switch.pt/groups/8101/transactions";
        String expectedContent1 = "{\"transactions\":[]}";
        int expectedStatus1 = 200;

        //Act
        MvcResult mvcResult1 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //STAGE 2 - Testing post method (the content and status returned)

        //Arrange
        String uri = "/people/8101@switch.pt/groups/8101/transactions";

        String amount = "2595.20";
        String dateTime = "2000-01-01 11:30:30";
        String type = "Credit";
        String description = "jantar switch";
        String categoryDesignation = "comida";
        String debitAccountID = "8102";
        String creditAccountID = "8105";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String inputJson = super.mapToJson(infoDTO);

        String expectedContent2 = "{\"status\":\"NOT_FOUND\",\"message\":\"Credit account is not owned by " +
                "the group\",\"errors\":[\"Credit account is not owned by the group\"]}";
        int expectedStatus2 = 404;

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        int actualStatus2 = mvcResult.getResponse().getStatus();
        String actualContent2 = mvcResult.getResponse().getContentAsString();

        //Assert
        assertEquals(expectedStatus2, actualStatus2);
        assertEquals(expectedContent2, actualContent2);

        //STAGE 3 - Verify if the new added transaction was saved in database

        //Arrange
        String expectedContent3 = "{\"transactions\":[]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);
    }

    /**
     * Test for method createTransactionForGroup
     * Transaction not created - Type field is invalid
     */
    @Transactional
    @Test
    void createTransactionForGroupCaseInvalidTypeFieldTest() throws Exception {

        //STAGE 1 - Verify if the group's transaction list is empty at test beginning

        //Arrange
        String uriGetTransactions = "/people/8101@switch.pt/groups/8101/transactions";
        String expectedContent1 = "{\"transactions\":[]}";
        int expectedStatus1 = 200;

        //Act
        MvcResult mvcResult1 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent1 = mvcResult1.getResponse().getContentAsString();
        int actualStatus1 = mvcResult1.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent1, actualContent1);
        assertEquals(expectedStatus1, actualStatus1);

        //Arrange
        String uri = "/people/8101@switch.pt/groups/8101/transactions";

        String amount = "2595.20";
        String dateTime = "2000-01-01 11:30:30";
        String type = "Credittt";
        String description = "jantar switch";
        String categoryDesignation = "comida";
        String debitAccountID = "8105";
        String creditAccountID = "8102";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String inputJson = super.mapToJson(infoDTO);

        String expectedContent2 = "{\"status\":\"BAD_REQUEST\",\"message\":\"Invalid field\",\"errors\":[\"Invalid " +
                "field\"]}";
        int expectedStatus2 = 400;

        //Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType
                (MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
        String actualContent2 = mvcResult.getResponse().getContentAsString();
        int actualStatus2 = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent2, actualContent2);
        assertEquals(expectedStatus2, actualStatus2);

        //STAGE 3 - Verify if the new added transaction was saved in database

        //Arrange
        String expectedContent3 = "{\"transactions\":[]}";
        int expectedStatus3 = 200;

        //Act
        MvcResult mvcResult3 = this.mvc.perform(MockMvcRequestBuilders.get(uriGetTransactions)).andReturn();
        String actualContent3 = mvcResult3.getResponse().getContentAsString();
        int actualStatus3 = mvcResult3.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent3, actualContent3);
        assertEquals(expectedStatus3, actualStatus3);
    }

}