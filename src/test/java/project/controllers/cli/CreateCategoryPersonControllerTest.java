//package project.controllers;
//
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import project.model.domainentities.person.Person;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//class CreateCategoryPersonControllerTest {
//
//    private CreateCategoryPersonController control;
//    private Person rui;
//
//    @BeforeEach
//    public void init() {
//        control = new CreateCategoryPersonController();
//        rui = new Person("Rui", "Porto", "Porto", "1999-02-02", null, null);
//    }
//
//    /**
//     * Test for a person create a category controller
//     * Happy case
//     */
//    @Test
//    void createCategoryPersonHappyCaseTest() {
//        //Arrange
//        String expected = "CategoryRepository{categories=[Category{designation=Designation{designation='bills'}}]}";
//
//        //Act
//        boolean result = control.addCategoryToPerson(rui, repository, "bills");
//        String actual = repository.toString();
//
//        //Assert
//        assertTrue(result);
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for a person create a category controller
//     * False if try to add a category already existent
//     */
//    @Test
//    void createCategoryPersonNotTrueTest() {
//        //Arrange
//        control.addCategoryToPerson(rui, repository, "bills");
//        String expected = "CategoryRepository{categories=[Category{designation=Designation{designation='bills'}}]}";
//
//        //Act
//        boolean result = control.addCategoryToPerson(rui, repository, "bills");
//        String actual = repository.toString();
//
//        //Assert
//        assertFalse(result);
//        assertEquals(expected, actual);
//    }
//
//}