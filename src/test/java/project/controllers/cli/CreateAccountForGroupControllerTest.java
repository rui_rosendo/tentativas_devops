package project.controllers.cli;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.CreateAccountForGroupResponseDTO;
import project.exceptions.AccountAlreadyExistsException;
import project.exceptions.GroupConflictException;
import project.exceptions.GroupNotFoundException;
import project.frameworkddd.IUS007Service;
import project.model.entities.account.Account;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
class CreateAccountForGroupControllerTest {

    @Autowired
    GroupRepository groupRepo;
    @Autowired
    AccountRepository accountRepo;
    @Autowired
    IUS007Service service;

    private GroupID groupID;
    private LedgerID ledgerID;
    private Group group;

    private AccountID newAccountID;
    private Account account;

    private CreateAccountForGroupController controller;
    private PersonID personID;

    @BeforeEach
    public void init() {

        String prefix = "777";
        controller = new CreateAccountForGroupController(service);
        groupID = new GroupID(prefix + "1");
        ledgerID = new LedgerID(prefix + "1");
        personID = new PersonID(prefix + "1@switch.pt");
        group = new Group(groupID, "family", "2019-12-20", personID, ledgerID);
        newAccountID = new AccountID(prefix + "1");
        account = new Account(newAccountID, "food", "food");

    }

    /**
     * Test for CreateAccountForGroupController Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor- Happy case")
    void CreateAccountForGroupControllerConstructorTest() {
        //ASSERT
        assertTrue(controller instanceof CreateAccountForGroupController);
    }

    /**
     * Test for createAccountForGroup
     * Happy Case
     * <p>
     * GroupRepository contains groupID
     * AccountRepository doesn't contain accountID
     * Group list of accounts doesn't contain accountID
     */
    @Test
    @DisplayName("Test for createAccountForGroup - Happy case")
    void createAccountForGroupHappyCaseTest() {
        //ARRANGE
        String groupID = "7771";
        String groupDescription = "family";
        String accountID = "7771";
        String accountDenomination = "food";
        String personID = "7771@switch.pt";
        groupRepo.save(group);

        CreateAccountForGroupResponseDTO expected = new CreateAccountForGroupResponseDTO(groupID, groupDescription, accountID, accountDenomination);

        //ACT
        CreateAccountForGroupResponseDTO result = controller.createAccountForGroup(accountID, groupID, accountDenomination, groupDescription, personID);
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for createAccountForGroup
     * Sad case
     * GroupRepository doesn't contain groupID
     * <p>
     * Has to return GroupNotFound Exception
     */
    @Test
    @DisplayName("Test for createAccountForGroup - GroupID doesn't exists in the group repository")
    void createAccountForGroupDontExistTest() {

        //ARRANGE
        String accountID = "7771";
        String groupID = "7772";
        String denomination = "denomination";
        String description = "description";
        String personID = "7001@switch.pt";

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupNotFoundException.class, () -> {
            controller.createAccountForGroup(accountID, groupID, denomination, description, personID);
        });
    }

    /**
     * Test for createAccountForGroup
     * Sad case
     * GroupRepository contains groupID
     * AccountRepository contains accountID
     * <p>
     * Has to return AccountAlreadyExists Exception
     */
    @Test
    @DisplayName("Test for createAccountForGroup - Account already exists in the accountRepository")
    void createAccountForGroupAccountAlreadyExistsInTheRepositoryTest() {
        //ARRANGE
        groupRepo.save(group);
        accountRepo.save(account);

        String accountID = "7771";
        String groupID = "7771";
        String denomination = "denomination";
        String description = "description";
        String personID = "7771@switch.pt";

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            controller.createAccountForGroup(accountID, groupID, denomination, description, personID);
        });
    }

    /**
     * Test for createAccountForGroup
     * Sad case
     * GroupRepository contains groupID
     * AccountRepository dont contains accountID
     * <p>
     * Has to return PersonIsNotManagerOfTheGroupException Exception
     */
    @Test
    @DisplayName("Test for createAccountForGroup - Person Not Manager of Group")
    void createAccountForGroupPersonNotManagerTest() {
        //ARRANGE

        groupRepo.save(group);

        String accountID = "7771";
        String groupID = "7771";
        String denomination = "denomination";
        String description = "description";
        String personID = "7772@switch.pt";

        //ACT
        //ASSERT
        Assertions.assertThrows(GroupConflictException.class, () -> {
            controller.createAccountForGroup(accountID, groupID, denomination, description, personID);
        });
    }
}