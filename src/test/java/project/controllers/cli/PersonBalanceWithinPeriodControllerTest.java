//package project.controllers;
//
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import project.model.domainentities.account.Account;
//import project.model.domainentities.ledger.Type;
//import project.model.domainentities.person.Person;
//import project.model.domainentities.shared.Category;
//import project.model.domainentities.shared.Period;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//
//class PersonBalanceWithinPeriodControllerTest {
//
//    /**
//     * Test for getBalanceBetweenDatesControllerTest
//     */
//    @Test
//    @DisplayName("Test for getBalanceBetweenDatesControllerTest - Ensure correct balance with some transactions within timeframe")
//    void personBalanceBetweenDatesControllerHappyCaseTest() {
//        //ARRANGE
//        PersonBalanceWithinPeriodController control = new PersonBalanceWithinPeriodController();
//        Person diogo = new Person("diogo", "rua do almada", "Porto", "1996-05-27", null, null);
//
//        Account smithsGroceriesAccount = new Account("Smith's Groceries", "Account just for Smith family's groceries");
//        Account zeCreditAccount = new Account("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        diogo.addAccount("Smith's Groceries", "Account just for Smith family's groceries");
//        diogo.addAccount("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//
//        Category newCategory = new Category("Groceries");
//        diogo.addCategory("Groceries");
//
//        diogo.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "food", newCategory, smithsGroceriesAccount, zeCreditAccount);
//        diogo.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "water", newCategory, smithsGroceriesAccount, zeCreditAccount);
//        diogo.addTransaction(50, Type.DEBIT, "2020-01-15 00:00:00", "food", newCategory, smithsGroceriesAccount, zeCreditAccount);
//
//        Period period = new Period("2020-01-05 00:00:00", "2020-01-20 00:00:00");
//
//        double expected = 50;
//
//        // ACT
//        double actual = control.getBalanceWithinPeriod(diogo, period);
//
//        //ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getBalanceBetweenDatesControllerTest
//     */
//    @Test
//    @DisplayName("Test for getBalanceBetweenDatesControllerTest - Ensure correct balance with some transactions within timeframe")
//    void personBalanceBetweenDatesControllerSadCaseTest() {
//        //ARRANGE
//        PersonBalanceWithinPeriodController control = new PersonBalanceWithinPeriodController();
//        Person diogo = new Person("diogo", "rua do almada", "Porto", "1996-05-27", null, null);
//
//        Account smithsGroceriesAccount = new Account("Smith's Groceries", "Account just for Smith family's groceries");
//        Account zeCreditAccount = new Account("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        diogo.addAccount("Smith's Groceries", "Account just for Smith family's groceries");
//        diogo.addAccount("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//
//        Category newCategory = new Category("Groceries");
//        diogo.addCategory("Groceries");
//
//        diogo.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "food", newCategory, smithsGroceriesAccount, zeCreditAccount);
//        diogo.addTransaction(100, Type.CREDIT, "2020-01-10 00:00:00", "water", newCategory, smithsGroceriesAccount, zeCreditAccount);
//        diogo.addTransaction(50, Type.DEBIT, "2020-01-15 00:00:00", "food", newCategory, smithsGroceriesAccount, zeCreditAccount);
//
//        Period period = new Period("2020-01-05 00:00:00", "2020-01-20 00:00:00");
//
//        double expected = 35;
//
//        // ACT
//        double actual = control.getBalanceWithinPeriod(diogo, period);
//
//        //ASSERT
//        assertNotEquals(expected, actual);
//    }
//
//    /**
//     * Test for getBalanceBetweenDatesControllerTest
//     * Negative Balance
//     */
//    @Test
//    @DisplayName("Test for getBalanceBetweenDatesControllerTest - Ensure correct negative balance with some transactions within timeframe")
//    void personBalanceBetweenDatesControllerNegativeBalanceTest() {
//        //ARRANGE
//        PersonBalanceWithinPeriodController control = new PersonBalanceWithinPeriodController();
//        Person joao = new Person("joao", "Santa Bárbara", "Estarreja", "1987-04-17", null, null);
//
//        Account smithsGroceriesAccount = new Account("Smith's Groceries", "Account just for Smith family's groceries");
//        Account zeCreditAccount = new Account("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        joao.addAccount("Smith's Groceries", "Account just for Smith family's groceries");
//        joao.addAccount("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//
//        Category newCategory = new Category("Groceries");
//        joao.addCategory("Groceries");
//
//        joao.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "food", newCategory, smithsGroceriesAccount, zeCreditAccount);
//        joao.addTransaction(50, Type.CREDIT, "2020-01-10 00:00:00", "water", newCategory, smithsGroceriesAccount, zeCreditAccount);
//        joao.addTransaction(100, Type.DEBIT, "2020-01-15 00:00:00", "food", newCategory, smithsGroceriesAccount, zeCreditAccount);
//
//        Period period = new Period("2020-01-05 00:00:00", "2020-01-20 00:00:00");
//
//        double expected = -50;
//
//        // ACT
//        double actual = control.getBalanceWithinPeriod(joao, period);
//
//        //ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getBalanceBetweenDatesControllerTest
//     * Border Case
//     */
//    @Test
//    @DisplayName("Test for getBalanceBetweenDatesControllerTest - Ensure correct balance with some transactions within timeframe - border case")
//    void personBalanceBetweenDatesControllerBorderCaseTest() {
//        //ARRANGE
//        PersonBalanceWithinPeriodController control = new PersonBalanceWithinPeriodController();
//        Person joao = new Person("joao", "Santa Bárbara", "Estarreja", "1987-04-17", null, null);
//
//        Account smithsGroceriesAccount = new Account("Smith's Groceries", "Account just for Smith family's groceries");
//        Account zeCreditAccount = new Account("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//        joao.addAccount("Smith's Groceries", "Account just for Smith family's groceries");
//        joao.addAccount("Zé & Associados", "Current account of the 'A Loja do Zé' grocery shop");
//
//        Category newCategory = new Category("Groceries");
//        joao.addCategory("Groceries");
//
//        joao.addTransaction(20, Type.DEBIT, "2020-01-01 00:00:00", "food", newCategory, smithsGroceriesAccount, zeCreditAccount);
//        joao.addTransaction(50, Type.CREDIT, "2020-01-10 00:00:00", "water", newCategory, smithsGroceriesAccount, zeCreditAccount);
//        joao.addTransaction(100, Type.DEBIT, "2020-01-15 00:00:00", "food", newCategory, smithsGroceriesAccount, zeCreditAccount);
//
//        Period period = new Period("2020-01-10 00:00:00", "2020-01-15 00:00:00");
//
//        double expected = -50;
//
//        // ACT
//        double actual = control.getBalanceWithinPeriod(joao, period);
//
//        //ASSERT
//        assertEquals(expected, actual);
//    }
//
//    /**
//     * Test for getBalanceBetweenDatesControllerTest
//     * Ensure 0 if no transactions
//     */
//    @Test
//    @DisplayName("Test for getBalanceBetweenDatesControllerTest - Ensure 0 if no transactions")
//    void personBalanceBetweenDatesControllerEnsure0BalanceTest() {
//        //ARRANGE
//        PersonBalanceWithinPeriodController control = new PersonBalanceWithinPeriodController();
//        Person joao = new Person("joao", "Santa Bárbara", "Estarreja", "1987-04-17", null, null);
//
//        Period period = new Period("2020-01-10 00:00:00", "2020-01-15 00:00:00");
//
//        double expected = 0;
//
//        // ACT
//        double actual = control.getBalanceWithinPeriod(joao, period);
//
//        //ASSERT
//        assertEquals(expected, actual);
//    }
//
//}