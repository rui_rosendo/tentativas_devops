package project.controllers.cli;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import project.ProjectApplication;
import project.dto.CheckIfPeopleAreSiblingsResponseDTO;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS001Service;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = ProjectApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CheckIfPeopleAreSiblingsControllerTest {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    LedgerRepository ledgerRepository;

    @Autowired
    IUS001Service service;

    private CheckIfPeopleAreSiblingsController controller;

    @BeforeAll
    private void init() {
        controller = new CheckIfPeopleAreSiblingsController(service);
    }

    /**
     * Person1ID 10005 and Person1ID 10006 have the same mother.
     * Ensures it returns true when two people have the same mother.
     * Expected content true.
     */
    @Test
    @DisplayName("Ensure true with same mother test")
    void ensureTrueWithSameMotherTest() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO expected = new CheckIfPeopleAreSiblingsResponseDTO("true");

        //Act
        CheckIfPeopleAreSiblingsResponseDTO result = controller.checkPeopleAreSiblings("10005@switch.pt", "10006@switch.pt");

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Person1ID 10005 and Person1ID 10007 have the same father.
     * Ensures it returns true when two people have the same father.
     * Expected content true.
     */
    @Test
    @DisplayName("Ensure true with same father test")
    void ensureTrueWithSameFatherTest() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO expected = new CheckIfPeopleAreSiblingsResponseDTO("true");

        //Act
        CheckIfPeopleAreSiblingsResponseDTO result = controller.checkPeopleAreSiblings("10005@switch.pt", "10007@switch.pt");

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Person1ID 10006 and Person1ID 10007 are in each other's siblings list.
     * Ensures it returns true when two people are in each other's siblings list.
     * Expected content boolean true.
     */
    @Test
    @DisplayName("Ensure true by siblings list: Person2 is present in Person1 Siblings list")
    void ensureTrueBySiblingsListTest() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO expected = new CheckIfPeopleAreSiblingsResponseDTO("true");

        //Act
        CheckIfPeopleAreSiblingsResponseDTO result = controller.checkPeopleAreSiblings("10006@switch.pt", "10007@switch.pt");

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Person1ID 10007 and Person1ID 10006 are in each other's siblings list.
     * Ensures it returns true when two people are in each other's siblings list,
     * whatever the order of the parameters inputted (reversing the order).
     * Expected content boolean true.
     */
    @Test
    @DisplayName("Ensure true by siblings list: Person1 is present in Person2 Siblings list")
    void ensureTrueByReciprocalSiblingsListTest() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO expected = new CheckIfPeopleAreSiblingsResponseDTO("true");

        //Act
        CheckIfPeopleAreSiblingsResponseDTO result = controller.checkPeopleAreSiblings("10007@switch.pt", "10006@switch.pt");

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Ensures it returns false when two people are not in each other's siblings list,
     * don´t have the same father and don´t have the same mother.
     * Expected content boolean false.
     */
    @Test
    @DisplayName("Ensure false by mother and father and siblings list: people don't have the same mother, father or are present in the siblings list")
    void ensureFalseByMotherAndFatherAndSiblingsListTest() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO expected = new CheckIfPeopleAreSiblingsResponseDTO("false");

        //Act
        CheckIfPeopleAreSiblingsResponseDTO result = controller.checkPeopleAreSiblings("10007@switch.pt", "10008@switch.pt");

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Assert PersonNotFoundException is thrown if person at least one of the person not in the Person Repository.
     */
    @Test
    @DisplayName("Ensure fails when person not found in repository")
    void ensureFailsWhenPersonNotFoundTest() {
        //Arrange
        //Act
        //Assert
        assertThrows(PersonNotFoundException.class, () -> {
            CheckIfPeopleAreSiblingsResponseDTO result = controller.checkPeopleAreSiblings("10007@switch.pt", "10009@switch.pt");
        });
    }

    /**
     * True if control is instance of PersonsAreSiblingController
     */
    @Test
    @DisplayName("Ensure true class instance of")
    void ensureTrueClassInstanceOfTest() {
        //Assert
        assertTrue(controller instanceof CheckIfPeopleAreSiblingsController);
    }

}