package project.controllers.cli;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import project.ProjectApplication;
import project.application.services.CreateCategoryForGroupService;
import project.dto.CreateCategoryForGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;
import project.exceptions.CategoryAlreadyExistsException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonIsNotManagerOfTheGroupException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest(classes = ProjectApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateCategoryForGroupControllerTest {

    @Autowired
    CreateCategoryForGroupController controller;


    /**
     * createCategoryForGroup - Happy Case
     */
    @Test
    @DisplayName("Test for createCategoryForGroup - Happy Case")
    public void createCategoryForGroupHappyCaseTest() {

        //ARRANGE
        String personID = "51010@switch.pt";
        String groupID = "510200";
        String designation = "Bolas";
        String groupDescription = "Futebol";


        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO(personID, groupID, designation);

        CreateCategoryForGroupResponseDTO expected = new CreateCategoryForGroupResponseDTO(groupID, groupDescription, designation);
        CreateCategoryForGroupService mockService = Mockito.mock(CreateCategoryForGroupService.class);
        Mockito.when(mockService.createCategoryForGroup(requestDTO)).thenReturn(expected);

        //ACT
        CreateCategoryForGroupResponseDTO result = controller.createCategoryForGroup(personID, groupID, designation);

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for CreateCategoryForGroup
     * Ensure CategoryAlreadyExistsException is throw when added a Category that already exists
     */
    @Test
    @DisplayName("Ensure CategoryAlreadyExistsException is throw when added a Category that already exists")
    void createCategoryForGroupWithCategoryExistentInTheGroupTest() {

        String personID = "51010@switch.pt";
        String groupID = "510200";
        String designation = "Arbitros";
        String groupDescription = "Futebol";


        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO(personID, groupID, designation);

        CreateCategoryForGroupResponseDTO expected = new CreateCategoryForGroupResponseDTO(groupID, groupDescription, designation);
        CreateCategoryForGroupService mockService = Mockito.mock(CreateCategoryForGroupService.class);
        Mockito.when(mockService.createCategoryForGroup(requestDTO)).thenReturn(expected);

        //Act
        //Assert
        assertThrows(CategoryAlreadyExistsException.class, () -> {
            controller.createCategoryForGroup("51010@switch.pt", "510200", "Arbitros");
        });
    }

    /**
     * Test for CreateCategoryForGroup
     * Ensure GroupNotFoundException exception is throw when addCategory to a groupID that is not in groupRepository
     */
    @Test
    @DisplayName("Ensure GroupNotFoundException exception is throw when addCategory to a groupID that is not in groupRepository")
    void createCategoryForGroupWithGroupIDNotInGroupRepositoryTest() {

        String personID = "51010@switch.pt";
        String groupID = "510100";
        String designation = "Bolas";
        String groupDescription = "Futebol";


        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO(personID, groupID, designation);

        CreateCategoryForGroupResponseDTO expected = new CreateCategoryForGroupResponseDTO(groupID, groupDescription, designation);
        CreateCategoryForGroupService mockService = Mockito.mock(CreateCategoryForGroupService.class);
        Mockito.when(mockService.createCategoryForGroup(requestDTO)).thenReturn(expected);

        //Act
        //Assert
        assertThrows(GroupNotFoundException.class, () -> {
            controller.createCategoryForGroup("51010@switch.pt", "510100", "Bolas");
        });

    }

    /**
     * Test for CreateCategoryForGroup
     * Ensure PersonIsNotManagerOfTheGroupException is throw the person isn´t manager of the group
     */
    @Test
    @DisplayName("Ensure PersonIsNotManagerOfTheGroupException is throw the person isn´t manager of the group")
    void createCategoryForGroupWithPersonIsNotManagerOfTheGroupTest() {

        String personID = "51020@switch.pt";
        String groupID = "510200";
        String designation = "Bolas";
        String groupDescription = "Futebol";


        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO(personID, groupID, designation);

        CreateCategoryForGroupResponseDTO expected = new CreateCategoryForGroupResponseDTO(groupID, groupDescription, designation);
        CreateCategoryForGroupService mockService = Mockito.mock(CreateCategoryForGroupService.class);
        Mockito.when(mockService.createCategoryForGroup(requestDTO)).thenReturn(expected);

        //Act
        //Assert
        assertThrows(PersonIsNotManagerOfTheGroupException.class, () -> {
            controller.createCategoryForGroup("51020@switch.pt", "510200", "Bolas");
        });

    }
}
