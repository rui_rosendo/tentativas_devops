package project.controllers.cli;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GetFamilyGroupsDTO;
import project.frameworkddd.IUS004Service;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class GetFamilyGroupsControllerTest {

    @Autowired
    private IUS004Service service;

    @Autowired
    private GetFamilyGroupsController controller;

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor - Happy case")
    void constructorHappyCaseTest() {
        // Arrange
//        GetFamilyGroupsController controller = new GetFamilyGroupsController(service);
//
        // Act
        // Assert
        assertTrue(controller instanceof GetFamilyGroupsController);
    }

    /**
     * Test for getFamilyGroups
     * Ensure works with repository with family and non-family groups
     */
    @Test
    @DisplayName("Test for getFamilyGroups - Ensure works with repository with family and non-family groups")
    void getFamilyEnsureWorksWithRepositoryWithFamilyAndNonFamilyGroupsTest() {
        // Arrange
//        GetFamilyGroupsService service = new GetFamilyGroupsService(groupRepositoryDB, isFamilyService);
//        GetFamilyGroupsController controller = new GetFamilyGroupsController(service);

        Set<String> expectedString = new HashSet<>();
        expectedString.add("400301");
        expectedString.add("400361");
        expectedString.add("400371");

        GetFamilyGroupsDTO expected = new GetFamilyGroupsDTO(expectedString);
        Mockito.when(service.getFamilyGroups()).thenReturn(expected);
        // Act
        GetFamilyGroupsDTO actual = controller.getFamilyGroups();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getFamilyGroups
     * Ensure works with repository with only non-family groups
     */
    @Test
    @DisplayName("Test for getFamilyGroups - Ensure works with repository with only non-family groups")
    public void getFamilyGroupsEnsureWorksWithRepositoryWithOnlyNonFamilyGroupsTestJpa() {
        // Arrange
        Set<String> expectedString = new HashSet<>();
        GetFamilyGroupsDTO outputDTOExpected = new GetFamilyGroupsDTO(expectedString);

        Mockito.when(service.getFamilyGroups()).thenReturn(outputDTOExpected);

        //ACT
        GetFamilyGroupsDTO outputDTO = controller.getFamilyGroups();

        //ASSERT
        assertEquals(outputDTOExpected, outputDTO);
    }


}