package project.controllers.cli;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import project.ProjectApplication;
import project.dto.GetFamilyGroupsDTO;
import project.frameworkddd.IUS004Service;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = ProjectApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GetFamilyGroupsControllerIT {

    @Autowired
    IUS004Service service;

    @Autowired
    GetFamilyGroupsController controller;

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor - Happy case")
    void constructorHappyCaseTest() {
        // Arrange
        // Act
        // Assert
        assertTrue(controller instanceof GetFamilyGroupsController);
    }

    /**
     * Test for getFamilyGroups
     * Ensure works with repository with family and non-family groups
     */
    @Test
    @DisplayName("Test for getFamilyGroups - Ensure works with repository with family and non-family groups")
    void getFamilyEnsureWorksWithRepositoryWithFamilyAndNonFamilyGroupsTest() {
        // Arrange
        Set<String> expectedString = new HashSet<>();
        expectedString.add("4507");
        expectedString.add("4508");
        expectedString.add("4501");

        GetFamilyGroupsDTO expected = new GetFamilyGroupsDTO(expectedString);
        // Act
        GetFamilyGroupsDTO actual = controller.getFamilyGroups();

        // Assert
        assertEquals(expected, actual);
    }
}