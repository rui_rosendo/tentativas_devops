package project.controllers.cli;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import project.dto.CreateAccountForPersonRequestDTO;
import project.dto.CreateAccountForPersonResponseDTO;
import project.exceptions.AccountAlreadyExistsException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS006Service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
@ActiveProfiles("test")
class CreateAccountForPersonControllerTest {

    @Autowired
    CreateAccountForPersonController control;

    @Autowired
    IUS006Service service;

    /**
     * Test for CreateAccountForPersonController Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor- Happy case")
    void CreateAccountForPersonControllerConstructorTest() {
        //ASSERT
        assertTrue(control instanceof CreateAccountForPersonController);
    }

    /**
     * Test for createAccountForPerson
     * Happy Case
     */
    @Test
    @DisplayName("Test for createAccountForPerson - Happy case")
    void createAccountForPersonHappyCaseTest() {
        //ARRANGE
        String personID = "6664";
        String personName = "T-800, Terminator";
        String accountID = "66607";
        String denomination = "Clothes";
        String description = "Timetravel suits";

        CreateAccountForPersonRequestDTO requestDTO = new CreateAccountForPersonRequestDTO(accountID, personID, denomination, description);
        CreateAccountForPersonResponseDTO expected = new CreateAccountForPersonResponseDTO(personID, personName, accountID, denomination);

        Mockito.when(service.createAccountForPerson(requestDTO)).thenReturn(expected);

        //ACT
        CreateAccountForPersonResponseDTO result = control.createAccountForPerson(accountID, personID, denomination, description);
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for createAccountForPerson
     * PersonID doesn't exist in the database
     */
    @Test
    @DisplayName("Test for createAccountForPerson - PersonID doesn't exist in the database")
    void createAccountPersonDoesntExistTest() {
        //ARRANGE
        String personID = "66617";
        String accountID = "66608";
        String denomination = "Clothes";
        String description = "Timetravel suits";

        CreateAccountForPersonRequestDTO requestDTO = new CreateAccountForPersonRequestDTO(accountID, personID, denomination, description);

        Mockito.when(service.createAccountForPerson(requestDTO)).thenThrow(PersonNotFoundException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            control.createAccountForPerson(accountID, personID, denomination, description);
        });
    }

    /**
     * Test for createAccountForPerson
     * Account already exist in the database
     */
    @Test
    @DisplayName("Test for createAccountForPerson - Account already exist in the database")
    void createAccountPersonAccountAlreadyExistsInTheRepositoryTest() {
        //ARRANGE
        String accountID = "66609";
        String denomination = "Clothes";
        String description = "Timetravel suits";
        String personID = "66603";

        CreateAccountForPersonRequestDTO requestDTO = new CreateAccountForPersonRequestDTO(accountID, personID, denomination, description);

        Mockito.when(service.createAccountForPerson(requestDTO)).thenThrow(AccountAlreadyExistsException.class);

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            control.createAccountForPerson(accountID, personID, denomination, description);
        });
    }

}