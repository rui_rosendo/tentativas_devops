package project.controllers.cli;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.CreateAccountForPersonResponseDTO;
import project.exceptions.AccountAlreadyExistsException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS006Service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
class CreateAccountForPersonControllerIT {

    @Autowired
    IUS006Service service;

    @Autowired
    CreateAccountForPersonController control;

    /**
     * Test for CreateAccountForPersonController Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor- Happy case")
    void CreateAccountForPersonControllerConstructorTest() {
        //ASSERT
        assertTrue(control instanceof CreateAccountForPersonController);
    }

    /**
     * Test for createAccountForPerson
     * Happy Case
     */
    @Test
    @DisplayName("Test for createAccountForPerson - Happy case")
    void createAccountForPersonHappyCaseTest() {
        //ARRANGE
        String expectedPersonID = "6661@switch.pt";
        String expectedPersonName = "Rambo, John";
        String expectedAccountID = "66607";
        String expectedDenomination = "rations";

        CreateAccountForPersonResponseDTO expected = new CreateAccountForPersonResponseDTO(expectedPersonID, expectedPersonName, expectedAccountID, expectedDenomination);
        //ACT
        CreateAccountForPersonResponseDTO result = control.createAccountForPerson("66607", "6661@switch.pt", "rations", "food");
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for createAccountForPerson
     * PersonID doesn't exist in the person repository
     */
    @Test
    @DisplayName("Test for createAccountForPerson - PersonID doesn't exist in the person repository")
    void createAccountPersonDoesntExistTest() {
        //ARRANGE
        String accountID = "66607";
        String personID = "6666@switch.pt";
        String denomination = "rations";
        String description = "food";

        //ACT
        //ASSERT
        Assertions.assertThrows(PersonNotFoundException.class, () -> {
            control.createAccountForPerson(accountID, personID, denomination, description);
        });
    }

    /**
     * Test for createAccountForPerson
     * Account already exists in the database
     */
    @Test
    @DisplayName("Test for createAccountForPerson - Account already exists in the database")
    void createAccountPersonAccountAlreadyExistsInTheRepositoryTest() {
        //ARRANGE
        String accountID = "66601";
        String personID = "6665@switch.pt";
        String denomination = "rations";
        String description = "food";

        //ACT
        //ASSERT
        Assertions.assertThrows(AccountAlreadyExistsException.class, () -> {
            control.createAccountForPerson(accountID, personID, denomination, description);
        });
    }

}