package project.controllers.cli;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.ProjectApplication;
import project.dto.AddMemberRequestDTO;
import project.dto.AddMemberResponseDTO;
import project.exceptions.GroupNotFoundException;
import project.exceptions.MemberAlreadyExistsException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS003Service;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.PersonRepository;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest(classes = ProjectApplication.class)
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AddMemberToGroupControllerTest {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    IUS003Service service;

    @Autowired
    AddMemberToGroupController controller;


    /**
     * Test for constructor
     * Happy Case
     */
    @Test
    @DisplayName("Test for AddMemberToGroupServiceTest Constructor - HappyCase")
    void AddMemberGroupControllerHappyCaseTest() {
        // Assert
        assertTrue(controller instanceof AddMemberToGroupController);
    }

    /**
     * Test for addMemberToGroup
     * Happy case
     */
    @Test
    @DisplayName("Test for addMember - HappyCase")
    void addMemberToGroupHappyCaseTest() {
        //Arrange
        String newMemberID = "111";
        String groupID = "4444";

        AddMemberRequestDTO requestDTO = new AddMemberRequestDTO(newMemberID, groupID);
        AddMemberResponseDTO expected = new AddMemberResponseDTO(groupID, "Grupo da família Alves", newMemberID);

        Mockito.when(service.addMember(requestDTO)).thenReturn(expected);

        //Act
        AddMemberResponseDTO result = controller.addMemberToGroup(newMemberID, groupID);

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for AddMemberToGroup
     * <p>
     * Ensure exception is thrown when adding creator ID again in group
     */
    @Test
    void addMemberToGroupAddingCreatorAgainCaseTest() {
        String newMemberID = "333";
        String groupID = "4444";

        AddMemberRequestDTO requestDTO = new AddMemberRequestDTO(newMemberID, groupID);
        AddMemberResponseDTO expected = new AddMemberResponseDTO(groupID, "Grupo da família Alves", newMemberID);

        Mockito.when(service.addMember(requestDTO)).thenThrow(MemberAlreadyExistsException.class);

        assertThrows(MemberAlreadyExistsException.class, () -> {
            controller.addMemberToGroup(newMemberID, groupID);

        });

    }

    /**
     * Test for addMemberToGroup
     * <p>
     * Ensure exception is thrown when MemberID already present in group
     */
    @Test
    void addMemberToGroupAddingMemberIDAlreadyPresentCaseTest() {
        //Arrange
        String newMemberID = "222";
        String groupID = "4444";

        AddMemberRequestDTO requestDTO = new AddMemberRequestDTO(newMemberID, groupID);

        Mockito.when(service.addMember(requestDTO)).thenThrow(MemberAlreadyExistsException.class);

        assertThrows(MemberAlreadyExistsException.class, () -> {
            controller.addMemberToGroup(newMemberID, groupID);
        });
    }


    /**
     * Test for addMemberToGroup
     * Ensure exception is thrown when a Person with given PersonID isn't found in repository case
     */
    @Test
    @DisplayName("Test for addMember - No person with given PersonID case")
    void addMemberToGroupNoPersonWithGivenIDCaseTest() {
        //Arrange
        String newMemberID = "33783";
        String groupID = "4444";

        AddMemberRequestDTO requestDTO = new AddMemberRequestDTO(newMemberID, groupID);

        Mockito.when(service.addMember(requestDTO)).thenThrow(PersonNotFoundException.class);

        //Act
        //Assert
        assertThrows(PersonNotFoundException.class, () -> {
            controller.addMemberToGroup(newMemberID, groupID);
        });
    }

    /**
     * Test for addMember
     * Ensure exception is thrown when a Group with given GroupID isn't found in repository case
     */
    @Test
    @DisplayName("Test for addMember - No group with given GroupID case")
    void addMemberToGroupNoGroupWithGivenIDCaseTest() {
        //Arrange
        String newMemberID = "333";
        String groupID = "1111";

        AddMemberRequestDTO requestDTO = new AddMemberRequestDTO(newMemberID, groupID);


        Mockito.when(service.addMember(requestDTO)).thenThrow(GroupNotFoundException.class);

        //Act
        //Assert
        assertThrows(GroupNotFoundException.class, () -> {
            controller.addMemberToGroup(newMemberID, groupID);
        });
    }
}