package project.controllers.cli;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.ProjectApplication;
import project.dto.CreateGroupResponseDTO;
import project.exceptions.GroupAlreadyExistsException;
import project.exceptions.GroupLedgerAlreadyExistsException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS002_1Service;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest(classes = ProjectApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateGroupControllerTest {

    @Autowired
    PersonRepository personRepo;

    @Autowired
    GroupRepository groupRepo;

    @Autowired
    LedgerRepository ledgerRepo;

    @Autowired
    IUS002_1Service service;

    private CreateGroupController controller;

    @BeforeAll
    private void init() {
        controller = new CreateGroupController(service);
    }

    /**
     * Test for createGroupController
     * Test constructor
     */
    @Test
    @DisplayName("Test constructor - createGroupController")
    void createGroupConstructorTest() {
        assertTrue(controller instanceof CreateGroupController);
    }

    /**
     * Test for createGroup method
     * <p>
     * Happy case
     */
    @Test
    @DisplayName("Test for createGroup method - Happy Case")
    void createGroupHappyCaseTest() {
        //Arrange
        CreateGroupResponseDTO expected = new CreateGroupResponseDTO("21019", "thisIsADescription");

        //Act
        CreateGroupResponseDTO result = controller.createGroup("21019", "thisIsADescription", "2020-05-27", "21001@switch.pt", "21039");

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for createGroup method
     * <p>
     * Ensure exception is thrown if creator ID doesn't exist in person repository
     */
    @Test
    @DisplayName("Ensure exception is thrown if creator ID doesn't exist in person repository")
    void createGroupEnsureExceptionIfCreatorDoesNotExistTest() {
        //Arrange
        //Act
        //Assert
        assertThrows(PersonNotFoundException.class, () -> {
            controller.createGroup("21019", "thisIsADescription", "2020-05-27", "21009@switch.pt", "21039");
        });
    }

    /**
     * Test for createGroup method
     * <p>
     * Ensure exception is thrown if group with all the same attributes (including ID) already exists
     * in the group repository
     */
    @Test
    @DisplayName("Ensure exception is thrown if group with all the same attributes (including ID) already exists")
    void createGroupEnsureExceptionIfGroupAlreadyExistsTest() {
        //Arrange
        //Act
        //Assert
        assertThrows(GroupAlreadyExistsException.class, () -> {
            controller.createGroup("21011", "Switchadas", "1987-04-17", "21001@switch.pt", "21031");
        });
    }

    /**
     * Test for createGroup method
     * <p>
     * Ensure an exception if group with different attributes but same ID already exists
     * in the group repository
     */
    @Test
    @DisplayName("Ensure an exception if group with different attributes but same ID already exists")
    void createGroupEnsureExceptionIfGroupWithSameIDAlreadyExistsTest() {
        //Arrange
        //Act
        //Assert
        assertThrows(GroupAlreadyExistsException.class, () -> {
            controller.createGroup("21011", "Switchadas2", "2020-05-27", "21002@switch.pt", "21038");
        });
    }

    /**
     * Test for createGroup method
     * <p>
     * Ensure an exception if ledger ID already exists in the ledger repository
     */
    @Test
    @DisplayName("Ensure an exception if ledger ID already exists in the ledger repository")
    void createGroupEnsureExceptionIfLedgerIDAlreadyExistsTest() {
        //Arrange
        //Act
        //Assert
        assertThrows(GroupLedgerAlreadyExistsException.class, () -> {
            controller.createGroup("21018", "Switchadas3", "2020-05-27", "21001@switch.pt", "21031");
        });
    }
}