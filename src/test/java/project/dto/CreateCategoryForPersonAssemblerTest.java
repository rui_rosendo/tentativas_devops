package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CreateCategoryForPersonAssemblerTest {
    /**
     * Test for CreateCategoryForPersonAssembler Constructor
     * Happy Case
     */
    @Test
    @DisplayName("Test for CreateCategoryForPersonAssembler Constructor - Happy Case")
    void createCategoryForPersonAssemblerConstructorTest() {
        CreateCategoryForPersonAssembler assembler = new CreateCategoryForPersonAssembler();
        assertTrue(assembler instanceof CreateCategoryForPersonAssembler);
    }

    /**
     * Test for mapToRequestDTO
     * Happy case
     */
    @Test
    @DisplayName("Test for mapToRequestDT0 - Happy Case")
    void mapToRequestDTOHappyCaseTest() {

        //ARRANGE
        CreateCategoryForPersonRequestDTO expected = new CreateCategoryForPersonRequestDTO("99@switch.pt", "isep");

        //ACT
        CreateCategoryForPersonRequestDTO result = CreateCategoryForPersonAssembler.mapToRequestDTO("99@switch.pt", "isep");

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for mapToResponseDTO
     * Happy case
     */
    @Test
    @DisplayName("Test for mapToResponseDTO - Happy Case - POST")
    void mapToResponseDTOHappyCaseTest() {

        //ARRANGE
        CreateCategoryForPersonResponseDTO expected = new CreateCategoryForPersonResponseDTO("99@switch.pt", "switch");

        //ACT
        CreateCategoryForPersonResponseDTO result = CreateCategoryForPersonAssembler.mapToResponseDTO("99@switch.pt", "switch");

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for mapToResponseDTO
     * Happy case
     */
    @Test
    @DisplayName("Test for mapToResponseDTO - Happy Case - GET")
    void mapToResponseDTO1HappyCaseTest() {

        //ARRANGE
        PersonID personID = new PersonID("1@switch.pt");
        PersonID motherPersonID = new PersonID("2@switch.pt");
        PersonID fatherPersonID = new PersonID("3@switch.pt");
        LedgerID ledgerID = new LedgerID("1");

        Person person = new Person(personID, "Manuel", "Rua Santa Catarina 88",
                "Vila Real", "1994-12-11",
                motherPersonID, fatherPersonID, ledgerID);

        person.addCategory("isep");
        person.addCategory("switch");

        Set<String> list = new HashSet<>();
        list.add("isep");
        list.add("switch");
        CategoriesDTO expected = new CategoriesDTO(list);


        //ACT
        CategoriesDTO result = CreateCategoryForPersonAssembler.mapToResponseDTO(person);

        //ASSERT
        assertEquals(expected, result);
    }
}