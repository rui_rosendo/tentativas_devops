package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreateGroupRequestDTOTest {

    private String groupID;
    private String description;
    private String creationDate;
    private String creatorID;
    private String groupLedgerID;

    @BeforeEach
    void init() {
        groupID = "1L";
        description = "description";
        creationDate = "2000-01-01";
        creatorID = "1L";
        groupLedgerID = "1L";
    }

    /**
     * Test for CreateGroupRequestDTO constructor
     */
    @Test
    void constructorTest() {
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        assertTrue(requestDTO instanceof CreateGroupRequestDTO);
    }

    /**
     * Test for getGroupID method
     * Happy Case
     */
    @Test
    void getGroupIDHappyCaseTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String expected = "1L";

        //Act
        String result = requestDTO.getGroupID();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getGroupID method
     * Ensure not equals
     */
    @Test
    void getGroupIDNotEqualsTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String expected = "2L";

        //Act
        String result = requestDTO.getGroupID();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getDescription method
     * Happy Case
     */
    @Test
    void getDescriptionHappyCaseTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String expected = "description";

        //Act
        String result = requestDTO.getDescription();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getDescription method
     * Ensure not equals
     */
    @Test
    void getDescriptionNotEqualsTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String expected = "different description";

        //Act
        String result = requestDTO.getDescription();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getCreationDate method
     * Happy Case
     */
    @Test
    void getCreationDateHappyCaseTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String expected = "2000-01-01";

        //Act
        String result = requestDTO.getCreationDate();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getCreationDate method
     * Ensure not equals
     */
    @Test
    void getCreationDateNotEqualsTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String expected = "1900-01-01";

        //Act
        String result = requestDTO.getCreationDate();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getCreatorID method
     * Happy Case
     */
    @Test
    void getCreatorIDHappyCaseTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String expected = "1L";

        //Act
        String result = requestDTO.getCreatorEmail();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getCreatorID method
     * Ensure Not Equals
     */
    @Test
    void getCreatorIDNotEqualsTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String expected = "2L";

        //Act
        String result = requestDTO.getCreatorEmail();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getGroupLedgerID method
     * Happy Case
     */
    @Test
    void getGroupLedgerIDHappyCaseTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String expected = "1L";

        //Act
        String result = requestDTO.getGroupLedgerID();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getGroupLedgerID method
     * Ensure not equals
     */
    @Test
    void getGroupLedgerIDNotEqualsTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String expected = "2L";

        //Act
        String result = requestDTO.getGroupLedgerID();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for equals
     * Ensure true when object is the same
     */
    @Test
    void testEqualsEnsureTrueWithSameObjectTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);

        //Act
        boolean result = requestDTO.equals(requestDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false when comparing different class objects
     */
    @Test
    void testEqualsEnsureFalseDifferentClassTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String test = "2";

        //Act
        boolean result = requestDTO.equals(test);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when comparing with a null object
     */
    @Test
    void testEqualsEnsureFalseWithNullTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String test = null;

        //Act
        boolean result = requestDTO.equals(test);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Hapy Case - 2 different objects with same attributes
     */
    @Test
    void testEqualsHappyCaseTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        CreateGroupRequestDTO requestDTO2 = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);

        //Act
        boolean result = requestDTO.equals(requestDTO2);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false when groupId is different
     */
    @Test
    void testEqualsEnsureFalseDifferentGroupIDTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String differentGroupID = "5L";
        CreateGroupRequestDTO requestDTO2 = new CreateGroupRequestDTO(differentGroupID, description, creationDate, creatorID, groupLedgerID);

        //Act
        boolean result = requestDTO.equals(requestDTO2);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when description is different
     */
    @Test
    void testEqualsEnsureFalseDifferentDescriptionTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String differentDescription = "different description";
        CreateGroupRequestDTO requestDTO2 = new CreateGroupRequestDTO(groupID, differentDescription, creationDate, creatorID, groupLedgerID);

        //Act
        boolean result = requestDTO.equals(requestDTO2);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when creationDate is different
     */
    @Test
    void testEqualsEnsureFalseDifferentCreationDateTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String differentCreationDate = "1500-01-01";
        CreateGroupRequestDTO requestDTO2 = new CreateGroupRequestDTO(groupID, description, differentCreationDate, creatorID, groupLedgerID);

        //Act
        boolean result = requestDTO.equals(requestDTO2);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when creatorID is different
     */
    @Test
    void testEqualsEnsureFalseDifferentCreatorIDTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String differentCreatorID = "5L";
        CreateGroupRequestDTO requestDTO2 = new CreateGroupRequestDTO(groupID, description, creationDate, differentCreatorID, groupLedgerID);

        //Act
        boolean result = requestDTO.equals(requestDTO2);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false when groupLedgerID is different
     */
    @Test
    void testEqualsEnsureFalseDifferentGroupLedgerIDTest() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        String differentGroupLedgerID = "5L";
        CreateGroupRequestDTO requestDTO2 = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, differentGroupLedgerID);

        //Act
        boolean result = requestDTO.equals(requestDTO2);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        CreateGroupRequestDTO requestDTO = new CreateGroupRequestDTO(groupID, description, creationDate, creatorID, groupLedgerID);
        int expected = -28822724;

        //Act
        int result = requestDTO.hashCode();

        //Assert
        assertEquals(expected, result);
    }
}