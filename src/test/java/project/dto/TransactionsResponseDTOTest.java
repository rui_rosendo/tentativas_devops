package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.GroupID;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class TransactionsResponseDTOTest {

    TransactionDTO transactionDTO1;
    TransactionDTO transactionDTO2;
    List<TransactionDTO> transactionsDTOs;

    @BeforeEach
    void init() {
        transactionDTO1 = new TransactionDTO("1000", "Debit", "1991-22-12 00:00:00", "diapers", "baby stuff", "1", "2");
        transactionDTO2 = new TransactionDTO("50", "Debit", "1991-22-12 00:00:00", "milk", "baby stuff", "1", "2");
        transactionsDTOs = new ArrayList<>();
        transactionsDTOs.add(transactionDTO1);
        transactionsDTOs.add(transactionDTO1);
    }

    /**
     * Test for TransactionsDTO constructor
     */
    @Test
    @DisplayName("Test for TransactionsDTO constructor")
    void constructorHappyCaseTest() {
        List<TransactionDTO> expected = new ArrayList<>();
        TransactionsResponseDTO dto = new TransactionsResponseDTO(expected);
        //ASSERT
        assertTrue(dto instanceof TransactionsResponseDTO);
    }

    /**
     * Test for getAccounts
     * Happy case
     */
    @Test
    @DisplayName(" Test for getAccounts - Happy Case")
    void getAccountsHappyCaseTest() {
        //ARRANGE
        TransactionsResponseDTO dto = new TransactionsResponseDTO(transactionsDTOs);

        //ACT
        List<TransactionDTO> result = dto.getTransactions();

        //ASSERT
        assertEquals(transactionsDTOs, result);
    }

    /**
     * Test for equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - HappyCase")
    void equalsHappyCaseTest() {
        //Arrange
        TransactionsResponseDTO dto1 = new TransactionsResponseDTO(transactionsDTOs);
        TransactionsResponseDTO dto2 = new TransactionsResponseDTO(transactionsDTOs);

        //Act
        boolean result = dto1.equals(dto2);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        TransactionsResponseDTO dto1 = new TransactionsResponseDTO(transactionsDTOs);
        TransactionsResponseDTO dto2 = null;

        //Act
        boolean result = dto1.equals(dto2);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with object from another class")
    void equalsEnsureFalseWithObjectFromAnotherClassTest() {
        //Arrange
        TransactionsResponseDTO dto = new TransactionsResponseDTO(transactionsDTOs);
        GroupID groupID = new GroupID("1");

        //Act
        boolean result = dto.equals(groupID);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true if is same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //Arrange
        Set<String> expected = new HashSet<>();
        TransactionsResponseDTO dto = new TransactionsResponseDTO(transactionsDTOs);

        //Act
        boolean result = dto.equals(dto);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure works")
    void hashCodeEnsureWorksTest() {
        //Arrange
        TransactionsResponseDTO dto = new TransactionsResponseDTO(transactionsDTOs);
        int expected = 1052;

        //Act
        int result = dto.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for toString
     * Ensure works
     */
    @Test
    @DisplayName("Test for toString - Ensure works")
    void toStringEnsureWorksTest() {
        //Arrange
        TransactionsResponseDTO dto = new TransactionsResponseDTO(transactionsDTOs);
        String expected = "TransactionsResponseDTO{transactions=[TransactionDTO{amount='1000', dateTime='1991-22-12 00:00:00', type='Debit', description='diapers', category='baby stuff', debitAccountID='1', creditAccountID='2'}, TransactionDTO{amount='1000', dateTime='1991-22-12 00:00:00', type='Debit', description='diapers', category='baby stuff', debitAccountID='1', creditAccountID='2'}]}";

        //Act
        String result = dto.toString();

        //Assert
        assertEquals(expected, result);
    }

}