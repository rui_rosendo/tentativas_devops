package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CreateCategoryForPersonResponseDTOTest {
    /**
     * Test for CreateCategoryForPersonResponseDTO constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryForPersonResponseDTO constructor - Happy case")
    void constructorCreateCategoryForPersonResponseDTOHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonResponseDTO dto = new CreateCategoryForPersonResponseDTO("6", "switch");

        //ACT
        //ASSERT
        assertTrue(dto instanceof CreateCategoryForPersonResponseDTO);
    }

    /**
     * Test for getPersonID
     * Happy case
     */
    @Test
    @DisplayName("Test for getPersonID - Happy case")
    void getPersonIDHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonResponseDTO dto = new CreateCategoryForPersonResponseDTO("1", "switch");
        String expected = "1";

        //ACT
        String result = dto.getPersonID();

        //ASSERT
        assertEquals(expected, result);
    }


    /**
     * Test for getNewCategory
     * Happy case
     */
    @Test
    @DisplayName("Test for geNewCategory - Happy case")
    void getNewCategoryHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonResponseDTO dto = new CreateCategoryForPersonResponseDTO("1", "switch");
        String expected = "switch";

        //ACT
        String result = dto.designation();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for Equals
     * Same object
     */
    @Test
    @DisplayName("Test for equals - Same object")
    void equalsHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonResponseDTO dto = new CreateCategoryForPersonResponseDTO("1", "switch");

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Two Equal Objects
     */
    @Test
    @DisplayName("Test for equals - Two Equal Objects")
    void equalsTwoEqualObjectsHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonResponseDTO dto = new CreateCategoryForPersonResponseDTO("1", "switch");
        CreateCategoryForPersonResponseDTO dto2 = new CreateCategoryForPersonResponseDTO("1", "switch");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Not Equals personID Test
     */
    @Test
    @DisplayName("Test for equals - personID Not Equals")
    void notEqualsPersonIDTest() {
        //ARRANGE
        CreateCategoryForPersonResponseDTO dto = new CreateCategoryForPersonResponseDTO("1", "switch");
        CreateCategoryForPersonResponseDTO dto2 = new CreateCategoryForPersonResponseDTO("2", "isep");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }


    /**
     * Test for Equals
     * Not Equals newCategory Test
     */
    @Test
    @DisplayName("Test for equals - newCategory Not Equals")
    void notEqualsNewCategoryTest() {
        //ARRANGE
        CreateCategoryForPersonResponseDTO dto = new CreateCategoryForPersonResponseDTO("1", "switch");
        CreateCategoryForPersonResponseDTO dto2 = new CreateCategoryForPersonResponseDTO("1", "isep");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * null object
     */
    @Test
    @DisplayName("Test for equals - null ")
    void equalsNullTest() {
        //ARRANGE
        CreateCategoryForPersonResponseDTO dto = new CreateCategoryForPersonResponseDTO("1", "switch");

        //ACT
        boolean result = dto.equals(null);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * different object
     */
    @Test
    @DisplayName("Test for equals - different Object ")
    void equalsDifferentObjectTest() {
        //ARRANGE
        CreateCategoryForPersonResponseDTO requestDTO = new CreateCategoryForPersonResponseDTO("1", "switch");

        Set<String> categories = new HashSet<>();
        categories.add("equipamentos");
        CategoriesDTO dto = new CategoriesDTO(categories);
        //ACT
        boolean result = requestDTO.equals(dto);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for hashCode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hashCode - Happy Case ")
    void hashCodeHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonResponseDTO dto = new CreateCategoryForPersonResponseDTO("1", "switch");

        int expected = -889470748;

        //ACT
        int result = dto.hashCode();

        //ASSERT
        assertEquals(expected, result);
    }

}