package project.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreateTransactionInfoDTOTest {

    /**
     * Test for CreateTransactionForGroupInfoDTO constructor
     */
    @Test
    void constructorTest() {
        //Arrange
        String amount = "amount";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime,
                type, description, categoryDesignation, debitAccountID, creditAccountID);

        //Act
        //Assert
        assertTrue(infoDTO instanceof CreateTransactionInfoDTO);
    }

    /**
     * Test for getAmount method
     * Happy Case
     */
    @Test
    void getAmountHappyCaseTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String expected = "1000";

        //Act
        String actual = infoDTO.getAmount();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getAmount method
     * Ensure not equals
     */
    @Test
    void getAmountEnsureNotEqualsTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String expected = "2000";

        //Act
        String actual = infoDTO.getAmount();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getDateTime method
     * Happy Case
     */
    @Test
    void getDateTimeHappyCaseTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime,
                type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "2000-01-01 00 00 00";

        //Act
        String actual = infoDTO.getDateTime();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getDateTime method
     * Ensure not equals
     */
    @Test
    void getDateTimeEnsureNotEqualsTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime,
                type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "1900-01-01 00 00 00";

        //Act
        String actual = infoDTO.getDateTime();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getType method
     * Happy Case
     */
    @Test
    void getTypeHappyCaseTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String expected = "1";

        //Act
        String actual = infoDTO.getType();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getType method
     * Ensure not equals
     */
    @Test
    void getTypeEnsureNotEqualsTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String expected = "-1";

        //Act
        String actual = infoDTO.getType();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getDescription method
     * Happy Case
     */
    @Test
    void getDescriptionHappyCaseTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String expected = "description";

        //Act
        String actual = infoDTO.getDescription();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getDescription method
     * Ensure not equals
     */
    @Test
    void getDescriptionEnsureNotEqualsTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String expected = "different description";

        //Act
        String actual = infoDTO.getDescription();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getCategoryDesignation method
     * Happy Case
     */
    @Test
    void getCategoryDesignationHappyCaseTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String expected = "categoryDesignation";

        //Act
        String actual = infoDTO.getCategoryDesignation();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getCategoryDesignation method
     * Ensure not equals
     */
    @Test
    void getCategoryDesignationEnsureNotEqualsTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String expected = "different categoryDesignation";

        //Act
        String actual = infoDTO.getCategoryDesignation();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getDebitAccountID method
     * Happy Case
     */
    @Test
    void getDebitAccountIDHappyCaseTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime,
                type, description, categoryDesignation, debitAccountID, creditAccountID);

        String expected = "1000";

        //Act
        String actual = infoDTO.getDebitAccountID();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getDebitAccountID method
     * Ensure not equals
     */
    @Test
    void getDebitAccountIDEnsureNotEqualsTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String expected = "9999";

        //Act
        String actual = infoDTO.getDebitAccountID();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getCreditAccountID method
     * Happy Case
     */
    @Test
    void getCreditAccountIDHappyCaseTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String expected = "2000";

        //Act
        String actual = infoDTO.getCreditAccountID();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getCreditAccountID method
     * Ensure not equals
     */
    @Test
    void getCreditAccountIDEnsureNotEqualsTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String expected = "1000";

        //Act
        String actual = infoDTO.getCreditAccountID();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for equals method
     * Ensure true when comparing the same object
     */
    @Test
    void testEqualsEnsureTrueWhenComparingSameObjectTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        //Act
        boolean actual = infoDTO.equals(infoDTO);

        //Assert
        assertTrue(actual);
    }

    /**
     * Test for equals method
     * Ensure false when other object is null
     */
    @Test
    void testEqualsEnsureFalseWhenOtherObjectIsNullTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        CreateTransactionInfoDTO otherInfoDTO = null;

        //Act
        boolean actual = infoDTO.equals(otherInfoDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when other object is from different class
     */
    @Test
    void testEqualsEnsureFalseWhenOtherObjectIsFromDifferentClassTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String otherRequestDTO = "other";

        //Act
        boolean actual = infoDTO.equals(otherRequestDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure true when all attributes are equals
     */
    @Test
    void testEqualsEnsureTrueWhenAllAttributesAreEqualsTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        CreateTransactionInfoDTO otherInfoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        //Act
        boolean actual = infoDTO.equals(otherInfoDTO);

        //Assert
        assertTrue(actual);
    }

    /**
     * Test for equals method
     * Ensure false when amount is different
     */
    @Test
    void testEqualsEnsureFalseWhenAmountIsDifferentTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionInfoDTO otherInfoDTO =
                new CreateTransactionInfoDTO(differentAttribute, dateTime, type, description, categoryDesignation,
                        debitAccountID, creditAccountID);

        //Act
        boolean actual = infoDTO.equals(otherInfoDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when dateTime is different
     */
    @Test
    void testEqualsEnsureFalseWhenDateTimeIsDifferentTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionInfoDTO otherInfoDTO =
                new CreateTransactionInfoDTO(amount, differentAttribute, type, description, categoryDesignation,
                        debitAccountID, creditAccountID);

        //Act
        boolean actual = infoDTO.equals(otherInfoDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when type is different
     */
    @Test
    void testEqualsEnsureFalseWhenTypeIsDifferentTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionInfoDTO otherInfoDTO =
                new CreateTransactionInfoDTO(amount, dateTime, differentAttribute, description, categoryDesignation,
                        debitAccountID, creditAccountID);

        //Act
        boolean actual = infoDTO.equals(otherInfoDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when description is different
     */
    @Test
    void testEqualsEnsureFalseWhenDescriptionIsDifferentTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionInfoDTO otherInfoDTO =
                new CreateTransactionInfoDTO(amount, dateTime, type, differentAttribute, categoryDesignation,
                        debitAccountID, creditAccountID);

        //Act
        boolean actual = infoDTO.equals(otherInfoDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when categoryDesignation is different
     */
    @Test
    void testEqualsEnsureFalseWhenCategoryDesignationIsDifferentTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionInfoDTO otherInfoDTO =
                new CreateTransactionInfoDTO(amount, dateTime, type, description, differentAttribute, debitAccountID,
                        creditAccountID);

        //Act
        boolean actual = infoDTO.equals(otherInfoDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when debitAccountID is different
     */
    @Test
    void testEqualsEnsureFalseWhenDebitAccountIDIsDifferentTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionInfoDTO otherInfoDTO =
                new CreateTransactionInfoDTO(amount, dateTime, type, description, categoryDesignation,
                        differentAttribute, creditAccountID);

        //Act
        boolean actual = infoDTO.equals(otherInfoDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when creditAccountID is different
     */
    @Test
    void testEqualsEnsureFalseWhenCreditAccountIDIsDifferentTest() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        String differentAttribute = "different attribute";

        CreateTransactionInfoDTO otherInfoDTO =
                new CreateTransactionInfoDTO(amount, dateTime, type, description, categoryDesignation, debitAccountID
                        , differentAttribute);

        //Act
        boolean actual = infoDTO.equals(otherInfoDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for hashCode method
     */
    @Test
    void testHashCode() {
        //Arrange
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";
        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        int expected = -1579475159;

        //Act
        int actual = infoDTO.hashCode();

        //Assert
        assertEquals(expected, actual);
    }

}