package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.GroupID;

import static org.junit.jupiter.api.Assertions.*;

class CreateCategoryForGroupInfoDTOTest {

    /**
     * Test for CreateCategoryForGroupInfoDTO constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryForGroupInfoDTO constructor - Happy case")
    void constructorCreateCategoryForGroupInfoDTOHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupInfoDTO dto = new CreateCategoryForGroupInfoDTO("Futebol");
        //ACT
        //ASSERT
        assertTrue(dto instanceof CreateCategoryForGroupInfoDTO);
    }

    /**
     * Test for getDesignation
     * Happy case
     */
    @Test
    @DisplayName("Test for getDesignation - Happy case")
    void getDesignationHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupInfoDTO dto = new CreateCategoryForGroupInfoDTO("Futebol");
        String expected = "Futebol";

        //ACT
        String result = dto.getDesignation();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for Equals
     * Happy case - same object
     */
    @Test
    @DisplayName("Test for Equals - Happy case")
    void equalsHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupInfoDTO dto = new CreateCategoryForGroupInfoDTO("Futebol");

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Happy case - same attributes
     */
    @Test
    @DisplayName("Test for Equals - Same attributes")
    void equalsHappyCaseSameAttributesTest() {
        //ARRANGE
        CreateCategoryForGroupInfoDTO dto = new CreateCategoryForGroupInfoDTO("Futebol");
        CreateCategoryForGroupInfoDTO outroDto = new CreateCategoryForGroupInfoDTO("Futebol");

        //ACT
        boolean result = dto.equals(outroDto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Null object
     */
    @Test
    @DisplayName("Test for Equals - Null Object")
    void equalsNullTest() {
        //ARRANGE
        CreateCategoryForGroupInfoDTO dto = new CreateCategoryForGroupInfoDTO("Futebol");

        //ACT
        boolean result = dto.equals(null);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Different object
     */
    @Test
    @DisplayName("Test for Equals - Different Object")
    void equalsDifferentObjectTest() {
        //ARRANGE
        CreateCategoryForGroupInfoDTO dto = new CreateCategoryForGroupInfoDTO("Futebol");
        GroupID groupID = new GroupID("111");

        //ACT
        boolean result = dto.equals(groupID);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for hashCode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hasCode - Happy Case")
    void hashCodeHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupInfoDTO dto = new CreateCategoryForGroupInfoDTO("Futebol");
        int expected = 1160593982;

        //ACT
        int result = dto.hashCode();

        //ASSERT
        assertEquals(expected, result);
    }
}