package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CreateCategoryForGroupAssemblerTest {

    /**
     * Test for CreateCategoryForGroupAssembler Constructor
     * Happy Case
     */
    @Test
    @DisplayName("Test for CreateCategoryForGroupAssembler Constructor - Happy Case")
    void createCategoryForGroupAssemblerConstructorTest() {
        CreateCategoryForGroupAssembler assembler = new CreateCategoryForGroupAssembler();
        assertTrue(assembler instanceof CreateCategoryForGroupAssembler);
    }

    /**
     * Test for mapToRequestDTO
     * Happy case
     */
    @Test
    @DisplayName("Test for mapToRequestDT0 - Happy Case")
    void mapToRequestDTOHappyCaseTest() {

        //ARRANGE
        CreateCategoryForGroupRequestDTO expected = new CreateCategoryForGroupRequestDTO("99@switch.pt", "12", "Equipamentos");

        //ACT
        CreateCategoryForGroupRequestDTO result = CreateCategoryForGroupAssembler.mapToRequestDTO("99@switch.pt", "12", "Equipamentos");

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for mapToResponseDTO
     * Happy case
     */
    @Test
    @DisplayName("Test for mapToResponseDTO - Happy Case - POST")
    void mapToResponseDTOHappyCaseTest() {

        //ARRANGE
        CreateCategoryForGroupResponseDTO expected = new CreateCategoryForGroupResponseDTO("99", "Futebol", "Equipamentos");

        //ACT
        CreateCategoryForGroupResponseDTO result = CreateCategoryForGroupAssembler.mapToResponseDTO("99", "Futebol", "Equipamentos");

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for mapToResponseDTO
     * Happy case
     */
    @Test
    @DisplayName("Test for mapToResponseDTO - Happy Case - GET")
    void mapToResponseDTO1HappyCaseTest() {

        //ARRANGE
        GroupID groupID = new GroupID("1");
        PersonID personID = new PersonID("1@switch.pt");
        LedgerID ledgerID = new LedgerID("1");

        Group group = new Group(groupID, "switch", "2018-12-25", personID, ledgerID);
        group.addCategory("Equipamentos");
        group.addCategory("Futebol");

        Set<String> list = new HashSet<>();
        list.add("Equipamentos");
        list.add("Futebol");
        CategoriesDTO expected = new CategoriesDTO(list);


        //ACT
        CategoriesDTO result = CreateCategoryForGroupAssembler.mapToResponseDTO(group);

        //ASSERT
        assertEquals(expected, result);
    }
}