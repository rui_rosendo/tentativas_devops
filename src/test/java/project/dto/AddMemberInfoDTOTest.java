package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AddMemberInfoDTOTest {

    /**
     * Test for AddMemberInfoDTOTest constructor
     * Happy case
     */
    @DisplayName("Test for constructor - Happy case")
    @Test
    void constructorHappyCaseTest() {
        // Arrange
        String newMemberID = "11";
        AddMemberInfoDTO dto = new AddMemberInfoDTO(newMemberID);

        // Assert
        assertTrue(dto instanceof AddMemberInfoDTO);
    }

    /**
     * Test for getNewMemberID
     */
    @Test
    @DisplayName("Test for getNewMemberID - Happy case")
    void getNewMemberIDHappyCaseTest() {
        // Arrange
        String newMemberID = "11";
        AddMemberInfoDTO dto = new AddMemberInfoDTO(newMemberID);
        String expected = "11";

        // Act
        String result = dto.getPersonEmail();
        // Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getNewMemberID
     */
    @Test
    @DisplayName("Test for getNewMemberID - Sad case")
    void getNewMemberIDSadCaseTest() {
        // Arrange
        String newMemberID = "11";
        AddMemberInfoDTO dto = new AddMemberInfoDTO(newMemberID);
        String expected = "33";

        // Act
        String result = dto.getPersonEmail();
        // Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for Equals
     * Happy case
     */
    @DisplayName("Test for Equals - Happy case")
    @Test
    void equalsHappyCaseTest() {
        //ARRANGE
        String expectedMemberID = "11";
        AddMemberInfoDTO expected = new AddMemberInfoDTO(expectedMemberID);

        String newMemberID = "11";
        AddMemberInfoDTO dto = new AddMemberInfoDTO(newMemberID);

        //ACT
        boolean result = dto.equals(expected);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Sad case
     */
    @DisplayName("Test for Equals - Ensure false with different personIDs")
    @Test
    void equalsDifferentPersonIDsCaseTest() {
        //ARRANGE
        String expectedMemberID = "11";

        AddMemberInfoDTO expected = new AddMemberInfoDTO(expectedMemberID);

        String newMemberID = "33";
        AddMemberInfoDTO dto = new AddMemberInfoDTO(newMemberID);

        //ACT
        boolean result = dto.equals(expected);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Sad case
     */
    @DisplayName("Test for Equals - Ensure false with null personID")
    @Test
    void equalsNullPersonIDCaseTest() {
        //ARRANGE
        String expectedMemberID = "11";
        AddMemberInfoDTO expected = new AddMemberInfoDTO(expectedMemberID);

        AddMemberInfoDTO dto = new AddMemberInfoDTO(null);

        //ACT
        boolean result = dto.equals(expected);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for Equals - Ensure false with null object")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        String newMemberID = "11";
        AddMemberInfoDTO dto = new AddMemberInfoDTO(newMemberID);

        MembersDTO dto2 = null;

        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different types of objects
     */
    @Test
    @DisplayName("Test for Equals - Ensure false with different types of objects")
    void equalsEnsureFalseWithDifferentTypesOfObjectsTest() {
        //Arrange
        String newMemberID = "11";
        AddMemberInfoDTO dto = new AddMemberInfoDTO(newMemberID);

        List<String> expectedMembersIDs = new ArrayList<>();
        List<String> expectedMembersNames = new ArrayList<>();
        List<Boolean> expectedManagers = new ArrayList<>();

        MembersDTO dto2 = new MembersDTO(expectedMembersIDs, expectedMembersNames, expectedManagers);

        //Act
        boolean result = dto.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Same object
     */
    @DisplayName("Test for Equals - Ensure true with same object")
    @Test
    void equalsSameObjectCaseTest() {
        //ARRANGE
        String newMemberID = "11";
        AddMemberInfoDTO dto = new AddMemberInfoDTO(newMemberID);

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure works")
    void hashCodeEnsureWorksTest() {
        //Arrange
        String memberID = "11";
        AddMemberInfoDTO dto = new AddMemberInfoDTO(memberID);

        int expected = 1599;
        //Act
        int result = dto.hashCode();
        //Assert
        assertEquals(expected, result);
    }

}