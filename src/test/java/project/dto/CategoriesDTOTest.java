package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.AccountID;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CategoriesDTOTest {

    /**
     * Test for CreateCategoryForGroupDTO constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryForGroupDTO constructor - Happy case")
    void constructorCreateCategoryForGroupDTOHappyCaseTest() {
        //ARRANGE
        Set<String> list = new HashSet<>();
        list.add("equipamentos");
        CategoriesDTO dto = new CategoriesDTO(list);
        //ACT
        //ASSERT
        assertTrue(dto instanceof CategoriesDTO);
    }

    /**
     * Test for getCategories
     * Happy case
     */
    @Test
    @DisplayName("Test for getCategories - Happy case")
    void getCategoriesHappyCaseTest() {
        //ARRANGE

        Set<String> expected = new HashSet<>();
        expected.add("equipamentos");
        expected.add("material");
        CategoriesDTO dto = new CategoriesDTO(expected);

        //ACT
        Set<String> result = dto.getCategories();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for Equals
     * Test for equals - Ensure true with same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //ARRANGE
        Set<String> categories = new HashSet<>();
        CategoriesDTO dto = new CategoriesDTO(categories);

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Ensure true with two objects with the same attribute
     */
    @Test
    @DisplayName("Test for equals - Ensure true with two objects with the same attribute ")
    void equalsEnsureTrueWithObjectsWithSameAttributesTest() {
        //ARRANGE
        Set<String> categories = new HashSet<>();
        categories.add("Futebol");
        CategoriesDTO dto = new CategoriesDTO(categories);
        CategoriesDTO dto1 = new CategoriesDTO(categories);

        //ACT
        boolean result = dto.equals(dto1);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Ensure false with different objects
     */
    @Test
    @DisplayName("Test for equals - Ensure false with different objects")
    void equalsEnsureFalseWithDifferentObjectsTest() {
        //ARRANGE
        Set<String> categories = new HashSet<>();
        categories.add("Futebol");
        Set<String> categories1 = new HashSet<>();
        categories.add("Comida");
        CategoriesDTO dto = new CategoriesDTO(categories);
        CategoriesDTO dto1 = new CategoriesDTO(categories1);

        //ACT
        boolean result = dto.equals(dto1);

        //ASSERT
        assertFalse(result);
    }


    /**
     * Test for Equals
     * null object
     */
    @Test
    @DisplayName("Test for equals - null ")
    void equalsNullTest() {

        //ARRANGE
        Set<String> categories = new HashSet<>();
        CategoriesDTO dto = new CategoriesDTO(categories);
        CategoriesDTO dto2 = null;

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * different object
     */
    @Test
    @DisplayName("Test for equals - different class Object ")
    void equalsDifferentObjectTest() {

        //ARRANGE
        Set<String> categories = new HashSet<>();
        CategoriesDTO dto = new CategoriesDTO(categories);

        AccountID accountID = new AccountID("11");

        //ACT
        boolean result = dto.equals(accountID);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for hashCode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hashCode - Happy Case ")
    void hashCodeHappyCaseTest() {

        //ARRANGE
        Set<String> categories = new HashSet<>();
        CategoriesDTO dto = new CategoriesDTO(categories);
        int expected = 31;

        //ACT
        int result = dto.hashCode();

        //ASSERT
        assertEquals(expected, result);
    }

}