package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CreateAccountForGroupAssemblerTest {

    private PersonID newPersonID;
    private GroupID newGroupID;
    private LedgerID newLedgerID;
    private AccountID newAccountID;


    @BeforeEach
    public void initialize() {
        newPersonID = new PersonID("12@switch.pt");
        newGroupID = new GroupID("765");
        newLedgerID = new LedgerID("765");
        newAccountID = new AccountID("758");

    }

    /**
     * Test for CreateAccountForGroupAssembler Constructor - Happy Case
     */
    @Test
    @DisplayName("Test for CreateAccountForGroupAssembler Constructor - Happy Case")
    void instanceOf() {
        CreateAccountForGroupAssembler assembler = new CreateAccountForGroupAssembler();
        assertTrue(assembler instanceof CreateAccountForGroupAssembler);
    }

    /**
     * Test for mapToDTO Method
     * Happy case
     */
    @Test
    void createDTOFromDomainObjectHappyCase() {

        //ARRANGE
        Group newGroup = new Group(newGroupID, "isep", "2020-05-21", newPersonID, newLedgerID);
        newGroup.addAccount(newAccountID);

        Set<String> expectedString = new HashSet<>();
        expectedString.add("758");

        CreateAccountForGroupDTO expected = new CreateAccountForGroupDTO(expectedString);
        expected.setAccountsIDs(expectedString);

        //ACT
        CreateAccountForGroupDTO result = CreateAccountForGroupAssembler.mapToDTO(newGroup);

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for mapToDTO Method
     * Sad case
     */
    @Test
    void createDTOFromDomainObjectSadCase() {

        //ARRANGE
        Group newGroup = new Group(newGroupID, "isep", "2020-05-21", newPersonID, newLedgerID);
        newGroup.addAccount(newAccountID);

        Set<String> expectedString = new HashSet<>();

        CreateAccountForGroupDTO expected = new CreateAccountForGroupDTO(expectedString);
        expected.setAccountsIDs(expectedString);

        //ACT
        CreateAccountForGroupDTO result = CreateAccountForGroupAssembler.mapToDTO(newGroup);

        //ASSERT
        assertNotEquals(expected, result);
    }

    /**
     * Test for  mapToDTORequest method
     * Happy Case
     */
    @Test
    void mapToDTORequestHappyCaseTest() {

        //ARRANGE
        String accountID = "12";
        String groupID = "123";
        String denomination = "gym ";
        String description = "pay gym bills";
        String personID = "12@switch.pt";

        CreateAccountForGroupRequestDTO requestDTO = new CreateAccountForGroupRequestDTO(accountID, groupID, denomination, description, personID);

        //ACT
        CreateAccountForGroupRequestDTO result = CreateAccountForGroupAssembler.mapToRequestDTO(accountID, groupID, denomination, description, personID);

        //ASSERT
        assertEquals(requestDTO, result);
    }

    /**
     * Test for  mapToDTORequest method
     * Sad case
     */
    @Test
    void mapToDTORequestSadCaseTest() {

        //ARRANGE
        String accountID = "12";
        String accountID2 = "42";
        String groupID = "123";
        String denomination = "gym ";
        String description = "pay gym bills";
        String personID = "12@switch.pt";

        CreateAccountForGroupRequestDTO requestDTO = new CreateAccountForGroupRequestDTO(accountID, groupID, denomination, description, personID);

        //ACT
        CreateAccountForGroupRequestDTO result = CreateAccountForGroupAssembler.mapToRequestDTO(accountID2, groupID, denomination, description, personID);

        //ASSERT
        assertNotEquals(requestDTO, result);
    }

}