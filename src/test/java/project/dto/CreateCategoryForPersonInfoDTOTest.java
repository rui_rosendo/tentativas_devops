package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.PersonID;

import static org.junit.jupiter.api.Assertions.*;

class CreateCategoryForPersonInfoDTOTest {

    /**
     * Test for CreateCategoryForPersonInfoDTO constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryForPersonInfoDTO constructor - Happy case")
    void constructorCreateCategoryForPersonInfoDTOHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonInfoDTO dto = new CreateCategoryForPersonInfoDTO("switch");
        //ACT
        //ASSERT
        assertTrue(dto instanceof CreateCategoryForPersonInfoDTO);
    }

    /**
     * Test for getDesignation
     * Happy case
     */
    @Test
    @DisplayName("Test for getDesignation - Happy case")
    void getDesignationHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonInfoDTO dto = new CreateCategoryForPersonInfoDTO("switch");
        String expected = "switch";

        //ACT
        String result = dto.getDesignation();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for Equals
     * Happy case - same object
     */
    @Test
    @DisplayName("Test for Equals - Happy case")
    void equalsHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonInfoDTO dto = new CreateCategoryForPersonInfoDTO("switch");

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Happy case - same attributes
     */
    @Test
    @DisplayName("Test for Equals - Same attributes")
    void equalsHappyCaseSameAttributesTest() {
        //ARRANGE
        CreateCategoryForPersonInfoDTO dto = new CreateCategoryForPersonInfoDTO("switch");
        CreateCategoryForPersonInfoDTO outroDto = new CreateCategoryForPersonInfoDTO("switch");

        //ACT
        boolean result = dto.equals(outroDto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Null object
     */
    @Test
    @DisplayName("Test for Equals - Null Object")
    void equalsNullTest() {
        //ARRANGE
        CreateCategoryForPersonInfoDTO dto = new CreateCategoryForPersonInfoDTO("switch");

        //ACT
        boolean result = dto.equals(null);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Different object
     */
    @Test
    @DisplayName("Test for Equals - Different Object")
    void equalsDifferentObjectTest() {
        //ARRANGE
        CreateCategoryForPersonInfoDTO dto = new CreateCategoryForPersonInfoDTO("switch");
        PersonID personID = new PersonID("111@switch.pt");

        //ACT
        boolean result = dto.equals(personID);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for hashCode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hasCode - Happy Case")
    void hashCodeHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonInfoDTO dto = new CreateCategoryForPersonInfoDTO("carros");
        int expected = -1367590495;

        //ACT
        int result = dto.hashCode();

        //ASSERT
        assertEquals(expected, result);
    }
}