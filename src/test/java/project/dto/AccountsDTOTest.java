package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.GroupID;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class AccountsDTOTest {

    AccountDTO accountDTO1;
    AccountDTO accountDTO2;
    List<AccountDTO> accountsDTOs;

    @BeforeEach
    void init() {
        accountDTO1 = new AccountDTO("blue pill", "red pill", "morpheus");
        accountDTO2 = new AccountDTO("red pill", "blue pill", "also morpheus");
        accountsDTOs = new ArrayList<>();
        accountsDTOs.add(accountDTO1);
        accountsDTOs.add(accountDTO2);

    }

    /**
     * Test for AccountsDTO constructor
     */
    @Test
    @DisplayName("Test for AccountsDTO constructor")
    void constructorHappyCaseTest() {
        List<AccountDTO> expected = new ArrayList<>();
        AccountsDTO dto = new AccountsDTO(expected);
        //ASSERT
        assertTrue(dto instanceof AccountsDTO);
    }

    /**
     * Test for getAccounts
     * Happy case
     */
    @Test
    @DisplayName(" Test for getAccounts - Happy Case")
    void getAccountsHappyCaseTest() {
        //ARRANGE
        AccountsDTO dto = new AccountsDTO(accountsDTOs);

        //ACT
        List<AccountDTO> result = dto.getAccounts();

        //ASSERT
        assertEquals(accountsDTOs, result);
    }

    /**
     * Test for equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - HappyCase")
    void equalsHappyCaseTest() {
        //Arrange
        AccountsDTO dto1 = new AccountsDTO(accountsDTOs);
        AccountsDTO dto2 = new AccountsDTO(accountsDTOs);

        //Act
        boolean result = dto1.equals(dto2);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        AccountsDTO dto1 = new AccountsDTO(accountsDTOs);
        AccountsDTO dto2 = null;

        //Act
        boolean result = dto1.equals(dto2);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with object from another class")
    void equalsEnsureFalseWithObjectFromAnotherClassTest() {
        //Arrange
        AccountsDTO dto = new AccountsDTO(accountsDTOs);
        GroupID groupID = new GroupID("1");

        //Act
        boolean result = dto.equals(groupID);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true if is same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //Arrange
        Set<String> expected = new HashSet<>();
        AccountsDTO dto = new AccountsDTO(accountsDTOs);

        //Act
        boolean result = dto.equals(dto);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure works")
    void hashCodeEnsureWorksTest() {
        //Arrange
        AccountsDTO dto = new AccountsDTO(accountsDTOs);
        int expected = 229207011;

        //Act
        int result = dto.hashCode();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for toString
     * Ensure works
     */
    @Test
    @DisplayName("Test for toString - Ensure works")
    void toStringEnsureWorksTest() {
        //Arrange
        AccountsDTO dto = new AccountsDTO(accountsDTOs);
        String expected = "AccountsDTO{accounts=[AccountDTO{denomination='blue pill', description='red pill', accountID='morpheus'}, AccountDTO{denomination='red pill', description='blue pill', accountID='also morpheus'}]}";

        //Act
        String result = dto.toString();

        //Assert
        assertEquals(expected, result);
    }

}