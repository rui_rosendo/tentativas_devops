package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CreateCategoryForPersonRequestDTOTest {
    /**
     * Test for CreateCategoryForPersonRequestDTO constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryForPersonRequestDTO constructor - Happy case")
    void constructorCreateCategoryForPersonRequestDTOHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonRequestDTO dto = new CreateCategoryForPersonRequestDTO("6", "switch");

        //ACT
        //ASSERT
        assertTrue(dto instanceof CreateCategoryForPersonRequestDTO);
    }

    /**
     * Test for getPersonID
     * Happy case
     */
    @Test
    @DisplayName("Test for getPersonID - Happy case")
    void getPersonIDHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonRequestDTO dto = new CreateCategoryForPersonRequestDTO("1", "switch");
        String expected = "1";

        //ACT
        String result = dto.getPersonID();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getDesignation
     * Happy case
     */
    @Test
    @DisplayName("Test for getDesignation - Happy case")
    void getDesignationHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonRequestDTO dto = new CreateCategoryForPersonRequestDTO("1", "switch");
        String expected = "switch";

        //ACT
        String result = dto.getDesignation();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for Equals
     * Same object
     */
    @Test
    @DisplayName("Test for equals - Same object")
    void equalsHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonRequestDTO dto = new CreateCategoryForPersonRequestDTO("1", "switch");

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Two Equal Objects
     */
    @Test
    @DisplayName("Test for equals - Two Equal Objects")
    void equalsTwoEqualObjectsHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonRequestDTO dto = new CreateCategoryForPersonRequestDTO("1", "switch");
        CreateCategoryForPersonRequestDTO dto2 = new CreateCategoryForPersonRequestDTO("1", "switch");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Not Equals personID Test
     */
    @Test
    @DisplayName("Test for equals - Not Equals - PersonID")
    void notEqualsPersonIDTest() {
        //ARRANGE
        CreateCategoryForPersonRequestDTO dto = new CreateCategoryForPersonRequestDTO("1", "switch");
        CreateCategoryForPersonRequestDTO dto2 = new CreateCategoryForPersonRequestDTO("2", "switch");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Not Equals Designation Test
     */
    @Test
    @DisplayName("Test for equals - Not Equals - Designation")
    void notEqualsDesignationTest() {
        //ARRANGE
        CreateCategoryForPersonRequestDTO dto = new CreateCategoryForPersonRequestDTO("1", "Farmacia");
        CreateCategoryForPersonRequestDTO dto2 = new CreateCategoryForPersonRequestDTO("1", "Comida");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * null object
     */
    @Test
    @DisplayName("Test for equals - null ")
    void equalsNullTest() {
        //ARRANGE
        CreateCategoryForPersonRequestDTO dto = new CreateCategoryForPersonRequestDTO("1", "Contas");

        //ACT
        boolean result = dto.equals(null);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * different object
     */
    @Test
    @DisplayName("Test for equals - different Object ")
    void equalsDifferentObjectTest() {
        //ARRANGE
        CreateCategoryForPersonRequestDTO requestDTO = new CreateCategoryForPersonRequestDTO("1", "Ferias");


        Set<String> categories = new HashSet<>();
        categories.add("equipamentos");

        CategoriesDTO dto = new CategoriesDTO(categories);
        //ACT
        boolean result = requestDTO.equals(dto);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for hashCode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hashCode - Happy Case ")
    void hashCodeHappyCaseTest() {
        //ARRANGE
        CreateCategoryForPersonRequestDTO dto = new CreateCategoryForPersonRequestDTO("1", "Compras");

        int expected = -1679811515;

        //ACT
        int result = dto.hashCode();

        //ASSERT
        assertEquals(expected, result);
    }
}