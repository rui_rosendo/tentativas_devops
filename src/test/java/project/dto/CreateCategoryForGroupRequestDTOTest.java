package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CreateCategoryForGroupRequestDTOTest {

    /**
     * Test for CreateCategoryForGroupRequestDTO constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryForGroupRequestDTO constructor - Happy case")
    void constructorCreateCategoryForGroupRequestDTOHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO dto = new CreateCategoryForGroupRequestDTO("6", "12", "Equipamentos");

        //ACT
        //ASSERT
        assertTrue(dto instanceof CreateCategoryForGroupRequestDTO);
    }

    /**
     * Test for getPersonID
     * Happy case
     */
    @Test
    @DisplayName("Test for getPersonID - Happy case")
    void getPersonIDHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO dto = new CreateCategoryForGroupRequestDTO("1", "12", "Equipamentos");
        String expected = "1";

        //ACT
        String result = dto.getPersonEmail();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getGroupID
     * Happy case
     */
    @Test
    @DisplayName("Test for getGroupID - Happy case")
    void getGroupIDHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO dto = new CreateCategoryForGroupRequestDTO("1", "12", "Equipamentos");
        String expected = "12";

        //ACT
        String result = dto.getGroupID();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getDesignation
     * Happy case
     */
    @Test
    @DisplayName("Test for getDesignation - Happy case")
    void getDesignationHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO dto = new CreateCategoryForGroupRequestDTO("1", "12", "Equipamentos");
        String expected = "Equipamentos";

        //ACT
        String result = dto.getDesignation();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for Equals
     * Same object
     */
    @Test
    @DisplayName("Test for equals - Same object")
    void equalsHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO dto = new CreateCategoryForGroupRequestDTO("1", "12", "Farmacia");

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Two Equal Objects
     */
    @Test
    @DisplayName("Test for equals - Two Equal Objects")
    void equalsTwoEqualObjectsHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO dto = new CreateCategoryForGroupRequestDTO("1", "12", "Farmacia");
        CreateCategoryForGroupRequestDTO dto2 = new CreateCategoryForGroupRequestDTO("1", "12", "Farmacia");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Not Equals PersonID Test
     */
    @Test
    @DisplayName("Test for equals - Not Equals - PersonID")
    void notEqualsPersonIDTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO dto = new CreateCategoryForGroupRequestDTO("1", "12", "Farmacia");
        CreateCategoryForGroupRequestDTO dto2 = new CreateCategoryForGroupRequestDTO("2", "13", "Comida");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Not Equals groupID Test
     */
    @Test
    @DisplayName("Test for equals - Not Equals - GroupID")
    void notEqualsGroupIDTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO dto = new CreateCategoryForGroupRequestDTO("1", "12", "Farmacia");
        CreateCategoryForGroupRequestDTO dto2 = new CreateCategoryForGroupRequestDTO("1", "13", "Comida");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Not Equals Designation Test
     */
    @Test
    @DisplayName("Test for equals - Not Equals - Designation")
    void notEqualsDesignationTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO dto = new CreateCategoryForGroupRequestDTO("1", "12", "Farmacia");
        CreateCategoryForGroupRequestDTO dto2 = new CreateCategoryForGroupRequestDTO("1", "12", "Comida");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * null object
     */
    @Test
    @DisplayName("Test for equals - null ")
    void equalsNullTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO dto = new CreateCategoryForGroupRequestDTO("1", "2", "Contas");

        //ACT
        boolean result = dto.equals(null);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * different object
     */
    @Test
    @DisplayName("Test for equals - different Object ")
    void equalsDifferentObjectTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO requestDTO = new CreateCategoryForGroupRequestDTO("1", "11", "Ferias");


        Set<String> categories = new HashSet<>();
        categories.add("equipamentos");

        CategoriesDTO dto = new CategoriesDTO(categories);
        //ACT
        boolean result = requestDTO.equals(dto);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for hashCode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hashCode - Happy Case ")
    void hashCodeHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupRequestDTO dto = new CreateCategoryForGroupRequestDTO("1", "12", "Compras");

        int expected = -1679688476;

        //ACT
        int result = dto.hashCode();

        //ASSERT
        assertEquals(expected, result);
    }
}