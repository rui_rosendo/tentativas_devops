package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CreateCategoryForGroupResponseDTOTest {

    /**
     * Test for CreateCategoryForGroupResponseDTO constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for CreateCategoryForGroupResponseDTO constructor - Happy case")
    void constructorCreateCategoryForGroupResponseDTOHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO dto = new CreateCategoryForGroupResponseDTO("6", "SWITCH", "Equipamentos");

        //ACT
        //ASSERT
        assertTrue(dto instanceof CreateCategoryForGroupResponseDTO);
    }

    /**
     * Test for getGroupID
     * Happy case
     */
    @Test
    @DisplayName("Test for getGroupID - Happy case")
    void getGroupIDHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO dto = new CreateCategoryForGroupResponseDTO("1", "switch", "Equipamentos");
        String expected = "1";

        //ACT
        String result = dto.getGroupID();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getGroupDescription
     * Happy case
     */
    @Test
    @DisplayName("Test for getGroupDescription - Happy case")
    void getGroupDescriptionHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO dto = new CreateCategoryForGroupResponseDTO("1", "switch", "Equipamentos");
        String expected = "switch";

        //ACT
        String result = dto.getGroupDescription();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getNewCategory
     * Happy case
     */
    @Test
    @DisplayName("Test for geNewCategory - Happy case")
    void getNewCategoryHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO dto = new CreateCategoryForGroupResponseDTO("1", "switch", "Equipamentos");
        String expected = "Equipamentos";

        //ACT
        String result = dto.getNewCategory();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for Equals
     * Same object
     */
    @Test
    @DisplayName("Test for equals - Same object")
    void equalsHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO dto = new CreateCategoryForGroupResponseDTO("1", "switch", "Farmacia");

        //ACT
        boolean result = dto.equals(dto);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Two Equal Objects
     */
    @Test
    @DisplayName("Test for equals - Two Equal Objects")
    void equalsTwoEqualObjectsHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO dto = new CreateCategoryForGroupResponseDTO("1", "switch", "Farmacia");
        CreateCategoryForGroupResponseDTO dto2 = new CreateCategoryForGroupResponseDTO("1", "switch", "Farmacia");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Not Equals groupID Test
     */
    @Test
    @DisplayName("Test for equals - groupID Not Equals")
    void notEqualsGroupIDTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO dto = new CreateCategoryForGroupResponseDTO("1", "switch", "Farmacia");
        CreateCategoryForGroupResponseDTO dto2 = new CreateCategoryForGroupResponseDTO("2", "isep", "Comida");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Not Equals groupDescription Test
     */
    @Test
    @DisplayName("Test for equals - groupDescription Not Equals")
    void notEqualsGroupDescriptionTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO dto = new CreateCategoryForGroupResponseDTO("1", "switch", "Farmacia");
        CreateCategoryForGroupResponseDTO dto2 = new CreateCategoryForGroupResponseDTO("1", "isep", "Comida");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Not Equals newCategory Test
     */
    @Test
    @DisplayName("Test for equals - newCategory Not Equals")
    void notEqualsNewCategoryTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO dto = new CreateCategoryForGroupResponseDTO("1", "switch", "Farmacia");
        CreateCategoryForGroupResponseDTO dto2 = new CreateCategoryForGroupResponseDTO("1", "switch", "Comida");

        //ACT
        boolean result = dto.equals(dto2);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * null object
     */
    @Test
    @DisplayName("Test for equals - null ")
    void equalsNullTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO dto = new CreateCategoryForGroupResponseDTO("1", "switch", "Contas");

        //ACT
        boolean result = dto.equals(null);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for Equals
     * different object
     */
    @Test
    @DisplayName("Test for equals - different Object ")
    void equalsDifferentObjectTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO requestDTO = new CreateCategoryForGroupResponseDTO("1", "switch", "Ferias");

        Set<String> categories = new HashSet<>();
        categories.add("equipamentos");
        CategoriesDTO dto = new CategoriesDTO(categories);
        //ACT
        boolean result = requestDTO.equals(dto);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Test for hashCode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hashCode - Happy Case ")
    void hashCodeHappyCaseTest() {
        //ARRANGE
        CreateCategoryForGroupResponseDTO dto = new CreateCategoryForGroupResponseDTO("1", "switch", "Compras");

        int expected = 811363889;

        //ACT
        int result = dto.hashCode();

        //ASSERT
        assertEquals(expected, result);
    }

}