package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class GroupDTOTest {

    private Set<String> members;
    private Set<String> managers;
    private String creatorID;
    private String memberID;
    private String groupID;
    private String ledgerID;
    private String description;
    private String creationDate;

    @BeforeEach
    void init() {
        members = new HashSet<>();
        creatorID = "1L";
        memberID = "2L";
        members.add(creatorID);
        members.add(memberID);
        managers = new HashSet<>();
        managers.add(creatorID);
        groupID = "1L";
        ledgerID = "1L";
        description = "description";
        creationDate = "2020-01-01";
    }


    /**
     * Test for GroupDTO constructor
     */
    @Test
    void constructorTest() {
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);
        assertTrue(groupDTO instanceof GroupDTO);
    }

    /**
     * Test for getMembers method
     * <p>
     * Happy Case - contains several members
     */
    @Test
    void getMembersHappyCaseTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        Set<String> expected = new HashSet<>();
        expected.add("1L");
        expected.add("2L");

        //Act
        Set<String> result = groupDTO.getMembers();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for getMembers method
     * <p>
     * Ensure Equals for empty Set of members
     */
    @Test
    void getMembersEnsureEqualsForEmptyMembersTest() {
        //Arrange
        Set<String> emptyMembers = new HashSet<>();
        GroupDTO groupDTO = new GroupDTO(emptyMembers, managers, groupID, ledgerID, description, creationDate);

        Set<String> expected = new HashSet<>();

        //Act
        Set<String> result = groupDTO.getMembers();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for getMembers method
     * <p>
     * Ensure Not Equals
     */
    @Test
    void getMembersEnsureNotEqualsTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        Set<String> expected = new HashSet<>();
        expected.add("2L");

        //Act
        Set<String> result = groupDTO.getMembers();

        //Assert
        assertNotEquals(expected, result);

    }

    /**
     * Test for getManagers method
     * <p>
     * Happy Case
     */
    @Test
    void getManagersHappyCaseTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        Set<String> expected = new HashSet<>();
        expected.add("1L");

        //Act
        Set<String> result = groupDTO.getManagers();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getManagers method
     * <p>
     * Ensure Equals for empty Set of members
     */
    @Test
    void getManagersEnsureEqualsForEmptyManagersTest() {
        //Arrange
        Set<String> emptyManagers = new HashSet<>();
        GroupDTO groupDTO = new GroupDTO(members, emptyManagers, groupID, ledgerID, description, creationDate);

        Set<String> expected = new HashSet<>();

        //Act
        Set<String> result = groupDTO.getManagers();

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for getManagers method
     * <p>
     * Ensure Not Equals
     */
    @Test
    void getManagersEnsureNotEqualsTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        Set<String> expected = new HashSet<>();
        expected.add("2L");

        //Act
        Set<String> result = groupDTO.getManagers();

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for getGroupID method
     * <p>
     * Happy Case
     */
    @Test
    void getGroupIDHappyCaseTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String expected = "1L";

        //Act
        String result = groupDTO.getGroupID();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for getGroupID method
     * <p>
     * Ensure not equals
     */
    @Test
    void getGroupIDEnsureNotEquals() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String expected = "2L";

        //Act
        String result = groupDTO.getGroupID();

        //Assert
        assertNotEquals(expected, result);

    }

    /**
     * Test for getLedgerID method
     * <p>
     * Happy Case
     */
    @Test
    void getLedgerIDHappyCaseTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String expected = "1L";

        //Act
        String result = groupDTO.getLedgerID();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for getLedgerID method
     * <p>
     * Ensure not equals
     */
    @Test
    void getLedgerIDEnsureNotEquals() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String expected = "2L";

        //Act
        String result = groupDTO.getLedgerID();

        //Assert
        assertNotEquals(expected, result);

    }

    /**
     * Test for getDescription method
     * <p>
     * Happy Case
     */
    @Test
    void getDescriptionHappyCaseTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String expected = "description";

        //Act
        String result = groupDTO.getDescription();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for getDescription method
     * <p>
     * Ensure not equals
     */
    @Test
    void getDescriptionEnsureNotEqualsTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String expected = "different description";

        //Act
        String result = groupDTO.getDescription();

        //Assert
        assertNotEquals(expected, result);

    }

    /**
     * Test for getCreationDate method
     * <p>
     * Happy Case
     */
    @Test
    void getCreationDateHappyCaseTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String expected = "2020-01-01";

        //Act
        String result = groupDTO.getCreationDate();

        //Assert
        assertEquals(expected, result);

    }

    /**
     * Test for getCreationDate method
     * <p>
     * Ensure not equals
     */
    @Test
    void getCreationDateEnsureNotEqualsTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String expected = "1900-01-01";

        //Act
        String result = groupDTO.getCreationDate();

        //Assert
        assertNotEquals(expected, result);

    }

    /**
     * Test for equals
     * <p>
     * Happy Case - Comparing same object
     */
    @Test
    void testEqualsHappyCaseTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        //Act
        boolean result = groupDTO.equals(groupDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * <p>
     * Happy Case - 2 different GroupDTO objects with same attributes
     */
    @Test
    void testEqualsHappyCaseDifferentObjectsSameAttributesTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        GroupDTO otherGroupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if one of the objects is null
     */
    @Test
    void testEqualsEnsureFalseIfOtherIsNullTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        GroupDTO otherGroupDTO = null;

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertFalse(result);

    }

    /**
     * Test for equals
     * <p>
     * Ensure false if class is different
     */
    @Test
    void testEqualsEnsureFalseIfClassIsDifferentTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String other = "other";

        //Act
        boolean result = groupDTO.equals(other);

        //Assert
        assertFalse(result);

    }

    /**
     * Test for equals
     * <p>
     * Ensure false if members is different
     */
    @Test
    void testEqualsEnsureFalseDifferentMembersTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        Set<String> otherMembers = new HashSet<>();
        GroupDTO otherGroupDTO = new GroupDTO(otherMembers, managers, groupID, ledgerID, description, creationDate);

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if managers is different
     */
    @Test
    void testEqualsEnsureFalseDifferentManagersTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        Set<String> otherManagers = new HashSet<>();
        GroupDTO otherGroupDTO = new GroupDTO(members, otherManagers, groupID, ledgerID, description, creationDate);

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if groupID is different
     */
    @Test
    void testEqualsEnsureFalseDifferentGroupIDTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String otherGroupID = "10L";
        GroupDTO otherGroupDTO = new GroupDTO(members, managers, otherGroupID, ledgerID, description, creationDate);

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if ledgerID is different
     */
    @Test
    void testEqualsEnsureFalseDifferentLedgerIDTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String otherLedgerID = "10L";
        GroupDTO otherGroupDTO = new GroupDTO(members, managers, groupID, otherLedgerID, description, creationDate);

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if description is different
     */
    @Test
    void testEqualsEnsureFalseDifferentDescriptionTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String otherDescription = "other description";
        GroupDTO otherGroupDTO = new GroupDTO(members, managers, groupID, ledgerID, otherDescription, creationDate);

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * <p>
     * Ensure false if creationDate is different
     */
    @Test
    void testEqualsEnsureFalseDifferentCreationDateTest() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);

        String otherCreationDate = "1900-01-01";
        GroupDTO otherGroupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, otherCreationDate);

        //Act
        boolean result = groupDTO.equals(otherGroupDTO);

        //Assert
        assertFalse(result);
    }

    @Test
    void testHashCode() {
        //Arrange
        GroupDTO groupDTO = new GroupDTO(members, managers, groupID, ledgerID, description, creationDate);
        int expected = -1654925145;

        //Act
        int result = groupDTO.hashCode();

        //Assert
        assertEquals(expected, result);

    }
}