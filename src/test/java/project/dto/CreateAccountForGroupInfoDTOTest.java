package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.GroupID;

import static org.junit.jupiter.api.Assertions.*;

class CreateAccountForGroupInfoDTOTest {
    /**
     * Test for CreateAccountForPersonInfoDTO constructor
     */
    @Test
    @DisplayName("Test for CreateAccountForPersonInfoDTO constructor")
    void constructorHappyCaseTest() {
        CreateAccountForGroupInfoDTO dto = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        //ASSERT
        assertTrue(dto instanceof CreateAccountForGroupInfoDTO);
    }

    /**
     * Test for getAccountID
     * Happy case
     */
    @Test
    @DisplayName(" Test for getAccountsIDs - Happy Case")
    void getAccountIDHappyCaseTest() {
        //ARRANGE
        CreateAccountForGroupInfoDTO dto = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        String expected = "1";
        //ACT
        String result = dto.getAccountID();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getDenomination
     * Happy case
     */
    @Test
    @DisplayName(" Test for getDenomination- Happy Case")
    void getDenominationHappyCaseTest() {
        //ARRANGE
        CreateAccountForGroupInfoDTO dto = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        String expected = "denomination";
        //ACT
        String result = dto.getDenomination();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getDescription
     * Happy case
     */
    @Test
    @DisplayName(" Test for getDescription Happy Case")
    void getDescriptionHappyCaseTest() {
        //ARRANGE
        CreateAccountForGroupInfoDTO dto = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        String expected = "description";
        //ACT
        String result = dto.getDescription();
        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for equals
     * Happy case
     */
    @Test
    @DisplayName("Test for equals - HappyCase")
    void equalsHappyCaseTest() {
        //Arrange
        CreateAccountForGroupInfoDTO dto1 = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        CreateAccountForGroupInfoDTO dto2 = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        CreateAccountForGroupInfoDTO dto1 = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        CreateAccountForPersonInfoDTO dto2 = null;
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with diferent class object
     */
    @Test
    @DisplayName("Test for equals - Ensure false with object from another class")
    void equalsEnsureFalseWithObjectFromAnotherClassTest() {
        //Arrange
        CreateAccountForGroupInfoDTO dto1 = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        GroupID groupID = new GroupID("1");
        //Act
        boolean result = dto1.equals(groupID);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true if is same object
     */
    @Test
    @DisplayName("Test for equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //Arrange
        CreateAccountForGroupInfoDTO dto1 = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        //Act
        boolean result = dto1.equals(dto1);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false if accountID is different
     */
    @Test
    @DisplayName("Test for equals - Ensure false if accountID is different")
    void equalsEnsureFalseWithDifferentAccountIDTest() {
        //Arrange
        CreateAccountForGroupInfoDTO dto1 = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        CreateAccountForGroupInfoDTO dto2 = new CreateAccountForGroupInfoDTO("2", "denomination", "description");

        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false if denomination is different
     */
    @Test
    @DisplayName("Test for equals - Ensure false if denomination is different")
    void equalsEnsureFalseWithDifferentDenominationTest() {
        //Arrange
        CreateAccountForGroupInfoDTO dto1 = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        CreateAccountForGroupInfoDTO dto2 = new CreateAccountForGroupInfoDTO("1", "different", "description");

        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false if description is different
     */
    @Test
    @DisplayName("Test for equals - Ensure false if description is different")
    void equalsEnsureFalseWithDifferentDescriptionTest() {
        //Arrange
        CreateAccountForGroupInfoDTO dto1 = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        CreateAccountForGroupInfoDTO dto2 = new CreateAccountForGroupInfoDTO("1", "denomination", "different");

        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure works")
    void hashCodeEnsureWorksTest() {
        //Arrange
        CreateAccountForGroupInfoDTO dto1 = new CreateAccountForGroupInfoDTO("1", "denomination", "description");
        int expected = -685860601;
        //Act
        int result = dto1.hashCode();
        //Assert
        assertEquals(expected, result);
    }
}
