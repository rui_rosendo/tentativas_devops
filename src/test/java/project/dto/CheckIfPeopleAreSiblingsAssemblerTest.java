package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class CheckIfPeopleAreSiblingsAssemblerTest {

    /**
     *  Test for mapToRequestDTO - Happy case
     */
    @Test
    @DisplayName("Method mapToRequestDTO - Happy Case")
    void mapToRequestDTOHappyCase() {
        //Arrange

        String id1String = "1";
        String id2String = "2";

        CheckIfPeopleAreSiblingsRequestDTO expected = new CheckIfPeopleAreSiblingsRequestDTO(id1String, id2String);

        //Act
        CheckIfPeopleAreSiblingsRequestDTO result = CheckIfPeopleAreSiblingsAssembler.mapToRequestDTO(id1String, id2String);

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for mapToRequestDTO - Fail Case
     */
    @Test
    @DisplayName("Method mapToRequestDTO - Happy Case")
    void mapToRequestDTOFailCase() {
        //Arrange
        String id1String = "1";
        String id2String = "2";
        String id3String = "3";

        CheckIfPeopleAreSiblingsRequestDTO expected = new CheckIfPeopleAreSiblingsRequestDTO(id1String, id2String);

        //Act
        CheckIfPeopleAreSiblingsRequestDTO result = CheckIfPeopleAreSiblingsAssembler.mapToRequestDTO(id1String, id3String);

        //Assert
        assertNotEquals(expected, result);
    }

    /**
     * Test for mapToResponseDTO - Happy Case
     */
    @Test
    @DisplayName("Method mapToResponseDTO - Happy Case")
    void mapToResponseDTOHappyCase() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO expected = new CheckIfPeopleAreSiblingsResponseDTO("true");

        //Act
        CheckIfPeopleAreSiblingsResponseDTO result = CheckIfPeopleAreSiblingsAssembler.mapToResponseDTO("true");

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for mapToResponseDTO - Fail Case
     */
    @Test
    @DisplayName("Method mapToResponseDTO - Fail Case")
    void mapToResponseDTOFailCase() {
        //Arrange
        CheckIfPeopleAreSiblingsResponseDTO expected = new CheckIfPeopleAreSiblingsResponseDTO("false");

        //Act
        CheckIfPeopleAreSiblingsResponseDTO result = CheckIfPeopleAreSiblingsAssembler.mapToResponseDTO("true");

        //Assert
        assertNotEquals(expected, result);
    }
}