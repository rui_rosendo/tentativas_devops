package project.exceptions;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomRestExceptionHandlerTest {

    /**
     * Test for PersonNotFoundException handler
     * Ensure equals with default message
     */
    @Test
    void handlePersonNotFoundTest() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Person not found";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.NOT_FOUND, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handlePersonNotFound(new PersonNotFoundException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for MemberAlreadyExistsException handler
     * Ensure equals with default message
     */
    @Test
    void handleMemberAlreadyExistsTest() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Member already exists";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handleMemberAlreadyExists(new MemberAlreadyExistsException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for GroupAlreadyExistsException handler
     * Ensure equals with default message
     */
    @Test
    void handleGroupAlreadyExistsTest() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Group already exists";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handleGroupAlreadyExists(new GroupAlreadyExistsException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for GroupNotFoundException handler
     * Ensure equals with default message
     */
    @Test
    void handleGroupNotFoundTest() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Group not found";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.NOT_FOUND, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handleGroupNotFound(new GroupNotFoundException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for GroupConflictException handler
     * Ensure equals with default message
     */
    @Test
    void handleGroupConflict() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Group conflict";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handleGroupConflict(new GroupConflictException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for AccountNotFoundException handler
     * Ensure equals with default message
     */
    @Test
    void handleAccountNotFound() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Account not found";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.NOT_FOUND, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handleAccountNotFound(new AccountNotFoundException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for AccountAlreadyExistsException handler
     * Ensure equals with default message
     */
    @Test
    void handleAccountAlreadyExists() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Account already exists";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handleAccountAlreadyExists(new AccountAlreadyExistsException());

        //Assert
        assertEquals(expected, actual);

    }


    /**
     * Test for CategoryNotFoundException handler
     * Ensure equals with default message
     */
    @Test
    void handleCategoryNotFound() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Category not found";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.NOT_FOUND, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handleCategoryNotFound(new CategoryNotFoundException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for CategoryAlreadyExistsException handler
     * Ensure equals with default message
     */
    @Test
    void handleCategoryAlreadyExists() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Category already exists";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handleCategoryAlreadyExists(new CategoryAlreadyExistsException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for PersonIsNotManagerOfTheGroupException handler
     * Ensure equals with default message
     */
    @Test
    void handlePersonIsNotManagerOfTheGroup() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Person is not manager of the group";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.FORBIDDEN, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handlePersonIsNotManagerOfTheGroup(new PersonIsNotManagerOfTheGroupException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for InvalidFieldException handler
     * Ensure equals with default message
     */
    @Test
    void handleInvalidField() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Invalid field";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.BAD_REQUEST, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handleInvalidField(new InvalidFieldException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for GroupLedgerAlreadyExistsException handler
     * Ensure equals with default message
     */
    @Test
    void handleGroupLedgerAlreadyExists() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Group ledger already exists";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(), expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handleGroupLedgerAlreadyExists(new GroupLedgerAlreadyExistsException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for LedgerNotFoundException handler
     * Ensure equals with default message
     */
    @Test
    void handleLedgerNotFoundTest() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Ledger not found";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.NOT_FOUND, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(),
                expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual = customRestExceptionHandler.handleLedgerNotFound(new LedgerNotFoundException());

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for TransactionAlreadyExistsException handler
     * Ensure equals with default message
     */
    @Test
    void handleTransactionAlreadyExistsTest() {
        //Arrange
        CustomRestExceptionHandler customRestExceptionHandler = new CustomRestExceptionHandler();

        String expectedMessage = "Transaction already exists";
        List<String> expectedMessageList = new ArrayList<>();
        expectedMessageList.add(expectedMessage);
        ApiError expectedApiError = new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, expectedMessage, expectedMessageList);
        ResponseEntity<Object> expected = new ResponseEntity<>(expectedApiError, new HttpHeaders(),
                expectedApiError.getStatus());

        //Act
        ResponseEntity<Object> actual =
                customRestExceptionHandler.handleTransactionAlreadyExists(new TransactionAlreadyExistsException());

        //Assert
        assertEquals(expected, actual);

    }
}