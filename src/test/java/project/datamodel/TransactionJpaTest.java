package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.entities.ledger.Amount;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.Description;

import static org.junit.jupiter.api.Assertions.assertTrue;

class TransactionJpaTest {

    @Test
    void transactionJpaAllArgsContructorTest() {
        //Arrange
        LedgerJpa ledgerJpa = new LedgerJpa("100");
        Amount amount = new Amount("1000");
        Description description = new Description("description");
        Category category = new Category("designation");
        AccountID debitAccountId = new AccountID("10");
        AccountID creditAccountId = new AccountID("20");
        TransactionJpa transactionJpa = new TransactionJpa(ledgerJpa, amount, "2020-01-01 00:00:00", "Debit",
                description,
                category, debitAccountId, creditAccountId);

        //Act
        //Assert
        assertTrue(transactionJpa instanceof TransactionJpa);
    }

}