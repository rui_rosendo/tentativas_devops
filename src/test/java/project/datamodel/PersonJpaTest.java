package project.datamodel;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PersonJpaTest {

    private PersonID personID;
    private LedgerID ledgerID;

    @BeforeEach
    public void init() {
        personID = new PersonID("1@switch.pt");
        ledgerID = new LedgerID("10");

    }

    /**
     * Test for PersonJpa Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for PersonJpa Constructor - Happy case")
    void constructorHappyCaseTest() {
        // ARRANGE
        PersonJpa joao = new PersonJpa(personID, "joao", "Travessa Santa Barbara",
                "Estarreja", "1987-04-17", ledgerID);

        // ACT
        // ASSERT
        assertTrue(joao instanceof PersonJpa);
    }

    /**
     * Test for get name
     */
    @Test
    @DisplayName("Test for get name")
    void getNameTest() {
        // ARRANGE
        PersonJpa joao = new PersonJpa(personID, "joao", "Travessa Santa Barbara", "Estarreja",
                "1987-04-17", ledgerID);

        // ACT
        String expected = "joao";
        String result = joao.getName();

        // ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for get address
     */
    @Test
    @DisplayName("Test for get address")
    void getAddressTest() {
        // ARRANGE
        PersonJpa joao = new PersonJpa(personID, "joao", "Travessa Santa Barbara", "Estarreja",
                "1987-04-17", ledgerID);

        // ACT
        String expected = "Travessa Santa Barbara";
        String result = joao.getAddress();

        // ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for get birthPlace
     */
    @Test
    @DisplayName("Test for get birthPlace")
    void getBirthPlaceTest() {
        // ARRANGE
        PersonJpa joao = new PersonJpa(personID, "joao", "Travessa Santa Barbara", "Estarreja",
                "1987-04-17", ledgerID);

        // ACT
        String expected = "Estarreja";
        String result = joao.getBirthPlace();

        // ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for get birthDate
     */
    @Test
    @DisplayName("Test for get birthDate")
    void getBirthDateTest() {
        // ARRANGE
        PersonJpa joao = new PersonJpa(personID, "joao", "Travessa Santa Barbara", "Estarreja",
                "1987-04-17", ledgerID);

        // ACT
        String expected = "1987-04-17";
        String result = joao.getBirthDate();

        // ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for get person id
     */
    @Test
    @DisplayName("Test for get person id")
    void getPersonIdTest() {
        // ARRANGE
        PersonJpa joao = new PersonJpa(personID, "joao", "Travessa Santa Barbara", "Estarreja",
                "1987-04-17", ledgerID);

        // ACT
        PersonID expected = personID;
        PersonID result = joao.getId();

        // ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for ledger id
     */
    @Test
    @DisplayName("Test for ledger id")
    void getLedgerIdTest() {
        // ARRANGE
        PersonJpa joao = new PersonJpa(personID, "joao", "Travessa Santa Barbara", "Estarreja",
                "1987-04-17", ledgerID);

        // ACT
        LedgerID expected = ledgerID;
        LedgerID result = joao.getLedgerID();

        // ASSERT
        assertEquals(expected, result);
    }
}