package project.datamodel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GroupDomainDataAssemblerTest {
    /**
     * Test for GroupDomainDataAssemblerTest - toData
     */
    @Test
    @DisplayName("Test for GroupDomainDataAssemblerTest - toData")
    void toDataTest() {
        //ARRANGE
        GroupID groupID = new GroupID("1");
        PersonID creatorID = new PersonID("111@switch.pt");
        LedgerID ledgerID = new LedgerID("4");

        Group group = new Group(groupID, "Family", "2020-04-20", creatorID, ledgerID);
        GroupJpa expected = new GroupJpa("1", "Family", "2020-04-20", "111@switch.pt", "4");

        //ACT
        GroupJpa result = GroupDomainDataAssembler.toData(group);

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for GroupDomainDataAssemblerTest - toDomain
     */
    @Test
    @DisplayName("Test for GroupDomainDataAssemblerTest - toDomain")
    void toDomainTest() {
        //ARRANGE
        GroupID groupID = new GroupID("1");
        PersonID creatorID = new PersonID("111@switch.pt");
        LedgerID ledgerID = new LedgerID("4");

        Group expected = new Group(groupID, "Family", "2020-04-20", creatorID, ledgerID);
        GroupJpa groupJpa = new GroupJpa("1", "Family", "2020-04-20", "111@switch.pt", "4");

        List<Category> categoriesList = new ArrayList<>();
        categoriesList.add(new Category("Comida"));
        groupJpa.setCategories(categoriesList);

        List<AccountID> accountsList = new ArrayList<>();
        accountsList.add(new AccountID("4"));
        groupJpa.setAccountsIds(accountsList);

        //ACT
        Group result = GroupDomainDataAssembler.toDomain(groupJpa);

        //ASSERT
        assertEquals(expected, result);
    }
}