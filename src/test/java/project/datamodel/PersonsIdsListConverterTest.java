package project.datamodel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.PersonID;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PersonsIdsListConverterTest {

    /**
     * Test for convertToDatabaseColumn
     * Happy Case - List with elements
     */
    @Test
    @DisplayName("convertToDatabaseColumn - Happy Case - List with elements")
    void convertToDatabaseColumnListWithElements() {
        //Arrange
        List<PersonID> personIDList = new ArrayList<>();
        personIDList.add(new PersonID("1@switch.pt"));
        personIDList.add(new PersonID("2@switch.pt"));
        String expected = "1@switch.pt,2@switch.pt";
        PersonsIdsListConverter converter = new PersonsIdsListConverter();
        //Act
        String result = converter.convertToDatabaseColumn(personIDList);
        //Arrange
        assertEquals(expected, result);
    }

    /**
     * Test for convertToDatabaseColumn
     * Happy Case - Empty List
     */
    @Test
    @DisplayName("convertToDatabaseColumn - Happy Case - Empty List ")
    void convertToDatabaseColumnEmptyList() {
        //Arrange
        List<PersonID> personIDList = new ArrayList<>();
        String expected = "";
        PersonsIdsListConverter converter = new PersonsIdsListConverter();
        //Act
        String result = converter.convertToDatabaseColumn(personIDList);
        //Arrange
        assertEquals(expected,result);
    }

    /**
     * Test convertToEntityAttribute
     * Happy Case - String with elements
     */
    @Test
    @DisplayName(" convertToEntityAttribute -  Happy Case - String with elements")
    void convertToEntityAttributeStringWithElements() {
        //Arrange
        List<PersonID> expected = new ArrayList<>();
        expected.add(new PersonID("1@switch.pt"));
        expected.add(new PersonID("2@switch.pt"));
        String input = "1@switch.pt,2@switch.pt";
        PersonsIdsListConverter converter = new PersonsIdsListConverter();
        //Act
        List<PersonID> result = converter.convertToEntityAttribute(input);
        //Arrange
        assertEquals(expected, result);

    }

    /**
     * Test convertToEntityAttribute
     * Happy Case - String without elements
     */
    @Test
    @DisplayName(" convertToEntityAttribute -  Happy Case - String without elements")
    void convertToEntityAttributeStringWithoutElements() {
        //Arrange
        List<PersonID> expected = new ArrayList<>();
        String input = "";
        PersonsIdsListConverter converter = new PersonsIdsListConverter();
        //Act
        List<PersonID> result = converter.convertToEntityAttribute(input);
        //Arrange
        assertEquals(expected,result);
    }

}