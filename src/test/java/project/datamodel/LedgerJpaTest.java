package project.datamodel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.LedgerID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LedgerJpaTest {

    /**
     * Test for LedgerJpa empty constructor
     */
    @Test
    @DisplayName("Test for LedgerJpa constructor - No args constructor")
    void ledgerJpaVoidConstructor() {
        LedgerJpa ledgerJpa = new LedgerJpa();

        //assert
        assertTrue(ledgerJpa instanceof LedgerJpa);


    }

    /**
     * Test for LedgerJpa constructor - With string id
     */
    @Test
    @DisplayName("Test for LedgerJpa constructor - With String id")
    void ledgerJpaConstructor() {

        LedgerJpa ledgerJpa = new LedgerJpa("12");

        //assert
        assertTrue(ledgerJpa instanceof LedgerJpa);

    }

    /**
     * Test for LedgerJpa constructor - with ledgerID
     */
    @Test
    @DisplayName("Test for LedgerJpa constructor - LedgerID ledgerID")
    void ledgerJpaSecondConstructor() {

        //arrange
        LedgerID ledgerID = new LedgerID("1311");
        LedgerJpa ledgerJpa = new LedgerJpa(ledgerID);


        //assert
        assertTrue(ledgerJpa instanceof LedgerJpa);

    }

    /**
     * Test for LedgerJpa get method
     */
    @Test
    @DisplayName("Test for getLedgerJpa ")
    void ledgerJpaGetMethod() {

        //arrange
        LedgerID ledgerID = new LedgerID("1311");

        LedgerJpa ledgerJpa = new LedgerJpa(ledgerID);

        //act
        String expected = "1311";
        String result = ledgerJpa.getId().toStringDTO();

        //assert
        assertEquals(expected, result);

    }

    /**
     * Test for LedgerJpa set method
     */
    @Test
    @DisplayName("Test for setLedgerJpa")
    void ledgerJpaSetMethod() {

        //arrange
        LedgerID ledgerID = new LedgerID("1311");
        LedgerID newLedgerID = new LedgerID("3");

        LedgerJpa ledgerJpa = new LedgerJpa(ledgerID);

        ledgerJpa.setId(newLedgerID);

        //act
        String expected = "3";
        String result = ledgerJpa.getId().toStringDTO();

        //assert
        assertEquals(expected, result);
    }

}