package project.datamodel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.model.entities.shared.AccountID;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class IdsListConverterTest {

    @Test
    @DisplayName("Test for CategoriesListConverter - convert To Data base Column ")
    void convertToDatabaseColumn() {
        //Arrange
        IdsListConverter converter = new IdsListConverter();

        List<AccountID> accountIDS = new ArrayList<>();
        accountIDS.add(new AccountID("11"));
        accountIDS.add(new AccountID("12"));

        String expected = "11,12";

        //Act
        String result = converter.convertToDatabaseColumn(accountIDS);

        //Assert
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Test for CategoriesListConverter - convert To Entity Attribute ")
    void convertToEntityAttribute() {
        //Assert
        IdsListConverter converter = new IdsListConverter();

        List<AccountID> expected = new ArrayList<>();
        expected.add(new AccountID("11"));
        expected.add(new AccountID("12"));

        String accountIDS = "11,12";

        //Act
        List<AccountID> result = converter.convertToEntityAttribute(accountIDS);

        //Assert
        assertEquals(expected, result);

    }
}