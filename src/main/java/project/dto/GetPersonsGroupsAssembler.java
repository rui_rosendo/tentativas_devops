package project.dto;

import project.model.entities.group.Group;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public final class GetPersonsGroupsAssembler {

    /**
     * Constructor for GetPersonsGroupsAssembler
     * Created to override the public constructor Java creates automatically if there is not a constructor for a class
     */
    public GetPersonsGroupsAssembler() {
        //Intentionally empty
    }

    /**
     * mapToDTO method
     *
     * @param groups Set of group objects
     * @return GetPersonsGroupsResponseDTO
     */
    public static GetPersonsGroupsResponseDTO mapToDTO(Set<Group> groups) {
        List<GroupDTOMinimal> getPersonsGroupsDTO = new ArrayList<>();

        for (Group group : groups) {
            GroupDTOMinimal groupDTO = new GroupDTOMinimal(
                    group.getID().toStringDTO(),
                    group.getDescription().getDescriptionValue(),
                    group.getCreationDate().toStringDTO());
            getPersonsGroupsDTO.add(groupDTO);
        }

        return new GetPersonsGroupsResponseDTO(getPersonsGroupsDTO);
    }
}
