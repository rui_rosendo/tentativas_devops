package project.dto;

import project.model.entities.group.Group;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;
import project.model.entities.shared.PersonsIDs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class AddMemberAssembler {

    /**
     * Constructor method left empty because class is static
     */
    AddMemberAssembler() {
        //This constructor is intentionally empty
    }

    /**
     * Method to request an entry DTO
     *
     * @param newMemberEmail ID of the new member to be added to group
     * @param groupID        ID of the group to which the new member is to be added
     * @return a AddMemberRequestDTO that encapsulates two string attributes (newMemberEmail and groupID)
     */
    public static AddMemberRequestDTO mapToRequestDTO(String newMemberEmail, String groupID) {
        return new AddMemberRequestDTO(newMemberEmail, groupID);
    }

    /**
     * Method to retrieve the list of membersIDs of given group as a DTO
     *
     * @param group group
     * @return a MembersDTO that encapsulates the list ( Set<String> ) of membersIDs (converted to strings) of given group
     */
    public static MembersDTO mapToResponseDTO(Group group, Set<Person> members) {
        MembersDTO membersDTO;
        //Set result
        List<String> personsIDsStrings = new ArrayList<>();
        List<String> namesStrings = new ArrayList<>();
        List<Boolean> isManagersStrings = new ArrayList<>();

        //1- Percorrer o set de ID's de pessoas a transformar cada um deles em string
        //2 - Adicionar o ID em string ao set accountsIDsInString(result)
        for (Person member: members) {
            personsIDsStrings.add(member.getID().getPersonEmail());
            namesStrings.add(member.getName().toStringDTO());
            isManagersStrings.add(group.hasManagerID(member.getID()));
        }

        //Transformar o atributo do DTO no conteúdo do set personsIDsStrings
        membersDTO = new MembersDTO(personsIDsStrings, namesStrings, isManagersStrings);

        return membersDTO;
    }

    /**
     * Method to retrieve the member added to the group
     * with group information(groupID and groupDescription)
     *
     * @param groupID          ID of group to which the new member is to be added
     * @param groupDescription Description of the group
     * @param personEmail      Id of person to be added to group
     * @return AddMemberResponseDTO with all information in String Format
     */
    public static AddMemberResponseDTO mapToResponseDTO(String groupID, String groupDescription, String personEmail) {
        return new AddMemberResponseDTO(groupID, groupDescription, personEmail);

    }
}
