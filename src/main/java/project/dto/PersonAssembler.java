package project.dto;

public class PersonAssembler {

    /**
     * Constructor for PersonAssembler
     * * Created to override the public constructor Java creates automatically if there is not a constructor for a class
     */
    public PersonAssembler() {
        //Intentionally empty
    }

    /**
     * mapToDTO method
     *
     * @param email
     * @param name
     * @param address
     * @param birthDate
     * @param birthplace
     * @return
     */
    public static GetPersonInfoResponseDTO mapToResponseDTO(String email, String name, String address, String birthDate, String birthplace) {
        return new GetPersonInfoResponseDTO(email, name, address, birthDate, birthplace);
    }
}
