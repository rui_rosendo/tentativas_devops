package project.dto;

import java.util.Objects;

public class AddMemberInfoDTO {

    private String personEmail;

    /**
     * Empty Constructor method
     * used for deserialization and injection for Spring Boot
     */
    private AddMemberInfoDTO() {
        //empty constructor for deserialization
    }

    /**
     * Constructor method
     *
     * @param personEmail ID of the new member to be added to group
     */
    public AddMemberInfoDTO(String personEmail) {
        setPersonEmail(personEmail);
    }

    /**
     * Get method
     *
     * @return ID of the new member to be added to group
     */
    public String getPersonEmail() {
        return personEmail;
    }

    /**
     * Set method
     *
     * @param personEmail ID of the new member to be added to group
     */
    private void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddMemberInfoDTO that = (AddMemberInfoDTO) o;
        return Objects.equals(personEmail, that.personEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personEmail);
    }
}
