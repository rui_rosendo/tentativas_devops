package project.dto;

import project.model.entities.Categories;
import project.model.entities.person.Person;
import project.model.entities.shared.Category;

import java.util.HashSet;
import java.util.Set;

public final class CreateCategoryForPersonAssembler {

    /**
     * Constructor for CreateCategoryForPersonAssembler
     * Created to override the public constructor Java creates automatically if there is not a constructor for a class
     */
    CreateCategoryForPersonAssembler() {
        //Intentionally empty
    }

    /**
     * method mapToResponseDTO
     *
     * @param person instance of person
     * @return instance of CategoriesDTO
     */
    public static CategoriesDTO mapToResponseDTO(Person person) {

        Categories categoriesVO = person.getCategories();             // ir a person buscar o VO categories
        Set<Category> categories = categoriesVO.getCategoriesValue();     // ir ao VO categories buscar cada categoria

        Set<String> categoriesStrings = new HashSet<>();             // set onde vao ser guardadas a categorias

        for (Category category : categories) {
            categoriesStrings.add(category.toStringDTO());           // adiciona as categorias uma a uma ao Set criado, no formato string
        }

        return new CategoriesDTO(categoriesStrings);
    }

    /**
     * Method mapToDtoRequest
     *
     * @param personID    personID
     * @param personID    personID
     * @param designation of category
     * @return instance of CreateCategoryForPersonRequestDTO
     */
    public static CreateCategoryForPersonRequestDTO mapToRequestDTO(String personID, String designation) {
        return new CreateCategoryForPersonRequestDTO(personID, designation);
    }

    /**
     * Method mapToResponseDTO
     *
     * @param personID
     * @param designation
     * @return instance of CreateCategoryForPersonResponseDTO
     */
    public static CreateCategoryForPersonResponseDTO mapToResponseDTO(String personID, String designation) {
        return new CreateCategoryForPersonResponseDTO(personID, designation);
    }

}
