package project.dto;

public final class CreateTransactionForGroupAssembler {

    /**
     * Constructor of CreateTransactionForGroupAssembler
     */
    CreateTransactionForGroupAssembler() {
        //Contructor is intentionally empty
    }

    /**
     * Method mapToRequestDTO
     * maps the information passed to the controller (Request Body as an infoDTO and path variables) into a new DTO with
     * the information needed by the CreateTransactionForGroupService to create a new transaction.
     *
     * @param personEmail The identity of the group member who is creating the new transaction as a String
     * @param groupID     The identity of the group to be created the new transaction as a String
     * @param infoDTO     Aggregates the remaining needed information to create a transaction
     * @return DTO with all the information to create a group transaction to be passed to the service
     */
    public static CreateTransactionForGroupRequestDTO mapToRequestDTO(String personEmail, String groupID,
                                                                      CreateTransactionInfoDTO infoDTO) {

        return new CreateTransactionForGroupRequestDTO(personEmail, groupID, infoDTO.getAmount(), infoDTO.getDateTime(),
                infoDTO.getType(), infoDTO.getDescription(), infoDTO.getCategoryDesignation(),
                infoDTO.getDebitAccountID(),
                infoDTO.getCreditAccountID());
    }

    /**
     * Method mapToResponseDTO
     * maps the information that is supposed to return to the presentation layer when a new group transaction is created
     * inside a new DTO
     *
     * @param groupID                The identity of the group to whom the new transaction was created as a String
     * @param groupDescription       The description of the group to whom the new transaction was created as a String
     * @param amount                 The amount of the created transaction
     * @param type                   The type of the created transaction
     * @param transactionDescription The description of the created transaction
     * @return DTO with the information related to the created transaction to be returned to the presentation layer
     */
    public static CreateTransactionForGroupResponseDTO mapToResponseDTO(String groupID, String groupDescription,
                                                                        String amount, String type,
                                                                        String transactionDescription) {

        return new CreateTransactionForGroupResponseDTO(groupID, groupDescription, amount, type,
                transactionDescription);
    }

}