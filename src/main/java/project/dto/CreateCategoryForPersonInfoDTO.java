package project.dto;

import java.util.Objects;

public class CreateCategoryForPersonInfoDTO {

    private String designation;

    /**
     * Constructor for CreateCategoryForPersonInfoDTO
     *
     * @param designation category of category
     */
    public CreateCategoryForPersonInfoDTO(String designation) {
        setDesignation(designation);
    }

    /**
     * Constructor for CreateCategoryForPersonInfoDTO
     */
    private CreateCategoryForPersonInfoDTO() {
        //Added because of the dependency injection of the framework spring. Needs a empty constructor.
    }

    /**
     * method for getDesignation
     *
     * @return category
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * method setDesignation
     *
     * @param designation
     */
    private void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateCategoryForPersonInfoDTO that = (CreateCategoryForPersonInfoDTO) o;
        return Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation);
    }
}
