package project.dto;

import java.util.Objects;

public class CreateGroupRequestDTO {

    private String groupID;
    private String description;
    private String creationDate;
    private String creatorEmail;
    private String groupLedgerID;

    /**
     * Constructor of CreateGroupRequestDTO Class
     *
     * @param groupID       - Identity of the new group to be created as a String
     * @param description   - Description of the new group to be created as a String
     * @param creationDate  - Creation date of the new group to be created as a String
     * @param creatorEmail  - Identity of the new group's creator person as a String, who will be member and manager of the group
     * @param groupLedgerID - Identity of the new group's ledger to be created upon group's creation
     */
    public CreateGroupRequestDTO(String groupID, String description, String creationDate, String creatorEmail, String groupLedgerID) {
        setGroupID(groupID);
        setDescription(description);
        setCreationDate(creationDate);
        setCreatorEmail(creatorEmail);
        setGroupLedgerID(groupLedgerID);
    }

    /**
     * getGroupID method
     *
     * @return the identity of the group to be created as a String
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * setGroupID method
     * sets the value of the CreateGroupRequestDTO's groupID attribute to contain the new group's ID as a String
     *
     * @param groupID - Identity of the new group to be created as a String
     */
    private void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    /**
     * getDescription method
     *
     * @return the description of the new group to be created as a String
     */
    public String getDescription() {
        return description;
    }

    /**
     * setDescription method
     * sets the value of the CreateGroupRequestDTO's description attribute to contain the new group's description as a String
     *
     * @param description - description of the new group to be created as a String
     */
    private void setDescription(String description) {
        this.description = description;
    }

    /**
     * getCreationDate method
     *
     * @return the creation date of the new group to be created as a String
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * setCreationDate method
     * sets the value of the CreateGroupRequestDTO's creationDate attribute to contain the new group's creationDate as a String
     *
     * @param creationDate - creation date of the new group to be created as a String
     */
    private void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * getCreatorID method
     *
     * @return the identity of the person creator of the new group to be created as a String
     */
    public String getCreatorEmail() {
        return creatorEmail;
    }

    /**
     * setCreatorID method
     * sets the value of the CreateGroupRequestDTO's creatorID attribute to contain the identity of the new group's creator as a String
     *
     * @param creatorEmail - identity of the person who is the creator of the new group as a String
     */
    private void setCreatorEmail(String creatorEmail) {
        this.creatorEmail = creatorEmail;
    }

    /**
     * getGroupLedgerID method
     *
     * @return the identity of the ledger of the group to be created as a String
     */
    public String getGroupLedgerID() {
        return groupLedgerID;
    }

    /**
     * setGroupLedgerID method
     * sets the value of the CreateGroupRequestDTO's groupLedgerID attribute to contain the identity of the new group's ledger as a String
     *
     * @param groupLedgerID - identity of the new group's ledger as a String
     */
    private void setGroupLedgerID(String groupLedgerID) {
        this.groupLedgerID = groupLedgerID;
    }

    /**
     * Override of equals method
     *
     * @param o an Object
     * @return True if this Object is equal Object o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateGroupRequestDTO that = (CreateGroupRequestDTO) o;
        return Objects.equals(groupID, that.groupID) &&
                Objects.equals(description, that.description) &&
                Objects.equals(creationDate, that.creationDate) &&
                Objects.equals(creatorEmail, that.creatorEmail) &&
                Objects.equals(groupLedgerID, that.groupLedgerID);
    }

    /**
     * Override of hashCode method
     *
     * @return an integer with the hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(groupID, description, creationDate, creatorEmail, groupLedgerID);
    }
}
