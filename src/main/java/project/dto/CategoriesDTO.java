package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class CategoriesDTO extends RepresentationModel<CategoriesDTO> {

    private Set<String> categories;

    /**
     * Constructor for CreateCategoryForGroupDTO
     */
    public CategoriesDTO(Set<String> categories) {
        setCategories(categories);
    }

    /**
     * Method getCategories
     *
     * @return set of Strings with categories
     */
    public Set<String> getCategories() {
        return Collections.unmodifiableSet(categories);
    }

    /**
     * Method setCategories
     *
     * @param categories
     */
    private void setCategories(Set<String> categories) {
        this.categories = Collections.unmodifiableSet(categories);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoriesDTO that = (CategoriesDTO) o;
        return Objects.equals(categories, that.categories);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categories);
    }
}
