package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class MembersDTO extends RepresentationModel<MembersDTO> {

    private List<String> membersIDs;
    private List<String> names;
    private List<Boolean> isManager;


    /**
     * Constructor for MembersDTO
     */
    public MembersDTO(List<String> membersIDs, List<String> names, List<Boolean> isManager) {
        setMembersIDs(membersIDs);
        setNames(names);
        setIsManager(isManager);
    }

    /**
     * Get method
     *
     * @return Group list of members IDs
     */
    public List<String> getMembersIDs() {
        return Collections.unmodifiableList(membersIDs);
    }

    /**
     * List method
     *
     * @param membersIDs Group list of members IDs in string format
     */
    private void setMembersIDs(List<String> membersIDs) {
        this.membersIDs = Collections.unmodifiableList(membersIDs);
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public List<Boolean> getIsManager() {
        return isManager;
    }

    public void setIsManager(List<Boolean> isManager) {
        this.isManager = isManager;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MembersDTO that = (MembersDTO) o;
        return Objects.equals(membersIDs, that.membersIDs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(membersIDs);
    }
}
