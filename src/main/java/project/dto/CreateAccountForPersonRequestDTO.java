package project.dto;

import java.util.Objects;

public class CreateAccountForPersonRequestDTO {

    private String accountID;
    private String personEmail;
    private String accountDenomination;
    private String accountDescription;

    /**
     * Constructor for CreateAccountForPersonRequestDTO
     *
     * @param accountID           AccountID of the account to be created
     * @param personEmail         PersonID of the person who wants to create the new account
     * @param accountDenomination Denomination of the account to be created
     * @param accountDescription  Description of the account to be created
     */
    public CreateAccountForPersonRequestDTO(String accountID, String personEmail, String accountDenomination, String accountDescription) {
        setAccountID(accountID);
        setPersonEmail(personEmail);
        setAccountDenomination(accountDenomination);
        setAccountDescription(accountDescription);
    }

    /**
     * Method getAccountID
     *
     * @return object AccountID
     */
    public String getAccountID() {
        return accountID;
    }

    /**
     * Method setAccountID
     *
     * @param accountID String AccountID
     */
    private void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    /**
     * Method getPersonID
     *
     * @return object PersonID
     */
    public String getPersonEmail() {
        return personEmail;
    }

    /**
     * Method setPersonID
     *
     * @param personEmail String AccountID
     */
    private void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    /**
     * Method getDenomination
     *
     * @return denomination of the account
     */
    public String getAccountDenomination() {
        return accountDenomination;
    }

    /**
     * Method setDenomination
     *
     * @param accountDenomination Denomination of the account
     */
    private void setAccountDenomination(String accountDenomination) {
        this.accountDenomination = accountDenomination;
    }

    /**
     * Method getDescription
     *
     * @return description of the account
     */
    public String getAccountDescription() {
        return accountDescription;
    }

    /**
     * Method setDescription
     *
     * @param accountDescription Description of the account
     */
    private void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateAccountForPersonRequestDTO that = (CreateAccountForPersonRequestDTO) o;
        return Objects.equals(accountID, that.accountID) &&
                Objects.equals(personEmail, that.personEmail) &&
                Objects.equals(accountDenomination, that.accountDenomination) &&
                Objects.equals(accountDescription, that.accountDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountID, personEmail, accountDenomination, accountDescription);
    }
}
