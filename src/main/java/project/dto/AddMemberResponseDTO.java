package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class AddMemberResponseDTO extends RepresentationModel<AddMemberResponseDTO> {

    private String newMemberEmail;
    private String groupID;
    private String groupDescription;

    /**
     * Constructor method
     *
     * @param groupID          Id of the group to which the new member is to be added
     * @param groupDescription Description of the group
     * @param memberID         Id of person to be added to group
     */
    public AddMemberResponseDTO(String groupID, String groupDescription, String memberID) {
        setNewMemberEmail(memberID);
        setGroupID(groupID);
        setGroupDescription(groupDescription);
    }

    /**
     * get method
     *
     * @return ID of the new member to be added to group
     */
    public String getNewMemberEmail() {
        return newMemberEmail;
    }

    /**
     * set method
     *
     * @param newMemberEmail ID of the new member to be added to group
     */
    private void setNewMemberEmail(String newMemberEmail) {
        this.newMemberEmail = newMemberEmail;
    }

    /**
     * get method
     *
     * @return ID of the group to which the new member is to be added
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * set method
     *
     * @param groupID ID of the group to which the new member is to be added
     */
    private void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    /**
     * get method
     *
     * @return description of the group
     */
    public String getGroupDescription() {
        return groupDescription;
    }

    /**
     * set method
     *
     * @param groupDescription description of the group
     */
    private void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddMemberResponseDTO that = (AddMemberResponseDTO) o;
        return Objects.equals(newMemberEmail, that.newMemberEmail) &&
                Objects.equals(groupID, that.groupID) &&
                Objects.equals(groupDescription, that.groupDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), newMemberEmail, groupID, groupDescription);
    }
}
