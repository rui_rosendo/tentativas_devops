package project.dto;

import java.util.Objects;

public class CheckIfPeopleAreSiblingsRequestDTO {
    String personEmail1;
    String personEmail2;

    /**
     * Constructor of PeopleAreSiblingsRequestDTO.
     *
     * @param personEmail1 ID of person 1
     * @param personEmail2 ID of person 2
     */
    public CheckIfPeopleAreSiblingsRequestDTO(String personEmail1, String personEmail2) {
        setPersonEmail1(personEmail1);
        setPersonEmail2(personEmail2);
    }

    /**
     * Get personID1
     *
     * @return personID1
     */
    public String getPersonEmail1() {
        return personEmail1;
    }

    /**
     * Set personID1
     *
     * @param personID ID of person
     */
    private void setPersonEmail1(String personID) {
        this.personEmail1 = personID;
    }

    /**
     * Get personID2
     *
     * @return personID2
     */
    public String getPersonEmail2() {
        return personEmail2;
    }

    /**
     * Set personID2
     *
     * @param personID ID of person
     */
    private void setPersonEmail2(String personID) {
        this.personEmail2 = personID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CheckIfPeopleAreSiblingsRequestDTO that = (CheckIfPeopleAreSiblingsRequestDTO) o;
        return Objects.equals(personEmail1, that.personEmail1) &&
                Objects.equals(personEmail2, that.personEmail2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personEmail1, personEmail2);
    }

}


