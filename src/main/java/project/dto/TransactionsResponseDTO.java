package project.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.List;

@Getter
@EqualsAndHashCode(callSuper = false)
public class TransactionsResponseDTO extends RepresentationModel<TransactionsResponseDTO> {

    private List<TransactionDTO> transactions;

    /**
     * Constructor for TransactionsResponseDTO
     *
     * @param transactions Set of transactions as a TRing
     */
    public TransactionsResponseDTO(List<TransactionDTO> transactions) {
        this.transactions = Collections.unmodifiableList(transactions);
    }

    @Override
    public String toString() {
        return "TransactionsResponseDTO{" +
                "transactions=" + transactions +
                '}';
    }
}
