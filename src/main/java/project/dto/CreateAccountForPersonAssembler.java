package project.dto;

public class CreateAccountForPersonAssembler {

    /**
     * Constructor for CreateAccountForPersonAssembler
     * Created to override the public constructor Java creates automatically if there is not a constructor for a class
     */
    CreateAccountForPersonAssembler() {
        //Intentionally empty
    }

    /**
     * Method MapToResponseDTO
     * <p>
     * Assembles a CreateAccountForPersonResponseDTO
     *
     * @param personEmail  String of PersonID
     * @param name         String of name of the person
     * @param accountID    String of accountID
     * @param denomination String of denomination
     * @return CreateAccountForPersonResponseDTO    Response DTO with parameters
     */
    public static CreateAccountForPersonResponseDTO mapToResponseDTO(String personEmail, String name, String accountID, String denomination) {
        return new CreateAccountForPersonResponseDTO(personEmail, name, accountID, denomination);
    }

    /**
     * Method mapToDTORequest
     * <p>
     * Assembles a CreateAccountForPersonRequestDTO
     *
     * @param accountID    AccountID of the account to be created
     * @param personEmail  PersonID of the person who wants to create the account
     * @param denomination Denomination of the account to be created
     * @param description  Description of the account to be created
     * @return CreateAccountForPersonRequestDTO    Response DTO with parameters
     */
    public static CreateAccountForPersonRequestDTO mapToRequestDTO(String accountID, String personEmail, String denomination, String description) {
        return new CreateAccountForPersonRequestDTO(accountID, personEmail, denomination, description);
    }

}
