package project.infrastructure.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.datamodel.PersonDomainDataAssembler;
import project.datamodel.PersonJpa;
import project.infrastructure.repositories.jpa.PersonJpaRepository;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

@Component
public class PersonRepositoryDB implements PersonRepository {

    @Autowired
    PersonJpaRepository personJpaRepository;

    /**
     * Constructor of class PersonRepository
     */
    public PersonRepositoryDB() {
    }

    /**
     * Method findById
     *
     * @param personID PersonID
     * @return Optional<Person> containing person in repository
     */
    public Optional<Person> findById(PersonID personID) {

        Optional<Person> result = Optional.empty();

        if (personID != null) {

            Optional<PersonJpa> optionalPersonJpa = personJpaRepository.findById(personID);

            if (optionalPersonJpa.isPresent()) {
                PersonJpa personJpa = optionalPersonJpa.get();

                Person person = PersonDomainDataAssembler.toDomain(personJpa);

                result = Optional.of(person);
            }
        }
        return result;
    }

    /**
     * Method save
     *
     * @param person Person
     * @return Person that was saved to repository
     */
    public Person save(Person person) {

        PersonJpa personJpa = PersonDomainDataAssembler.toData(person);

        PersonJpa savedPersonJpa = personJpaRepository.save(personJpa);

        return PersonDomainDataAssembler.toDomain(savedPersonJpa);
    }
}