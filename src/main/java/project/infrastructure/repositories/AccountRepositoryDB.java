package project.infrastructure.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.datamodel.AccountDomainDataAssembler;
import project.datamodel.AccountJpa;
import project.infrastructure.repositories.jpa.AccountJpaRepository;
import project.model.entities.account.Account;
import project.model.entities.shared.AccountID;
import project.model.specifications.repositories.AccountRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class AccountRepositoryDB implements AccountRepository {

    final Set<Account> accounts;

    @Autowired
    AccountJpaRepository accountRepositoryJpa;

    /**
     * Constructor of class ListOfAccounts
     */
    public AccountRepositoryDB() {
        accounts = new HashSet<>();
    }

    /**
     * Method Save
     * Save the account in the database
     *
     * @param account Account
     * @return
     */
    public Account save(Account account) {

        AccountJpa accountJpa = AccountDomainDataAssembler.toData(account);

        AccountJpa savedAccountJpa = accountRepositoryJpa.save(accountJpa);

        Account savedAccount = AccountDomainDataAssembler.toDomain(savedAccountJpa);

        return savedAccount;
    }

    /**
     * Method findByAccountID
     * gets a account with a given ID from the JpaRepository
     *
     * @param accountID
     * @return
     */
    public Optional<Account> findById(AccountID accountID) {
        Optional<Account> result = Optional.empty();


        if (accountID != null) {
            Optional<AccountJpa> opAccountJpa = accountRepositoryJpa.findById(accountID);

            if (opAccountJpa.isPresent()) {
                AccountJpa accountJpa = opAccountJpa.get();

                Account account = AccountDomainDataAssembler.toDomain(accountJpa);

                result = Optional.of(account);
            }
        }

        return result;
    }

    /**
     * Method existsById
     * Verifies if a given AccountId is in the repository
     *
     * @param accountID to be confirmed if present in the repository
     * @return True if the account is present in the repository else return false
     */
    public boolean existsById(AccountID accountID) {
        return accountRepositoryJpa.existsById(accountID);
    }
}