package project.model.specifications.repositories;

import project.model.entities.ledger.Ledger;
import project.model.entities.shared.LedgerID;

import java.util.Optional;

/**
 * Interface LedgerRepository
 */
public interface LedgerRepository {

    Optional<Ledger> findById(final LedgerID ledgerID);

    Ledger save(final Ledger object);

}
