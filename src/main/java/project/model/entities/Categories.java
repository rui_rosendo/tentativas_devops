package project.model.entities;

import project.exceptions.InvalidFieldException;
import project.model.entities.shared.Category;

import java.util.HashSet;
import java.util.Set;

public class Categories {

    final Set<Category> categoriesValue;

    /**
     * Constructor for class ListOfCategories
     */
    public Categories() {
        categoriesValue = new HashSet<>();
    }

    /**
     * Create and add category to listOfCategories
     *
     * @param designation Designation for Category
     * @return True if addition is successful or False if category already exists and does not add
     * @throws IllegalArgumentException if new category instantiation fails (isStringValid)
     */
    public boolean addCategory(String designation) {
        Category newCategory = new Category(designation);
        return this.categoriesValue.add(newCategory);
    }

    /**
     * Check if listOfCategories has newCategory
     *
     * @param newCategory new category to search for
     */
    public boolean hasCategory(Category newCategory) {
        if (newCategory != null) {
            return this.categoriesValue.contains(newCategory);
        } else {
            throw new InvalidFieldException("Input 'category' is invalid!");
        }
    }

    /**
     * Method for getCategories
     *
     * @return set of category
     */
    public Set<Category> getCategoriesValue() {
        return categoriesValue;
    }

    /**
     *ToString Categories
     * @return
     */
    @Override
    public String toString() {
        return "Categories{" +
                "categories=" + categoriesValue +
                '}';
    }
}
