package project.model.entities.group;

import lombok.Setter;
import project.exceptions.InvalidFieldException;
import project.frameworkddd.Root;
import project.model.entities.Categories;
import project.model.entities.CreationDate;
import project.model.entities.shared.*;

import java.util.ArrayList;
import java.util.Objects;

public class Group implements Root<GroupID> {
    @Setter
    private PersonsIDs members;
    private PersonsIDs managers;
    @Setter
    private Categories categories;
    private GroupID id;
    private LedgerID ledgerID;
    @Setter
    private AccountsIDs accountsIDs;
    private Description description;
    private CreationDate creationDate;

    /**
     * Constructor for Class Group
     *
     * @param groupID      ID of group
     * @param description  Description of group
     * @param creationDate Creation Date
     * @param creatorID    IF of Person creator of the group
     * @param ledgerID     ID of ledger of the group
     */
    public Group(GroupID groupID, String description, String creationDate, PersonID creatorID, LedgerID ledgerID) {
        setId(groupID);
        setLedgerId(ledgerID);
        this.accountsIDs = new AccountsIDs();
        this.members = new PersonsIDs();
        this.managers = new PersonsIDs();
        this.categories = new Categories();
        setGroupDescription(description);
        setCreationDate(creationDate);
        this.members.addPersonID(creatorID);
        this.managers.addPersonID(creatorID);
    }

    public PersonsIDs getManagersIDs() {
        return managers;
    }

    public Categories getCategories() {
        return categories;
    }

    public GroupID getID() {
        return this.id;
    }

    /**
     * Set GroupId
     * Throws exception if null
     *
     * @param id ID of group
     */
    private void setId(GroupID id) {
        if (id != null) {
            this.id = id;
        } else {
            throw new InvalidFieldException("Input 'id' cannot be null!");
        }
    }

    /**
     * Method getLedgerID
     *
     * @return LedgerID
     */
    public LedgerID getLedgerID() {
        return ledgerID;
    }

    /**
     * Mehod getAccountsIDs
     *
     * @return AccountsIDs
     */
    public AccountsIDs getAccountsIDs() {
        return accountsIDs;
    }

    /**
     * Method getDescription
     *
     * @return Description
     */
    public Description getDescription() {
        return description;
    }

    /**
     * MethodCreationDate
     *
     * @return CreationGate
     */
    public CreationDate getCreationDate() {
        return creationDate;
    }


    /**
     * Create attribute creationDate
     * Checks if creationDate is appropriate (not null, not blank)
     *
     * @param creationDate Date of creation
     */
    private void setCreationDate(String creationDate) {
        this.creationDate = new CreationDate(creationDate);
    }

    /**
     * Set LedgerId
     * Throws exception if null
     *
     * @param ledgerID ID of ledger
     */
    private void setLedgerId(LedgerID ledgerID) {
        if (ledgerID != null) {
            this.ledgerID = ledgerID;
        } else {
            throw new InvalidFieldException("Input 'id' cannot be null!");
        }
    }

    /**
     * Create attribute description
     * Checks if description is appropriate (not null, not blank)
     *
     * @param description Group Description
     */
    private void setGroupDescription(String description) {
        this.description = new Description(description);
    }

    /**
     * Method addMember
     * Adds member to the memberList
     *
     * @param personID ID of Person to add to group
     */
    public boolean addMemberID(PersonID personID) {
        return this.members.addPersonID(personID);
    }

    /**
     * Method addMember
     * Adds member to the memberList
     *
     * @param personID Identity of the new person manager to be added to group
     * @return True if personID was added and false if not
     */
    public boolean addManagerID(PersonID personID) {
        return this.managers.addPersonID(personID);
    }

    /**
     * Add an account to accounts
     *
     * @param accountID ID of  account to add
     * @return True/False if successful
     */
    public boolean addAccount(AccountID accountID) {
        return this.accountsIDs.addAccountID(accountID);
    }

    /**
     * Add a category to listOfCategories
     * Delegates functionality to ListOfCategories class
     *
     * @param designation Designation of category to add
     * @return True/False if successful, Exception from ListOfCategories
     */
    public boolean addCategory(String designation) {
        return this.categories.addCategory(designation);
    }

    /**
     * Check if a personID given by parameter is in a group
     *
     * @param personID search personID
     * @return True if the personID is in the group
     */
    public boolean hasMemberID(PersonID personID) {
        return members.containsPersonID(personID);
    }

    /**
     * Check if a personID given by parameter is in a group's managers list
     *
     * @param personID search personID
     * @return True if the personID is in the group's managers list
     */
    public boolean hasManagerID(PersonID personID) {
        return managers.containsPersonID(personID);
    }

    /**
     * Get all member IDs
     *
     * @return Instance of PersonsIDs with all member IDs
     */
    public PersonsIDs getMembersIDs() {
        return members;
    }

    /**
     * Method sameIdentityAs
     *
     * @param other New person to compare identity
     * @return True if this Person has same id as other
     */
    public boolean sameIdentityAs(Group other) {
        return this.id.sameValueAs(other.id);
    }

    /**
     * Method hasAccountID
     *
     * @param accountID accountID
     * @return true if contains, false if not
     */
    public boolean hasAccountID(AccountID accountID) {
        return accountsIDs.getAccountsIDsValue().contains(accountID);
    }


    /**
     * Override of equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group other = (Group) o;
        return sameIdentityAs(other);
    }

    /**
     * Override of hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(description, creationDate);
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "Group{" +
                "groupID=" + id +
                ", members=" + members +
                ", managers=" + managers +
                ", ledger=" + ledgerID +
                ", categories=" + categories +
                ", accountIDs=" + accountsIDs +
                ", description=" + description +
                ", creationDate=" + creationDate +
                '}';
    }

    /**
     * Get Creator ID
     *
     * @return
     */
    public PersonID getCreatorID() {
        ArrayList<PersonID> managersIds = new ArrayList<>(this.managers.getPersonIDs());
        return managersIds.get(0);
    }
}
