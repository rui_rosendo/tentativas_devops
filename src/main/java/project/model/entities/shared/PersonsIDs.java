package project.model.entities.shared;

import project.frameworkddd.ValueObject;

import java.util.HashSet;
import java.util.Set;

public class PersonsIDs implements ValueObject {
    private final Set<PersonID> personIDs;

    /**
     * Constructor for PersonIDs
     */
    public PersonsIDs() {
        personIDs = new HashSet<>();
    }

    /**
     * Add a new personID
     *
     * @param personID personID to add
     * @return true if successful, false if repeated
     */
    public boolean addPersonID(PersonID personID) {
        return this.personIDs.add(personID);
    }

    /**
     * Check if contains personID
     *
     * @param personID personID to check
     * @return true if found, false if not
     */
    public boolean containsPersonID(PersonID personID) {
        return this.personIDs.contains(personID);
    }

    /**
     * Get size of array
     *
     * @return size of personIDs
     */
    public int size() {
        return this.personIDs.size();
    }

    /**
     * Get all personIDs
     *
     * @return new Set containing all personIDs
     */
    public Set<PersonID> getPersonIDs() {
        Set<PersonID> members = new HashSet<>();
        members.addAll(personIDs);
        return members;
    }

    @Override
    public String toString() {
        return "Persons{" +
                "persons=" + personIDs +
                '}';
    }
}
