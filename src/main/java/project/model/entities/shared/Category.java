package project.model.entities.shared;

import lombok.NoArgsConstructor;
import project.frameworkddd.ValueObject;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@NoArgsConstructor
@Embeddable
public class Category implements ValueObject, Serializable {

    private Designation designation;

    /**
     * Constructor for class Category
     *
     * @param designation Designation of category
     */
    public Category(String designation) {
        this.designation = new Designation(designation);
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "Category{" +
                "designation=" + designation +
                '}';
    }

    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Category category = (Category) o;
        return Objects.equals(designation, category.designation);
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(designation);
    }

    /**
     * toStringDTO method
     *
     * @return String designation
     */
    public String toStringDTO() {
        return designation.toStringDTO();
    }

}


