package project.model.entities.ledger;

import lombok.Getter;
import lombok.Setter;
import project.exceptions.InvalidFieldException;
import project.frameworkddd.Root;
import project.model.entities.Transactions;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.Period;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


public class Ledger implements Root<LedgerID> {
    @Getter
    @Setter
    private Transactions transactions;
    @Getter
    private LedgerID id;


    /**
     * Constructor of class Ledger
     */
    public Ledger(LedgerID id) {
        setLedgerID(id);
        this.transactions = new Transactions();
    }

    /**
     * Create and add Transaction to Ledger
     *
     * @param amount          Amount of the Transaction
     * @param type            Type of the Transaction
     * @param date            Date of the Transaction
     * @param description     Description of the Transaction
     * @param category        Category of the Transaction
     * @param debitAccountID  ID Debit Account of the Transaction
     * @param creditAccountID ID Credit Account of the Transaction
     * @return True/False if successful
     */

    /**
     * Set ledgerID
     * Throws exception if null
     *
     * @param id ID of Ledger
     */
    private void setLedgerID(LedgerID id) {
        if (id != null) {
            this.id = id;
        } else {
            throw new InvalidFieldException("Input 'id' cannot be null!");
        }
    }

    public void addTransaction(String amount, Type type, String date, String description, Category category,
                               AccountID debitAccountID, AccountID creditAccountID) {
        Transaction newTransaction = new Transaction(amount, type, date, description, category, debitAccountID,
                creditAccountID);
        transactions.addTransaction(newTransaction);
    }

    /**
     * Get balance of ledger within period
     *
     * @param period Period of search
     * @return balance
     */
    public double getBalanceWithinPeriod(Period period) {
        double balance = 0;
        for (Transaction transaction : this.getTransactionsWithinPeriod(period)) {
            balance += transaction.getFactoredAmount();
        }
        return balance;
    }

    /**
     * Get transactions within period dates
     *
     * @param period Period of search
     * @return set of transactions
     */
    public Set<Transaction> getTransactionsWithinPeriod(Period period) {
        Set<Transaction> result = new HashSet<>();
        for (Transaction transaction : transactions.getAll()) {
            if (transaction.isWithinPeriod(period)) {
                result.add(transaction);
            }
        }
        return result;
    }

    /**
     * Get list of transactions from ledger that have a specific account and are within a period
     *
     * @param accountID Each transaction is checked for the presence of this specific account whether as debit
     *                  account or credit account
     * @param period    Period of search
     * @return set of transactions
     */
    public Set<Transaction> getTransactionsOfAccountWithinPeriod(AccountID accountID, Period period) {
        Set<Transaction> transactionsOfAccountWithinPeriod = new HashSet<>();
        Set<Transaction> transactionsWithinPeriod = getTransactionsWithinPeriod(period);
        for (Transaction transaction : transactionsWithinPeriod) {
            if (transaction.hasAccountID(accountID)) {
                transactionsOfAccountWithinPeriod.add(transaction);
            }
        }
        return transactionsOfAccountWithinPeriod;
    }


    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "Ledger{" +
                "ledgerID=" + id +
                ", transactions=" + transactions +
                '}';
    }

    /**
     * Get getLedgerID
     *
     * @return ledgerID
     */
    public LedgerID getID() {
        return id;
    }

    /**
     * Check if the the objects have the same identity
     *
     * @param other
     * @return
     */
    public boolean sameIdentityAs(Ledger other) {
        return this.id.sameValueAs(other.id);
    }

    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Ledger other = (Ledger) o;
        return sameIdentityAs(other);
    }

    /**
     * Override hashcode
     *
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean sameAs(Object other) {
        return false;
    }

}
