package project.model.entities;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Birthdate {

    LocalDate birthdateValue;

    /**
     * Constructor for Class Birthdate
     * Calls isDateValid for validation
     * throw a exception if the string (null, empty, composed by just spaces or not format valid)
     *
     * @param birthdate Birthdate
     */
    public Birthdate(String birthdate) {
        setBirthdateValue(birthdate);
    }

    /**
     * Create attribute birthdate
     *
     * @param birthdateValue birthdate of person
     */
    private void setBirthdateValue(String birthdateValue) {
        this.birthdateValue = LocalDate.parse(birthdateValue, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    /**
     * Override equals
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Birthdate that = (Birthdate) o;
        return Objects.equals(birthdateValue, that.birthdateValue);
    }

    /**
     * Override hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(birthdateValue);
    }

    /**
     * Override toString
     */
    @Override
    public String toString() {
        return "Birthdate{" +
                "birthdate='" + birthdateValue + '\'' +
                '}';
    }

    public String getBirthdateValue() {
        return birthdateValue.toString();
    }
}
