package project.model.entities;

import project.exceptions.TransactionAlreadyExistsException;
import project.model.entities.ledger.Transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Transactions {

    final List<Transaction> transactionsValue;

    /**
     * Constructor for Class Transactions
     */
    public Transactions() {
        this.transactionsValue = new ArrayList<>();
    }

    /**
     * Method addTransaction
     * adds transaction to transactions
     *
     * @param newTransaction new Transaction to be added to transactions
     * @return True if addition is successful or False if newTransaction already exists and does not add
     */
    public void addTransaction(Transaction newTransaction) {
        if (transactionsValue.contains(newTransaction)) {
            throw new TransactionAlreadyExistsException();
        } else {
            this.transactionsValue.add(newTransaction);
        }

    }

    /**
     * Method getAll
     * gets all transactions in transactions
     *
     * @return a Set of every transaction in transactions
     */
    public List<Transaction> getAll() {
//        List<Transaction> allTransactions = new ArrayList<>();
//        allTransactions.addAll(this.transactionsValue);
        return Collections.unmodifiableList(transactionsValue);
    }

    /**
     * Method size
     * gets the size of Transactions
     *
     * @return a integer that corresponds to the size of the transactions HashSet
     */
    public int size() {
        return this.transactionsValue.size();
    }

    /**
     * Override toString method
     *
     * @return String of every transaction in transactions
     */
    @Override
    public String toString() {
        return "Transactions{" +
                "transactions=" + transactionsValue +
                '}';
    }
}
