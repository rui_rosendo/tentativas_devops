package project.datamodel;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import project.model.entities.ledger.Amount;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.Description;

import javax.persistence.*;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "transactions")
public class TransactionJpa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "ledger_id")
    private LedgerJpa ledgerJpa;

    @Embedded
    private Amount amount;

    private String dateTime;

    private String type;

    @Embedded
    private Description description;

    @Embedded
    private Category category;

    @Embedded
    @AttributeOverride(name = "accountIdValue", column = @Column(name = "debit_account_id"))
    private AccountID debitAccountId;

    @Embedded
    @AttributeOverride(name = "accountIdValue", column = @Column(name = "credit_account_id"))
    private AccountID creditAccountId;

    public TransactionJpa(LedgerJpa ledgerJpa, Amount amount, String dateTime, String type, Description description,
                          Category category,
                          AccountID debitAccountId, AccountID creditAccountId) {
        this.ledgerJpa = ledgerJpa;
        this.amount = amount;
        this.dateTime = dateTime;
        this.type = type;
        this.description = description;
        this.category = category;
        this.debitAccountId = debitAccountId;
        this.creditAccountId = creditAccountId;
    }

    @Override
    public String toString() {
        return "TransactionJpa{" +
                "id=" + id +
//                ", ledgerJpa=" + ledgerJpa +
                ", amount=" + amount +
                ", dateTime='" + dateTime + '\'' +
                ", type='" + type + '\'' +
                ", description=" + description +
                ", category=" + category +
                ", debitAccountId=" + debitAccountId +
                ", creditAccountId=" + creditAccountId +
                '}';
    }
}
