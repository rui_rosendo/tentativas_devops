package project.datamodel;

import org.springframework.stereotype.Service;
import project.model.entities.person.Person;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.PersonID;
import project.model.entities.shared.PersonsIDs;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class PersonDomainDataAssembler {

    /**
     * Constructor
     */
    private PersonDomainDataAssembler() {
        //Empty constructor
    }

    /**
     * Method toData
     * <p>
     * Converts domain object of Person to JPA representation.
     * This method is supposed to be called for a person that has null mother and father.
     * It creates the JPA representation with the Person's PersonID, Name, Address, BirthPlace, BirthDate and LedgerID.
     * Furthermore, it considers the Person's AccountsIDs and Siblings.
     */
    public static PersonJpa toData(Person person) {

        PersonJpa personJpa = new PersonJpa(person.getID(),
                person.getName().toStringDTO(),
                person.getAddress().getAddressValue(),
                person.getBirthPlace().getBirthplaceValue(),
                person.getBirthDate().getBirthdateValue(),
                person.getLedgerID());

        // Parents
        personJpa.setParents(person.getMotherID(), person.getFatherID());

        // Siblings
        List<PersonID> personsIDs = new ArrayList<>(person.getSiblingsIDs().getPersonIDs());
        personJpa.setSiblings(personsIDs);

        // Accounts
        Set<AccountID> accountsIdsSet = person.getAccountsIDs().getAccountsIDsValue();
        List<AccountID> accountsIdsList = new ArrayList<>();
        accountsIdsList.addAll(accountsIdsSet);
        personJpa.setAccountsIds(accountsIdsList);

        return personJpa;
    }

    /**
     * Method toDomain
     * <p>
     * Converts JPA representation of Person to domain object.
     */
    public static Person toDomain(PersonJpa personJpa) {

        PersonID motherID = personJpa.getMother();
        PersonID fatherID = personJpa.getFather();

        Person savedPerson = new Person(personJpa.getId(), personJpa.getName(), personJpa.getAddress(),
                personJpa.getBirthPlace(), personJpa.getBirthDate(), motherID, fatherID, personJpa.getLedgerID());

        PersonsIDs siblingsIDs = new PersonsIDs();
        for (PersonID siblingID : personJpa.getSiblings()) {
            siblingsIDs.addPersonID(siblingID);
        }
        savedPerson.setSiblingsIDs(siblingsIDs);

        // Accounts
        List<AccountID> accountIDS = new ArrayList<>(personJpa.getAccountsIds());
        for (AccountID accountID : accountIDS) {
            savedPerson.addAccount(accountID);
        }

        return savedPerson;
    }

}
