package project.datamodel;

import project.model.entities.shared.AccountID;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Converter
public class IdsListConverter implements AttributeConverter<List<AccountID>, String> {

    //TODO : Create an interface for VO Id's to generalize this converter to all List of Ids

    /**
     * method convertToDatabaseColumn
     * Converts a List of AccountID in a string with all accountID separated with ','
     * to represent in the column of the data base table
     *
     * @param accountIDS List of AccountID
     * @return String with all AccountID separated with ','
     */
    @Override
    public String convertToDatabaseColumn(List<AccountID> accountIDS) {

        List<String> accountsIdsStg = new ArrayList<>();
        if (accountIDS != null) {
            for (AccountID accountID : accountIDS) {
                accountsIdsStg.add(accountID.toStringDTO());

            }
        }
        return String.join(",", accountsIdsStg);
    }

    /**
     * method convertToEntityAttribute
     * converts a string with AccountsIDs in string format to a List of AccountID
     *
     * @param s string with all AccountsIDs in string format separated with ',' ("example,example")
     * @return List of VOs AccountID
     */
    @Override
    public List<AccountID> convertToEntityAttribute(String s) {

        List<String> accountsIdsStg = new ArrayList<>(Arrays.asList(s.split(",")));

        List<AccountID> accountsIds = new ArrayList<>();

        if (!accountsIdsStg.get(0).equals("")) {
            for (String accountIdStg : accountsIdsStg) {
                accountsIds.add(new AccountID(accountIdStg));
            }
        }

        return accountsIds;
    }
}
