package project.datamodel;

import org.springframework.stereotype.Service;
import project.model.entities.group.Group;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.PersonID;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class GroupDomainDataAssembler {

    public static GroupJpa toData(Group group) {
        GroupJpa groupJpa = new GroupJpa(group.getID(),
                group.getDescription(),
                group.getCreationDate(),
                group.getCreatorID(),
                group.getLedgerID());
        Set<PersonID> membersSet = group.getMembersIDs().getPersonIDs();
        List<PersonID> membersList = new ArrayList<>();
        membersList.addAll(membersSet);
        groupJpa.setMembers(membersList);
        Set<AccountID> accountsIdsSet = group.getAccountsIDs().getAccountsIDsValue();
        List<AccountID> accountsIdsList = new ArrayList<>();
        accountsIdsList.addAll(accountsIdsSet);
        groupJpa.setAccountsIds(accountsIdsList);
        Set<Category> categoriesSet = group.getCategories().getCategoriesValue();
        List<Category> categoriesList = new ArrayList<>();
        categoriesList.addAll(categoriesSet);
        groupJpa.setCategories(categoriesList);
        return groupJpa;
    }

    public static Group toDomain(GroupJpa groupJpa) {
        Group group = new Group(groupJpa.getId(), groupJpa.getDescription().getDescriptionValue(),
                groupJpa.getCreationDate().toStringDTO(), groupJpa.getManagers().get(0),
                groupJpa.getLedgerID());
        List<PersonID> membersId = new ArrayList<>();
        membersId.addAll(groupJpa.getMembers());
        List<AccountID> accountIDS = new ArrayList<>();
        accountIDS.addAll(groupJpa.getAccountsIds());
        List<Category> categories = new ArrayList<>();
        categories.addAll(groupJpa.getCategories());
        for (PersonID memberID : membersId) {
            group.addMemberID(memberID);
        }
        for (AccountID accountID : accountIDS) {
            group.addAccount(accountID);
        }
        for (Category category : categories) {
            group.addCategory(category.toStringDTO());
        }
        return group;
    }
}
