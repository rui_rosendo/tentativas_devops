package project.datamodel;

import lombok.Data;
import lombok.NoArgsConstructor;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "persons")
public class PersonJpa {

    @AttributeOverrides({@AttributeOverride(name = "personEmail", column = @Column(name = "PersonEmail"))})
    @EmbeddedId
    private PersonID id;

    private String name;
    private String address;
    private String birthPlace;
    private String birthDate;
    private LedgerID ledgerID;

    @ElementCollection
    @CollectionTable(name = "PersonAccounts", joinColumns = @JoinColumn(name = "PersonEmail"))
    private List<AccountID> accountsIds;

    @ElementCollection
    @CollectionTable(name = "siblings", joinColumns = @JoinColumn(name = "SiblingEmail"))
    private List<PersonID> siblings;

    @AttributeOverrides({@AttributeOverride(name = "personEmail", column = @Column(name = "motherEmail"))})
    @Embedded
    private PersonID mother;

    @AttributeOverrides({@AttributeOverride(name = "personEmail", column = @Column(name = "fatherEmail"))})
    @Embedded
    private PersonID father;

    /**
     * Constructor with Strings as parameters
     *
     * @param id         ID of Person
     * @param name       Name of Person
     * @param address    Address of Person
     * @param birthPlace Birthplace of Person
     * @param birthDate  Birthdate of Person
     * @param ledgerID   LedgerID of Person
     */
    public PersonJpa(String id, String name, String address, String birthPlace, String birthDate, String ledgerID) {
        this.id = new PersonID(id);

        this.name = name;
        this.address = address;
        this.birthPlace = birthPlace;
        this.birthDate = birthDate;
        this.ledgerID = new LedgerID(ledgerID);
        accountsIds = new ArrayList<>();
        siblings = new ArrayList<>();
    }

    /**
     * Constructor with VOs as parameters
     *
     * @param id ID of Person
     * @param name Name of Person
     * @param address Address of Person
     * @param birthPlace Birthplace of Person
     * @param birthDate Birthdate of Person
     * @param ledgerID LedgerID of Person
     */
    public PersonJpa(PersonID id, String name, String address, String birthPlace, String birthDate, LedgerID ledgerID) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.birthPlace = birthPlace;
        this.birthDate = birthDate;
        this.ledgerID = ledgerID;
        accountsIds = new ArrayList<>();
        siblings = new ArrayList<>();
    }

    /**
     * Set accounts IDs PersonJpa
     *
     * @param accountsIds Accounts of Person
     */
    public void setAccountsIds(List<AccountID> accountsIds) {
        this.accountsIds = Collections.unmodifiableList(accountsIds);
    }

    /**
     * Set parents of Person
     *
     * @param motherID PersonID of mother
     * @param fatherID PersonID of father
     */
    public void setParents(PersonID motherID, PersonID fatherID) {
        if (motherID != null && fatherID != null) {
            this.mother = motherID;
            this.father = fatherID;
        }
    }

    /**
     * Set siblings of Person
     *
     * @param siblings List of PersonID of siblings
     */
    public void setSiblings(List<PersonID> siblings) {
        this.siblings = Collections.unmodifiableList(siblings);
    }

}
