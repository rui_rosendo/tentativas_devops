package project.datamodel;

import project.model.entities.shared.Category;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Converter
public class CategoriesListConverter implements AttributeConverter<List<Category>, String> {

    //TODO : Create an interface for VOs to generalize this converter to all List of VOs

    /**
     * method convertToDatabaseColumn
     * Converts a List of Category in a string with all categories separated with ','
     * to represent in the column of the data base table
     *
     * @param categories List of Category
     * @return String with all categories separated with ','
     */
    @Override
    public String convertToDatabaseColumn(List<Category> categories) {

        List<String> categoriesString = new ArrayList<>();
        for (Category category : categories) {
            categoriesString.add(category.toStringDTO());
        }

        return String.join(",", categoriesString);
    }

    /**
     * method convertToEntityAttribute
     * converts a string with categories in string format to a List of Category
     *
     * @param s string with all categories in string format separated with ',' ("example,example")
     * @return List of VOs of Category
     */
    @Override
    public List<Category> convertToEntityAttribute(String s) {

        List<String> categoriesString = new ArrayList<>(Arrays.asList(s.split(",")));

        List<Category> categories = new ArrayList<>();
        if (!categoriesString.get(0).equals("")) {
            for (String categoryStg : categoriesString) {
                categories.add(new Category(categoryStg));
            }
        }

        return categories;
    }
}
