package project.frameworkddd;


import project.dto.TransactionsResponseDTO;

public interface IUS010Service {

    TransactionsResponseDTO getTransactionsOfAccountWithinPeriodByPersonID(String personID, String accountID, String initialDate, String finalDate);

    TransactionsResponseDTO getTransactionsOfAccountWithinPeriodByGroupID(String personID, String groupID, String accountID, String initialDate, String finalDate);

}
