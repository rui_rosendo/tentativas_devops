package project.frameworkddd;

import project.dto.AccountDTO;
import project.dto.AccountsDTO;
import project.dto.CreateAccountForGroupRequestDTO;
import project.dto.CreateAccountForGroupResponseDTO;
import project.model.entities.account.Account;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.AccountID;

public interface IUS007Service {

    CreateAccountForGroupResponseDTO createAccountForGroup(CreateAccountForGroupRequestDTO requestDTO);

    AccountDTO getAccountByID(String requestAccountID);

    AccountsDTO getAccountsByGroupID(String requestGroupID);

    Group getGroupByID(GroupID groupID);

    Account getAccountByID(AccountID accountID);

}
