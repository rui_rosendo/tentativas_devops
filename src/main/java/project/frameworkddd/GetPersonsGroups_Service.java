package project.frameworkddd;

import project.dto.GetPersonsGroupsResponseDTO;

public interface GetPersonsGroups_Service {
    GetPersonsGroupsResponseDTO getPersonsGroups(String personID);
}
