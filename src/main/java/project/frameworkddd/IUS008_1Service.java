package project.frameworkddd;


import project.dto.CreateTransactionForGroupRequestDTO;
import project.dto.CreateTransactionForGroupResponseDTO;
import project.dto.TransactionsResponseDTO;

public interface IUS008_1Service {
    CreateTransactionForGroupResponseDTO createTransactionForGroup(CreateTransactionForGroupRequestDTO requestDTO);

    TransactionsResponseDTO getTransactionsByGroupID(String personID, String groupID);
}
