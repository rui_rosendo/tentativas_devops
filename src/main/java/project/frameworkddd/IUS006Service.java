package project.frameworkddd;

import project.dto.AccountsDTO;
import project.dto.CreateAccountForPersonRequestDTO;
import project.dto.CreateAccountForPersonResponseDTO;

public interface IUS006Service {
    CreateAccountForPersonResponseDTO createAccountForPerson(CreateAccountForPersonRequestDTO requestDTO);

    AccountsDTO getAccountsByPersonID(String personID);
}
