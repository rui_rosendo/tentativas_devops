package project.frameworkddd;

import project.dto.CheckIfPeopleAreSiblingsRequestDTO;
import project.dto.CheckIfPeopleAreSiblingsResponseDTO;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;

public interface IUS001Service {
    CheckIfPeopleAreSiblingsResponseDTO isSibling(CheckIfPeopleAreSiblingsRequestDTO requestDTO);

    Person getByID(PersonID personID);
}
