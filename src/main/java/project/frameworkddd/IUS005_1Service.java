package project.frameworkddd;

import project.dto.CategoriesDTO;
import project.dto.CreateCategoryForGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;

public interface IUS005_1Service {
    CreateCategoryForGroupResponseDTO createCategoryForGroup(CreateCategoryForGroupRequestDTO requestDTO);

    CategoriesDTO getCategoriesByGroupID(String groupID);
    
}
