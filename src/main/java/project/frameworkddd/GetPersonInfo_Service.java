package project.frameworkddd;

import project.dto.GetPersonInfoResponseDTO;

public interface GetPersonInfo_Service {

    GetPersonInfoResponseDTO getPersonInfo(String personID);
}
