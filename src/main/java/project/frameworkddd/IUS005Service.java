package project.frameworkddd;

import project.dto.CategoriesDTO;
import project.dto.CreateCategoryForPersonRequestDTO;
import project.dto.CreateCategoryForPersonResponseDTO;

public interface IUS005Service {
    CreateCategoryForPersonResponseDTO createCategoryForPerson(CreateCategoryForPersonRequestDTO requestDTO);

    CategoriesDTO getCategoriesByPersonID(String personID);
}
