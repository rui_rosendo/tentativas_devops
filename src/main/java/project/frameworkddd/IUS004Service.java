package project.frameworkddd;

import project.dto.GetFamilyGroupsDTO;

public interface IUS004Service {
    GetFamilyGroupsDTO getFamilyGroups();

}
