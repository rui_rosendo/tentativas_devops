package project.frameworkddd;

import project.dto.AddMemberRequestDTO;
import project.dto.AddMemberResponseDTO;
import project.dto.MembersDTO;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;

public interface IUS003Service {

    AddMemberResponseDTO addMember(AddMemberRequestDTO addMemberRequestDTO);

    MembersDTO getMembersByGroupID(String groupID);

    Person getPersonByID(PersonID personID);
}
