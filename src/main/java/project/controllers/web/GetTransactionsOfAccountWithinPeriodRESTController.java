package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.TransactionsResponseDTO;
import project.frameworkddd.IUS010Service;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class GetTransactionsOfAccountWithinPeriodRESTController {

    @Autowired
    private IUS010Service service;

    @GetMapping("/people/{personID}/groups/{groupID}/transactionsFilter")
    public ResponseEntity<Object> getTransactionsOfAccountWithinPeriodByGroupID(@PathVariable String personID,
                                                                                @PathVariable String groupID,
                                                                                @RequestParam(name = "accountID") String accountID,
                                                                                @RequestParam(name = "initialDate") String initialDate,
                                                                                @RequestParam(name = "finalDate") String finalDate) {

        TransactionsResponseDTO responseDTO = service.getTransactionsOfAccountWithinPeriodByGroupID(personID, groupID, accountID, initialDate, finalDate);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @GetMapping("/people/{personID}/transactionsFilter")
    public ResponseEntity<Object> getTransactionsOfAccountWithinPeriodByPersonID(@PathVariable String personID,
                                                                                 @RequestParam(name = "accountID") String accountID,
                                                                                 @RequestParam(name = "initialDate") String initialDate,
                                                                                 @RequestParam(name = "finalDate") String finalDate) {

        TransactionsResponseDTO responseDTO = service.getTransactionsOfAccountWithinPeriodByPersonID(personID, accountID, initialDate, finalDate);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}
