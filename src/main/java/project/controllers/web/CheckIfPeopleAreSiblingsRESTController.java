package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import project.dto.CheckIfPeopleAreSiblingsAssembler;
import project.dto.CheckIfPeopleAreSiblingsRequestDTO;
import project.dto.CheckIfPeopleAreSiblingsResponseDTO;
import project.frameworkddd.IUS001Service;

/**
 * US001 - Como gestor de sistema quero saber se determinada pessoa é irmão/irmã de outra
 */
@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CheckIfPeopleAreSiblingsRESTController {

    @Autowired
    IUS001Service service;

    /**
     * Method CheckIfPeopleAreSiblings
     * Does a GET request and returns it in a request body.
     * Checks if person 1 and person 2 are siblings (using respective IDs)
     *
     * @param personEmail1 Email of person 1
     * @param personEmail2 Email of person 2
     * @return ResponseEntity with a string of areSiblings and HttpStatus.
     */
    @GetMapping("/people/{personEmail1}/siblings/{personEmail2}")
    public ResponseEntity<Object> checkIfPeopleAreSiblings(@PathVariable String personEmail1, @PathVariable String personEmail2) {
        CheckIfPeopleAreSiblingsRequestDTO requestDTO = CheckIfPeopleAreSiblingsAssembler.mapToRequestDTO(personEmail1, personEmail2);
        CheckIfPeopleAreSiblingsResponseDTO responseDTO = service.isSibling(requestDTO);
        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

}

