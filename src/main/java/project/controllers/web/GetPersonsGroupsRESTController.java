package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import project.dto.GetPersonsGroupsResponseDTO;
import project.dto.GroupDTOMinimal;
import project.frameworkddd.GetPersonsGroups_Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class GetPersonsGroupsRESTController {

    @Autowired
    private GetPersonsGroups_Service service;

    /**
     * Method getPersonsGroups
     * Operates upon a GET operation with no request body
     * ResponseEntity encompasses GroupsDTO, HttpStatus.OK
     *
     * @return GroupsDTO and HttpStatus.
     */
    @GetMapping("people/{personID}/groups")
    public ResponseEntity<Object> getPersonsGroups(@PathVariable String personID) {

        GetPersonsGroupsResponseDTO result = service.getPersonsGroups(personID);

        for (GroupDTOMinimal groupDTO : result.getGroupsDTOs()) {
            String groupID = groupDTO.getGroupID();
            Link linkToGroup = linkTo(methodOn(CreateGroupRESTController.class).getGroupByID(groupID)).withSelfRel().withRel("_self");
            groupDTO.add(linkToGroup);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
