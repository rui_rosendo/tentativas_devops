package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.*;
import project.frameworkddd.IUS008_1Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CreateTransactionForGroupRESTController {

    @Autowired
    private IUS008_1Service service;

    /**
     * createTransactionForGroup method
     * <p>
     * Intends to create and add a new transaction to the ledger of a given group.
     * Method - POST
     * URI - /people/{personEmail}/groups/{groupID}/transactions
     * Path variables - personEmail and GroupID
     *
     * @param infoDTO     - Data Transfer Object that contains the necessary information to create a transaction
     * @param personEmail - The identity of the person member who's creating the group transaction
     * @param groupID     - The identity of the group to be added the new transaction (to its ledger)
     * @return Response Entity with a body (a ResponseDTO or an error body in case an exception is thrown by the
     * application) and a Http status
     */
    @PostMapping("/people/{personEmail}/groups/{groupID}/transactions")
    public ResponseEntity<Object> createTransactionForGroup(@RequestBody CreateTransactionInfoDTO infoDTO,
                                                            @PathVariable String personEmail,
                                                            @PathVariable String groupID) {

        CreateTransactionForGroupRequestDTO requestDTO = CreateTransactionForGroupAssembler.mapToRequestDTO(personEmail,
                groupID, infoDTO);

        CreateTransactionForGroupResponseDTO responseDTO = service.createTransactionForGroup(requestDTO);

        Link transactionsLink =
                linkTo(CreateTransactionForGroupRESTController.class).slash("people").slash(personEmail).slash("groups").slash(groupID).slash("transactions").withRel("group_transactions");
        responseDTO.add(transactionsLink);

        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    @GetMapping("/people/{personEmail}/groups/{groupID}/transactions")
    public ResponseEntity<Object> getTransactionsByGroupID(@PathVariable String personEmail,
                                                           @PathVariable String groupID) {

        TransactionsResponseDTO responseDTO = service.getTransactionsByGroupID(personEmail, groupID);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}
