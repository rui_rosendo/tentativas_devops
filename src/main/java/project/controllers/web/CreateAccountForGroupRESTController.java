package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.*;
import project.frameworkddd.IUS007Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CreateAccountForGroupRESTController {

    @Autowired
    private IUS007Service service;

    /**
     * createAccountForGroup method
     *
     * @param info CreateAccountForGroupInfoDTO
     * @return ResponseEntity with a body that includes a link to the created account and the HttpStatus.
     * Uses HttpStatus.CREATED for success, HttpStatus.UNPROCESSABLE_ENTITY if failed due to business logic
     */
    @PostMapping("/people/{personEmail}/groups/{groupID}/accounts")
    public ResponseEntity<Object> createAccountForGroup(@RequestBody CreateAccountForGroupInfoDTO info, @PathVariable String personEmail, @PathVariable String groupID) {

        CreateAccountForGroupRequestDTO requestDTO = CreateAccountForGroupAssembler.mapToRequestDTO(info.getAccountID(), groupID, info.getDenomination(), info.getDescription(), personEmail);
        CreateAccountForGroupResponseDTO result = service.createAccountForGroup(requestDTO);

        Link selfLink = linkTo(methodOn(CreateAccountForGroupRESTController.class).getAccountByID(info.getAccountID())).withSelfRel();
        Link linkToAccounts = linkTo(methodOn(CreateAccountForGroupRESTController.class).getAccountsByGroupID(groupID)).withRel("groupAccounts");
        Link linkToGroup = linkTo(methodOn(CreateGroupRESTController.class).getGroupByID(groupID)).withRel("group");

        result.add(selfLink);
        result.add(linkToAccounts);
        result.add(linkToGroup);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /**
     * getAccountByID method
     *
     * @param accountID - identity of the account to be searched and returned
     * @return - the information of the account to be searched in a DTO and a Http status OK
     */
    @GetMapping("/accounts/{accountID}")
    public ResponseEntity<Object> getAccountByID(@PathVariable final String accountID) {

        AccountDTO result = service.getAccountByID(accountID);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * getAccountsByGroupID method
     *
     * @param groupID - identity of the group whose accounts are to be searched and returned
     * @return - the identity of the group's accounts and a link to each account along with an Http status ok
     */
    @GetMapping("/groups/{groupID}/accounts")
    public ResponseEntity<Object> getAccountsByGroupID(@PathVariable String groupID) {

        AccountsDTO result = service.getAccountsByGroupID(groupID);

        for (AccountDTO accountDTO : result.getAccounts()) {
            String accountID = accountDTO.getAccountID();
            Link linkToAccount = linkTo(methodOn(CreateAccountForGroupRESTController.class).getAccountByID(accountID)).withRel("_self");
            accountDTO.add(linkToAccount);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
