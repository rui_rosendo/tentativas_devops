package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.*;
import project.frameworkddd.IUS002_1Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CreateGroupRESTController {

    @Autowired
    private IUS002_1Service service;

    /**
     * Method createGroup
     * Operates through RequestMapping of the Post type
     *
     * @param info an instance of CreateGroupInfoDTO which represents a DTO from the client
     * @return ResponseEntity with body and HttpStatus
     */
    @PostMapping("/people/{personEmail}/groups")
    public ResponseEntity<Object> createGroup(@RequestBody CreateGroupInfoDTO info, @PathVariable String personEmail) {

        CreateGroupRequestDTO requestDTO = CreateGroupAssembler.mapToRequestDTO(info.getGroupID(), info.getDescription(), info.getCreationDate(), personEmail, info.getGroupLedgerID());
        CreateGroupResponseDTO result = service.createGroup(requestDTO);

        Link selfLink = linkTo(methodOn(CreateGroupRESTController.class).getGroupByID(info.getGroupID())).withSelfRel();
        result.add(selfLink);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /**
     * Method getGroupByID
     * Operates through RequestMapping of the Get type
     *
     * @param groupID
     * @return
     */
    @GetMapping("/groups/{groupID}")
    public ResponseEntity<Object> getGroupByID(@PathVariable final String groupID) {

        GroupDTO result = service.getGroupByID(groupID);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
