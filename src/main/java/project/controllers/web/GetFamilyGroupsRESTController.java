package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import project.dto.GetFamilyGroupsDTO;
import project.frameworkddd.IUS004Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class GetFamilyGroupsRESTController {

    @Autowired
    private IUS004Service service;

    /**
     * Method getFamilyGroups
     * Operates upon a GET operation with no request body
     * ResponseEntity encompasses GetFamilyGroupsDTO, HttpStatus.OK
     *
     * @return GetFamilyGroupsDTO and HttpStatus.
     */
    @GetMapping("/groups/family")
    public ResponseEntity<Object> getFamilyGroups() {

        GetFamilyGroupsDTO result = service.getFamilyGroups();

        for (String groupID : result.getFamilyGroupsIDs()) {
                Link linkToGroupID = linkTo(methodOn(CreateGroupRESTController.class).getGroupByID(groupID)).withSelfRel().withRel("group" + groupID);
                result.add(linkToGroupID);
            }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
