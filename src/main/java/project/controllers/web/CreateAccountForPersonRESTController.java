package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.*;
import project.frameworkddd.IUS006Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * US06- Como utilizador, quero criar uma conta para mim, atribuindo-lhe uma denominação e uma descrição,
 * para posteriormente poder ser usada nos meus movimentos.
 */
@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CreateAccountForPersonRESTController {

    @Autowired
    private IUS006Service service;

    /**
     * Method createAccountForPerson
     *
     * @param info CreateAccountForPersonInfoDTO
     * @return Optional<CreateAccountForPersonDTO> and HttpStatus.
     * Uses HttpStatus.CREATED for success, HttpStatus.UNPROCESSABLE_ENTITY if failed due to business logic
     */
    @PostMapping("/people/{personEmail}/accounts")
    public ResponseEntity<Object> createAccountForPerson(@RequestBody CreateAccountForPersonInfoDTO info, @PathVariable String personEmail) {

        CreateAccountForPersonRequestDTO requestDTO = CreateAccountForPersonAssembler.mapToRequestDTO(info.getAccountID(), personEmail, info.getDenomination(), info.getDescription());
        CreateAccountForPersonResponseDTO result = service.createAccountForPerson(requestDTO);

        // link for hypermedia controls. Link available for account created.
        // This is using the method in CreateAccountForGroupRESTController(US007)
        Link selfLink = linkTo(methodOn(CreateAccountForGroupRESTController.class).getAccountByID(info.getAccountID()))
                .withSelfRel();

        // link for hypermedia controls. Link available for Person accounts.
        Link accountsLink = linkTo(methodOn(CreateAccountForPersonRESTController.class).getAccountsByPersonID(personEmail))
                .withRel("accounts");

        result.add(selfLink);
        result.add(accountsLink);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /**
     * Method getAccountsByPersonID
     * <p>
     * Method to get Accounts of a person
     *
     * @param personID String of personID
     * @return AccountsDTO List of accounts
     */
    @GetMapping("/people/{personID}/accounts")
    public ResponseEntity<Object> getAccountsByPersonID(@PathVariable String personID) {

        AccountsDTO result = service.getAccountsByPersonID(personID);

        for (AccountDTO accountDTO : result.getAccounts()) {
            String accountID = accountDTO.getAccountID();
            Link linkToAccount = linkTo(methodOn(CreateAccountForGroupRESTController.class).getAccountByID(accountID)).withRel("_self");
            accountDTO.add(linkToAccount);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}

