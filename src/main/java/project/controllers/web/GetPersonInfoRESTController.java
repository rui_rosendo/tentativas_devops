package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import project.dto.GetPersonInfoResponseDTO;
import project.frameworkddd.GetPersonInfo_Service;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class GetPersonInfoRESTController {

    @Autowired
    GetPersonInfo_Service service;

    /**
     * Method getPersonByID
     * Operates through RequestMapping of the Get type
     *
     * @param personID
     * @return
     */
    @GetMapping("/people/{personID}")
    public ResponseEntity<Object> getPersonByID(@PathVariable final String personID) {

        GetPersonInfoResponseDTO result = service.getPersonInfo(personID);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
