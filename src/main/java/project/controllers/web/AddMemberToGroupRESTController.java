package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.*;
import project.frameworkddd.IUS003Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class AddMemberToGroupRESTController {
//    @Autowired
//    private PersonRepositoryDB personRepo;
//    @Autowired
//    private GroupRepositoryDB groupRepo;
    @Autowired
    private IUS003Service serviceUS003;


    /**
     * addMemberToGroup Method
     * Operates upon a POST method
     *
     * @param info    AddMemberInfoDTO info as RequestBody
     * @param groupID groupID in the Path
     * @return ResponseEntity and HttpStatus, uses HttpStatus.CREATED for success, HttpStatus.UNPROCESSABLE_ENTITY for errors with specific error message
     */
    @PostMapping("/groups/{groupID}/members")
    public ResponseEntity<Object> addMemberToGroup(@RequestBody AddMemberInfoDTO info, @PathVariable String groupID) {

        AddMemberRequestDTO requestDTO = AddMemberAssembler.mapToRequestDTO(info.getPersonEmail(), groupID);

        AddMemberResponseDTO result = serviceUS003.addMember(requestDTO);

        Link membersLink = linkTo(methodOn(AddMemberToGroupRESTController.class).getMembersByGroupID(groupID)).withRel("members");
        result.add(membersLink);

        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    /**
     * getMembersByGroupID method
     * Operates upon a GET method
     *
     * @param groupID groupID in the Path
     * @return ResponseEntity and HttpStatus
     */
    @GetMapping("/groups/{groupID}/members")
    public ResponseEntity<Object> getMembersByGroupID(@PathVariable String groupID) {

        MembersDTO result = serviceUS003.getMembersByGroupID(groupID);

        for (String memberID : result.getMembersIDs()) {
            Link linkToMembers = linkTo(AddMemberToGroupRESTController.class).slash(groupID).slash("member").slash(memberID).withRel("member " + memberID);
            result.add(linkToMembers);

        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
