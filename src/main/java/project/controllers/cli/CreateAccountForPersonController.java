package project.controllers.cli;

import org.springframework.stereotype.Controller;
import project.dto.CreateAccountForPersonAssembler;
import project.dto.CreateAccountForPersonRequestDTO;
import project.dto.CreateAccountForPersonResponseDTO;
import project.frameworkddd.IUS006Service;

/**
 * US06- Como utilizador, quero criar uma conta para mim, atribuindo-lhe uma denominação e uma descrição,
 * para posteriormente poder ser usada nos meus movimentos.
 */
@Controller
public class CreateAccountForPersonController {

    private final IUS006Service service;

    /**
     * Constructor for person creating account controller class
     */
    public CreateAccountForPersonController(IUS006Service service) {
        this.service = service;
    }

    /**
     * Method used to add a new account to a Person
     * Delegates functionality CreateAccountPersonService class
     *
     * @param accountID    AccountID of the the account to be created
     * @param personEmail  PersonID of the person adding a new account
     * @param denomination Denomination of account to add
     * @param description  Description of account to add
     * @return CreateAccountForPersonDTO if the Person exists and the new account doesn't exist in the repository and is added to the person's list of accounts, Optional.empty() in other cases
     */
    public CreateAccountForPersonResponseDTO createAccountForPerson(String accountID, String personEmail, String denomination, String description) {
        CreateAccountForPersonRequestDTO dto = CreateAccountForPersonAssembler.mapToRequestDTO(accountID, personEmail, denomination, description);
        return service.createAccountForPerson(dto);
    }


}

