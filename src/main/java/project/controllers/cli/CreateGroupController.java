package project.controllers.cli;

import project.dto.CreateGroupAssembler;
import project.dto.CreateGroupRequestDTO;
import project.dto.CreateGroupResponseDTO;
import project.frameworkddd.IUS002_1Service;

// US02.1 - As a user, I want to create a group, making myself the manager of the group.

public class CreateGroupController {

    private final IUS002_1Service createGroupService;

    /**
     * Constructor for CreateGroupController
     *
     * @param createGroupService - Service to be used by the controller to create group
     */
    public CreateGroupController(IUS002_1Service createGroupService) {
        this.createGroupService = createGroupService;
    }

    /**
     * createGroup method
     * <p>
     * This method calls the CreateGroupService giving it the necessary parameters to create a new Group instance.
     * Inside it creates a requestDTO by calling mapToRequestDTO
     *
     * @param groupID       - the identity of the new group object to be created (as a GroupID)
     * @param description   - the description of the new group object to be created
     * @param creationDate  - creation date of the new group object to be created
     * @param creatorEmail  - the identity of the Person creator of the new group object to be created (as a PersonID)
     * @param groupLedgerID - the identity of the Ledger of the new group object to be created (as a LedgerID)
     * @return an Optional of GroupDTO
     */
    public CreateGroupResponseDTO createGroup(String groupID, String description, String creationDate, String creatorEmail, String groupLedgerID) {
        CreateGroupRequestDTO requestDTO = CreateGroupAssembler.mapToRequestDTO(groupID, description, creationDate, creatorEmail, groupLedgerID);
        return createGroupService.createGroup(requestDTO);
    }
}