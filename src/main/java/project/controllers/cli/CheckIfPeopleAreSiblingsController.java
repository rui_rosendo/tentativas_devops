package project.controllers.cli;

import project.dto.CheckIfPeopleAreSiblingsAssembler;
import project.dto.CheckIfPeopleAreSiblingsRequestDTO;
import project.dto.CheckIfPeopleAreSiblingsResponseDTO;
import project.frameworkddd.IUS001Service;

/**
 * US01- Como gestor de sistema, quero saber se determinada pessoa é irmão/irmã de outra. É irmão/irmã de
 * outra se tem a mesma mãe ou o mesmo pai, ou se na lista de irmãos está incluída a outra.
 * A relação de irmão é bidirecional, i.e. se A é irmão de B, então B é irmão de A.
 */
public class CheckIfPeopleAreSiblingsController {

    IUS001Service service;

    /**
     * Constructor for people are siblings controller class
     *
     * @param checkIfPeopleAreSiblingsService Service to perform functionality
     */
    public CheckIfPeopleAreSiblingsController(IUS001Service checkIfPeopleAreSiblingsService) {
        this.service = checkIfPeopleAreSiblingsService;
    }

    /**
     * Calls the method isSibling from checkSiblingService to check if person 1 and person 2 are siblings (using respective IDs)
     *
     * @param personEmail1 ID of person 1
     * @param personEmail2 ID of person 2
     * @return CheckIfPeopleAreSiblingsResponseDTO
     */
    public CheckIfPeopleAreSiblingsResponseDTO checkPeopleAreSiblings(String personEmail1, String personEmail2) {
        CheckIfPeopleAreSiblingsRequestDTO checkIfPeopleAreSiblingsRequestDTO = CheckIfPeopleAreSiblingsAssembler.mapToRequestDTO(personEmail1, personEmail2);
        return this.service.isSibling(checkIfPeopleAreSiblingsRequestDTO);
    }

}
