package project.controllers.cli;

import org.springframework.stereotype.Controller;
import project.dto.GetFamilyGroupsDTO;
import project.frameworkddd.IUS004Service;

/**
 * US04 - Como gestor, quero saber quais os grupos que sao família, i.e grupo constítuido
 * por pai, mae e um sub-conjunto(não vazio, mas podem ser todos) dos respectivos filhos.
 */
@Controller
public class GetFamilyGroupsController {

    private final IUS004Service service;

    /**
     * Constructor for Groups are family controller class
     */
    public GetFamilyGroupsController(IUS004Service service) {
        this.service = service;
    }

    /**
     * Get family groups
     *
     * @return String representation of family groups
     */
    public GetFamilyGroupsDTO getFamilyGroups() {
        return service.getFamilyGroups();
    }

}
