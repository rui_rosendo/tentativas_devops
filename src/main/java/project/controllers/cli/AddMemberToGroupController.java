package project.controllers.cli;

import org.springframework.stereotype.Controller;
import project.dto.AddMemberAssembler;
import project.dto.AddMemberRequestDTO;
import project.dto.AddMemberResponseDTO;
import project.frameworkddd.IUS003Service;

/**
 * US03- Como gestor de sistema, quero acrescentar pessoas ao grupo;
 */
@Controller
public class AddMemberToGroupController {

    IUS003Service service;

    /**
     * Constructor for group add a member controller class
     */
    public AddMemberToGroupController(IUS003Service addMemberToGroupService) {
        this.service = addMemberToGroupService;
    }

    /**
     * Adds a PersonID to the list of members of a Group
     *
     * @param newMemberEmail PersonID to be added to members list of group
     * @param groupID        ID of group to which new member is going to be added
     * @return an Optional with MembersDTO (list of members of group, after addiction of new member, as a String),
     * or empty (in case of intended new member ID did not exist in repository, if group ID did not exist in repository,
     * or if new member ID already existed in given group members list)
     */
    public AddMemberResponseDTO addMemberToGroup(String newMemberEmail, String groupID) {
        AddMemberRequestDTO addMemberRequestDTO = AddMemberAssembler.mapToRequestDTO(newMemberEmail, groupID);
        return service.addMember(addMemberRequestDTO);
    }
}
