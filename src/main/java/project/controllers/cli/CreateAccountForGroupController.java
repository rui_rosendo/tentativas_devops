package project.controllers.cli;

import project.dto.CreateAccountForGroupAssembler;
import project.dto.CreateAccountForGroupRequestDTO;
import project.dto.CreateAccountForGroupResponseDTO;
import project.frameworkddd.IUS007Service;

/**
 * US07- Como responsável do grupo, quero criar uma conta do grupo, atribuindo-lhe uma denominação e uma descrição,
 * para posteriormente poder ser usada nos movimentos do grupo.
 */
public class CreateAccountForGroupController {

    private final IUS007Service service;

    /**
     * Constructor for the class CreateAccountForGroupController
     */
    public CreateAccountForGroupController(IUS007Service service) {
        this.service = service;
    }

    /**
     * Method createAccountForGroup
     * Creates a new instance of service and calls method createAccountForGroup
     * to create a new account to a group
     * <p>
     * Delegates responsibility to CreateAccountForGroupService
     *
     * @param accountID    - identity of the new account to be created and added
     * @param groupID      - identity of the group who's creating the account and adding to his list of accounts
     * @param denomination - denomination to be attributed to the new account
     * @param description  - description to be attributed to the new account
     * @return True if the Group exists and the new account is added to the repository and to the group's
     * list of accounts
     */
    public CreateAccountForGroupResponseDTO createAccountForGroup(String accountID, String groupID, String denomination, String description, String personID) {
        CreateAccountForGroupRequestDTO dto = CreateAccountForGroupAssembler.mapToRequestDTO(accountID, groupID, denomination, description, personID);
        return service.createAccountForGroup(dto);
    }
}
