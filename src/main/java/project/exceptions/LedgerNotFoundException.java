package project.exceptions;

import static project.exceptions.Messages.LEDGERNOTFOUND;

public class LedgerNotFoundException extends RuntimeException {
    static final long serialVersionUID = -1159385135552734815L;

    /**
     * Exception for when group not found
     * Uses exceptions.Messages for exception message
     */
    public LedgerNotFoundException() {
        super(LEDGERNOTFOUND);
    }

    /**
     * Exception for when group not found
     * Uses provided message for exception message
     */
    public LedgerNotFoundException(String message) {
        super(message);
    }

}
