package project.exceptions;

import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;

@EqualsAndHashCode
public class ApiError {

    private HttpStatus status;
    private String message;
    private List<String> errors;

    public ApiError() {
        super();
    }

    /**
     * Constructor for ApiError
     *
     * @param status  HttpStatus to include in ApiError
     * @param message Message to include in ApiError
     * @param errors  List of errors to include in ApiError
     */
    public ApiError(final HttpStatus status, final String message, final List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    /**
     * Constructor for ApiError
     *
     * @param status  HttpStatus to include in ApiError
     * @param message Message to include in ApiError
     * @param error   error to include in ApiError (is transformed to list of errors with single item)
     */
    public ApiError(final HttpStatus status, final String message, final String error) {
        super();
        this.status = status;
        this.message = message;
        this.errors = Collections.singletonList(error);
    }

    /**
     * Get status
     *
     * @return HttpStatus
     */
    public HttpStatus getStatus() {
        return status;
    }

    /**
     * Get message
     *
     * @return Message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Get errors
     *
     * @return List of errors
     */
    public List<String> getErrors() {
        return errors;
    }

}
