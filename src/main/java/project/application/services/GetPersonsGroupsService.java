package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.GetPersonsGroupsAssembler;
import project.dto.GetPersonsGroupsResponseDTO;
import project.frameworkddd.GetPersonsGroups_Service;
import project.frameworkddd.IUS003Service;
import project.model.entities.group.Group;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Set;

@Service
public class GetPersonsGroupsService implements GetPersonsGroups_Service {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    IUS003Service IUS003Service;

    /**
     * Constructor for AddMemberToGroupService
     *
     * @param groupRepository  Instance of GroupRepository containing all groups
     * @param personRepository Instance of PersonRepository containing all persons
     */
    public GetPersonsGroupsService(PersonRepository personRepository, GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
        this.personRepository = personRepository;
    }

    /**
     * Method getPersonsGroups
     *
     * @param personID ID of person
     * @return Set of groupIDs
     */
    public GetPersonsGroupsResponseDTO getPersonsGroups(String personID) {
        PersonID personIDDomain = new PersonID(personID);

        IUS003Service.getPersonByID(personIDDomain);

        Set<Group> personsGroups = new HashSet<>();

        for (Group group : groupRepository.findAll()) {
            if (group.hasMemberID(personIDDomain)) {
                personsGroups.add(group);
            }
        }

        return GetPersonsGroupsAssembler.mapToDTO(personsGroups);
    }

}