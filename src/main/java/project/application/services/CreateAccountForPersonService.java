package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.*;
import project.exceptions.AccountAlreadyExistsException;
import project.frameworkddd.IUS003Service;
import project.frameworkddd.IUS006Service;
import project.frameworkddd.IUS007Service;
import project.model.entities.account.Account;
import project.model.entities.person.Person;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * US06- Como utilizador, quero criar uma conta para mim, atribuindo-lhe uma denominação e uma descrição,
 * para posteriormente poder ser usada nos meus movimentos.
 */
@Service
public class CreateAccountForPersonService implements IUS006Service {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    IUS003Service IUS003Service;

    @Autowired
    IUS007Service IUS007Service;

    /**
     * Constructor for CreateAccountForPersonService Class
     *
     * @param personRepository  repository that stores Person instances
     * @param accountRepository repository that stores Account instances
     */
    public CreateAccountForPersonService(PersonRepository personRepository, AccountRepository accountRepository) {
        this.personRepository = personRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * Method createAccountForPerson
     *
     * @param requestDTO RequestDTO
     * @return CreateAccountForPersonResponseDTO
     */
    public CreateAccountForPersonResponseDTO createAccountForPerson(CreateAccountForPersonRequestDTO requestDTO) {
        PersonID personID = new PersonID(requestDTO.getPersonEmail());
        Person person = IUS003Service.getPersonByID(personID);

        AccountID accountID = new AccountID(requestDTO.getAccountID());
        Optional<Account> optAccount = accountRepository.findById(accountID);

        if (optAccount.isPresent()) {
            throw new AccountAlreadyExistsException();
        }

        String accountDenomination = requestDTO.getAccountDenomination();
        String accountDescription = requestDTO.getAccountDescription();

        Account newAccount = new Account(accountID, accountDenomination, accountDescription);
        Account savedAccount = accountRepository.save(newAccount);

        person.addAccount(savedAccount.getID());
        Person savedPerson = personRepository.save(person);

        CreateAccountForPersonResponseDTO result;
        result = CreateAccountForPersonAssembler.mapToResponseDTO(
                savedPerson.getID().getPersonEmail(),
                savedPerson.getName().toStringDTO(),
                savedAccount.getID().toStringDTO(),
                savedAccount.getDenomination().getDenominationValue());

        return result;
    }

    /**
     * Method getAccountsByPersonID
     * <p>
     * Method to get Accounts of a person
     *
     * @param id String of id
     * @return AccountsDTO List of accounts
     */
    public AccountsDTO getAccountsByPersonID(String id) {
        PersonID personID = new PersonID(id);
        Person person = IUS003Service.getPersonByID(personID);

        List<Account> accounts = new ArrayList<>();
        for (AccountID accountID : person.getAccountsIDs().getAccountsIDsValue()) {
            accounts.add(IUS007Service.getAccountByID(accountID));
        }

        return AccountsAssembler.mapToDTO(accounts);

    }
}
