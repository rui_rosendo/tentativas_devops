package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.TransactionsAssembler;
import project.dto.TransactionsResponseDTO;
import project.exceptions.AccountConflictException;
import project.exceptions.AccountNotFoundException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.LedgerNotFoundException;
import project.frameworkddd.IUS010Service;
import project.model.entities.account.Account;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.ledger.Transaction;
import project.model.entities.person.Person;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.Period;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GetTransactionsOfAccountWithinPeriodService implements IUS010Service {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private LedgerRepository ledgerRepository;

    @Autowired
    private AccountRepository accountRepository;

    /**
     * Method getTransactionsWithinPeriodByPersonID
     * Uses internal method getTransactionsWithinPeriodByLedgerID
     *
     * @param personID    ID of person
     * @param initialDate Initial filter date
     * @param finalDate   Final filter date
     */
    @Override
    public TransactionsResponseDTO getTransactionsOfAccountWithinPeriodByPersonID(String personID, String accountID, String initialDate, String finalDate) {

        Optional<Person> personOpt = personRepository.findById(new PersonID(personID));
        if (!personOpt.isPresent()) {
            throw new GroupNotFoundException();
        }

        Person person = personOpt.get();
        AccountID accountIDDomain = new AccountID(accountID);

        if (!person.hasAccountID(accountIDDomain)) {
            throw new AccountConflictException();
        }


        Optional<Account> accountOpt = accountRepository.findById(accountIDDomain);
        if (!accountOpt.isPresent()) {
            throw new AccountNotFoundException();
        }

        LedgerID ledgerID = person.getLedgerID();
        Period period = new Period(initialDate, finalDate);

        return getTransactionsOfAccountWithinPeriodByLedgerID(ledgerID, accountIDDomain, period);
    }

    /**
     * Method getTransactionsWithinPeriodByGroupID
     * Uses internal method getTransactionsWithinPeriodByLedgerID
     *
     * @param groupID     ID of group
     * @param initialDate Initial filter date
     * @param finalDate   Final filter date
     */
    @Override
    public TransactionsResponseDTO getTransactionsOfAccountWithinPeriodByGroupID(String personID, String groupID, String accountID, String initialDate, String finalDate) {

        PersonID personIDDomain = new PersonID(personID);

        Optional<Person> personOpt = personRepository.findById(personIDDomain);
        if (!personOpt.isPresent()) {
            throw new GroupNotFoundException();
        }

        Optional<Group> groupOpt = groupRepository.findById(new GroupID(groupID));
        if (!groupOpt.isPresent()) {
            throw new GroupNotFoundException();
        }

        Group group = groupOpt.get();

        if (!group.hasMemberID(personIDDomain)) {
            throw new AccountConflictException();
        }

        AccountID accountIDDomain = new AccountID(accountID);

        if (!group.hasAccountID(accountIDDomain)) {
            throw new AccountConflictException();
        }

        Optional<Account> accountOpt = accountRepository.findById(accountIDDomain);
        if (!accountOpt.isPresent()) {
            throw new AccountNotFoundException();
        }

        LedgerID ledgerID = group.getLedgerID();
        Period period = new Period(initialDate, finalDate);

        return getTransactionsOfAccountWithinPeriodByLedgerID(ledgerID, accountIDDomain, period);
    }

    /**
     * Method getTransactionsWithinPeriodByLedgerID
     * IS used by both getTransactionsWithinPeriodByPersonID and getTransactionsWithinPeriodByGroupID
     *
     * @param ledgerID ID of ledger
     * @param period   Period of filter
     */
    public TransactionsResponseDTO getTransactionsOfAccountWithinPeriodByLedgerID(LedgerID ledgerID, AccountID accountID, Period period) {

        Optional<Ledger> ledgerOpt = ledgerRepository.findById(ledgerID);
        if (!ledgerOpt.isPresent()) {
            throw new LedgerNotFoundException();
        }

        Ledger ledger = ledgerOpt.get();
        List<Transaction> transactions = new ArrayList<>(ledger.getTransactionsOfAccountWithinPeriod(accountID, period));

        return TransactionsAssembler.mapToDTO(transactions);
    }

}
