package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CreateGroupAssembler;
import project.dto.CreateGroupRequestDTO;
import project.dto.CreateGroupResponseDTO;
import project.dto.GroupDTO;
import project.exceptions.GroupAlreadyExistsException;
import project.exceptions.GroupLedgerAlreadyExistsException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS002_1Service;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

@Service
public class CreateGroupService implements IUS002_1Service {

    @Autowired
    PersonRepository personRepo;

    @Autowired
    GroupRepository groupRepo;

    @Autowired
    LedgerRepository ledgerRepo;

    /**
     * Constructor of CreateGroupService Class
     *
     * @param personRepo PersonRepository object which stores Person objects
     * @param groupRepo  GroupRepository object which stores Group objects
     * @param ledgerRepo LedgerRepository object which stores Ledger objects
     */
    public CreateGroupService(PersonRepository personRepo, GroupRepository groupRepo, LedgerRepository ledgerRepo) {
        this.personRepo = personRepo;
        this.groupRepo = groupRepo;
        this.ledgerRepo = ledgerRepo;
    }

    /**
     * createGroup method
     * <p>
     * This method intends to create a new Group object and it's Ledger object. Returns an Exception either filled,
     * if the group's creator doesn't exist in personRepo or if there is already either a Ledger with same ledger
     * identity in ledgerRepo or a Group with the same group identity in the groupRepo.
     *
     * @param requestDTO
     * @return
     */
    public CreateGroupResponseDTO createGroup(CreateGroupRequestDTO requestDTO) {
        PersonID creatorID = new PersonID(requestDTO.getCreatorEmail());
        LedgerID groupLedgerID = new LedgerID(requestDTO.getGroupLedgerID());
        GroupID groupID = new GroupID(requestDTO.getGroupID());
        String description = requestDTO.getDescription();
        String creationDate = requestDTO.getCreationDate();

        CreateGroupResponseDTO result;

        Optional<Person> optPerson = personRepo.findById(new PersonID(requestDTO.getCreatorEmail()));
        if (!optPerson.isPresent())
            throw new PersonNotFoundException();

        Optional<Group> optGroup = groupRepo.findById(new GroupID(requestDTO.getGroupID()));
        if (optGroup.isPresent())
            throw new GroupAlreadyExistsException();

        Optional<Ledger> optLedger = ledgerRepo.findById(new LedgerID(requestDTO.getGroupLedgerID()));
        if (optLedger.isPresent())
            throw new GroupLedgerAlreadyExistsException();

        Ledger newLedger = new Ledger(groupLedgerID);
        Group newGroup = new Group(groupID, description, creationDate, creatorID, groupLedgerID);
        Group savedGroup = groupRepo.save(newGroup);
        ledgerRepo.save(newLedger);
        result = (CreateGroupAssembler.mapToResponseDTO(savedGroup));
        return result;
    }

    /**
     * get group by iD
     *
     * @param id of the group
     * @return
     */
    public GroupDTO getGroupByID(String id) {
        GroupDTO result;

        GroupID groupID = new GroupID(id);
        Optional<Group> optGroup = this.groupRepo.findById(groupID);

        if (optGroup.isPresent()) {
            Group group = optGroup.get();
            result = CreateGroupAssembler.mapToDTO(group);
        } else {
            throw new GroupNotFoundException();
        }
        return result;
    }
}
