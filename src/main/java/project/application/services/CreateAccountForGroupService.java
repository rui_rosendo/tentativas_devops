package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.*;
import project.exceptions.AccountAlreadyExistsException;
import project.exceptions.AccountNotFoundException;
import project.exceptions.GroupConflictException;
import project.exceptions.GroupNotFoundException;
import project.frameworkddd.IUS007Service;
import project.model.entities.account.Account;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CreateAccountForGroupService implements IUS007Service {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    AccountRepository accountRepository;

    /**
     * @param groupRepository   repository that stores Group instances
     * @param accountRepository repository that stores Account instances
     */
    public CreateAccountForGroupService(GroupRepository groupRepository, AccountRepository accountRepository) {

        this.groupRepository = groupRepository;
        this.accountRepository = accountRepository;
    }

    /**
     * createAccountForGroup method
     * creates a new account and adds it to the accountRepository and the group's list of accounts.
     *
     * @param requestDTO - identity of the new account to be created and added
     * @return CreateAccountForGroupResponseDTO with the groupID, the description of this group, the new accountID and the account denomination
     * or exceptions in case of the person who tries to create account is not manager, if account already exists in repository,
     * or if group ID did not exist in repository
     */
    public CreateAccountForGroupResponseDTO createAccountForGroup(CreateAccountForGroupRequestDTO requestDTO) {
        GroupID groupID = new GroupID(requestDTO.getGroupID());

        CreateAccountForGroupResponseDTO result;

        Optional<Group> opGroup = groupRepository.findById(groupID);

        if (!opGroup.isPresent()) {
            throw new GroupNotFoundException();
        }
        Group group = opGroup.get();
        AccountID accountID = new AccountID(requestDTO.getAccountID());
        PersonID managerID = new PersonID(requestDTO.getPersonEmail());

        if (accountRepository.findById(accountID).isPresent()) {
            throw new AccountAlreadyExistsException();
        }

        if (!group.hasManagerID(managerID)) {
            throw new GroupConflictException("The person with " + managerID + " doesn't have manager privileges");
        }
        String denomination = requestDTO.getAccountDenomination();
        String description = requestDTO.getAccountDescription();

        Account newAccount = new Account(accountID, denomination, description);
        Account savedAccount = accountRepository.save(newAccount);

        group.addAccount(savedAccount.getID());
        Group savedGroup = groupRepository.save(group);

        result = CreateAccountForGroupAssembler.mapToResponseDTO(
                savedGroup.getID().toStringDTO(),
                savedGroup.getDescription().getDescriptionValue(),
                savedAccount.getID().toStringDTO(),
                savedAccount.getDenomination().getDenominationValue());

        return result;
    }

    /**
     * getAccountByID method
     *
     * @param requestAccountID - identity of the account to be searched and returned
     * @return the details of the account to be searched in a DTO
     */
    public AccountDTO getAccountByID(String requestAccountID) {

        AccountID accountID = new AccountID(requestAccountID);
        AccountDTO result;

        Optional<Account> opAccount = accountRepository.findById(accountID);

        if (!opAccount.isPresent()) {
            throw new AccountNotFoundException();
        }

        Account account = opAccount.get();
        result = AccountAssembler.mapToResponseDTO(account.getDenomination().getDenominationValue(), account.getDescription().getDescriptionValue(), account.getID().toStringDTO());

        return result;
    }

    /**
     * Method getAccountByID
     *
     * @param accountID String of accountID
     */
    public Account getAccountByID(AccountID accountID) {

        Optional<Account> accountOpt = accountRepository.findById(accountID);

        Account account;

        if (!accountOpt.isPresent()) {
            throw new AccountNotFoundException();
        } else {
            account = accountOpt.get();
        }

        return account;
    }

    /**
     * Method getGroupByID
     *
     * @param groupID String of groupID
     */
    public Group getGroupByID(GroupID groupID) {

        Optional<Group> groupOpt = groupRepository.findById(groupID);

        Group group;

        if (!groupOpt.isPresent()) {
            throw new GroupNotFoundException();
        } else {
            group = groupOpt.get();
        }

        return group;
    }

    /**
     * getAccountsByGroupID
     *
     * @param requestGroupID - identity of the group whose accounts are to be searched and returned
     * @return AccountsDTO
     */
    public AccountsDTO getAccountsByGroupID(String requestGroupID) {

        GroupID groupID = new GroupID(requestGroupID);
        Group group = getGroupByID(groupID);

        List<Account> accounts = new ArrayList<>();
        for (AccountID accountID : group.getAccountsIDs().getAccountsIDsValue()) {
            accounts.add(getAccountByID(accountID));
        }

        return AccountsAssembler.mapToDTO(accounts);
    }
}
