package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.GetFamilyGroupsAssembler;
import project.dto.GetFamilyGroupsDTO;
import project.frameworkddd.IUS004Service;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.services.IsFamilyService;
import project.model.specifications.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Set;

@Service
public class GetFamilyGroupsService implements IUS004Service {

    @Autowired
    GroupRepository groupRepositoryDB;

    @Autowired
    IsFamilyService service;

    /**
     * Constructor for GetFamilyGroupsService Class
     *
     * @param groupRepositoryDB Instance of GroupRepository containing all groups
     */
    public GetFamilyGroupsService(GroupRepository groupRepositoryDB, IsFamilyService service) {
        this.groupRepositoryDB = groupRepositoryDB;
        this.service = service;
    }

    /**
     * Method getFamilyGroups
     * Retrieves all groups from repository that are family
     *
     * @return toString of Set containing family groups
     */
    public GetFamilyGroupsDTO getFamilyGroups() {
        Set<Group> familyGroups = new HashSet<>();
        Set<GroupID> familyGroupsIDS = new HashSet<>();

        for (Group group : groupRepositoryDB.findAll()) {
            if (service.isFamily(group)) {
                familyGroups.add(group);
            }
        }

        for (Group group : familyGroups) {
            familyGroupsIDS.add(group.getID());
        }

        return GetFamilyGroupsAssembler.mapToDTO(familyGroupsIDS);
    }
}
