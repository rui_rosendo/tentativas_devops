package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.AddMemberAssembler;
import project.dto.AddMemberRequestDTO;
import project.dto.AddMemberResponseDTO;
import project.dto.MembersDTO;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS003Service;
import project.infrastructure.repositories.GroupRepositoryDB;
import project.infrastructure.repositories.PersonRepositoryDB;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class AddMemberToGroupService implements IUS003Service {

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    PersonRepository personRepository;

    /**
     * Constructor for AddMemberToGroupService
     *
     * @param groupRepositoryDB  Instance of GroupRepository containing all groups
     * @param personRepositoryDB Instance of PersonRepository containing all persons
     */
    public AddMemberToGroupService(PersonRepositoryDB personRepositoryDB, GroupRepositoryDB groupRepositoryDB) {
        this.groupRepository = groupRepositoryDB;
        this.personRepository = personRepositoryDB;
    }

    /**
     * Adds a PersonID to the list of members of a Group
     *
     * @param addMemberRequestDTO Encapsulates two string attributes (newMemberID and groupID) that are converted into objects (PersonID and GroupID)
     * @return AddMemberResponseDTO with the groupID, the description of this group, and the member added to this group
     * or exceptions in case of intended new member ID did not exist in repository, if group ID did not exist in repository,
     * or if new member ID already existed in given group members list)
     */
    public AddMemberResponseDTO addMember(AddMemberRequestDTO addMemberRequestDTO) {
        PersonID newMemberID = new PersonID(addMemberRequestDTO.getNewMemberEmail());
        GroupID groupID = new GroupID(addMemberRequestDTO.getGroupID());
        AddMemberResponseDTO result;

        Person member = getPersonByID(newMemberID);

        Group group = getGroupByID(groupID);
        group.addMemberID(newMemberID);
        Group savedGroup = groupRepository.save(group);
        result = (AddMemberAssembler.mapToResponseDTO(savedGroup.getID().toStringDTO(), savedGroup.getDescription().getDescriptionValue(), addMemberRequestDTO.getNewMemberEmail()));

        return result;
    }

    /**
     * Method getPersonByID
     *
     * @param personID
     * @return
     */
    public Person getPersonByID(PersonID personID) {

        Optional<Person> personOpt = personRepository.findById(personID);

        Person person;

        if (!personOpt.isPresent()) {
            throw new PersonNotFoundException();
        } else {
            person = personOpt.get();
        }

        return person;
    }

    /**
     * Method getGroupByID
     *
     * @param groupID
     * @return
     */
    public Group getGroupByID(GroupID groupID) {

        Optional<Group> groupOpt = groupRepository.findById(groupID);

        Group group;

        if (!groupOpt.isPresent()) {
            throw new GroupNotFoundException();
        } else {
            group = groupOpt.get();
        }
        return group;
    }

    /**
     * Method getMembersByGroupID
     *
     * @param groupID
     * @return
     */
    public MembersDTO getMembersByGroupID(String groupID) {
        GroupID newGroupID = new GroupID(groupID);
        Group group = getGroupByID(newGroupID);

        // Ir ao repo person buscar members
        Set<Person> members = new HashSet<>();
        for (PersonID memberID : group.getMembersIDs().getPersonIDs()) {
            members.add(getPersonByID(memberID));
        }

        return AddMemberAssembler.mapToResponseDTO(group, members);
    }
}