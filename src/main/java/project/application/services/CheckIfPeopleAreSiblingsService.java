package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CheckIfPeopleAreSiblingsAssembler;
import project.dto.CheckIfPeopleAreSiblingsRequestDTO;
import project.dto.CheckIfPeopleAreSiblingsResponseDTO;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUS001Service;
import project.model.entities.person.Person;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

@Service
public class CheckIfPeopleAreSiblingsService implements IUS001Service {

    @Autowired
    private final PersonRepository personRepository;

    /**
     * Constructor for check people are siblings service
     *
     * @param personRepository Person repository
     */
    public CheckIfPeopleAreSiblingsService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    /**
     * Method findPersonById
     *
     * @param personID String of personID
     */
    public Person getByID(PersonID personID) {

        Optional<Person> personOpt = personRepository.findById(personID);

        Person person;

        if (!personOpt.isPresent()) {
            throw new PersonNotFoundException();
        } else {
            person = personOpt.get();
        }

        return person;
    }

    /**
     * Method to find if two people are siblings
     *
     * @param requestDTO DTO with IDs of two people
     * @return True if they are siblings, false if they are not siblings and throw an exception if at least one of the persons doesnt exist
     */
    public CheckIfPeopleAreSiblingsResponseDTO isSibling(CheckIfPeopleAreSiblingsRequestDTO requestDTO) {

        Person person1 = getByID(new PersonID(requestDTO.getPersonEmail1()));
        Person person2 = getByID(new PersonID(requestDTO.getPersonEmail2()));

        boolean isSibling = person1.isSibling(person2);
        String result = String.format("%b", isSibling);

        return CheckIfPeopleAreSiblingsAssembler.mapToResponseDTO(result);
    }
}
