import React, {useContext} from "react";
import NewContext from "../context/NewContext";

function TableGroupsHeader() {
    const {groupsHeaders} = useContext(NewContext);
    const {id, description, creationDate} = groupsHeaders;

    return (
        <thead>
        <tr>
            <th style={{width: "50px"}}>{id}></th>
            <th style={{width: "500px"}}>{description}></th>
            <th style={{width: "200px"}}>{creationDate}></th>
        </tr>
        </thead>
    )
}

export default TableGroupsHeader