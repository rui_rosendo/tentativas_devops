import React, {Component} from 'react';
import Logout from "../Logout";
import TableGroups from "./TableGroups";
import TableTransactions from "./TableTransactions";

class App1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            teste: []
        }
    }

    componentDidMount() {

        fetch('http://localhost:8080/accounts/8105')
            .then(res => res.json())
            .then((data) => {
                this.setState({teste: data})
            })
            .catch(console.log)
    }

    render() {

        return (
            <div>
                <h1>Hello World - G4 is ONLINE</h1>

                <h1>AccountID : {this.state.teste.accountID}</h1>
                <br/>
                <h1>Denomination : {this.state.teste.denomination}</h1>
                <br/>

                <h1>Description : {this.state.teste.description}</h1>
                <Logout/>

                <TableGroups/>

                <br/>

                <TableTransactions/>
            </div>

        )
    }
}

export default App1
