import React from 'react';
import Logout from "../Logout";
import AppContext from '../context/AppContext';
import TableGroups from "./TableGroups";
import TableTransactions from "./TableTransactions";

function App2() {
    const {state} = React.useContext(AppContext);
    const {username} = state;
    return (
        <div>
            {<h1>Hello World - {username}</h1>}

            <Logout/>

            <TableGroups user={username}/>

            <br/>

            <TableTransactions user={username}/>


        </div>

    )

}

export default App2
