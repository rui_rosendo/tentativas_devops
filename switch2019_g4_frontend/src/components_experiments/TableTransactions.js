import React, {useContext, useEffect} from "react";
import NewContext from "../context/NewContext";
import {
    fetchGroupTransactionsFailure,
    fetchGroupTransactionsStarted,
    fetchGroupTransactionsSuccess,
    URL_API
} from "../context/NewActions";
import TableTransactionsHeader from "./TableTransactionsHeader";
import TableTransactionsBody from "./TableTransactionsBody";

function TableTransactions(props) {
    const {state, dispatch} = useContext(NewContext);
    const {transactions} = state;
    const {loading, error, data} = transactions;

    useEffect(() => {
        dispatch(fetchGroupTransactionsStarted());
        fetch(`${URL_API}/people/8101@switch.pt/groups/8101/transactions`)
            .then(res => res.json()).then(res => dispatch(fetchGroupTransactionsSuccess(res.transactions)))
            .catch(err => dispatch(fetchGroupTransactionsFailure(err.message)))
    }, []);

    if (loading === true) {
        return (<h1>Loading ....</h1>);
    } else {
        if (error !== null) {
            return (<h1>Error ....</h1>);
        } else {
            if (data.length > 0) {
                return (
                    <div>
                        <table border="1">
                            <TableTransactionsHeader/>
                            <TableTransactionsBody/>
                        </table>
                    </div>
                );
            } else {
                return (<h1>No data ....</h1>);
            }
        }
    }
}

export default TableTransactions