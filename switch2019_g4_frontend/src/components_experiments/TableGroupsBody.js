import React, {useContext} from "react";
import NewContext from "../context/NewContext";

function TableGroupsBody() {
    const {state} = useContext(NewContext);
    const {groups} = state;
    const {data} = groups;

    const rows = data.map((row, index) => {
        return (
            <tr key={index}>
                <td>{row.groupID}</td>
                <td>{row.description}</td>
                <td>{row.creationDate}</td>
            </tr>

        )
    });

    return (
        <tbody>{rows}</tbody>
    )
}

export default TableGroupsBody