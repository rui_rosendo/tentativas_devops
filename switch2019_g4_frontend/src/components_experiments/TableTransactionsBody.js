import React, {useContext} from "react";
import NewContext from "../context/NewContext";

function TableTransactionsBody() {
    const {state} = useContext(NewContext);
    const {transactions} = state;
    const {data} = transactions;

    const rows = data.map((row, index) => {
        return (
            <tr key={index}>
                <td>{row.amount}</td>
                <td>{row.dateTime}</td>
                <td>{row.type}</td>
                <td>{row.description}</td>
                <td>{row.category}</td>
                <td>{row.debitAccountID}</td>
                <td>{row.creditAccountID}</td>
            </tr>
        )
    });

    return (
        <tbody>{rows}</tbody>
    )
}

export default TableTransactionsBody