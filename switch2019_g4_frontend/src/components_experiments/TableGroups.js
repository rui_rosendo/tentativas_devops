import React, {useContext, useEffect} from "react";
import NewContext from "../context/NewContext";
import {fetchGroupsFailure, fetchGroupsStarted, fetchGroupsSuccess, URL_API} from "../context/NewActions";
import TableGroupsHeader from "./TableGroupsHeader";
import TableGroupsBody from "./TableGroupsBody";

function TableGroups(props) {
    const {state, dispatch} = useContext(NewContext);
    const {groups} = state;
    const {loading, error, data} = groups;

    useEffect(() => {
        dispatch(fetchGroupsStarted());
        fetch(`${URL_API}/people/${props.user}/groups`)
            .then(res => res.json())
            .then(res => dispatch(fetchGroupsSuccess(res.groupsDTOs)))
            .catch(err => dispatch(fetchGroupsFailure(err.message)))
    }, []);

    if (loading === true) {
        return (<h1>Loading ....</h1>);
    } else {
        if (error !== null) {
            return (<h1>Error ....</h1>);
        } else {
            if (data.length > 0) {
                return (
                    <div>
                        <table border="1">
                            <TableGroupsHeader/>
                            <TableGroupsBody/>
                        </table>
                    </div>
                );
            } else {
                return (<h1>No data ....</h1>);
            }
        }
    }
}

export default TableGroups