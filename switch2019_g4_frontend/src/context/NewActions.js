export const URL_API = 'http://localhost:8080/';

export const FETCH_GROUPS_STARTED = 'FETCH_GROUPS_STARTED';
export const FETCH_GROUPS_SUCCESS = 'FETCH_GROUPS_SUCCESS';
export const FETCH_GROUPS_FAILURE = 'FETCH_GROUPS_FAILURE';

export function fetchGroupsStarted() {
    return {
        type: FETCH_GROUPS_STARTED,

    }
}

export function fetchGroupsSuccess(groups) {
    return {
        type: FETCH_GROUPS_SUCCESS,
        payload: {
            data:
                [...groups]
        }

    }
}

export function fetchGroupsFailure(message) {
    return {
        type: FETCH_GROUPS_FAILURE,
        payload: {
            error: message
        }
    }
}

export const FETCH_GROUP_TRANSACTIONS_STARTED = 'FETCH_USER_TODOS_STARTED';
export const FETCH_GROUP_TRANSACTIONS_SUCCESS = 'FETCH_USER_TODOS_SUCCESS';
export const FETCH_GROUP_TRANSACTIONS_FAILURE = 'FETCH_USER_TODOS_FAILURE';
export const DELETE_GROUP_TRANSACTIONS = 'DELETE_USER_TODOS';

export function fetchGroupTransactionsStarted() {
    return {
        type: FETCH_GROUP_TRANSACTIONS_STARTED,
    }
}

export function fetchGroupTransactionsSuccess(transactions) {
    return {
        type: FETCH_GROUP_TRANSACTIONS_SUCCESS,
        payload: {
            data: [...transactions]
        }
    }
}

export function fetchGroupTransactionsFailure(message) {
    return {
        type: FETCH_GROUP_TRANSACTIONS_FAILURE,
        payload: {
            error: message
        }
    }
}

export function deleteGroupTransactions() {
    return {
        type: DELETE_GROUP_TRANSACTIONS
    }
}