import React, {useReducer} from "react";
import {Provider} from './NewContext'
import newReducer from "./NewReducer";
import PropTypes from "prop-types";

const initialState = {
    groups: {
        loading: true,
        error: null,
        data: []
    },
    transactions: {
        loading: false,
        error: null,
        data: []
    }
}

const groupsHeaders = {
    id: "ID",
    description: "Description",
    creationDate: "Creation Date"
}


const transactionsHeaders = {
    amount: "Amount",
    dateTime: "Date",
    type: "Type",
    description: "Description",
    category: "Category",
    debitAccount: "Debit Account ID",
    creditAccount: "Credit Account ID",
}

const NewProvider = (props) => {
    const [state, dispatch] = useReducer(newReducer, initialState)
    return (
        <Provider value={{
            state,
            groupsHeaders,
            transactionsHeaders,
            dispatch
        }}>
            {props.children}
        </Provider>
    )
}

NewProvider.propTypes = {
    children: PropTypes.node,
};

export default NewProvider