import {
    DELETE_GROUP_TRANSACTIONS,
    FETCH_GROUP_TRANSACTIONS_FAILURE,
    FETCH_GROUP_TRANSACTIONS_STARTED,
    FETCH_GROUP_TRANSACTIONS_SUCCESS,
    FETCH_GROUPS_FAILURE,
    FETCH_GROUPS_STARTED,
    FETCH_GROUPS_SUCCESS
} from "./NewActions";


function newReducer(state, action) {
    switch (action.type) {
        case FETCH_GROUPS_STARTED:
            return {
                ...state,
                groups: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_GROUPS_SUCCESS:
            return {
                ...state,
                groups: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_GROUPS_FAILURE:
            return {
                ...state,
                groups: {
                    loading: false,
                    error: action.payload.error,
                    data: [],
                }
            };
        case FETCH_GROUP_TRANSACTIONS_STARTED:
            return {
                ...state,
                transactions: {
                    loading: true,
                    error: null,
                    data: []
                }
            };
        case FETCH_GROUP_TRANSACTIONS_SUCCESS:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: null,
                    data: [...action.payload.data]
                }
            };
        case FETCH_GROUP_TRANSACTIONS_FAILURE:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: action.payload.error,
                    data: []
                }
            };
        case DELETE_GROUP_TRANSACTIONS:
            return {
                ...state,
                transactions: {
                    loading: false,
                    error: null,
                    data: []
                }
            };
        default:
            return state
    }
}

export default newReducer