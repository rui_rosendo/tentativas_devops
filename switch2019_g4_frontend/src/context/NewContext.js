import React from "react";

const NewContext = React.createContext();
export const {Provider} = NewContext;
export default NewContext