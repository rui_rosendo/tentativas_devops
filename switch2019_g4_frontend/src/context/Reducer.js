import {LOGIN, LOGOUT,} from './Actions'

function reducer(state, action) {
  switch (action.type) {
    case LOGIN:
      //Retorna o isLogged como true, e mantém o restante estado
      return {
        ...state,
        isLogged: true,
        username: action.username,
      };
    case LOGOUT:
      return {
        ...state,
        isLogged: false,
        username: "",
      };
    default:
      return state;
  }
}

export default reducer;
