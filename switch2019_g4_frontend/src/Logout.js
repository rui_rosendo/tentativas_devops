import React from 'react';
import {useHistory} from 'react-router-dom';
import AppContext from './context/AppContext';
import {logout} from './context/Actions';

function Logout() {
    const {dispatch} = React.useContext(AppContext);
    let history = useHistory();
    const handleClick = () => {
        history.push("/login");
        dispatch(logout());
//       
    };

    return (<button type="submit" onClick={handleClick}>Logout</button>);

}

export default Logout;