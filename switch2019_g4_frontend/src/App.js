import React from 'react';
import './App.css';
import SignIn from "./SignIn";

import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import AppContext from "./context/AppContext";

import PrivateRoute from './PrivateRoute';
import App2 from "./components_experiments/App2";


function App() {
    const {state} = React.useContext(AppContext);
    const {isLogged} = state;

    if (isLogged === true)
        return (
            <div>
                <Router>
                    <Switch>
                        {/*<PrivateRoute exact path='/home' component={App1}/>*/}
                        <PrivateRoute exact path='/home' component={App2}/>

                    </Switch>
                </Router>
            </div>


        );
    else {
        return (
            <Router>
                <div className="App">
                    <Route component={SignIn}/>
                </div>
            </Router>
        );
    }

}

export default App;