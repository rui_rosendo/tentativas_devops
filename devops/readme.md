#Application of DevOps tools in G4's SWitCH project


##1. Description of the implementation of the requirements

1 - Fork

...

# STEPS #
# Creating a fork of Group 4's Personal Finance team repository #
1 - In the G4 repository, click on the "create" or "+" sign on the left below the search icon
2 - At the bottom of that context menu, under the "Get to wotk" section, click the "Fork this repository" button
3 - Next configured the following settings:
    - Project - chose "create new project" and gave it a name "SWitCH2019_G4_DevOps"
    - Name - the thame given was ´switch2019_g4_devops´
    - Description - "SWitCH 2020 Group4 Project fork for DevOps"
    - Forking - set this to "Allow only private forks"
4 - After the fork was created, the team members and the professors were given access.

...

##2. Analysis of the alternative

...

##3. Implementation of the alternative

...

